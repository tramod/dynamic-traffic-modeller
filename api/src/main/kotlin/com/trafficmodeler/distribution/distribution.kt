package com.trafficmodeler.distribution

import com.kolovsky.dta.raadsen.java.DtmOriginDestinationMatrix
import com.kolovsky.dta.raadsen.java.DtmOriginDestinationPair
import com.kolovsky.dta.raadsen.java.DtmPiecewiseConstantFunction
import com.kolovsky.dta.raadsen.java.DtmZone
import java.lang.Math
import kotlin.math.pow


class DeterrenceFunction(private val alpha: Double, private val beta: Double){
    fun f(cost: Double): Double {
        return cost.pow(alpha) * Math.E.pow(-beta*cost)
    }
}

fun DtmOriginDestinationMatrix.updateZone(
    zone: DtmZone,
    incomingTrips: DtmPiecewiseConstantFunction,
    outgoingTrips: DtmPiecewiseConstantFunction
){
    val originPairs = pairs.filter { it.origin == zone }
    if (originPairs.isNotEmpty()){
        val originalOutgoingFlow = originPairs.map { it.flow }.reduce{ a, b -> a + b }
        val k = outgoingTrips.dividedBy(originalOutgoingFlow)
        originPairs.forEach { pair ->
            pair.flow = pair.flow.times(k)
        }
    }

    val destinationPairs = pairs.filter { it.destination == zone }
    if (destinationPairs.isNotEmpty()){
        val originalIncomingFlow = destinationPairs.map { it.flow }.reduce{ a, b -> a + b }
        val k2 = incomingTrips.dividedBy(originalIncomingFlow)
        destinationPairs.forEach { pair ->
            pair.flow = pair.flow.times(k2)
        }
    }
}

fun DtmOriginDestinationMatrix.addZone(
    zone: DtmZone,
    incomingTrips: DtmPiecewiseConstantFunction,
    outgoingTrips: DtmPiecewiseConstantFunction,
    deterrenceFunction: DeterrenceFunction
){

    fun List<DtmZone>.distribution(trips: DtmPiecewiseConstantFunction, out: Boolean = false): List<DtmOriginDestinationPair> {
        val dist = graph.performShortestPathSearchDual(zone.node).first
        val k = ArrayList<Double>(size)
        for(z in this){
            val cost = dist[z.node.index]
            if (cost.isInfinite()){
                k.add(0.0)
            } else {
                k.add(deterrenceFunction.f(cost))
            }

        }
        val a = 1.0/k.sum()
        if (a.isInfinite()){
            return emptyList()
        }

        return zip(k).map {
            val flow = trips * (a * it.second)
            if (out){
                DtmOriginDestinationPair(zone, it.first, flow)
            } else{
                DtmOriginDestinationPair(it.first, zone, flow)
            }

        }
    }

    originZones.distribution(incomingTrips).forEach {
        pairs.add(it)
    }

    val destinationPairs = destinationZones.distribution(outgoingTrips, true)
    destinationPairs.forEach {
        pairs.add(it)
    }

    originZones.add(zone)
    destinationZones.add(zone)
    zones.add(zone)
    zonesAccessTable[zone.node] = zone
    originZonesPairs[zone] = destinationPairs
}

