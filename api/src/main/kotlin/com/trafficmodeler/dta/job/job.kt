/**
Dynamic Traffic Modeller
Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.trafficmodeler.dta.job

import com.fasterxml.jackson.annotation.JsonIgnore
import com.kolovsky.dta.raadsen.java.*
import com.trafficmodeler.dta.api.ModelInfo
import com.trafficmodeler.dta.api.PostGISDatabase
import org.slf4j.LoggerFactory
import java.io.Serializable
import java.security.MessageDigest
import javax.xml.bind.DatatypeConverter
import kotlin.math.max
import kotlin.math.round

enum class JobStatus{ ERROR, FINISHED, RUNNING, WAITING }

abstract class Job(val modelName: String, private val jobManager: JobManager, jobID: String? = null): Runnable {
    private val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

    val jobId: String = jobID ?: generateJobId()
    var status: JobStatus = JobStatus.WAITING
    var result: Any? = null
    var error: Exception? = null
    var startTime: Long = System.currentTimeMillis()

    var worker: Thread? = null

    private fun generateJobId(): String {
        val randomString: String = (1..10)
                .map { i -> kotlin.random.Random.nextInt(0, charPool.size) }
                .map(charPool::get).joinToString("")

        return DatatypeConverter.printHexBinary(
                MessageDigest.getInstance("SHA-1")
                        .digest((randomString + System.currentTimeMillis()).toByteArray()))
                .toLowerCase()
    }

    fun liteJob(): LiteJob{
        return LiteJob(modelName, jobId, startTime, status)
    }

    fun persistentJob(): PersistentJob {
        return PersistentJob(modelName, jobId, status, result, startTime)
    }

    abstract fun compute(): Boolean

    override fun run() {
        worker = Thread.currentThread()
        if (status != JobStatus.WAITING){ return }
        status = JobStatus.RUNNING

        try {
            val persist = compute()
            status = JobStatus.FINISHED
            if (persist){
                jobManager.makeJobPersistent(this)
            }
        } catch (e: Exception){
            logger.error("JOB ERROR")
            e.printStackTrace()
            error = e
            status = JobStatus.ERROR
        }
    }

    companion object{
        private val logger = LoggerFactory.getLogger(this::class.java)
    }

}

class PersistentJob(val modelName: String,  val jobId: String,  var status: JobStatus, var result: Any?, var startTime: Long): Serializable

data class LiteJob(val modelName: String, val jobId: String, val startTime: Long, val status: JobStatus): Serializable {
    fun id(): String {
        return "${modelName}_$jobId"
    }
}

class ExampleJob(modelName: String, private val cacheName: String?, jobManager: JobManager,
                 jobID: String? = null): Job(modelName, jobManager, jobID){
    override fun compute(): Boolean {
        Thread.sleep(2000)
        result = "aaaaaaaaa"
        println("$jobId finished")

        if (cacheName != null){
            return true
        }
        return false
    }
}

class AssignmentJob(modelName: String, val settings: AssignmentJobSettings, jobManager: JobManager,
                    jobID: String? = null): Job(modelName, jobManager, jobID) {
    override fun compute(): Boolean {
        val loader = DtmModelLoader(settings.dbUrl)
        val db = PostGISDatabase(settings.dbUrl)
        val modelInfo = db.modelInfo(modelName, settings.day)

        // determine maximal node time
        val maximalNodeTime = if (settings.maximalNodeTime < 0.0){
            (modelInfo.endTime.time - modelInfo.startTime.time)/3_600_000.0
        } else {
            settings.maximalNodeTime
        }

        // determine
        val maximalSimulationTime = if (settings.maximalSimulationTime < 0.0){
            maximalNodeTime + 2
        } else {
            settings.maximalSimulationTime
        }

        // LOAD MODEL FROM DATABASE
        // read graph
        val g = loader.readGraph(modelName)
        val graph = g.first
        val edgeLanes = g.second

        // read ODM
        val odm = loader.readOriginDestinationMatrix(modelName, graph, settings.day)

        odm.initializeOrigins()

        // apply changes
        applyChanges(settings, odm, CallTime.BEFORE_INTERSECTION, db)

        // include intersections
        loader.includeIntersectionsToGraph(modelName, graph, edgeLanes)

        applyChangesAfterGraph(settings, odm)

        // apply changes
        applyChanges(settings, odm, CallTime.AFTER_INTERSECTION, db)

        DtmModelLoader.allOrNothingAssignment(odm, maximalNodeTime, true, settings.numOfTimeIntervals)

        // DYNAMIC NETWORK LOADING MODEL
        val dnl = DtmEventGeneralLinkTransmissionModel(graph, odm.originZones, true)
        dnl.fanStep = settings.fanStep
        dnl.flowThreshold = settings.flowThreshold
        dnl.mixtureFlowThreshold = settings.mixtureFlowThreshold

        // DYNAMIC USER EQUILIBRIUM
        val gradientProjection = DtmReducedGradientProjection(settings.gradientStep)
        val due = DtmDiscreteDestinationDynamicUserEquilibrium(dnl, maximalSimulationTime, odm,
                gradientProjection, 0.0, maximalNodeTime, settings.numOfTimeIntervals)

        due.run(settings.maximalIteration, 0.0)

        // RESULTS
        val edges: ArrayList<AssignmentResultItem> = ArrayList()
        var bestEdges = due.bestResultEdges
        if (settings.maximalIteration == 1){
            bestEdges = graph.edges
        }
        for (edge in bestEdges){
            val outflow = edge.outflow
                    .toDiscreteFunction(settings.numOfTimeIntervals, 0.0, maximalNodeTime, 0.5)
                    .values.map { round(it) }
            val inflow = edge.inflow
                    .toDiscreteFunction(settings.numOfTimeIntervals, 0.0, maximalNodeTime, 0.5)
                    .values.map { round(it) }
            val travelTimeRaw = edge.arrivalFunction.toTravelTimeFunction()
                    .toDiscreteFunction(settings.numOfTimeIntervals, 0.0, maximalNodeTime, 0.5)
                    .values.map { round(it*1E4)/1E4 }
            val travelTime = travelTimeRaw.map { max(it, edge.length/edge.fundamentalDiagram.maximalSpeed) }.toList()
            edges.add(AssignmentResultItem(edge.id, outflow, inflow, travelTime))
        }

        result = AssignmentResult(edges, modelInfo)

        if (settings.cacheName != null){
            return true
        }
        return false
    }

    fun applyChanges(settings: AssignmentJobSettings, odm: DtmOriginDestinationMatrix, callTime: CallTime, db: PostGISDatabase){
        val sortedUpdates = settings.update.sortedBy { it.order }
        for (change in sortedUpdates){
            if (change.callTime == callTime){
                change.apply(odm, db, modelName)
            }
        }
        odm.graph.initializeStructures()
    }

    fun applyChangesAfterGraph(settings: AssignmentJobSettings, odm: DtmOriginDestinationMatrix){
        val sortedUpdates = settings.update.sortedBy { it.order }
        for (change in sortedUpdates){
            change.applyAfterGraph(odm)
        }
        odm.graph.initializeStructures()
    }

}

data class AssignmentResultItem (
        val edgeId: Int,
        val outFlow: List<Double>,
        val inFlow: List<Double>,
        val travelTime: List<Double>
): Serializable

data class AssignmentResult(
        val result: List<AssignmentResultItem>,
        val modelInfo: ModelInfo
): Serializable

data class AssignmentJobSettings(
        /** order is important  */
        val update: List<Change> = emptyList(),
        val numOfTimeIntervals: Int = 12,
        val fanStep: Double = 100000.0,
        val flowThreshold: Double = 5.0,
        val mixtureFlowThreshold: Double = 0.02,
        val day: Int = 0,
        val maximalIteration: Int = 10,
        val gradientStep: Double = 0.05,
        val maximalNodeTime: Double = -1.0,
        val maximalSimulationTime: Double = -1.0,
        val cacheName: String? = null,
        @JsonIgnore var dbUrl: String = ""
)