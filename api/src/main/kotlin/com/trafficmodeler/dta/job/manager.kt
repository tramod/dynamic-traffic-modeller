/**
Dynamic Traffic Modeller
Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.trafficmodeler.dta.job

import com.github.benmanes.caffeine.cache.Cache
import com.github.benmanes.caffeine.cache.Caffeine
import org.mapdb.DB
import org.mapdb.DBException
import org.mapdb.DBMaker.fileDB
import org.mapdb.Serializer
import org.slf4j.LoggerFactory
import java.io.File
import java.util.concurrent.ConcurrentMap
import java.util.concurrent.ScheduledThreadPoolExecutor
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap

/**
 * Manager the jobs
 * @param cacheSize maximal number of jobs that are stored in cache
 * @param jobExpiration job lifetime (number of second)
 */
class JobManager(cacheSize: Long, jobExpiration: Long, dbFileName: String, corePoolSize: Int){
    // MapDB persistent jobs
    private val database: DB
    /** <jobId, job> on disk */
    private var persistentJobs: ConcurrentMap<String, PersistentJob>
    /** <modelName, <jobId, liteJob> in memory */
    private var persistentJobsIndexMem: HashMap<String, HashMap<String, LiteJob>>
    /** <jobId, liteJob> on disk */
    private val persistentJobsIndex: ConcurrentMap<String, LiteJob>
    /** <jobId, job> in memory */
    private val allJobs: Cache<String, Job> = Caffeine.newBuilder()
            .expireAfterWrite(jobExpiration, TimeUnit.SECONDS)
            .maximumSize(cacheSize).build()

    init {
        database = try {
            fileDB(dbFileName).make()
        } catch (e: DBException){
            logger.warn("The database is broken. I am deleting it and creating new one.")
            if (!File(dbFileName).delete()) {
                logger.warn("The database can not be deleted.")
            }
            fileDB(dbFileName).make()
        }


        persistentJobs = database.hashMap("jobs", Serializer.STRING, Serializer.JAVA).createOrOpen() as ConcurrentMap<String, PersistentJob>

        persistentJobsIndex = database.hashMap("jobsIndex", Serializer.STRING, Serializer.JAVA).createOrOpen() as ConcurrentMap<String, LiteJob>
        persistentJobsIndexMem = HashMap()
        for (job in persistentJobsIndex.values){
            if (!persistentJobsIndexMem.containsKey(job.modelName)){
                persistentJobsIndexMem[job.modelName] = HashMap()
            }
            persistentJobsIndexMem[job.modelName]!![job.jobId] = job
        }

        database.commit()
    }

    private val executor: ScheduledThreadPoolExecutor = ScheduledThreadPoolExecutor(corePoolSize)

    /**
     * Execute the job and cache the result
     */
    fun startJob(job: Job){
        allJobs.put(job.jobId, job)
        executor.execute(job)
    }

    fun killJob(job: Job){
        if (job.worker != null){
            job.worker!!.interrupt()
            executor.remove(job)
            job.status = JobStatus.ERROR
        }
        else{
            job.status = JobStatus.ERROR
        }
    }

    /**
     * @param jobId job ID
     * @return job by job ID
     */
    fun job(modelName: String, jobId: String): Job? {
        allJobs.getIfPresent(jobId)?.let {
            return it
        }
        return null
    }

    /**
     * @param modelName name of the model
     * @return list of jobs for the model
     */
    fun jobs(modelName: String): List<LiteJob> {
        return allJobs.asMap().values.filter { it.modelName == modelName }.map { it.liteJob() }
    }

    /**
     * @return number of running jobs
     */
    fun numberOfRunningJobs() = executor.activeCount

    fun persistentJob(modelName: String, jobId: String): PersistentJob? {
        persistentJobsIndexMem[modelName]?.let {
            it[jobId]?.let {
                return persistentJobs[jobId]!!
            }
        }
        return null
    }

    /**
     * Store the job to disk. This job alive application restart.
     * @param job job
     */
    fun makeJobPersistent(job: Job){
        // STORE JOB
        persistentJobs[job.jobId] = job.persistentJob()

        // STORE LIGHT JOB
        val liteJob = job.liteJob()
        persistentJobsIndex[liteJob.id()] = liteJob

        // FAST ACCESS
        if (!persistentJobsIndexMem.containsKey(job.modelName)){
            persistentJobsIndexMem[job.modelName] = HashMap()
        }
        persistentJobsIndexMem[job.modelName]!![job.jobId] = liteJob

        database.commit()
    }

    /**
     * @param modelName name of the model
     * @return list of persistent jobs for the model
     */
    fun persistentJobs(modelName: String): List<LiteJob> {
        persistentJobsIndexMem[modelName]?.let {
            return it.values.toList()
        }
        return emptyList()
    }

    /**
     * Delete stored job on disk
     */
    fun removePersistentJob(modelName: String, jobId: String){
        persistentJobs.remove(jobId)
        persistentJobsIndex.remove("${modelName}_${jobId}")
        persistentJobsIndexMem[modelName]?.remove(jobId)
        database.commit()
    }


    companion object{
        private val logger = LoggerFactory.getLogger(this::class.java)
    }
}