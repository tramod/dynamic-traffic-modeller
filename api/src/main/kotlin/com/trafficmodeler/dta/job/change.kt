
/**
Dynamic Traffic Modeller
Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.trafficmodeler.dta.job

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.kolovsky.dta.raadsen.java.*
import com.trafficmodeler.distribution.DeterrenceFunction
import com.trafficmodeler.distribution.addZone
import com.trafficmodeler.distribution.updateZone
import com.trafficmodeler.dta.api.PostGISDatabase
import com.trafficmodeler.dta.api.Tod
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.sql.Time

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = EdgeUpdate::class, name = EdgeUpdate.NAME),
    JsonSubTypes.Type(value = UpdateMovement::class, name = UpdateMovement.NAME),
    JsonSubTypes.Type(value = DeleteEdge::class, name = DeleteEdge.NAME),
    JsonSubTypes.Type(value = AddEdge::class, name = AddEdge.NAME),
    JsonSubTypes.Type(value = AddNode::class, name = AddNode.NAME),
    JsonSubTypes.Type(value = AddZone::class, name = AddZone.NAME),
    JsonSubTypes.Type(value = UpdateZone::class, name = UpdateZone.NAME)
)
abstract class Change (
    val type: String,
    @JsonIgnore val callTime: CallTime,
    @JsonIgnore val order: Int = 100
){

    @JsonIgnore
    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    abstract fun apply(odm: DtmOriginDestinationMatrix, db: PostGISDatabase, modelName: String)

    open fun applyAfterGraph(odm: DtmOriginDestinationMatrix){}

    fun fundamentalDiagram(speed: Double, capacity: Double, lanes: Int): DtmFundamentalDiagram {
        val criticalSpeed: Double = 0.8 * speed
        val maximalDensity: Double = 167.0 * lanes
        val waveSpeed: Double = capacity * criticalSpeed / (capacity - maximalDensity * criticalSpeed) - 1E-10

        return DtmFundamentalDiagram(speed, capacity, maximalDensity, criticalSpeed, waveSpeed)
    }
}

enum class CallTime {
    BEFORE_INTERSECTION, // have to be call before the intersections are initialized
    AFTER_INTERSECTION
}

data class EdgeUpdate(
    val edgeId: Int,
    val speed: Double,
    val capacity: Double,
    val lanes: Int
): Change(NAME, CallTime.AFTER_INTERSECTION){

    override fun apply(odm: DtmOriginDestinationMatrix, db: PostGISDatabase, modelName: String) {
        val edge = odm.graph.getEdgeById(edgeId)
        edge.fundamentalDiagram = fundamentalDiagram(speed, capacity, lanes)
        logger.info("Edge updated: edge_id = $edgeId")
    }

    companion object {
        const val NAME = "updateEdge"
    }
}

data class DeleteEdge(val edgeId: Int): Change(NAME, CallTime.AFTER_INTERSECTION, 50) {
    override fun apply(odm: DtmOriginDestinationMatrix, db: PostGISDatabase, modelName: String) {
        val edge = odm.graph.getEdgeById(edgeId)
        val sourceNode = edge.startNode
        val targetNode = edge.endNode

        sourceNode.dualEdges
            .filter { dualEdge -> dualEdge.endEdge == edge }
            .forEach {
                sourceNode.removeDualEdge(it)
                logger.info("Movement was deleted: node_id = $sourceNode")
            }

        targetNode.dualEdges
            .filter { dualEdge -> dualEdge.startEdge == edge }
            .forEach {
                targetNode.removeDualEdge(it)
                logger.info("Movement was deleted: node_id = $targetNode")
            }
    }

    companion object {
        const val NAME = "deleteEdge"
    }
}

data class UpdateMovement(
    val nodeId: Int,
    val sourceEdge: Int,
    val targetEdge: Int,
    val capacity: Double,
    val numberLanes: Int? = 1
): Change(NAME, CallTime.AFTER_INTERSECTION){

    override fun apply(odm: DtmOriginDestinationMatrix, db: PostGISDatabase, modelName: String) {
        val node = odm.graph.getNodeById(nodeId)
        val source = odm.graph.getEdgeById(sourceEdge)
        val target = odm.graph.getEdgeById(targetEdge)

        val movement: DtmDualEdge? = node.getDualEdge(source, target)
        if (capacity < 1E-10 && movement != null){
            node.removeDualEdge(movement)
            logger.info("Movement was deleted: node_id = $nodeId")
            return
        }

        val nodeModel = node.nodeModel
        if(nodeModel is DtmWRICNodeModel){
            val cap = nodeModel.capacityModel
            if (cap is DtmFixedCapacity){
                val i = node.incomingEdges.indexOf(source)
                val j = node.outgoingEdges.indexOf(target)
                cap.capacity[i][j] = capacity
                logger.info("Movement was updated: node_id = $nodeId")
            }
        }

    }
    companion object {
        const val NAME = "updateMovement"
    }
}

fun DtmNode.addAndConnectDualEdge(dualEdge: DtmDualEdge){
    addDualEdge(dualEdge)
    dualEdge.startEdge.outgoingDualEdges.add(dualEdge)
    dualEdge.endEdge.incomingDualEdges.add(dualEdge)
}

data class AddEdge(
    val speed: Double,
    val capacity: Double,
    val lanes: Int,
    val fromNodeId: Int,
    val toNodeId: Int,
    val length: Double,
    val edgeId: Int? = null
): Change(NAME, CallTime.BEFORE_INTERSECTION, 10){

    override fun apply(odm: DtmOriginDestinationMatrix, db: PostGISDatabase, modelName: String) {
        val fd = fundamentalDiagram(speed, capacity, lanes)
        val maxEdgeId = odm.graph.edges.maxOf { it.id }
        val edgeId = if (this.edgeId != null){
            if (this.edgeId <= maxEdgeId){
                throw IllegalArgumentException("The edge id must be bigger than $maxEdgeId")
            }
            this.edgeId
        } else {
            maxEdgeId + 1
        }

        odm.graph.addEdge(edgeId, fromNodeId, toNodeId, length, fd)

        // add movements
        val edge = odm.graph.getEdgeById(edgeId)
        val sourceNode = edge.startNode
        val targetNode = edge.endNode

        for (inEdge in sourceNode.incomingEdges){
            val mov = DtmDualEdge(inEdge, edge, sourceNode)
            sourceNode.addAndConnectDualEdge(mov)
            odm.graph.addDualEdge(mov)
            logger.info("New movement added: node_id = ${sourceNode.id}")
        }
        for (outEdge in targetNode.outgoingEdges){
            val mov = DtmDualEdge(edge, outEdge, targetNode)
            targetNode.addAndConnectDualEdge(mov)
            odm.graph.addDualEdge(mov)
            logger.info("New movement added: node_id = ${targetNode.id}")
        }

        logger.info("New edge added: edge_id = $edgeId")
    }

    companion object {
        const val NAME = "addEdge"
    }
}

enum class CtrlType{none, yield, stop, signal}

data class AddNode(val nodeId: Int, val ctrlType: CtrlType): Change(NAME, CallTime.BEFORE_INTERSECTION, 9){

    override fun apply(odm: DtmOriginDestinationMatrix, db: PostGISDatabase, modelName: String) {
        if (ctrlType == CtrlType.none){
            val maxId = odm.graph.nodes.maxOf { it.id }
            if (nodeId < maxId){
                throw IllegalArgumentException("The new node ID must be > $maxId")
            }
            val node = DtmNode(nodeId)
            odm.graph.addNode(node)
            logger.info("New node added: node_id = $nodeId")
        }
        else{
            TODO("Not yet implemented")
        }
    }

    override fun applyAfterGraph(odm: DtmOriginDestinationMatrix) {
        val node = odm.graph.getNodeById(nodeId)
        val priorities = DoubleArray(node.incomingEdges.size)
        priorities.fill(1000.0)
        val model = DtmNoFifoNodeModel(node, HashMap(), priorities)
        node.nodeModel = model
        logger.info("DtmNoFifoNodeModel model initialized: node_id = $nodeId")
    }

    companion object {
        const val NAME = "addNode"
    }
}

data class TodFlow(
    val tod: Int,
    val flow: Double
)

private fun List<TodFlow>.toPCF(tods: List<Tod>): DtmPiecewiseConstantFunction{
    val todMap = tods.associateBy { it.todId }

    val sorted = mapNotNull { flow -> todMap[flow.tod]?.let { it to flow.flow }  }.sortedBy { it.first.startTime }
    val zeroTime = sorted.first().first.startTime.time

    fun hoursOffset(time: Time): Double {
        return (time.time - zeroTime)/3600000.0
    }

    val x = sorted.map { hoursOffset(it.first.startTime) }
    val y = sorted.map { it.second }

    val f = DtmPiecewiseConstantFunction(x, y)
    f.addPoint(hoursOffset(sorted.last().first.endTime), f.lastY)

    return f
}

data class AddZone(
    val nodeId: Int,
    val incomingTrips: List<TodFlow>,
    val outgoingTrips: List<TodFlow>,
    val alpha: Double = 0.7,
    val beta: Double = 3.0
): Change(NAME, CallTime.BEFORE_INTERSECTION){

    override fun apply(odm: DtmOriginDestinationMatrix, db: PostGISDatabase, modelName: String) {
        val node = odm.graph.getNodeById(nodeId)
        val zoneId = odm.zones.maxOf { it.id } + 1
        val zone = DtmZone(zoneId, node)
        val f = DeterrenceFunction(alpha, beta)
        val tods = db.tods(modelName)
        odm.addZone(
            zone,
            incomingTrips.toPCF(tods),
            outgoingTrips.toPCF(tods),
            f
        )
    }

    companion object {
        const val NAME = "addZone"
    }
}

data class UpdateZone(
    val zoneId: Int,
    val incomingTrips: List<TodFlow>,
    val outgoingTrips: List<TodFlow>
): Change(NAME, CallTime.BEFORE_INTERSECTION){
    override fun apply(odm: DtmOriginDestinationMatrix, db: PostGISDatabase, modelName: String) {
        val zoneFeature = db.feature(modelName, "zone", "zone_id", zoneId)
        val nodeId = zoneFeature?.properties?.get("node_id") as? Int ?: throw IllegalArgumentException("Cannot find corresponding zone")
        val tods = db.tods(modelName)
        odm.updateZone(
            odm.getZoneByNode(odm.graph.getNodeById(nodeId)),
            incomingTrips.toPCF(tods),
            outgoingTrips.toPCF(tods),
        )
        logger.info("Update zone with: node_id = $nodeId")
    }

    companion object {
        const val NAME = "updateZone"
    }
}