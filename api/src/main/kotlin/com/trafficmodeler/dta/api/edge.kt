/**
Dynamic Traffic Modeller
Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.trafficmodeler.dta.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.geojson.Feature
import org.geojson.FeatureCollection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = ["*"])
@Tag(name = "edge", description = "edges")
class EdgeController {

    @Autowired
    lateinit var database: PostGISDatabase

    @GetMapping("/edges/{model}/info")
    @Operation(summary = "get statistics")
    fun info(@Parameter(description = "model name", example = "dtm_test")
             @PathVariable("model") model: String): Info {
        return Info(database.lastID(model, "edge", "edge_id"))
    }

    @GetMapping("/edges/{model}")
    @Operation(summary = "edges as GeoJson")
    fun edges(@Parameter(description = "model name", example = "dtm_test")
              @PathVariable("model") model: String): FeatureCollection {
        return database.featureCollection(model, "edge", "geometry")
    }

    @GetMapping("/edges/{model}/{edgeId}")
    @Operation(summary = "edge by Edge ID")
    fun edge(@Parameter(description = "model name", example = "dtm_test")
             @PathVariable("model") model: String,
             @PathVariable("edgeId") edgeId: Int): Feature {
        return database.feature(model, "edge", "edge_id", edgeId) ?: throw IllegalArgumentException("Any record.")
    }

    @PostMapping("/edges/{model}", produces = ["application/json"], consumes = ["application/json"])
    @Operation(summary = "create new edge")
    fun create(@Parameter(description = "model name", example = "dtm_test")
               @PathVariable("model") model: String,

               @Parameter(description = "edge feature")
               @RequestBody data: Feature): Feature {
        return database.insertFeature(model, "edge", "edge_id", data)
    }

    @DeleteMapping("/edges/{model}/{edgeId}")
    @Operation(summary = "delete node by Node ID")
    fun delete(@Parameter(description = "model name", example = "dtm_test")
               @PathVariable("model") model: String,
               @PathVariable("edgeId") edgeId: Int) {
        return database.delete(model, "edge", "edge_id", edgeId)
    }
}

data class Info(
    val lastId: Int
)