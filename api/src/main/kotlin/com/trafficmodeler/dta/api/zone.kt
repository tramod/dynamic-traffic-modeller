/**
Dynamic Traffic Modeller
Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.trafficmodeler.dta.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.geojson.Feature
import org.geojson.FeatureCollection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = ["*"])
@Tag(name = "zone", description = "zones")
class ZoneController {

    @Autowired
    lateinit var database: PostGISDatabase

    @GetMapping("/zones/{model}/info")
    @Operation(summary = "get statistics")
    fun info(@Parameter(description = "model name", example = "dtm_test")
             @PathVariable("model") model: String): Info {
        return Info(database.lastID(model, "zone", "zone_id"))
    }

    @GetMapping("/zones/{model}")
    @Operation(summary = "zones as GeoJson")
    fun zones(@Parameter(description = "model name", example = "dtm_test")
              @PathVariable("model") model: String): FeatureCollection {
        return database.featureCollection(model, "zone", "geometry")
    }

    @GetMapping("/zones/{model}/{zoneId}")
    @Operation(summary = "zone by Zone ID")
    fun zone(@Parameter(description = "model name", example = "dtm_test")
             @PathVariable("model") model: String,
             @PathVariable("zoneId") zoneId: Int): Feature {
        return database.feature(model, "zone", "zone_id", zoneId) ?: throw IllegalArgumentException("Any record.")
    }

    @PostMapping("/zones/{model}", produces = ["application/json"], consumes = ["application/json"])
    @Operation(summary = "create new zone")
    fun createZone(@Parameter(description = "model name", example = "dtm_test")
                   @PathVariable("model") model: String,

                   @Parameter(description = "zone feature")
                   @RequestBody data: Feature): Feature {
        return database.insertFeature(model, "zone", "zone_id", data)
    }

    @DeleteMapping("/zones/{model}/{zoneId}")
    @Operation(summary = "delete zone by Zone ID")
    fun deleteZone(@Parameter(description = "model name", example = "dtm_test")
                   @PathVariable("model") model: String,
                   @PathVariable("zoneId") zoneId: Int) {
        return database.delete(model, "zone", "zone_id", zoneId)
    }
}