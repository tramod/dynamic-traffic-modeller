/**
Dynamic Traffic Modeller
Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.trafficmodeler.dta.api

import com.trafficmodeler.dta.job.AssignmentJob
import com.trafficmodeler.dta.job.AssignmentJobSettings
import com.trafficmodeler.dta.job.JobManager
import com.trafficmodeler.dta.job.LiteJob
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = ["*"])
@RequestMapping(value = ["jobs"])
@Tag(name = "jobs", description = "Job management")
class JobController {

    @Autowired
    lateinit var jobManager: JobManager

    @Value("\${dtm.db.url}")
    lateinit var dbUrl: String

    @GetMapping("/{model}")
    @Operation(summary = "returns the list of all jobs for the model")
    fun list(@Parameter(description = "model name", example = "dtm_test")
             @PathVariable("model") model: String): List<LiteJob>{
        return  jobManager.jobs(model)
    }

    @PostMapping("/{model}")
    @Operation(description = "Start computing the job in new threads according the given settings in request body",
            summary = "creates a new job and starts computation")
    fun create(@Parameter(description = "model name", example = "dtm_test")
               @PathVariable("model") model: String,

               @Parameter(description = "settings for the job")
               @RequestBody data: AssignmentJobSettings): LiteJob {
        data.dbUrl = dbUrl
        val job = AssignmentJob(model, data, jobManager, data.cacheName)
        jobManager.startJob(job)
        return job.liteJob()
    }

    @GetMapping("/{model}/{job_id}/result")
    fun result(@Parameter(description = "model name", example = "dtm_test")
               @PathVariable("model") model: String,

               @Parameter(description = "Job ID")
               @PathVariable("job_id") jobId: String): Any {
        jobManager.job(model, jobId)?.let { it ->
            it.result?.let {
                return it
            }
            throw IllegalArgumentException("The job have not the result!")
        }
        throw IllegalArgumentException("Job with this jobId does not exist!")
    }

    @GetMapping("/{model}/{job_id}/status")
    @Operation(summary = "returns the status of the job (ERROR, FINISHED or RUNNING)")
    fun status(@Parameter(description = "model name", example = "dtm_test")
               @PathVariable("model") model: String,

               @Parameter(description = "Job ID")
               @PathVariable("job_id") jobId: String): LiteJob {
        jobManager.job(model, jobId)?.let {
            return it.liteJob()
        }
        throw IllegalArgumentException("Job with this jobId does not exist!")
    }

    @DeleteMapping("/{model}/{job_id}")
    @Operation(summary = "kill the job by job ID")
    fun delete(@Parameter(description = "model name", example = "dtm_test")
               @PathVariable("model") model: String,

               @Parameter(description = "Job ID")
               @PathVariable("job_id") jobId: String){
        jobManager.job(model, jobId)?.let {
            jobManager.killJob(it)
            return
        }
        throw IllegalArgumentException("Job with this jobId does not exist!")
    }
}