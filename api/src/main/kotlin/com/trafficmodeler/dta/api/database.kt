/**
Dynamic Traffic Modeller
Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.trafficmodeler.dta.api

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.dbcp2.BasicDataSource
import org.geojson.*
import org.geojson.jackson.CrsType
import java.io.Serializable
import java.lang.IllegalArgumentException
import java.sql.*

/**
 * Represent PostGIS database
 * @param url jdbc:postgresql://localhost:5432/db?user=username&password=password
 */
class PostGISDatabase(private val url: String) {

    private val connectionPool: BasicDataSource = initConnectionPool()
    private val objectMapper = ObjectMapper()

    private fun initConnectionPool(): BasicDataSource {
        val connectionPool = BasicDataSource()
        connectionPool.driverClassName = "org.postgresql.Driver"
        connectionPool.url = url
        connectionPool.initialSize = 1
        connectionPool.maxTotal = 10
        connectionPool.maxIdle = 10
        return connectionPool
    }

    /**
     * @param modelName name of the model
     * @param day weekday
     */
    fun modelInfo(modelName: String, day: Int): ModelInfo {
        connectionPool.connection.use {
            val sql = "SELECT min(start_time), max(end_time) FROM $modelName.tod WHERE day = ?"
            val st = it.prepareStatement(sql)
            st.setInt(1, day)
            val rs = st.executeQuery()
            if (rs.next()){
                val startTime = rs.getTime(1)
                val endTime = rs.getTime(2)
                return ModelInfo(startTime, endTime)
            }
            throw IllegalArgumentException("TOD table is empty.")
        }
    }

    private fun featureFromResultSet(resultSet: ResultSet, geometryColumn: String): Feature {
        val feature = Feature()
        val meta = resultSet.metaData
        val numCol = meta.columnCount

        // geometry
        val jsonContent = resultSet.getString(GEOJSON_COLUMN)
        if (jsonContent != null){
            val geometry = objectMapper.readValue(jsonContent, GeoJsonObject::class.java)
            feature.geometry = geometry
        }

        // properties
        for(i in 1..numCol){
            val colName = meta.getColumnName(i)
            if (colName != GEOJSON_COLUMN && colName != geometryColumn){
                val cell = resultSet.getObject(colName)
                if (cell is String || cell is Number || cell is Boolean || cell is Timestamp){
                    feature.setProperty(colName, cell)
                }
            }
        }

        return feature
    }

    fun lastID(schema: String, table: String, primaryKey: String): Int {
        connectionPool.connection.use {
            return lastID(it, schema, table, primaryKey)
        }
    }

    /**
     * Get last ID. Do not close the connection.
     */
    private fun lastID(connection: Connection, schema: String, table: String, primaryKey: String): Int {
        val sql = "SELECT max($primaryKey) FROM $schema.$table"
        val st = connection.prepareStatement(sql)
        val rs = st.executeQuery()
        if(rs.next()){
            return rs.getInt(1)
        }
        return 0
    }

    /**
     * Insert feature into table
     */
    fun insertFeature(schema: String, table: String, primaryKey: String?, feature: Feature,
                      geometryColumn: String = "geometry"): Feature {
        connectionPool.connection.use {
            primaryKey?.let { primaryKey ->
                if (!feature.properties.containsKey(primaryKey)){
                    feature.setProperty(primaryKey, lastID(it, schema, table, primaryKey) + 1)
                }
            }

            val fields = feature.properties.keys.joinToString(",") { "\"$it\"" }
            val questionMarks = feature.properties.keys.joinToString(",") { column ->
                if (column in setOf("start_time", "end_time")){
                    return@joinToString " ?::time"
                }
                " ?"
            }

            val sql = if (feature.geometry == null) {
                "INSERT INTO $schema.$table($fields) VALUES ($questionMarks)"
            } else {
                "INSERT INTO $schema.$table($fields, $geometryColumn) VALUES ($questionMarks, ST_SetSRID(ST_GeomFromGeoJSON(?), 4326))"
            }

            val st = it.prepareStatement(sql)

            var i = 1
            for (prop in feature.properties){
                when(val value: Any? = prop.value){
                    is String -> st.setString(i, value)
                    is Int -> st.setInt(i, value)
                    is Long -> st.setLong(i, value)
                    is Boolean -> st.setBoolean(i, value)
                    is Double -> st.setDouble(i, value)
                    is Float -> st.setFloat(i, value)
                    is List<*> -> {
                        if (value.isEmpty()){
                            st.setString(i, "{}")
                        }
                        else {
                            val type = when(value.first()){
                                is String -> "varchar"
                                is Int -> "int"
                                is Long -> "bigint"
                                is Boolean -> "bool"
                                is Double -> "double precision"
                                is Float -> "double precision"
                                else -> "varchar"
                            }
                            val array = it.createArrayOf(type, value.toTypedArray())
                            st.setArray(i, array)
                        }
                    }
                    else -> {
                        if (value == null){
                            st.setNull(i, Types.NULL)
                        }
                        else{
                            st.setString(i, value.toString())
                        }
                    }
                }
                i++
            }

            // add geometry
            feature.geometry?.let {
                val geometryString = objectMapper.writeValueAsString(feature.geometry)
                st.setString(i, geometryString)
            }

            st.executeUpdate()
            return feature
        }
    }

    /**
     * Get feature by ID.
     */
    fun feature(schema: String, table: String, primaryKey: String, id: Int,
                geometryColumn: String = "geometry"): Feature? {
        connectionPool.connection.use {
            val sql = "SELECT *, ST_asgeojson($geometryColumn) as $GEOJSON_COLUMN FROM $schema.$table WHERE $primaryKey = ?"
            val st = it.prepareStatement(sql)
            st.setInt(1, id)
            val rs = st.executeQuery()
            if(rs.next()){
                return featureFromResultSet(rs, geometryColumn)
            }
            return null
        }
    }

    /**
     * Delete feature by ID.
     */
    fun delete(schema: String, table: String, primaryKey: String, id: Int) {
        connectionPool.connection.use {
            val sql = "DELETE FROM $schema.$table WHERE $primaryKey = ?"
            val st = it.prepareStatement(sql)
            st.setInt(1, id)
            st.executeUpdate()
        }
    }


    /**
     * Returns GeoJson of the table
     * @param schema database schema name
     * @param table table name
     * @param geometryColumn name of the geometry column
     */
    fun featureCollection(schema: String, table: String, geometryColumn: String, where: String? = null): FeatureCollection {
        connectionPool.connection.use {
            var sql = "SELECT *, ST_asgeojson($geometryColumn) as $GEOJSON_COLUMN FROM $schema.$table"
            where?.let {
                sql += " WHERE $where"
            }

            val st = it.prepareStatement(sql)
            val rs = st.executeQuery()
            val meta = rs.metaData
            val numCol = meta.columnCount

            val featureCollection = FeatureCollection()
            val objectMapper = ObjectMapper()
            while(rs.next()){
                featureCollection.add(featureFromResultSet(rs, geometryColumn))
            }
            val crs = Crs()
            crs.type = CrsType.name
            crs.properties = mapOf("name" to "urn:ogc:def:crs:EPSG::4326")
            featureCollection.crs = crs
            return featureCollection
        }
    }

    fun tods(schema: String): List<Tod> {
        connectionPool.connection.use{
            val sql = "SELECT * FROM $schema.tod"
            val st = it.prepareStatement(sql)
            val rs = st.executeQuery()
            val tods = ArrayList<Tod>()
            while(rs.next()){
                tods.add(
                    Tod(rs.getInt("tod_id"), rs.getInt("day"), rs.getTime("start_time"), rs.getTime("end_time"))
                )
            }
            return tods
        }
    }

    fun tod(schema: String, todId: Int): Tod? {
        connectionPool.connection.use{
            val sql = "SELECT * FROM $schema.tod WHERE tod_id = ?"
            val st = it.prepareStatement(sql)
            st.setInt(1, todId)
            val rs = st.executeQuery()
            if(rs.next()){
                return Tod(rs.getInt("tod_id"), rs.getInt("day"), rs.getTime("start_time"), rs.getTime("end_time"))
            }
            return null
        }
    }

    fun addTod(schema: String, tod: Tod): Tod? {
        connectionPool.connection.use {
            val sql = "INSERT INTO $schema.tod(tod_id, day, start_time, end_time) VALUES (?, ?, ?, ?)"
            val st = it.prepareStatement(sql)
            st.setInt(1, tod.todId)
            st.setInt(2, tod.day)
            st.setTime(3, tod.startTime)
            st.setTime(4, tod.endTime)
            st.executeUpdate()
        }
        return tod(schema, tod.todId)
    }

    companion object {
        const val GEOJSON_COLUMN = "geojson"
    }

}

data class Tod(
    val todId: Int,
    val day: Int,
    val startTime: Time,
    val endTime: Time
)

data class ModelInfo (
        val startTime: Time,
        val endTime: Time
): Serializable