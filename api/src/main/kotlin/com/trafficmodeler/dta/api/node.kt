/**
Dynamic Traffic Modeller
Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.trafficmodeler.dta.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.geojson.Feature
import org.geojson.FeatureCollection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = ["*"])
@Tag(name = "node", description = "nodes")
class NodeController {

    @Autowired
    lateinit var database: PostGISDatabase

    @GetMapping("/nodes/{model}/info")
    @Operation(summary = "get statistics")
    fun info(@Parameter(description = "model name", example = "dtm_test")
             @PathVariable("model") model: String): Info {
        return Info(database.lastID(model, "node", "node_id"))
    }

    @GetMapping("/nodes/{model}")
    @Operation(summary = "nodes as GeoJson")
    fun nodes(@Parameter(description = "model name", example = "dtm_test")
              @PathVariable("model") model: String): FeatureCollection {
        return database.featureCollection(model, "node", "geometry")
    }

    @GetMapping("/nodes/{model}/{nodeId}")
    @Operation(summary = "node by Node ID")
    fun node(@Parameter(description = "model name", example = "dtm_test")
             @PathVariable("model") model: String,
             @PathVariable("nodeId") nodeId: Int): Feature {
        return database.feature(model, "node", "node_id", nodeId) ?: throw IllegalArgumentException("Any record.")
    }

    @PostMapping("/nodes/{model}", produces = ["application/json"], consumes = ["application/json"])
    @Operation(summary = "create new node")
    fun create(@Parameter(description = "model name", example = "dtm_test")
                   @PathVariable("model") model: String,

                   @Parameter(description = "node feature")
                   @RequestBody data: Feature): Feature {
        return database.insertFeature(model, "node", "node_id", data)
    }

    @DeleteMapping("/nodes/{model}/{nodeId}")
    @Operation(summary = "delete node by Node ID")
    fun delete(@Parameter(description = "model name", example = "dtm_test")
                   @PathVariable("model") model: String,
                   @PathVariable("nodeId") nodeId: Int) {
        return database.delete(model, "node", "node_id", nodeId)
    }
}