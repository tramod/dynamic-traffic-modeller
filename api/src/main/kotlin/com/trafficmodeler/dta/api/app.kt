/**
Dynamic Traffic Modeller
Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package com.trafficmodeler.dta.api

import com.trafficmodeler.dta.job.JobManager
import io.swagger.v3.oas.models.ExternalDocumentation
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.servers.Server
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@SpringBootApplication
class DynamicTrafficModelerApplication

fun main(args: Array<String>) {
	runApplication<DynamicTrafficModelerApplication>(*args)
}

@Configuration
class AppConfiguration(){
	@Value("\${dtm.jobManager.cacheSize}")
	val cacheSize: Long = 0
	@Value("\${dtm.jobManager.jobExpiration}")
	val jobExpiration: Long = 0
	@Value("\${dtm.jobManager.dbFileName}")
	lateinit var dbFileName: String
	@Value("\${dtm.jobManager.corePoolSize}")
	val corePoolSize: Int = 0
	@Value("\${dtm.db.url}")
	lateinit var dbUrl: String

	@Bean fun jobManager() = JobManager(cacheSize, jobExpiration, dbFileName, corePoolSize)
	@Bean fun database() = PostGISDatabase(dbUrl)
}

@Configuration
class OpenAPIConfig {
	@Bean
	fun springOpenAPI(): OpenAPI {
		return OpenAPI()
				.info(Info().title("Dynamic Traffic Modeller API")
                    .description("REST API for Dynamic Traffic Modeller")
					.version("0.0.1-alpha")
                ).externalDocs(
                    ExternalDocumentation()
                    .description("Dynamic Traffic Modeler external API documentation")
                    .url("https://gitlab.com/tramod/dynamic-traffic-modeller/-/blob/master/doc/api.md")
                )
            .addServersItem(Server().url("/").description("this"))
            .addServersItem(Server().url("https://dynamic-dev.trafficmodeler.com").description("development"))
            .addServersItem(Server().url("https://dynamic.trafficmodeler.com").description("production"))

    }
}

