/**
Dynamic Traffic Modeller
Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.trafficmodeler.dta.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.geojson.Feature
import org.geojson.FeatureCollection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = ["*"])
@Tag(name = "movements", description = "movements")
class MovementController {

    @Autowired
    lateinit var database: PostGISDatabase

    @GetMapping("/movements/{model}/node/{nodeId}")
    @Operation(summary = "movements as GeoJson")
    fun movements(
        @Parameter(description = "node id", example = "10")
        @PathVariable("nodeId") nodeId: Int,

        @Parameter(description = "model name", example = "dtm_test")
        @PathVariable("model") model: String
    ): FeatureCollection {
        return database.featureCollection(model, "mvmt", "geometry", "node_id = $nodeId")
    }

    @GetMapping("/movements/{model}/{mvmtId}")
    @Operation(summary = "movement by mvmt ID")
    fun movement(@Parameter(description = "model name", example = "dtm_test")
             @PathVariable("model") model: String,
             @PathVariable("mvmtId") mvmtId: Int): Feature {
        return database.feature(model, "mvmt", "mvmt_id", mvmtId) ?: throw IllegalArgumentException("Any record.")
    }

    @PostMapping("/movements/{model}", produces = ["application/json"], consumes = ["application/json"])
    @Operation(summary = "create new movement")
    fun create(@Parameter(description = "model name", example = "dtm_test")
               @PathVariable("model") model: String,

               @Parameter(description = "movement feature")
               @RequestBody data: Feature
    ): Feature {
        return database.insertFeature(model, "mvmt", "mvmt_id", data)
    }

    @DeleteMapping("/movements/{model}/{mvmtId}")
    @Operation(summary = "delete movement by Movement ID")
    fun delete(@Parameter(description = "model name", example = "dtm_test")
               @PathVariable("model") model: String,
               @PathVariable("mvmtId") movementId: Int) {
        return database.delete(model, "mvmt", "mvmt_id", movementId)
    }
}