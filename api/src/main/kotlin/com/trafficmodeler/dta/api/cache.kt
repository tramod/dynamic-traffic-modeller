/**
Dynamic Traffic Modeller
Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.trafficmodeler.dta.api

import com.trafficmodeler.dta.job.*
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.geojson.FeatureCollection

@RestController
@CrossOrigin(origins = ["*"])
@RequestMapping(value = ["caches"])
@Tag(name = "caches", description = "Cache/PersistentJob management")
class CacheController {

    @Autowired
    lateinit var jobManager: JobManager

    @Autowired
    lateinit var database: PostGISDatabase

    @GetMapping("/{model}")
    @Operation(summary = "returns the list of all persist jobs for the model")
    fun list(@Parameter(description = "model name", example = "dtm_test")
             @PathVariable("model") model: String): List<LiteJob> {
        return  jobManager.persistentJobs(model)
    }

    @GetMapping("/{model}/{job_id}")
    @Operation(summary = "get persist job")
    fun result(@Parameter(description = "model name", example = "dtm_test")
               @PathVariable("model") model: String,

               @Parameter(description = "Job ID / Cache Name")
               @PathVariable("job_id") jobId: String): PersistentJob {
        jobManager.persistentJob(model, jobId)?.let {
            return it
        }
        throw IllegalArgumentException("Job with this jobId/cacheName does not exist!")
    }

    @GetMapping("/{model}/{job_id}/full")
    @Operation(summary = "get persist job")
    fun full(@Parameter(description = "model name", example = "dtm_test")
               @PathVariable("model") model: String,

               @Parameter(description = "Job ID / Cache Name")
               @PathVariable("job_id") jobId: String): FeatureCollection {
        jobManager.persistentJob(model, jobId)?.let { job ->
            val json = database.featureCollection(model, "edge", "geometry")
            val map = HashMap<Int, AssignmentResultItem>()
            (job.result as AssignmentResult).result.forEach { map[it.edgeId] = it }
            json.features.forEach { feature ->
                val edgeId = feature.properties["edge_id"]
                map[edgeId]?.let { resultItem ->
                    feature.setProperty("inflow", resultItem.inFlow)
                    feature.setProperty("outflow", resultItem.outFlow)
                    feature.setProperty("travel_time", resultItem.travelTime)
                }
            }
            return json
        }
        throw IllegalArgumentException("Job with this jobId/cacheName does not exist!")
    }

    @DeleteMapping("/{model}/{job_id}")
    @Operation(summary = "remove persist job")
    fun delete(@Parameter(description = "model name", example = "dtm_test")
               @PathVariable("model") model: String,

               @Parameter(description = "Job ID / Cache Name")
               @PathVariable("job_id") jobId: String){
        jobManager.removePersistentJob(model, jobId)
    }
}