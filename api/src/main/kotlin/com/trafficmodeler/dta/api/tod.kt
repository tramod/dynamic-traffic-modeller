/**
Dynamic Traffic Modeller
Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.trafficmodeler.dta.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.geojson.Feature
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = ["*"])
@Tag(name = "tods", description = "Time of days")
class TodController(){
    @Autowired
    lateinit var database: PostGISDatabase

    @GetMapping("/tods/{model}")
    @Operation(summary = "list of TODs")
    fun tods(@Parameter(description = "model name", example = "dtm_test")
              @PathVariable("model") model: String): List<Tod> {
        return database.tods(model)
    }

    @GetMapping("/tods/{model}/{todId}")
    @Operation(summary = "get TOD by tod_id")
    fun tod(@Parameter(description = "model name", example = "dtm_test")
            @PathVariable("model") model: String,
            @PathVariable("todId") todId: Int): Tod {
        return database.tod(model, todId) ?: throw IllegalArgumentException("Tod does not exist.")
    }

    @PostMapping("/tods/{model}")
    @Operation(summary = "create TOD")
    fun create(
        @Parameter(description = "model name", example = "dtm_test")
        @PathVariable("model") model: String,

        @Parameter(description = "tod object")
        @RequestBody data: Tod
    ): Tod {
        return database.addTod(model, data) ?: throw IllegalArgumentException("Cannot crete tod")
    }

    @DeleteMapping("/tods/{model}/{todId}")
    @Operation(summary = "delete tod by TOD ID")
    fun delete(@Parameter(description = "model name", example = "dtm_test")
               @PathVariable("model") model: String,
               @PathVariable("todId") todId: Int) {
        return database.delete(model, "tod", "tod_id", todId)
    }

}