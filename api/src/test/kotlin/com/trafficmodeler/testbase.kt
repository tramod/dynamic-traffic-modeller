/**
Dynamic Traffic Modeller
Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.trafficmodeler

import com.trafficmodeler.dta.api.PostGISDatabase
import com.trafficmodeler.dta.job.CtrlType
import org.geojson.Feature
import org.geojson.LineString
import org.geojson.LngLatAlt
import org.geojson.Point
import org.junit.jupiter.api.BeforeEach
import java.io.File
import java.io.FileInputStream
import java.nio.charset.Charset
import java.util.*

open class TestBase {
    val dbUrl: String
    val testModelName = "dtm_unittest"
    val db: PostGISDatabase
    val testResultsFileName = "testResults.db"

    init {
        val file = File("src/main/resources/application.properties")
        val prop = Properties()
        FileInputStream(file).use { prop.load(it) }
        dbUrl = prop.getProperty("dtm.db.url")
        db = PostGISDatabase(dbUrl)
    }

    @BeforeEach
    fun before(){
        createSchema()
    }

    private fun createSchema(){
        val schema = File("../database/dtm-schema.sql").readText()
        val customizedSchema = schema.replace("name_of_model", testModelName)
        File("../database/dtm-schema.temp.sql").writeText(customizedSchema)

        val process = ProcessBuilder("psql", "-d", "postgis", "-U", "kolovsky", "-f", "../database/dtm-schema.temp.sql").start()
        process.inputStream.reader(Charset.defaultCharset()).use {
            it.readText()
        }
        process.onExit().get()
    }

    fun insertNode(nodeId: Int, ctrlType: CtrlType, useMovement: Boolean, x: Double = 0.0, y: Double = 0.0){
        val feature = Feature()
        feature.setProperty("node_id", nodeId)
        feature.setProperty("ctrl_type", ctrlType.toString().lowercase())
        feature.setProperty("use_movement", useMovement)
        feature.geometry = Point(x, y)
        db.insertFeature(testModelName, "node", "node_id", feature)
    }

    fun insertEdge(
        edge_id: Int,
        from_node_id: Int,
        to_node_id: Int,
        name: String? = null,
        capacity: Double = 1000.0,
        allowed_uses: List<String>? = null,
        lanes: Int = 1,
        free_speed: Double = 100.0,
        critical_speed: Double = 80.0,
        facility_type: String? = null,
        length: Double = 1.0,
        order: Int = 1
    ){
        val feature = Feature()
        feature.setProperty("edge_id", edge_id)
        feature.setProperty("name", name)
        feature.setProperty("from_node_id", from_node_id)
        feature.setProperty("to_node_id", to_node_id)
        feature.setProperty("capacity", capacity)
        feature.setProperty("allowed_uses", allowed_uses)
        feature.setProperty("lanes", lanes)
        feature.setProperty("free_speed", free_speed)
        feature.setProperty("critical_speed", critical_speed)
        feature.setProperty("facility_type", facility_type)
        feature.setProperty("length", length)
        feature.setProperty("order", order)

        feature.geometry = LineString(LngLatAlt(0.0, 0.0), LngLatAlt(1.0, 1.0))
        db.insertFeature(testModelName, "edge", "edge_id", feature)
    }

    fun insertMovement(
        mvmt_id: Int,
        node_id: Int,
        ib_edge_id: Int,
        ob_edge_id: Int,
        start_ib_lane: Int = 1,
        end_ib_lane: Int? = null,
        start_ob_lane: Int = 1,
        end_ob_lane: Int? = null,
        type: MovementType = MovementType.other,
        allowed_uses: List<String>? = null,
        capacity: Double = 1000.0
    ){
        val feature = Feature()
        feature.setProperty("mvmt_id", mvmt_id)
        feature.setProperty("node_id", node_id)
        feature.setProperty("ib_edge_id", ib_edge_id)
        feature.setProperty("start_ib_lane", start_ib_lane)
        feature.setProperty("end_ib_lane", end_ib_lane)
        feature.setProperty("ob_edge_id", ob_edge_id)
        feature.setProperty("start_ob_lane", start_ob_lane)
        feature.setProperty("end_ob_lane", end_ob_lane)
        feature.setProperty("type", type)
        feature.setProperty("allowed_uses", allowed_uses)
        feature.setProperty("capacity", capacity)

        db.insertFeature(testModelName, "mvmt", "mvmt_id", feature)
    }

    fun insertZone(
        zone_id: Int,
        node_id: Int,
        name: String? = null,
        incoming_trips: Double? = null,
        outgoing_trips: Double? = null
    ){
        val feature = Feature()
        feature.setProperty("zone_id", zone_id)
        feature.setProperty("name", name)
        feature.setProperty("node_id", node_id)
        feature.setProperty("incoming_trips", incoming_trips)
        feature.setProperty("outgoing_trips", outgoing_trips)

        db.insertFeature(testModelName, "zone", "zone_id", feature)
    }

    fun insertTOD(){
        val tod = Feature()
        tod.setProperty("tod_id", 1)
        tod.setProperty("day", 0)
        tod.setProperty("start_time", "09:00:00")
        tod.setProperty("end_time", "10:00:00")

        db.insertFeature(testModelName, "tod", "tod_id", tod)
    }

    fun insertPair(origin_zone_id: Int, destination_zone_id: Int, flow: Double, tod_id: Int = 1){
        val pair = Feature()
        pair.setProperty("origin_zone_id", origin_zone_id)
        pair.setProperty("destination_zone_id", destination_zone_id)
        pair.setProperty("tod_id", tod_id)
        pair.setProperty("flow", flow)

        db.insertFeature(testModelName, "odm", null, pair)

    }

    fun insertBraess(){
        insertNode(1, CtrlType.none, false)
        insertNode(2, CtrlType.none, false)
        insertNode(3, CtrlType.none, false)
        insertNode(4, CtrlType.none, false)

        insertEdge(1, 1, 2 )
        insertEdge(2, 1, 3 )
        insertEdge(3, 2, 3 )
        insertEdge(4, 2, 4 )
        insertEdge(5, 3, 4 )

        insertZone(1, 1)
        insertZone(4, 4)

        insertTOD()
        insertPair(1, 4, 1000.0)
    }
}

enum class MovementType { left_from_major, right_from_minor, through_from_minor, left_from_minor, other }

fun assertError(block: () -> Unit){
    try {
        block()
        assert(false)
    } catch (e: Throwable) {
        assert(true)
    }
}