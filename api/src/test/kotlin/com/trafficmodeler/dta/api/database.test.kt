package com.trafficmodeler.dta.api

import com.trafficmodeler.TestBase
import org.geojson.Feature
import org.geojson.Point
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

@Disabled
class DatabaseTest: TestBase(){

    @Test
    fun readGeoJson(){
        db.featureCollection("tm_pilsen_new", "node", "geometry")
    }

    @Test
    fun modelInfo(){
        val modeInfo = db.modelInfo("dtm_anaheim", 0)
        println(modeInfo.endTime)
    }

    @Test
    fun insert(){
        val f = Feature()
        f.setProperty("name", "nazev")
        f.setProperty("incoming_trips", 12)
        f.setProperty("outgoing_trips", 12)
        f.setProperty("node_id", 1)
        f.geometry = Point(14.0, 50.0)
        db.insertFeature("dtm_nguyen", "zone", "zone_id", f)
    }

    @Test
    fun delete(){
        db.delete("dtm_nguyen", "zone", "zone_id", 9)
    }
}