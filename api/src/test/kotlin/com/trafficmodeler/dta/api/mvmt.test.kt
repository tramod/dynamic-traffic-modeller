package com.trafficmodeler.dta.api

import com.trafficmodeler.TestBase
import com.trafficmodeler.assertError
import com.trafficmodeler.dta.job.CtrlType
import org.geojson.Feature
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

@Disabled
class MovementTest: TestBase() {

    @Test
    fun read(){
        insertNode(1, CtrlType.none, false)
        insertNode(2, CtrlType.none, false)
        insertNode(3, CtrlType.none, true)
        insertNode(4, CtrlType.none, false)

        insertEdge(1, 1, 3 )
        insertEdge(2, 2, 3 )
        insertEdge(3, 3, 4 )

        insertMovement(1, 3, 1, 3)
        insertMovement(2, 3, 2, 3)

        val collection = db.featureCollection(testModelName, "mvmt", "geometry", where = "node_id = 3")
        assert(collection.features.size == 2)
        val movement = db.feature(testModelName, "mvmt", "mvmt_id", 1)
        assert(movement != null)
    }

    @Test
    fun integration(){
        insertNode(2, CtrlType.none, false)
        insertNode(3, CtrlType.none, true)
        insertNode(4, CtrlType.none, false)

        insertEdge(2, 2, 3 )
        insertEdge(3, 3, 4 )

        val cont = MovementController()
        cont.database = db

        val movement = Feature()
        movement.setProperty("mvmt_id", 1)
        movement.setProperty("node_id", 3)
        movement.setProperty("ib_edge_id", 2)
        movement.setProperty("ob_edge_id", 3)
        movement.setProperty("start_ib_lane", 1)
        movement.setProperty("start_ob_lane", 1)
        movement.setProperty("type", "other")
        movement.setProperty("capacity", 1000.0)

        cont.create(testModelName, movement)
        val all = cont.movements(3, testModelName)
        assert(all.features.size == 1)
        val one = cont.movement(testModelName, 1)
        assert(one.properties["mvmt_id"] == 1)
        cont.delete(testModelName, 1)
        assertError {
            cont.movement(testModelName, 1)
        }

    }
}