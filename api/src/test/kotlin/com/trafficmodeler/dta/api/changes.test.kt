package com.trafficmodeler.dta.api

import com.kolovsky.dta.raadsen.java.DtmModelLoader
import com.trafficmodeler.TestBase
import com.trafficmodeler.dta.job.*
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

@Disabled
class ChangeTest: TestBase(){
    @Test
    fun test(){
        insertBraess()

        val setting = AssignmentJobSettings(
            update = listOf(
                UpdateMovement(3, 3, 5, 0.0),
                EdgeUpdate(3, 50.0, 500.0, 1),
                AddEdge(100.0, 1000.0, 1, 3, 2, 1.0),
                AddNode(5, CtrlType.none),
                AddEdge(100.0, 1000.0, 1, 4, 5, 1.0, edgeId = 30),
                AddZone(2, listOf(TodFlow(1, 100.0)), listOf(TodFlow(1, 100.0))),
                UpdateZone(1, listOf(TodFlow(1, 300.0)), listOf(TodFlow(1, 300.0)))
            ),
            dbUrl = dbUrl
        )
        val manager = JobManager(10, 100, "test.db", 1)
        val job = AssignmentJob(testModelName, setting, manager)

        job.apply {
            val loader = DtmModelLoader(settings.dbUrl)

            // LOAD MODEL FROM DATABASE
            // read graph
            val g = loader.readGraph(modelName)
            val graph = g.first
            val edgeLanes = g.second

            // read ODM
            val odm = loader.readOriginDestinationMatrix(modelName, graph, settings.day)

            odm.initializeOrigins()

            // apply changes
            applyChanges(settings, odm, CallTime.BEFORE_INTERSECTION, db)

            // include intersections
            loader.includeIntersectionsToGraph(modelName, graph, edgeLanes)

            applyChangesAfterGraph(settings, odm)

            // apply changes
            applyChanges(settings, odm, CallTime.AFTER_INTERSECTION, db)

            // ================================

            assert(graph.getNodeById(3).dualEdges.size == 3)
            graph.getEdgeById(3).fundamentalDiagram.apply {
                assert(flowCapacity == 500.0)
                assert(maximalSpeed == 50.0)
            }
            val e = graph.getEdgeById(6)
            assert(graph.getNodeById(2).dualEdges.filter { it.startEdge == e }.size == 2)
            assert(graph.getNodeById(4).dualEdges.size == 2)
            assert(graph.getNodeById(5) != null)

            assert(graph.getNodeById(5).nodeModel != null)

            assert(graph.getEdgeById(30) != null)

            assert(odm.pairs.map { it.flow }[1].firstY == 100.0)
            assert(odm.pairs.map { it.flow }[0].firstY == 300.0)
        }
    }
}