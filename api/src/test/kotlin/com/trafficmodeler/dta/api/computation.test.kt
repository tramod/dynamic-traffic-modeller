package com.trafficmodeler.dta.api

import com.trafficmodeler.TestBase
import com.trafficmodeler.dta.job.AssignmentJobSettings
import com.trafficmodeler.dta.job.AssignmentResult
import com.trafficmodeler.dta.job.JobManager
import com.trafficmodeler.dta.job.JobStatus
import org.junit.jupiter.api.Test

class ComputationTest: TestBase() {

    @Test
    fun braess(){
        insertBraess()
        val controller = JobController()
        controller.jobManager = JobManager(10, 86400, testResultsFileName, 1)
        controller.dbUrl = dbUrl

        val settings = AssignmentJobSettings(maximalIteration = 10, gradientStep = 0.02, flowThreshold = 2.0)

        var job = controller.create(testModelName, settings)
        while (job.status != JobStatus.FINISHED){
            Thread.sleep(1000)
            job = controller.status(testModelName, job.jobId)
            println(job)
        }
        val result = controller.result(testModelName, job.jobId) as AssignmentResult
        result.result.forEach {
            println(it)
        }
    }

    @Test
    fun empty(){}

}