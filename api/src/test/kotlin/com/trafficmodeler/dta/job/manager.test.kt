package com.trafficmodeler.dta.job

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.junit.jupiter.api.Test

class JobManagerTest{
    @Test
    fun test(){
        val manager = JobManager(12, 1000, "test.db", 3)

        val jobs = ArrayList<Job>()
        for (i in 0..8){
            val job = ExampleJob("test", null, manager)
            manager.startJob(job)
            jobs.add(job)
        }
        val j = ExampleJob("test", "cache", manager, "cache")
        jobs.add(j)
        manager.startJob(j)

        for (job in jobs){
            println(job.jobId)
        }

        Thread.sleep(1000)

        manager.killJob(jobs[1])
        manager.killJob(jobs[4])

        var check = true
        while (check){
            Thread.sleep(1000)
            check = false
            for (job in jobs){
                if (job.status == JobStatus.RUNNING || job.status == JobStatus.WAITING){
                    check = true
                }
            }
        }

        for (job in jobs){
            println(job.status)
        }
        assert(jobs[1].status == JobStatus.ERROR)
        assert(jobs[9].status == JobStatus.FINISHED)
        assert(jobs[4].status == JobStatus.ERROR)

        //assert(manager.persistentJob("test", "cache") != null)
    }

    @Test
    fun json(){
        val mapper = ObjectMapper().registerModule(KotlinModule())
        val string = mapper.writeValueAsString(DeleteEdge(125))

        val change = mapper.readValue(string, Change::class.java)
        assert(change.order == 50)
    }
}