/**
Dynamic Traffic Modeller
Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.trafficmodeler.distribution

import com.kolovsky.dta.raadsen.java.*
import com.trafficmodeler.TestBase
import com.trafficmodeler.dta.api.PostGISDatabase
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

class DistributionTest: TestBase() {
    @Disabled @Test
    fun anaheim(){
        val loader = DtmModelLoader(dbUrl)
        val odm = loader.readModel("dtm_anaheim", 0)
        val deterrenceFunction = DeterrenceFunction(0.7, 3.0)
        val trips = DtmPiecewiseConstantFunction(listOf(0.0, 1.0), listOf(100.0, 100.0))
        val zone = DtmZone(10000, odm.graph.getNodeByIndex(10))
        odm.addZone(zone, trips, trips, deterrenceFunction)
        odm.getOriginZonePairs(zone).forEach {
            println(it.flow)
        }

        println(PostGISDatabase(dbUrl).tods("dtm_anaheim"))
    }

    @Disabled @Test
    fun anaheimChange(){
        val fc = DtmFundamentalDiagram(120.0, 2000.0, 180.0, 61.0, -23.0)
        val graph = DtmGraph()
        graph.addEdge(1, 1, 2, 1.0, fc)
        val pairs = listOf(
            DtmTripleElement(1,2, DtmPiecewiseConstantFunction(listOf(0.0, 1.0), listOf(100.0, 100.0))),
            DtmTripleElement(1,2, DtmPiecewiseConstantFunction(listOf(0.0, 1.0), listOf(100.0, 100.0))),
            DtmTripleElement(1,2, DtmPiecewiseConstantFunction(listOf(0.0, 1.0), listOf(100.0, 100.0)))
        )
        val odm = DtmOriginDestinationMatrix(graph, pairs)
        odm.initializeOrigins()
        val zone = odm.zones.first()
        odm.updateZone(zone, DtmPiecewiseConstantFunction(listOf(0.0, 1.0), listOf(200.0, 200.0)), DtmPiecewiseConstantFunction(listOf(0.0, 1.0), listOf(200.0, 200.0)))
        odm.pairs.forEach {
            println(it.flow)
        }

    }
}