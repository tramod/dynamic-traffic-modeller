# REST API for Dynamic Traffic Modeller
The Swagger-ui runs at [localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

## Jobs
Maintain the management of the assignment jobs. 
```
POST    /jobs/{model}                       create new job
GET     /jobs/{model}                       list of all jobs
GET     /jobs/{model}/{job_id}/status       status of the job
GET     /jobs/{model}/{job_id}/result       result of the job
DELETE  /jobs/{model}/{job_id}              kill the job
```

### Job settings structure
Example of the settings for assignment job.
```json
{  
  "numOfTimeIntervals": 10,
  "fanStep": 100,
  "flowThreshold": 2,
  "mixtureFlowThreshold": 0.02,
  "day": 0,
  "maximalIteration": 10,
  "gradientStep": 0.1,
  "maximalNodeTime": 2.0,
  "maximalSimulationTime": 3.0,
  "cacheName": "default-model",
  "update": [
    {
      "type": "addEdge",
      "speed": 50,
      "capacity": 3000,
      "lanes": 2,
      "fromNodeId": 457,
      "toNodeId": 458,
      "length": 0.56
    },
    {
      "type": "updateEdge",
      "edgeId": 12,
      "speed": 50,
      "capacity": 1800,
      "lanes": 1
    }
  ]
}
```
List of parameters:

| parameter | optional | units | recommended value | description |
|-----------|----------|-------|-------------------|-------------|
| numOfTimeIntervals | false | veh/h | 15 min/interval | number od time intervals for simulation  |
| fanStep | false | veh/h | 10 000 | approximation of the fan effect    |
| flowThreshold | false | veh/h | 2 - 5 | propagation threshold for flow change events
| mixtureFlowThreshold | false | - | 0.02 | propagation threshold for mixture event
| day | false | - | 0 | day in week
| maximalIteration | false | # | 10 - 100 | number of iteration for DUE computation
| gradientStep | false | - | 0.1 | length of the descent step
| maximalNodeTime | true | hour |     | period where the distribution matrix is defined
| maximalSimulationTime | true | hour |   maximalNodeTime + 2  | length of the simulation
| cacheName | true | - |     |

### Changes
#### Update edge
```json
{
  "type": "updateEdge",
  "edgeId": 12,
  "speed": 50,
  "capacity": 1800,
  "lanes": 1
}
```
#### Add new edge
If source/target node belongs to type `none` (`ctrl_type = none`) than the movements associated with new edge are generated automaticaly.
If source/target node belongs to another type (`ctrl_type != none AND use_movement = true`) than you need to add movements manually.
The edge ID is not required parameter.
```json
{
  "type": "addEdge",
  "edgeId": 30,
  "speed": 50,
  "capacity": 3000,
  "lanes": 2,
  "fromNodeId": 457,
  "toNodeId": 458,
  "length": 0.56
}
```

#### Delete edge
Remove edge from graph including the movements associated with the edge.
```json
{
  "type": "deleteEdge",
  "edgeId": 456
}
```

#### Update or Delete movement
If the `capacity` is 0, the movement is removed. 
The editing of capacity is possible only for nodes with `ctrl_type != none AND use_movement = true`.
The removing is possible for every node type.
```json
{
  "type": "updateMovement",
  "nodeId": 404,
  "sourceEdge": 231,
  "targetEdge": 1261,
  "capacity": 200
}
```

#### Add Node (experimental)
For now only ctrlType `none` is supported.
The new `nodeId` must be > than the last nodeId.
```json
{
  "type": "addNode",
  "nodeId": 456,
  "ctrlType": "none"
}
```

#### Add Zone
Add new zone and recompute ODM.
The alpha and beta parameters are optional. Default values are 0.7 and 3.0. These parameters are used to trip distribution.
Used deterrence function:
```math
f(c) = c^{\alpha} e^{-\beta c}
```

```json
{
  "type": "addZone",
  "nodeId": 0,
  "incomingTrips": [
    {
      "tod": 0,
      "flow": 0
    }
  ],
  "outgoingTrips": [
    {
      "tod": 0,
      "flow": 0
    }
  ],
  "alpha": 0.7,
  "beta": 3.0
}
```

The updated flow must be specified for all tods.

#### Update zone flows
Update zone flows and recompute pair flow proportionally.
```json
{
  "type": "updateZone",
  "zoneId": 0,
  "incomingTrips": [
    {
      "tod": 0,
      "flow": 0
    }
  ],
  "outgoingTrips": [
    {
      "tod": 0,
      "flow": 0
    }
  ]
}
```
The updated flow must be specified for all tods.

### Result structure
The length of outFlow, inFlow and travelTime is numOfTimeIntervals.
```json
{
  "result": [
    {
      "edgeId": 12,
      "outFlow": [0.0, 12.3],
      "inFlow": [0.0, 12.3],
      "travelTime": [0.04, 0.043]
    }
  ],
  "modelInfo": {
    "startTime": "08:00:00",
    "endTime": "09:00:00"
  }
}
```
