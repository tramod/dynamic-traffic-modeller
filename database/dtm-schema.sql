-- version: 0.0.1
-- author: Frantisek Kolovsky
-- Dynamic Traffic Models - try to be compatible with GMNS

DROP SCHEMA IF EXISTS name_of_model CASCADE;

CREATE SCHEMA name_of_model;
SET search_path = name_of_model;

CREATE TABLE tod(
	tod_id int PRIMARY KEY,
	day int NOT NULL CHECK (0 <= day AND day <= 6),
	start_time time NOT NULL,
	end_time time NOT NULL CHECK (start_time < end_time)
);

CREATE TABLE node(
	node_id int PRIMARY KEY,
	ctrl_type varchar NOT NULL CHECK (ctrl_type IN ('none', 'yield', 'stop', 'signal')),
	use_movement bool NOT NULL,
	geometry public.geometry(Point,4326) NOT NULL
);

CREATE TABLE edge(
 	edge_id int PRIMARY KEY,
	"name" text,
  	from_node_id int NOT NULL REFERENCES node(node_id),
  	to_node_id int NOT NULL REFERENCES node(node_id),
  	capacity double precision NOT NULL,
	allowed_uses varchar[],
	lanes int NOT NULL,
  	free_speed double precision NOT NULL,
	critical_speed double precision NOT NULL,
  	facility_type varchar,
	"length" double precision NOT NULL,
	"order" int NOT NULL,
  	geometry public.geometry(LineString,4326) NOT NULL
);

CREATE TABLE zone
(
 	zone_id int PRIMARY KEY,
	"name" varchar,
 	node_id int NOT NULL REFERENCES node(node_id),
 	incoming_trips double precision,
	outgoing_trips double precision,
 	geometry public.geometry(Point,4326),
	"polygon" public.geometry(Polygon,4326)
);

-- time-dependent origin-destination matrix
CREATE TABLE odm(
 	origin_zone_id int REFERENCES zone(zone_id) NOT NULL,
 	destination_zone_id int REFERENCES zone(zone_id) NOT NULL,
 	tod_id int REFERENCES tod(tod_id) NOT NULL,
 	flow double precision NOT NULL,
 	PRIMARY KEY (origin_zone_id, destination_zone_id, tod_id)
);

CREATE TABLE mvmt(
	mvmt_id int PRIMARY KEY,
	node_id int NOT NULL REFERENCES node(node_id),
	ib_edge_id int NOT NULL REFERENCES edge(edge_id),
	start_ib_lane int NOT NULL,
	end_ib_lane int,
	ob_edge_id int NOT NULL REFERENCES edge(edge_id),
	start_ob_lane int NOT NULL,
	end_ob_lane int,
	"type" varchar NOT NULL CHECK ("type" IN ('left_from_major', 'right_from_minor', 'through_from_minor', 'left_from_minor', 'other')),
	allowed_uses varchar[],
	capacity double precision,
	geometry public.geometry(LineString, 4326)
);

CREATE TABLE mvmt_conflict(
	minor_mvmt_id int NOT NULL REFERENCES mvmt(mvmt_id),
	major_mvmt_id int NOT NULL REFERENCES mvmt(mvmt_id),
	coeficient double precision NOT NULL
);

CREATE TABLE signal_timing_plan(
	timing_plan_id int PRIMARY KEY,
	node_id int NOT NULL,
	cycle_length int NOT NULL
);


CREATE TABLE signal_timing_phase(
	timing_phase_id int PRIMARY KEY,
	timing_plan_id int NOT NULL REFERENCES signal_timing_plan(timing_plan_id),
	min_green int NOT NULL
);

CREATE TABLE signal_phase_mvmt(
	signal_phase_mvmt_id int PRIMARY KEY,
	mvmt_id int NOT NULL REFERENCES mvmt(mvmt_id),
	timing_phase_id int NOT NULL REFERENCES signal_timing_phase(timing_phase_id),
	signal_timing_plan int NOT NULL REFERENCES signal_timing_plan(timing_plan_id)
);

SET search_path = public;