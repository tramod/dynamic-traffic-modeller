# Dynamic traffic modeller API

## Deploy

Supervisor:
```
[program:dyn-tramod]
command= bash /home/gitlab-runner/dynamic-traffic-modeller/gradlew bootRun --args='--server.port=8103'
user=gitlab-runner
directory=/home/gitlab-runner/dynamic-traffic-modeller/
stderr_logfile=/home/gitlab-runner/dynamic-traffic-modeller/dyn-tramod.err.log
stdout_logfile=/home/gitlab-runner/dynamic-traffic-modeller/dyn-tramod.out.log
environment=HOME="/home/gitlab-runner",USER="gitlab-runner"
```

Nginx:
```
server {
    listen 80;
    listen [::]:80;

    server_name dynamic-dev.trafficmodeler.com;

	include /etc/nginx/snippets/security-headers.conf;

    gzip on;
	gzip_vary on;
	gzip_min_length 1000;
	gzip_proxied any;
	gzip_types text/css
		text/plain
		text/javascript
		application/javascript
		application/json
		application/x-javascript
		application/xml
		application/xml+rss
		application/xhtml+xml
		application/x-font-ttf
		application/x-font-opentype
		application/vnd.ms-fontobject
		image/svg+xml
		image/x-icon
		application/rss+xml
		application/atom_xml;

	location / {
         proxy_pass_request_headers on;
	     proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
         proxy_set_header X-Forwarded-Proto $scheme;
         proxy_set_header X-Forwarded-Port $server_port;
         proxy_set_header Host $host;
         proxy_pass http://localhost:8103/;
    }

}

```