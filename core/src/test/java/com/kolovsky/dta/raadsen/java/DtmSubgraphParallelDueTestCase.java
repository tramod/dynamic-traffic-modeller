/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DtmSubgraphParallelDueTestCase extends PostgresTestBase {

    /**
     * Generates a regular rectangle grid of crossroads
     * @param rowsCount
     * @param columnsCount
     * @param horizontalEdgeLength
     * @param verticalEdgeLength
     * @return
     */
    public static DtmOriginDestinationMatrix generateGrid(int rowsCount, int columnsCount, double horizontalEdgeLength, double verticalEdgeLength) {
        //Graph generation
        DtmGraph graph = new DtmGraph();
        DtmFundamentalDiagram fundamentalDiagram = new DtmFundamentalDiagram(120.0, 2000.0, 180.0, 61.0, -23.0);
        int nodeId1 = 0;
        int nodeId2 = 0;
        int nodeId3 = 0;
        int edgeId = 0;

        for (int i = 0; i < rowsCount; i++) {
            for (int j = 0; j < columnsCount; j++) {
                nodeId1 = i * columnsCount + j;
                nodeId2 = nodeId1 + 1;
                nodeId3 = (i + 1) * columnsCount + j;

                if (j < columnsCount - 1) {
                    graph.addEdge(edgeId, nodeId1, nodeId2, horizontalEdgeLength, fundamentalDiagram);
                    edgeId++;
                    graph.addEdge(edgeId, nodeId2, nodeId1, horizontalEdgeLength, fundamentalDiagram);
                    edgeId++;
                }

                if (i < rowsCount - 1) {
                    graph.addEdge(edgeId, nodeId1, nodeId3, verticalEdgeLength, fundamentalDiagram);
                    edgeId++;
                    graph.addEdge(edgeId, nodeId3, nodeId1, verticalEdgeLength, fundamentalDiagram);
                    edgeId++;
                }
            }
        }
        graph.initializeStructures();
        graph.constructDualGraph();

        //ODM generation
        List<DtmTripleElement<Integer, Integer, DtmPiecewiseConstantFunction>> pairsData = new ArrayList<>();
		/*
		double[] toRightTimes = {0.0, 1200.0, 2000.0, 3000.0};
		double[] toRightFlows = {0.0, 10.0, 12.0, 2.0};
		double[] toLeftTimes = {0.0, 1200.0, 2000.0, 2800.0, 3000.0};
		double[] toLeftFlows = {0.0, 2.0, 10.0, 8.0, 3.0};
		DtmPiecewiseConstantFunction flow = null;
		int originId = 0;
		int destinationId = 0;

		for (int i = 0; i < rowsCount; i++) {
			if (i % 2 == 0) {
			for (int j = 0; j < rowsCount; j++) {
				if (j % 2 == 0) {
				originId = i * columnsCount;
				destinationId = (j + 1) * columnsCount - 1;

				flow = new DtmPiecewiseConstantFunction();
				for (int k = 0; k < toRightTimes.length; k++) {
					flow.addPoint(toRightTimes[k] / 3600.0, toRightFlows[k]);
				}
				pairsData.add(new DtmTripleElement<>(originId, destinationId, flow));

				flow = new DtmPiecewiseConstantFunction();
				for (int k = 0; k < toLeftTimes.length; k++) {
					flow.addPoint(toLeftTimes[k] / 3600.0, toLeftFlows[k]);
				}
				pairsData.add(new DtmTripleElement<>(destinationId, originId, flow));
				}
			}
			}
		}
		*/
        //double[] times = {0.0, 1200.0, 2000.0, 3000.0};
        //double[] flows = {0.0, 5.0, 10.0, 0.0};
        double[] times = {0.0, 1200.0, 2000.0, 3000.0, 3600.0, 4200.0};
        double[] flows = {0.0, 50.0, 100.0, 50.0, 200.0, 0};
        DtmPiecewiseConstantFunction flow = null;
        int[] originsIds = {0, (rowsCount - 1) * columnsCount, columnsCount - 1, rowsCount * columnsCount - 1};
        //int[] originsIds = {0, rowsCount * columnsCount - 1};

        for (int i = 0; i < originsIds.length; i++) {
            for (int j = 0; j < originsIds.length; j++) {
                if (i != j) {
                    flow = new DtmPiecewiseConstantFunction();
                    for (int k = 0; k < times.length; k++) {
                        flow.addPoint(times[k] / 3600.0, flows[k]);
                    }
                    pairsData.add(new DtmTripleElement<>(originsIds[i], originsIds[j], flow));
                }
            }
        }

        DtmOriginDestinationMatrix originDestinationMatrix = new DtmOriginDestinationMatrix(graph, pairsData);
        originDestinationMatrix.initializeOrigins();

        //Nodes models generation
        DtmNodeModel nodeModel = null;
        double[] priorities = null;
        DtmNode node = null;

        for (int i = 0; i < rowsCount; i++) {
            for (int j = 0; j < columnsCount; j++) {
                node = graph.getNodeById(i * columnsCount + j);
                if (node.isOrigin()) {
                    priorities = new double[node.getIncomingEdges().size() + 1];
                    priorities[priorities.length - 1] = 10000.0;
                }
                else {
                    priorities = new double[node.getIncomingEdges().size()];
                }

                for (int k = 0; k < node.getIncomingEdges().size(); k++) {
                    //System.out.println("Node " + node.getId() + ", edge " + node.getIncomingEdges().get(k).getId());
                    priorities[k] = node.getIncomingEdges().get(k).getFundamentalDiagram().getFlowCapacity();
                }

                nodeModel = new DtmTampereNodeModel(node, new HashMap<>(), priorities);
                node.setNodeModel(nodeModel);
            }
        }

        return originDestinationMatrix;
    }

    @Test
    public void generatedGrid() {
        int intervals = 32;
        int rowsCount = 3;
        int columnsCount = 8;

        DtmOriginDestinationMatrix odm = generateGrid(rowsCount, columnsCount, 2.0, 2.0);
        DtmGraph graph = odm.getGraph();
        DtmModelLoader.allOrNothingAssignment(odm, 3.0, true, intervals);

        int nodeId1 = 0;
        //int nodeId2 = 0;
        //int nodeId3 = 0;
        int edgeId = 0;
        int subgraphId = 0;
        int neighboringSubgraphId = 0;
        boolean onBorder = false;
        DtmEdge edge = null;

        for (int i = 0; i < rowsCount; i++) {
            for (int j = 0; j < columnsCount; j++) {
                nodeId1 = i * columnsCount + j;
                //nodeId2 = nodeId1 + 1;
                //nodeId3 = (i + 1) * columnsCount + j;

                if (j < columnsCount / 2 - 1) {
                    subgraphId = 0;
                    neighboringSubgraphId = 0;
                    onBorder = false;
                }
                else if (j == columnsCount / 2 - 1) {
                    subgraphId = 0;
                    neighboringSubgraphId = 1;
                    onBorder = true;
                }
                else {
                    subgraphId = 1;
                    neighboringSubgraphId = 1;
                    onBorder = false;
                }

                graph.getNodeById(nodeId1).setSubgraphId(subgraphId);

                if (j < columnsCount - 1) {
                    edge = graph.getEdgeById(edgeId);
                    edge.setSubgraphId(subgraphId);
                    edge.setNeighboringSubgraphId(neighboringSubgraphId);
                    edge.setOnBorder(onBorder);
                    edgeId++;
                    edge = graph.getEdgeById(edgeId);
                    edge.setSubgraphId(neighboringSubgraphId);
                    edge.setNeighboringSubgraphId(subgraphId);
                    edge.setOnBorder(onBorder);
                    edgeId++;
                }

                if (i < rowsCount - 1) {
                    edge = graph.getEdgeById(edgeId);
                    edge.setSubgraphId(subgraphId);
                    edge.setNeighboringSubgraphId(subgraphId);
                    edge.setOnBorder(false);
                    edgeId++;
                    edge = graph.getEdgeById(edgeId);
                    edge.setSubgraphId(subgraphId);
                    edge.setNeighboringSubgraphId(subgraphId);
                    edge.setOnBorder(false);
                    edgeId++;
                }
            }
        }

        graph.determineLookAhead();
        System.out.println("Look ahead " + graph.getLookAhead());

        for (DtmEdge e: graph.getEdges()) {
            System.out.println("Edge " + e.getId() + ", subgraph " + e.getSubgraphId() + ((e.isOnBorder()) ? ", neighboring subgraph " + e.getNeighboringSubgraphId() : ""));
        }
        for (DtmNode n: graph.getNodes()) {
            System.out.println("Node " + n.getId() + ", subgraph " + n.getSubgraphId());
        }

        DtmSubgraphParallelEventGeneralLinkTransmissionModel dnl = new DtmSubgraphParallelEventGeneralLinkTransmissionModel(graph, odm.getOriginZones(), true, 2);
        dnl.setFanStep(10);
        dnl.setFlowThreshold(1.0);
        dnl.setMixtureFlowThreshold(0.01);

        //DNL
//		dnl.run(4.0, EventGeneralLinkTransmissionModel.DEFAULT_RELEASE_EVENT_EPSILON);

        //Dynamic User Equilibrium (DUE)
        DtmQuasiGradientProjection ue = new DtmQuasiGradientProjection(1.0, 0.5, 1.0);
        DtmSubgraphParallelDiscreteDestinationDynamicUserEquilibrium due = new DtmSubgraphParallelDiscreteDestinationDynamicUserEquilibrium(dnl, 4.0, odm, ue, 0.0, 3.0, intervals);
        due.run(20, 0.0);

        System.out.println(dnl.statistics.getBackwardEdgeEventsCount());
        System.out.println(dnl.statistics.getForwardEdgeEventsCount());
        System.out.println(dnl.statistics.getMixtureEdgeEventsCount());
    }

    @Disabled @Test
    public void nguyen() throws Exception {
        String modelName = "dtm_nguyen";
        DtmModelLoader loader = new DtmModelLoader(dbUrl);

        int intervals = 32;

        DtmOriginDestinationMatrix odm = loader.readModel(modelName, 0);
        DtmGraph graph = odm.getGraph();

        graph.getEdgeById(1).setSubgraphId(0);
        graph.getEdgeById(2).setSubgraphId(0);
        graph.getEdgeById(3).setSubgraphId(0);
        graph.getEdgeById(4).setSubgraphId(0);
        graph.getEdgeById(5).setSubgraphId(0);
        graph.getEdgeById(6).setSubgraphId(0);
        graph.getEdgeById(17).setSubgraphId(0);
        graph.getEdgeById(18).setSubgraphId(0);
        graph.getEdgeById(7).setSubgraphId(0);
        graph.getEdgeById(8).setSubgraphId(0);
        graph.getEdgeById(12).setSubgraphId(0);
        graph.getEdgeById(13).setSubgraphId(0);
        graph.getEdgeById(9).setSubgraphId(1);
        graph.getEdgeById(10).setSubgraphId(1);
        graph.getEdgeById(14).setSubgraphId(1);
        graph.getEdgeById(16).setSubgraphId(1);
        graph.getEdgeById(19).setSubgraphId(1);
        graph.getEdgeById(15).setSubgraphId(1);
        graph.getEdgeById(11).setSubgraphId(1);

        graph.getEdgeById(18).setOnBorder(true);
        graph.getEdgeById(18).setNeighboringSubgraphId(1);
        graph.getEdgeById(7).setOnBorder(true);
        graph.getEdgeById(7).setNeighboringSubgraphId(1);
        graph.getEdgeById(8).setOnBorder(true);
        graph.getEdgeById(8).setNeighboringSubgraphId(1);
        graph.getEdgeById(12).setOnBorder(true);
        graph.getEdgeById(12).setNeighboringSubgraphId(1);
        graph.getEdgeById(13).setOnBorder(true);
        graph.getEdgeById(13).setNeighboringSubgraphId(1);

        graph.getNodeById(1).setSubgraphId(0);
        graph.getNodeById(4).setSubgraphId(0);
        graph.getNodeById(12).setSubgraphId(0);
        graph.getNodeById(6).setSubgraphId(0);
        graph.getNodeById(5).setSubgraphId(0);
        graph.getNodeById(9).setSubgraphId(0);
        graph.getNodeById(7).setSubgraphId(1);
        graph.getNodeById(8).setSubgraphId(1);
        graph.getNodeById(10).setSubgraphId(1);
        graph.getNodeById(11).setSubgraphId(1);
        graph.getNodeById(2).setSubgraphId(1);
        graph.getNodeById(13).setSubgraphId(1);
        graph.getNodeById(3).setSubgraphId(1);

        graph.determineLookAhead();
        System.out.println("Look ahead " + graph.getLookAhead());

        for (DtmEdge edge: graph.getEdges()) {
            System.out.println("Edge " + edge.getId() + ", subgraph " + edge.getSubgraphId() + ((edge.isOnBorder()) ? ", neighboring subgraph " + edge.getNeighboringSubgraphId() : ""));
        }
        for (DtmNode node: graph.getNodes()) {
            System.out.println("Node " + node.getId() + ", subgraph " + node.getSubgraphId());
        }

        DtmModelLoader.allOrNothingAssignment(odm, 3.0, true, intervals);

        DtmSubgraphParallelEventGeneralLinkTransmissionModel dnl = new DtmSubgraphParallelEventGeneralLinkTransmissionModel(graph, odm.getOriginZones(), true, 2);
        dnl.setFanStep(10);
        dnl.setFlowThreshold(0.0);
        dnl.setMixtureFlowThreshold(0.000000);

        // Dynamic User Equilibrium (DUE)
        DtmQuasiGradientProjection ue = new DtmQuasiGradientProjection(1.0, 0.5, 1.0);
        DtmSubgraphParallelDiscreteDestinationDynamicUserEquilibrium due = new DtmSubgraphParallelDiscreteDestinationDynamicUserEquilibrium(dnl, 4.0, odm, ue, 0.0, 3.0, intervals);

        due.run(100, 0.0);
        System.out.println(dnl.statistics.getBackwardEdgeEventsCount());
        System.out.println(dnl.statistics.getForwardEdgeEventsCount());
        System.out.println(dnl.statistics.getMixtureEdgeEventsCount());
    }

    @Disabled
    @Test
    public void nguyenDnl() throws Exception {
        String modelName = "dtm_nguyen";
        DtmModelLoader loader = new DtmModelLoader(dbUrl);

        int intervals = 32;

        DtmOriginDestinationMatrix odm = loader.readModel(modelName, 0);
        DtmGraph graph = odm.getGraph();

        graph.getEdgeById(1).setSubgraphId(0);
        graph.getEdgeById(2).setSubgraphId(0);
        graph.getEdgeById(3).setSubgraphId(0);
        graph.getEdgeById(4).setSubgraphId(0);
        graph.getEdgeById(5).setSubgraphId(0);
        graph.getEdgeById(6).setSubgraphId(0);
        graph.getEdgeById(17).setSubgraphId(0);
        graph.getEdgeById(18).setSubgraphId(0);
        graph.getEdgeById(7).setSubgraphId(0);
        graph.getEdgeById(8).setSubgraphId(0);
        graph.getEdgeById(12).setSubgraphId(0);
        graph.getEdgeById(13).setSubgraphId(0);
        graph.getEdgeById(9).setSubgraphId(1);
        graph.getEdgeById(10).setSubgraphId(1);
        graph.getEdgeById(14).setSubgraphId(1);
        graph.getEdgeById(16).setSubgraphId(1);
        graph.getEdgeById(19).setSubgraphId(1);
        graph.getEdgeById(15).setSubgraphId(1);
        graph.getEdgeById(11).setSubgraphId(1);

        graph.getEdgeById(18).setOnBorder(true);
        graph.getEdgeById(18).setNeighboringSubgraphId(1);
        graph.getEdgeById(7).setOnBorder(true);
        graph.getEdgeById(7).setNeighboringSubgraphId(1);
        graph.getEdgeById(8).setOnBorder(true);
        graph.getEdgeById(8).setNeighboringSubgraphId(1);
        graph.getEdgeById(12).setOnBorder(true);
        graph.getEdgeById(12).setNeighboringSubgraphId(1);
        graph.getEdgeById(13).setOnBorder(true);
        graph.getEdgeById(13).setNeighboringSubgraphId(1);

        graph.getNodeById(1).setSubgraphId(0);
        graph.getNodeById(4).setSubgraphId(0);
        graph.getNodeById(12).setSubgraphId(0);
        graph.getNodeById(6).setSubgraphId(0);
        graph.getNodeById(5).setSubgraphId(0);
        graph.getNodeById(9).setSubgraphId(0);
        graph.getNodeById(7).setSubgraphId(1);
        graph.getNodeById(8).setSubgraphId(1);
        graph.getNodeById(10).setSubgraphId(1);
        graph.getNodeById(11).setSubgraphId(1);
        graph.getNodeById(2).setSubgraphId(1);
        graph.getNodeById(13).setSubgraphId(1);
        graph.getNodeById(3).setSubgraphId(1);

        graph.determineLookAhead();
        System.out.println("Look ahead " + graph.getLookAhead());

        for (DtmEdge edge: graph.getEdges()) {
            System.out.println("Edge " + edge.getId() + ", subgraph " + edge.getSubgraphId() + ((edge.isOnBorder()) ? ", neighboring subgraph " + edge.getNeighboringSubgraphId() : ""));
        }
        for (DtmNode node: graph.getNodes()) {
            System.out.println("Node " + node.getId() + ", subgraph " + node.getSubgraphId());
        }

        DtmModelLoader.allOrNothingAssignment(odm, 3.0, true, intervals);

        DtmSubgraphParallelEventGeneralLinkTransmissionModel dnl = new DtmSubgraphParallelEventGeneralLinkTransmissionModel(graph, odm.getOriginZones(), true, 2);
        dnl.setFanStep(10);
        dnl.setFlowThreshold(0.0);
        dnl.setMixtureFlowThreshold(0.000000);

        dnl.run(4.0, DtmEventGeneralLinkTransmissionModel.DEFAULT_RELEASE_EVENT_EPSILON);

        System.out.println(dnl.statistics.getBackwardEdgeEventsCount());
        System.out.println(dnl.statistics.getForwardEdgeEventsCount());
        System.out.println(dnl.statistics.getMixtureEdgeEventsCount());
    }
}
