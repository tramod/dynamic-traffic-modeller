/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

public class DtmPilsenTest extends PostgresTestBase {
    @Disabled
    @Test
    public void dnl() throws Exception {
        String modelName = "dtm_pilsen";
        DtmModelLoader loader = new DtmModelLoader(dbUrl);

        DtmOriginDestinationMatrix odm = loader.readModel(modelName, 0);

        DtmGraph graph = odm.getGraph();
        List<DtmNode> origins = odm.getOriginZones().stream().map((z) -> z.node).collect(Collectors.toList());
        List<DtmNode> destinations = odm.getDestinationZones().stream().map((z) -> z.node).collect(Collectors.toList());
        if (graph.isCoherent(origins, destinations)){
            System.out.println("Graph is coherent");
        }
        else{
            System.out.println("Graph is not coherent");
        }

        int intervals = 12;
        DtmModelLoader.allOrNothingAssignment(odm, 3.0, true, intervals);

        DtmEventGeneralLinkTransmissionModel dnl = new DtmEventGeneralLinkTransmissionModel(graph, odm.getOriginZones(), true);
        dnl.setFanStep(100000);
        dnl.setFlowThreshold(5.0);
        dnl.setMixtureFlowThreshold(0.02);

        dnl.run(3.0, 1E-10);

        loader.saveResult(modelName, "aon_fix_aa", true, graph.edges, intervals, 0.0, 3.0);
    }

    @Disabled
    @Test
    public void due() throws Exception {
        String modelName = "dtm_pilsen";
        DtmModelLoader loader = new DtmModelLoader(dbUrl);

        DtmOriginDestinationMatrix odm = loader.readModel(modelName, 0);
        DtmGraph graph = odm.getGraph();
        List<DtmNode> origins = odm.getOriginZones().stream().map((z) -> z.node).collect(Collectors.toList());
        List<DtmNode> destinations = odm.getDestinationZones().stream().map((z) -> z.node).collect(Collectors.toList());
        if (graph.isCoherent(origins, destinations)){
            System.out.println("Graph is coherent");
        }
        else{
            System.out.println("Graph is not coherent");
        }

        int intervals = 12;
        DtmModelLoader.allOrNothingAssignment(odm, 3.0, true, intervals);

        DtmEventGeneralLinkTransmissionModel dnl = new DtmEventGeneralLinkTransmissionModel(graph, odm.getOriginZones(), true);
        dnl.setFanStep(100000);
        dnl.setFlowThreshold(5.0);
        dnl.setMixtureFlowThreshold(0.02);

        //dnl.run(4.0, 1E-10);

        // Dynamic User Equilibrium (DUE)
        DtmReducedGradientProjection ue = new DtmReducedGradientProjection(0.04);
        DtmDiscreteDestinationDynamicUserEquilibrium due = new DtmDiscreteDestinationDynamicUserEquilibrium(dnl, 4.0,
                odm, ue, 0.0, 3.0, intervals);
        due.setNumberSearchPerInterval(1);

        due.run(100, 0.0);

        System.out.println(dnl.statistics.getBackwardEdgeEventsCount());
        System.out.println(dnl.statistics.getForwardEdgeEventsCount());
        System.out.println(dnl.statistics.getMixtureEdgeEventsCount());

        loader.saveResult(modelName, "aon_fix", true, due.bestResultEdges, intervals, 0.0, 3.0);
    }
}
