/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DtmContinuousTest {

    @Test
    public void flowShift(){
        DtmContinuousDynamicUserEquilibrium ue = new DtmContinuousDynamicUserEquilibrium(null,
                0, null, new DtmMinMaxGradientProjection(0.1), 0,
                0, 0);

        // ttfs
        List<Double> x1 = List.of(0.0, 2.0, 6.0, 8.0);
        List<Double> y1 = List.of(6.0, 3.0, 0.0, 0.0);
        DtmPiecewiseConstantFunction f1 = new DtmPiecewiseConstantFunction(x1, y1);

        List<Double> x2 = List.of(0.0, 4.0, 8.0);
        List<Double> y2 = List.of(4.0, 2.0, 2.0);
        DtmPiecewiseConstantFunction f2 = new DtmPiecewiseConstantFunction(x2, y2);

        // props
        List<Double> x3 = List.of(0.0, 8.0);
        List<Double> y3 = List.of(1.0, 1.0);
        DtmPiecewiseConstantFunction f3 = new DtmPiecewiseConstantFunction(x3, y3);

        List<Double> x4 = List.of(0.0, 8.0);
        List<Double> y4 = List.of(0.0, 0.0);
        DtmPiecewiseConstantFunction f4 = new DtmPiecewiseConstantFunction(x4, y4);


        DtmPiecewiseConstantFunction[] result = ue.shiftFlow(new DtmPiecewiseConstantFunction[]{f3, f4}, new DtmPiecewiseConstantFunction[]{f1, f2});
        for (DtmPiecewiseConstantFunction f: result){
            System.out.println(f);
        }

    }

    @Test
    public void boxSimplification(){
        DtmContinuousDynamicUserEquilibrium ue = new DtmContinuousDynamicUserEquilibrium(null,
                0, null, new DtmMinMaxGradientProjection(0.1), 0,
                0, 0);
        int size = 30;
        int numFunc = 5;
        List<Double> x = new ArrayList<>();
        List<List<Double>> ys = new ArrayList<>();
        for (int i = 0; i < numFunc - 1; i++){
            List<Double> f = new ArrayList<>();
            for (int t = 0; t < size-1; t++){
                double p = Math.random()/numFunc;
                f.add(p);
            }
            f.add(f.get(size-2));
            ys.add(f);
        }

        List<Double> last = new ArrayList<>();
        for (int t = 0; t < size; t++){
            double sum = 0.0;
            for (int i = 0; i < numFunc - 1; i++){
                sum += ys.get(i).get(t);
            }
            x.add((double) t);
            last.add(1.0 - sum);
        }
        ys.add(last);

        double[] sums = new double[size];
        for (int i = 0; i < numFunc; i++){
            for (int t = 0; t < size; t++){
                sums[t] += ys.get(i).get(t);
            }
        }
        System.out.println(Arrays.toString(sums));

        DtmPiecewiseConstantFunction[] r = DtmContinuousDynamicUserEquilibrium.boxSimplification(x, ys, 0.01);
        for (int t = 0; t < size; t++){
            double sum = 0.0;
            for (int i = 0; i < numFunc; i++){
                double v = r[i].valueAt(t);
                if (v < 0.0){
                    throw new RuntimeException("Value is negative");
                }
                sum += v;
            }
            if (Math.abs(sum - 1) > 1E-15){
                throw new RuntimeException("Sum is not 1.0 but "+sum);
            }
            System.out.print(sum + " ");

        }
        System.out.println();

        for (int i = 0; i < numFunc; i++){
            System.out.println(r[i]);
        }
    }
}
