/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DtmContinuousDueTest extends PostgresTestBase {
    @Test
    public void twin(){
        DtmGraph graph = new DtmGraph();
        DtmFundamentalDiagram fundamentalDiagram = new DtmFundamentalDiagram(120.0, 2000.0, 180.0, 61.0, -23.0);
        graph.addEdge(0, 0, 1, 1.0, fundamentalDiagram);
        graph.addEdge(1, 0, 1, 1.0, fundamentalDiagram);

        DtmPiecewiseConstantFunction inflow = new DtmPiecewiseConstantFunction();
        double[] times = {0.0, 50.0, 1000.0};
        double[] flows = {0.0, 1800, 1};
        for (int i = 0; i < times.length; i++) {
            inflow.addPoint(times[i] / 3600, flows[i]);
        }
        List<DtmTripleElement<Integer, Integer, DtmPiecewiseConstantFunction>> pairsData = new ArrayList<>();
        pairsData.add(new DtmTripleElement<>(0, 1, inflow));
        DtmOriginDestinationMatrix originDestinationMatrix = new DtmOriginDestinationMatrix(graph, pairsData);
        originDestinationMatrix.initializeOrigins();
        DtmNode node0 = graph.getNodeById(0);
        DtmNode node1 = graph.getNodeById(1);
        graph.initializeStructures();
        graph.constructDualGraph();

        List<Double> x1 = List.of(0.0, 0.3);
        List<Double> y1 = List.of(0.3, 0.3);
        List<Double> x2 = List.of(0.0, 0.3);
        List<Double> y2 = List.of(0.7, 0.7);
        DtmDistributionMatrix distributionMatrix = new DtmDistributionMatrix(new DtmPiecewiseConstantFunction[][] {{
            new DtmPiecewiseConstantFunction(x1, y1), new DtmPiecewiseConstantFunction(x2, y2)
        }});

        HashMap<DtmIdentifiable, DtmDistributionMatrix> destinationNodeDistribution = new HashMap<>();
        destinationNodeDistribution.put(originDestinationMatrix.getZoneByNode(node1), distributionMatrix);
        node0.setNodeModel(new DtmNoFifoNodeModel(node0, destinationNodeDistribution, new double[] {1.0}));
        node1.setNodeModel(new DtmNoFifoNodeModel(node1, null, null));

        DtmEventGeneralLinkTransmissionModel dynamicNetworkLoading = new DtmEventGeneralLinkTransmissionModel(graph,
                originDestinationMatrix.getOriginZones(), true);
        dynamicNetworkLoading.setFanStep(10);

        DtmGradientProjection projection = new DtmReducedGradientProjection(0.3);

        DtmContinuousDynamicUserEquilibrium due = new DtmContinuousDynamicUserEquilibrium(dynamicNetworkLoading,
                0.30, originDestinationMatrix, projection, 0.0, 0.3,
                100);
        due.setDiscretizationError(1E-6);
        due.setProportionsError(0.002);
        due.run(30, 0.0);

        DtmFunction[][] m = ((DtmDestinationNodeModel) graph.getNodeById(0).nodeModel).distributions.get(originDestinationMatrix.destinationZones.get(0)).getMatrix();
        DtmPiecewiseConstantFunction f1 = (DtmPiecewiseConstantFunction) m[0][0];
        DtmPiecewiseConstantFunction f2 = (DtmPiecewiseConstantFunction) m[0][1];
        System.out.println("x = " + f1.xValues);
        System.out.println("y = " + f1.yValues);
        //System.out.println("x = " + graph.getEdgeById(1).arrivalFunction.toTravelTimeFunction().xValues);
        //System.out.println("y = " + graph.getEdgeById(1).arrivalFunction.toTravelTimeFunction().yValues);
    }

    @Disabled @Test
    public void nguyen() throws Exception {
        String modelName = "dtm_nguyen";
        DtmModelLoader loader = new DtmModelLoader(dbUrl);

        int intervals = 100;
        double congestion = 1.0;

        DtmOriginDestinationMatrix odm = loader.readModel(modelName, 0);
        for (DtmOriginDestinationPair pair: odm.pairs){
            DtmPiecewiseConstantFunction flow = pair.getFlow();
            for (int i = 0; i < flow.getLength(); i++){
                flow.yValues.set(i, flow.yValues.get(i)*congestion);
            }
        }
        DtmGraph graph = odm.getGraph();

        DtmModelLoader.allOrNothingAssignment(odm, 3.0, false, intervals);

        DtmEventGeneralLinkTransmissionModel dnl = new DtmEventGeneralLinkTransmissionModel(graph, odm.getOriginZones(), true);
        dnl.setFanStep(10);
        dnl.setFlowThreshold(1.1);
        dnl.setMixtureFlowThreshold(0.0001);

        // Dynamic User Equilibrium (DUE)
        //DtmQuasiGradientProjection ue = new DtmQuasiGradientProjection(1.0, 0.15, 1.0);
        DtmGradientProjection ue = new DtmReducedGradientProjection(0.05);
        DtmContinuousDynamicUserEquilibrium due = new DtmContinuousDynamicUserEquilibrium(dnl, 4.0,
                odm, ue, 0.0, 3.0, intervals);
        due.setProportionsError(0.01);
        due.setDiscretizationError(10E-4);
        due.setPositionInterval(0.0);

        due.run(100, 0.0);
        System.out.println(dnl.statistics.getBackwardEdgeEventsCount());
        System.out.println(dnl.statistics.getForwardEdgeEventsCount());
        System.out.println(dnl.statistics.getMixtureEdgeEventsCount());
    }

    @Disabled
    @Test
    public void anaheim() throws Exception {
        String modelName = "dtm_anaheim";
        DtmModelLoader loader = new DtmModelLoader(dbUrl);

        int intervals = 16;

        DtmOriginDestinationMatrix odm = loader.readModel(modelName, 0);
        DtmGraph graph = odm.getGraph();
        DtmModelLoader.allOrNothingAssignment(odm, 3.0, false, intervals);

        DtmEventGeneralLinkTransmissionModel dnl = new DtmEventGeneralLinkTransmissionModel(graph, odm.getOriginZones(), true);
        dnl.setFanStep(100000);
        dnl.setFlowThreshold(3.0);
        dnl.setMixtureFlowThreshold(0.01);

        // Dynamic User Equilibrium (DUE)
        DtmGradientProjection ue = new DtmReducedGradientProjection(0.08);
        DtmContinuousDynamicUserEquilibrium due = new DtmContinuousDynamicUserEquilibrium(dnl, 10.0,
                odm, ue, 0.0, 3.0, intervals);
        due.setDiscretizationError(1E-2);
        due.setProportionsError(0.01);

        due.run(5, 0.0);

        System.out.println(dnl.statistics.getBackwardEdgeEventsCount());
        System.out.println(dnl.statistics.getForwardEdgeEventsCount());
        System.out.println(dnl.statistics.getMixtureEdgeEventsCount());
    }
}
