/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 *
 * @author Tomas Potuzak
 */
public class DtmDnlTestCase {

    @Test
    public void oneEdge() {
        DtmGraph graph = new DtmGraph();
        DtmFundamentalDiagram fundamentalDiagram = new DtmFundamentalDiagram(120.0, 2000.0, 180.0, 61.0, -23.0);
        graph.addEdge(0, 0, 1, 1.0, fundamentalDiagram);

        DtmPiecewiseConstantFunction inflow = new DtmPiecewiseConstantFunction();
        double[] times = {0.0, 50.0, 75.0, 120.0, 250.0, 320.0, 360.0, 430.0, 600.0};
        double[] flows = {0.0, 1800, 1809, 1800, 500, 505, 530, 500, 1};
        for (int i = 0; i < times.length; i++) {
            inflow.addPoint(times[i] / 3600, flows[i]);
        }
        List<DtmTripleElement<Integer, Integer, DtmPiecewiseConstantFunction>> pairsData = new ArrayList<>();
        pairsData.add(new DtmTripleElement<>(0, 1, inflow));
        DtmOriginDestinationMatrix originDestinationMatrix = new DtmOriginDestinationMatrix(graph, pairsData);
        originDestinationMatrix.initializeOrigins();
        graph.initializeStructures();
        graph.constructDualGraph();
        DtmNode node0 = graph.getNodeById(0);
        DtmNode node1 = graph.getNodeById(1);

        List<Double> xValues = new ArrayList<>();
        List<Double> yValues = new ArrayList<>();
        xValues.add(0.0);
        xValues.add(10.0);
        yValues.add(1.0);
        yValues.add(1.0);
        DtmDistributionMatrix distributionMatrix = new DtmDistributionMatrix(new DtmPiecewiseConstantFunction[][] {{new DtmPiecewiseConstantFunction(xValues, yValues)}});
        HashMap<DtmIdentifiable, DtmDistributionMatrix> destinationNodeDistribution = new HashMap<>();
        destinationNodeDistribution.put(originDestinationMatrix.getZoneByNode(node1), distributionMatrix);
        node0.setNodeModel(new DtmNoFifoNodeModel(node0, destinationNodeDistribution, new double[] {1.0}));
        node1.setNodeModel(new DtmNoFifoNodeModel(node1, null, null));

        DtmEventGeneralLinkTransmissionModel dynamicNetworkLoading = new DtmEventGeneralLinkTransmissionModel(graph, originDestinationMatrix.getOriginZones(), true);
        dynamicNetworkLoading.setFanStep(2);

        DtmLogger.setLoggingLevel(DtmLoggingLevels.INFO);
        DtmLogger.addWatchedObject(node0);
        DtmLogger.addWatchedObject(node1);
        DtmLogger.addWatchedObject(graph.getEdgeById(0));

        dynamicNetworkLoading.run(0.30, DtmEventGeneralLinkTransmissionModel.DEFAULT_RELEASE_EVENT_EPSILON);

    }

    @Test
    public void spillback() {
        DtmGraph graph = new DtmGraph();
        DtmFundamentalDiagram fundamentalDiagram1 = new DtmFundamentalDiagram(120.0, 2000.0, 180.0, 61.0, -23.0);
        DtmFundamentalDiagram fundamentalDiagram2 = fundamentalDiagram1;
        DtmFundamentalDiagram fundamentalDiagram3 = new DtmFundamentalDiagram(60.0, 1000.0, 100.0, 40.0, -15.0);
        graph.addEdge(0, 0, 1, 1.0, fundamentalDiagram1);
        graph.addEdge(1, 1, 2, 1.0, fundamentalDiagram2);
        graph.addEdge(2, 2, 3, 1.0, fundamentalDiagram3);
        graph.initializeStructures();
        graph.constructDualGraph();

        for (int i = 0; i < graph.getNodes().size(); i++) {
            System.out.println("" + graph.getNodes().get(i) + ",  in: " + graph.getNodes().get(i).getIncomingEdges() + ", out: " + graph.getNodes().get(i).getOutgoingEdges());
        }

        DtmPiecewiseConstantFunction inflow = new DtmPiecewiseConstantFunction();
        double[] times = {0.0, 50.0, 1500.0, 1800.0};
        double[] flows = {0.0, 1800, 500, 1};
        for (int i = 0; i < times.length; i++) {
            inflow.addPoint(times[i] / 3600, flows[i]);
        }
        List<DtmTripleElement<Integer, Integer, DtmPiecewiseConstantFunction>> pairsData = new ArrayList<>();
        pairsData.add(new DtmTripleElement<>(0, 3, inflow));
        DtmOriginDestinationMatrix originDestinationMatrix = new DtmOriginDestinationMatrix(graph, pairsData);
        originDestinationMatrix.initializeOrigins();

        //Initialization of node models
        List<Double> xValues = null;
        List<Double> yValues = null;
        DtmDistributionMatrix distributionMatrix = null;
        HashMap<DtmIdentifiable, DtmDistributionMatrix> destinationNodeDistribution = null;
        for (DtmNode node: graph.getNodes()) {
            if ((!node.getIncomingEdges().isEmpty() && !node.getOutgoingEdges().isEmpty()) || node.isOrigin()) {
                xValues = new ArrayList<>();
                yValues = new ArrayList<>();
                xValues.add(0.0);
                xValues.add(10.0);
                yValues.add(1.0);
                yValues.add(1.0);
                distributionMatrix = new DtmDistributionMatrix(new DtmPiecewiseConstantFunction[][] {{new DtmPiecewiseConstantFunction(xValues, yValues)}});
                destinationNodeDistribution = new HashMap<>();
                destinationNodeDistribution.put(originDestinationMatrix.getZoneByNode(graph.getNodeById(3)), distributionMatrix);
                node.setNodeModel(new DtmNoFifoNodeModel(node, destinationNodeDistribution, new double[] {1.0}));
            }
            else {
                node.setNodeModel(new DtmNoFifoNodeModel(node, null, null));
            }
        }

        DtmEventGeneralLinkTransmissionModel dynamicNetworkLoading = new DtmEventGeneralLinkTransmissionModel(graph, originDestinationMatrix.getOriginZones(), true);
        dynamicNetworkLoading.setFanStep(20.0);
        dynamicNetworkLoading.run(1.0, DtmEventGeneralLinkTransmissionModel.DEFAULT_RELEASE_EVENT_EPSILON);

        assert(graph.getEdgeById(0).getOutflow().valueAt(0.4) == 1000.0);
    }

    @Test
    public void freeSpillback() {
        DtmGraph graph = new DtmGraph();
        DtmFundamentalDiagram fundamentalDiagram1 = new DtmFundamentalDiagram(120.0, 2000.0, 180.0, 61.0, -23.0);
        DtmFundamentalDiagram fundamentalDiagram2 = fundamentalDiagram1;
        DtmFundamentalDiagram fundamentalDiagram3 = new DtmFundamentalDiagram(60.0, 1000.0, 100.0, 40.0, -15.0);
        graph.addEdge(0, 0, 1, 1.0, fundamentalDiagram1);
        graph.addEdge(1, 1, 2, 1.0, fundamentalDiagram2);
        graph.addEdge(2, 2, 3, 1.0, fundamentalDiagram3);

        graph.addEdge(3, 4, 2, 1.0, fundamentalDiagram1);
        graph.initializeStructures();
        graph.constructDualGraph();

        for (int i = 0; i < graph.getNodes().size(); i++) {
            System.out.println("" + graph.getNodes().get(i) + ",  in: " + graph.getNodes().get(i).getIncomingEdges() + ", out: " + graph.getNodes().get(i).getOutgoingEdges());
        }

        DtmPiecewiseConstantFunction inflow = new DtmPiecewiseConstantFunction();
        double[] times = {0.0, 1800.0};
        double[] flows = {1800, 1};
        for (int i = 0; i < times.length; i++) {
            inflow.addPoint(times[i] / 3600, flows[i]);
        }
        List<DtmTripleElement<Integer, Integer, DtmPiecewiseConstantFunction>> pairsData = new ArrayList<>();
        pairsData.add(new DtmTripleElement<>(0, 3, inflow));

        DtmPiecewiseConstantFunction inflow2 = new DtmPiecewiseConstantFunction();
        double[] times2 = {0.0, 700.0, 1000.0, 1800};
        double[] flows2 = {500, 1, 500.0, 1};
        for (int i = 0; i < times2.length; i++) {
            inflow2.addPoint(times2[i] / 3600, flows2[i]);
        }
        pairsData.add(new DtmTripleElement<>(4, 3, inflow2));


        DtmOriginDestinationMatrix originDestinationMatrix = new DtmOriginDestinationMatrix(graph, pairsData);
        originDestinationMatrix.initializeOrigins();

        //Initialization of node models
        List<Double> xValues = null;
        List<Double> yValues = null;
        DtmDistributionMatrix distributionMatrix = null;
        HashMap<DtmIdentifiable, DtmDistributionMatrix> destinationNodeDistribution = null;
        for (DtmNode node: graph.getNodes()) {
            if ((!node.getIncomingEdges().isEmpty() && !node.getOutgoingEdges().isEmpty()) || node.isOrigin()) {
                xValues = new ArrayList<>();
                yValues = new ArrayList<>();
                xValues.add(0.0);
                xValues.add(10.0);
                yValues.add(1.0);
                yValues.add(1.0);
                if (node.getId() == 2){
                    distributionMatrix = new DtmDistributionMatrix(new DtmPiecewiseConstantFunction[][] {{new DtmPiecewiseConstantFunction(xValues, yValues)}, {new DtmPiecewiseConstantFunction(xValues, yValues)}});
                    destinationNodeDistribution = new HashMap<>();
                    destinationNodeDistribution.put(originDestinationMatrix.getZoneByNode(graph.getNodeById(3)), distributionMatrix);
                    node.setNodeModel(new DtmNoFifoNodeModel(node, destinationNodeDistribution, new double[] {1.0, 1.0}));
                }
                else {
                    distributionMatrix = new DtmDistributionMatrix(new DtmPiecewiseConstantFunction[][] {{new DtmPiecewiseConstantFunction(xValues, yValues)}});
                    destinationNodeDistribution = new HashMap<>();
                    destinationNodeDistribution.put(originDestinationMatrix.getZoneByNode(graph.getNodeById(3)), distributionMatrix);
                    node.setNodeModel(new DtmNoFifoNodeModel(node, destinationNodeDistribution, new double[] {1.0}));

                }

            }
            else {
                node.setNodeModel(new DtmNoFifoNodeModel(node, null, null));
            }
        }

        DtmLogger.setLoggingLevel(DtmLoggingLevels.INFO);
        DtmLogger.addWatchedObject(graph.getEdgeById(1));

        DtmEventGeneralLinkTransmissionModel dynamicNetworkLoading = new DtmEventGeneralLinkTransmissionModel(graph, originDestinationMatrix.getOriginZones(), true);
        dynamicNetworkLoading.setFanStep(10.0);
        dynamicNetworkLoading.run(1.0, DtmEventGeneralLinkTransmissionModel.DEFAULT_RELEASE_EVENT_EPSILON);

        DtmEdge e = graph.getEdgeById(1);
        System.out.println("ix = " + e.inflow.xValues.toString());
        System.out.println("iy = " +e.inflow.yValues.toString());
        System.out.println("ox = " +e.outflow.xValues.toString());
        System.out.println("oy = " +e.outflow.yValues.toString());

        assert(graph.getEdgeById(1).getInflow().valueAt(0.3) == 999.0);

    }

    @Test
    public void braess() {
        DtmGraph graph = new DtmGraph();
        DtmFundamentalDiagram fundamentalDiagram = new DtmFundamentalDiagram(120.0, 2000.0, 180.0, 61.0, -23.0);
        graph.addEdge(0, 0, 1, 1.0, fundamentalDiagram);
        graph.addEdge(1, 0, 2, 1.0, fundamentalDiagram);
        graph.addEdge(2, 1, 2, 1.0, fundamentalDiagram);
        graph.addEdge(3, 1, 3, 1.0, fundamentalDiagram);
        graph.addEdge(4, 2, 3, 1.0, fundamentalDiagram);
        graph.initializeStructures();
        graph.constructDualGraph();

        for (int i = 0; i < graph.getNodes().size(); i++) {
            System.out.println("" + graph.getNodes().get(i) + ",  in: " + graph.getNodes().get(i).getIncomingEdges() + ", out: " + graph.getNodes().get(i).getOutgoingEdges());
        }

        DtmPiecewiseConstantFunction inflow = new DtmPiecewiseConstantFunction();
        double[] times = {0.0, 50.0, 100.0, 200.0};
        double[] flows = {0.0, 3000, 500, 1};
        for (int i = 0; i < times.length; i++) {
            inflow.addPoint(times[i] / 3600, flows[i]);
        }
        List<DtmTripleElement<Integer, Integer, DtmPiecewiseConstantFunction>> pairsData = new ArrayList<>();
        pairsData.add(new DtmTripleElement<>(0, 3, inflow));
        DtmOriginDestinationMatrix originDestinationMatrix = new DtmOriginDestinationMatrix(graph, pairsData);
        originDestinationMatrix.initializeOrigins();

        //Initialization of node models
        List<Double> xValues = null;
        List<Double> yValues = null;
        DtmDistributionMatrix distributionMatrix = null;
        HashMap<DtmIdentifiable, DtmDistributionMatrix> destinationNodeDistribution = null;

        //Node 0
        xValues = new ArrayList<>();
        yValues = new ArrayList<>();
        xValues.add(0.0);
        xValues.add(1.0);
        yValues.add(0.5);
        yValues.add(0.5);
        DtmPiecewiseConstantFunction dist1 = new DtmPiecewiseConstantFunction(xValues, yValues);
        xValues = new ArrayList<>();
        yValues = new ArrayList<>();
        xValues.add(0.0);
        xValues.add(1.0);
        yValues.add(0.5);
        yValues.add(0.5);
        DtmPiecewiseConstantFunction dist2 = new DtmPiecewiseConstantFunction(xValues, yValues);
        distributionMatrix = new DtmDistributionMatrix(new DtmPiecewiseConstantFunction[][] {{dist1, dist2}});
        destinationNodeDistribution = new HashMap<>();
        destinationNodeDistribution.put(originDestinationMatrix.getDestinationZones().get(0), distributionMatrix);
        graph.getNodeById(0).setNodeModel(new DtmNoFifoNodeModel(graph.getNodeById(0), destinationNodeDistribution, new double[] {1.0}));

        //Node 1
        xValues = new ArrayList<>();
        yValues = new ArrayList<>();
        xValues.add(0.0);
        xValues.add(1.0);
        yValues.add(0.5);
        yValues.add(0.5);
        DtmPiecewiseConstantFunction dist11 = new DtmPiecewiseConstantFunction(xValues, yValues);
        xValues = new ArrayList<>();
        yValues = new ArrayList<>();
        xValues.add(0.0);
        xValues.add(1.0);
        yValues.add(0.5);
        yValues.add(0.5);
        DtmPiecewiseConstantFunction dist21 = new DtmPiecewiseConstantFunction(xValues, yValues);
        distributionMatrix = new DtmDistributionMatrix(new DtmPiecewiseConstantFunction[][] {{dist11, dist21}});
        destinationNodeDistribution = new HashMap<>();
        destinationNodeDistribution.put(originDestinationMatrix.getDestinationZones().get(0), distributionMatrix);
        graph.getNodeById(1).setNodeModel(new DtmNoFifoNodeModel(graph.getNodeById(1), destinationNodeDistribution, new double[] {1.0}));

        //Node 2
        xValues = new ArrayList<>();
        yValues = new ArrayList<>();
        xValues.add(0.0);
        xValues.add(1.0);
        yValues.add(1.0);
        yValues.add(1.0);
        DtmPiecewiseConstantFunction dist12 = new DtmPiecewiseConstantFunction(xValues, yValues);
        xValues = new ArrayList<>();
        yValues = new ArrayList<>();
        xValues.add(0.0);
        xValues.add(1.0);
        yValues.add(1.0);
        yValues.add(1.0);
        DtmPiecewiseConstantFunction dist22 = new DtmPiecewiseConstantFunction(xValues, yValues);
        distributionMatrix = new DtmDistributionMatrix(new DtmPiecewiseConstantFunction[][] {{dist12} , {dist22}});
        destinationNodeDistribution = new HashMap<>();
        destinationNodeDistribution.put(originDestinationMatrix.getDestinationZones().get(0), distributionMatrix);
        graph.getNodeById(2).setNodeModel(new DtmNoFifoNodeModel(graph.getNodeById(2), destinationNodeDistribution, new double[] {1000.0, 1000.0}));

        //Node 3
        graph.getNodeById(3).setNodeModel(new DtmNoFifoNodeModel(graph.getNodeById(3), null, null));

        DtmEventGeneralLinkTransmissionModel dynamicNetworkLoading = new DtmEventGeneralLinkTransmissionModel(graph, originDestinationMatrix.getOriginZones(), true);
        dynamicNetworkLoading.setFanStep(10.0);
        dynamicNetworkLoading.run(0.15, DtmEventGeneralLinkTransmissionModel.DEFAULT_RELEASE_EVENT_EPSILON);
    }

    @Test
    public void bug1() {
        DtmGraph graph = new DtmGraph();
        DtmFundamentalDiagram fundamentalDiagram = new DtmFundamentalDiagram(120.0, 2000.0, 180.0, 61.0, -23.0);
        graph.addEdge(0, 0, 2, 1.0, fundamentalDiagram);
        graph.addEdge(1, 1, 2, 1.0, fundamentalDiagram);
        graph.addEdge(2, 2, 3, 1.0, fundamentalDiagram);
        graph.initializeStructures();
        graph.constructDualGraph();

        for (int i = 0; i < graph.getNodes().size(); i++) {
            System.out.println("" + graph.getNodes().get(i) + ",  in: " + graph.getNodes().get(i).getIncomingEdges() + ", out: " + graph.getNodes().get(i).getOutgoingEdges());
        }

        DtmPiecewiseConstantFunction inflow1 = new DtmPiecewiseConstantFunction();
        DtmPiecewiseConstantFunction inflow2 = new DtmPiecewiseConstantFunction();
        double[] times = {0.0, 2.0, 2.1, 2.2};
        double[] flows = {1800.0, 0.0, 0.0, 0.0};
        for (int i = 0; i < times.length; i++) {
            inflow1.addPoint(times[i], flows[i]);
        }
        times = new double[] {0.0, 0.3, 0.6, 2.1, 2.2};
        flows = new double[] {100.0, 500.0, 0.0, 0.0, 0.0};
        for (int i = 0; i < times.length; i++) {
            inflow2.addPoint(times[i], flows[i]);
        }
        List<DtmTripleElement<Integer, Integer, DtmPiecewiseConstantFunction>> pairsData = new ArrayList<>();
        pairsData.add(new DtmTripleElement<>(0, 3, inflow1));
        pairsData.add(new DtmTripleElement<>(1, 3, inflow2));
        DtmOriginDestinationMatrix originDestinationMatrix = new DtmOriginDestinationMatrix(graph, pairsData);
        originDestinationMatrix.initializeOrigins();

        //Initialization of node models
        List<Double> xValues = null;
        List<Double> yValues = null;
        DtmDistributionMatrix distributionMatrix = null;
        HashMap<DtmIdentifiable, DtmDistributionMatrix> destinationNodeDistribution = null;

        //Node 0
        xValues = new ArrayList<>();
        yValues = new ArrayList<>();
        xValues.add(0.0);
        xValues.add(1.0);
        yValues.add(1.0);
        yValues.add(1.0); //to ensure identical number of x and y values
        DtmPiecewiseConstantFunction dist0 = new DtmPiecewiseConstantFunction(xValues, yValues);
        distributionMatrix = new DtmDistributionMatrix(new DtmPiecewiseConstantFunction[][] {{dist0}});
        destinationNodeDistribution = new HashMap<>();
        destinationNodeDistribution.put(originDestinationMatrix.getDestinationZones().get(0), distributionMatrix);
        graph.getNodeById(0).setNodeModel(new DtmNoFifoNodeModel(graph.getNodeById(0), destinationNodeDistribution, new double[] {1.0}));

        //Node 1
        xValues = new ArrayList<>();
        yValues = new ArrayList<>();
        xValues.add(0.0);
        xValues.add(1.0);
        yValues.add(1.0);
        yValues.add(1.0); //to ensure identical number of x and y values
        DtmPiecewiseConstantFunction dist1 = new DtmPiecewiseConstantFunction(xValues, yValues);
        distributionMatrix = new DtmDistributionMatrix(new DtmPiecewiseConstantFunction[][] {{dist1}});
        destinationNodeDistribution = new HashMap<>();
        destinationNodeDistribution.put(originDestinationMatrix.getDestinationZones().get(0), distributionMatrix);
        graph.getNodeById(1).setNodeModel(new DtmNoFifoNodeModel(graph.getNodeById(1), destinationNodeDistribution, new double[] {1.0}));

        //Node 2
        xValues = new ArrayList<>();
        yValues = new ArrayList<>();
        xValues.add(0.0);
        xValues.add(2.0);
        yValues.add(1.0);
        yValues.add(1.0);
        DtmPiecewiseConstantFunction dist12 = new DtmPiecewiseConstantFunction(xValues, yValues);
        xValues = new ArrayList<>();
        yValues = new ArrayList<>();
        xValues.add(0.0);
        xValues.add(1.0);
        yValues.add(1.0);
        yValues.add(1.0);
        DtmPiecewiseConstantFunction dist22 = new DtmPiecewiseConstantFunction(xValues, yValues);
        distributionMatrix = new DtmDistributionMatrix(new DtmPiecewiseConstantFunction[][] {{dist12} , {dist22}});
        destinationNodeDistribution = new HashMap<>();
        destinationNodeDistribution.put(originDestinationMatrix.getDestinationZones().get(0), distributionMatrix);
        graph.getNodeById(2).setNodeModel(new DtmNoFifoNodeModel(graph.getNodeById(2), destinationNodeDistribution, new double[] {1.0, 1.0}));

        //Node 3
        graph.getNodeById(3).setNodeModel(new DtmNoFifoNodeModel(graph.getNodeById(3), null, null));

        DtmEventGeneralLinkTransmissionModel dynamicNetworkLoading = new DtmEventGeneralLinkTransmissionModel(graph, originDestinationMatrix.getOriginZones(), true);
        dynamicNetworkLoading.setFanStep(100000.0);
        dynamicNetworkLoading.run(2.5, DtmEventGeneralLinkTransmissionModel.DEFAULT_RELEASE_EVENT_EPSILON);

        DtmEdge e = graph.getEdgeById(0);
        System.out.println("ix = " + e.inflow.xValues.toString());
        System.out.println("iy = " +e.inflow.yValues.toString());
        System.out.println("ox = " +e.outflow.xValues.toString());
        System.out.println("oy = " +e.outflow.yValues.toString());

        assert(graph.getEdgeById(0).getOutflow().valueAt(1.0) == 1800.0);
    }
}
