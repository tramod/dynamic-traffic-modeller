/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.*;

public class NodeTest {

    public static void printMatrix(double[][] m){
        for (int i = 0; i < m.length; i++){
            for(int j = 0; j < m[i].length; j++){
                System.out.print(String.format(Locale.ENGLISH,"%.4f", m[i][j]));
                System.out.print("\t");
            }
            System.out.println();
        }
    }

    public double diffMatrix(double[][] m, double[][] mo){
        double sum = 0;
        for (int i = 0; i < m.length; i++){
            for(int j = 0; j < m[i].length; j++){
                sum += Math.abs(m[i][j] - mo[i][j]);
            }
        }
        return sum;
    }

    @Test
    public void nodeAlone(){
        DtmNode node = new DtmNode(1);
        double[] priorities = {1000, 2000, 1000, 2000.0};
        double[][] demand = {
                {0.0, 50, 150, 300},
                {100, 0.0, 300, 1600},
                {100, 100, 0.0, 600},
                {100, 800, 800, 0.0}
        };
        double[] supply = {1000, 2000, 1000, 2000.0};
        DtmTampereNodeModel nodeModel = new DtmTampereNodeModel(node, new HashMap<>(), priorities);
        DtmNoFifoNodeModel noFifoNodeModel = new DtmNoFifoNodeModel(node, new HashMap<>(), priorities);

        double[][] flow = nodeModel.solveUniversal(demand, supply);
        double[][] rightResult = {
                {0.0000, 50.0000, 150.0000, 300.0000},
                {68.4834, 0.0000, 205.4502, 1095.7346},
                {100.0000, 100.0000, 0.0000, 600.0000},
                {80.5687, 644.5498,	644.5498, 0.0000}
        };
        assert diffMatrix(flow, rightResult) < 1E-3;

        double[][] flowNoFIFO = noFifoNodeModel.solveUniversal(demand, supply);
        double[][] rightResultNoFIFO = {
                {0.0000, 50.0000, 150.0000, 300.0000},
                {100.0000, 0.0000, 300.0000, 1133.3333},
                {100.0000, 100.0000, 0.0000, 566.6667},
                {100.0000, 800.0000, 550.0000, 0.0000}
        };
        assert diffMatrix(flowNoFIFO, rightResultNoFIFO) < 1E-3;

        DtmPairElement<Double, Double>[][][] mri = getMutualRestrictionIntervals(4,4, new DtmPairElement<>(0.0, 1.0));
        DtmWRICNodeModel wricNodeModel = new DtmWRICNodeModel(node, new HashMap<>(), priorities, mri, null);
        double[][] flowWRIC = wricNodeModel.solveWRI(demand, supply);
        assert diffMatrix(flowWRIC, rightResult) < 1E-3;

        DtmPairElement<Double, Double>[][][] mriNoFIFO = getMutualRestrictionIntervals(4,4, new DtmPairElement<>(0.0, 0.0));
        DtmWRICNodeModel wricNodeModel2 = new DtmWRICNodeModel(node, new HashMap<>(), priorities, mriNoFIFO, null);
        double[][] flowWRIC2 = wricNodeModel2.solveWRI(demand, supply);
    }

    @SuppressWarnings("unchecked")
    private DtmPairElement<Double, Double>[][][] getMutualRestrictionIntervals(int numIncoming, int numOutgoing,
                                                                               DtmPairElement<Double, Double> fill){
        DtmPairElement<Double, Double>[][][] mri = (DtmPairElement<Double, Double>[][][]) Array.newInstance(
                DtmPairElement.class, numIncoming, numOutgoing, numOutgoing);

        for (int i = 0; i < numIncoming; i++){
            for (int j = 0; j < numOutgoing; j++){
                for (int j2 = 0; j2 < numOutgoing; j2++){
                    if (j != j2){
                        mri[i][j][j2] = fill;
                    }
                    else {
                        mri[i][j][j2] = new DtmPairElement<>(0.0, 1.0);
                    }
                }
            }
        }

        return mri;
    }

    @Test
    public void nodeFull(){
        DtmFundamentalDiagram fd = new DtmFundamentalDiagram(120.0, 2000.0, 180.0, 61.0, -23.0);
        DtmNode otherNode = new DtmNode(2);
        DtmZone otherZone = new DtmZone(2, otherNode);
        DtmNode node = new DtmNode(1);
        DtmZone zone = new DtmZone(1, node);

        double[] finalDestinationFlow = new double[4];
        Arrays.fill(finalDestinationFlow, 0.0);
        finalDestinationFlow[0] = 500;

        node.incomingEdges = new ArrayList<>();
        node.outgoingEdges = new ArrayList<>();

        node.incomingEdges.add(new DtmEdge(0, 1.0, otherNode, node, fd));
        node.incomingEdges.add(new DtmEdge(1, 1.0, otherNode, node, fd));
        node.incomingEdges.add(new DtmEdge(2, 1.0, otherNode, node, fd));
        node.incomingEdges.add(new DtmEdge(3, 1.0, otherNode, node, fd));
        node.incomingEdges.get(0).setDemand(500 + finalDestinationFlow[0]);
        node.incomingEdges.get(1).setDemand(2000 + finalDestinationFlow[1]);
        node.incomingEdges.get(2).setDemand(800 + finalDestinationFlow[2]);
        node.incomingEdges.get(3).setDemand(1700 + finalDestinationFlow[3]);
        node.incomingEdges.get(0).setOutflowMixture(new DtmMixture<>(List.of(new DtmPairElement<>(otherZone, 0.5), new DtmPairElement<>(zone, 0.5))));
        node.incomingEdges.get(1).setOutflowMixture(new DtmMixture<>(List.of(new DtmPairElement<>(otherZone, 1.0))));
        node.incomingEdges.get(2).setOutflowMixture(new DtmMixture<>(List.of(new DtmPairElement<>(otherZone, 1.0))));
        node.incomingEdges.get(3).setOutflowMixture(new DtmMixture<>(List.of(new DtmPairElement<>(otherZone, 1.0))));

        node.outgoingEdges.add(new DtmEdge(4, 1.0, node, otherNode, fd));
        node.outgoingEdges.add(new DtmEdge(5, 1.0, node, otherNode, fd));
        node.outgoingEdges.add(new DtmEdge(6, 1.0, node, otherNode, fd));
        node.outgoingEdges.add(new DtmEdge(7, 1.0, node, otherNode, fd));
        node.outgoingEdges.get(0).setSupply(1000);
        node.outgoingEdges.get(1).setSupply(2000);
        node.outgoingEdges.get(2).setSupply(1000);
        node.outgoingEdges.get(3).setSupply(2000);

        DtmPiecewiseConstantFunction[][] distributionMatrix = {
            {
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(0.0, 0.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(0.1, 0.1)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(0.3, 0.3)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(0.6, 0.6)),
            },
            {
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(0.05, 0.05)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(0.0, 0.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(0.15, 0.15)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(0.8, 0.8)),
            },
            {
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(0.125, 0.125)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(0.125, 0.125)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(0.0, 0.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(0.75, 0.75)),
            },
            {
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(1/17.0, 1/17.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(8/17.0, 8/17.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(8/17.0, 8/17.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(0.0, 0.0)),
            }
        };

        double[] priorities = {1000, 2000, 1000, 2000.0};
        HashMap<DtmIdentifiable, DtmDistributionMatrix> distributions = new HashMap<>();
        distributions.put(otherZone, new DtmDistributionMatrix(distributionMatrix));

        // MODELS
        DtmTampereNodeModel tampereNodeModel = new DtmTampereNodeModel(node, distributions, priorities);
        DtmNoFifoNodeModel noFifoNodeModel = new DtmNoFifoNodeModel(node, distributions, priorities);

        DtmTripleElement<List<Double>, List<Double>, List<DtmMixture<DtmIdentifiable>>> resultTampere = tampereNodeModel.solve(0.5);
        DtmTripleElement<List<Double>, List<Double>, List<DtmMixture<DtmIdentifiable>>> resultNoFIFO = noFifoNodeModel.solve(0.5);

        List<Double> rightResult = List.of(500.0, 1369.668246445498, 800.0, 1369.6682464454977);
        for (int i = 0; i < 4; i++){
            assert Math.abs(resultTampere.third.get(i).getPortion(otherZone) - 1.0) < 1E-10;
            assert Math.abs(resultTampere.first.get(i) - finalDestinationFlow[i]  - rightResult.get(i)) < 1E-10;
        }

        List<Double> rightResultNoFIFO = List.of(500.0, 1533.3333333333333, 766.6666666666666, 1450.0);
        for (int i = 0; i < 4; i++){
            assert resultNoFIFO.third.get(i).getPortion(otherZone) == 1.0;
            assert Math.abs(resultNoFIFO.first.get(i) - finalDestinationFlow[i] - rightResultNoFIFO.get(i)) < 1E-10;
        }
    }

    @Test
    public void capacity() throws Exception {
        @SuppressWarnings("unchecked")
        DtmPairElement<Double, Double>[][] cti = (DtmPairElement<Double, Double>[][]) Array.newInstance(
                DtmPairElement.class, 4, 4);
        for (int i = 0; i < 4; i++){
            Arrays.fill(cti[i], new DtmPairElement<>(1.0, 0.0));
        }
        cti[0][3] = new DtmPairElement<>(3.4, 0.021);
        cti[2][1] = new DtmPairElement<>(3.4, 0.021);
        cti[1][2] = new DtmPairElement<>(2.8, 0.038);
        cti[3][0] = new DtmPairElement<>(2.8, 0.038);
        cti[1][3] = new DtmPairElement<>(4.4, 0.036);
        cti[3][1] = new DtmPairElement<>(4.4, 0.036);
        cti[1][0] = new DtmPairElement<>(5.2, 0.022);
        cti[3][2] = new DtmPairElement<>(5.2, 0.022);

        double[][] nti = new double[4][4];
        for (int i = 0; i < 4; i++){
            Arrays.fill(nti[i], 1.0);
        }
        nti[0][3] = 2.6;
        nti[2][1] = 2.6;
        nti[1][2] = 3.1;
        nti[3][0] = 3.1;
        nti[1][3] = 3.3;
        nti[3][1] = 3.3;
        nti[1][0] = 3.5;
        nti[3][2] = 3.5;

        double[][] demand = {
                {0.0, 50, 150, 300},
                {100, 0.0, 300, 1600},
                {100, 100, 0.0, 600},
                {100, 800, 800, 0.0}
        };

        DtmBeta[] beta = {
                new DtmBeta(1, 0, 3, 1, 1.0),
                new DtmBeta(1, 0, 3, 0, 1.0),
                new DtmBeta(3, 2, 1, 3, 1.0),
                new DtmBeta(3, 2, 1, 2, 1.0),
                new DtmBeta(2, 3, 1, 3, 1.0),
                new DtmBeta(2, 3, 1, 2, 0.5),
                new DtmBeta(0, 1, 3, 1, 1.0),
                new DtmBeta(0, 1, 3, 0, 0.5),
                new DtmBeta(2, 0, 1, 3, 1.0),
                new DtmBeta(2, 0, 1, 2, 0.5),
                new DtmBeta(2, 0, 3, 1, 1.0),
                new DtmBeta(2, 0, 3, 0, 1.0),
                new DtmBeta(2, 0, 1, 0, 1.0),
                new DtmBeta(2, 0, 3, 2, 1.0),
                new DtmBeta(0, 2, 3, 1, 1.0),
                new DtmBeta(0, 2, 3, 0, 0.5),
                new DtmBeta(0, 2, 1, 3, 1.0),
                new DtmBeta(0, 2, 1, 2, 1.0),
                new DtmBeta(0, 2, 1, 0, 1.0),
                new DtmBeta(0, 2, 3, 2, 1.0),
                new DtmBeta(2, 1, 1, 3, 1.0),
                new DtmBeta(2, 1, 1, 2, 0.5),
                new DtmBeta(2, 1, 3, 1, 1.0),
                new DtmBeta(2, 1, 3, 0, 0.5),
                new DtmBeta(2, 1, 1, 0, 1.0),
                new DtmBeta(2, 1, 3, 2, 1.0),
                new DtmBeta(2, 1, 0, 1, 1.0),
                new DtmBeta(2, 1, 0, 2, 1.0),
                new DtmBeta(0, 3, 3, 1, 1.0),
                new DtmBeta(0, 3, 3, 0, 0.5),
                new DtmBeta(0, 3, 1, 3, 1.0),
                new DtmBeta(0, 3, 1, 2, 0.5),
                new DtmBeta(0, 3, 1, 0, 1.0),
                new DtmBeta(0, 3, 3, 2, 1.0),
                new DtmBeta(0, 3, 2, 3, 1.0),
                new DtmBeta(0, 3, 2, 0, 1.0)
        };

        DtmNode node = new DtmNode(1);
        DtmNodeCapacityEDIP m = new DtmNodeCapacityEDIP(Arrays.asList(beta), 4,4, cti, nti);
        double[] supply = {1000, 2000, 1000, 2000.0};
        double[] priorities = {1000, 1000, 1000, 1000.0};
        DtmPairElement<Double, Double>[][][] mri = getMutualRestrictionIntervals(4,4, new DtmPairElement<>(0.0, 1.0));
        DtmWRICNodeModel wricNodeModel = new DtmWRICNodeModel(node, new HashMap<>(), priorities, mri, m);

        double[][] flow = wricNodeModel.solveUniversal(demand, supply);
        double[][] rightFlow = {
                {0.0000, 28.9098, 86.7293, 173.4587},
                {68.1818, 0.0000, 204.5455,	1090.9091},
                {100.0000, 100.0000, 0.0000, 600.0000},
                {25.0075, 200.0596,	200.0596, 0.0000}
        };
        assert diffMatrix(flow, rightFlow) < 1E-3;
    }
}
