/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Testing of <code>PiecewiseConstantFunction</code>
 * @author Tomas Potuzak
 */
public class DtmPiecewiseConstantFunctionTestCase {

    @Test
    public void DtmPiecewiseConstantFunctionCommonParametersTest() {
        List<Double> xValues = new ArrayList<>();
        List<Double> yValues = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            xValues.add((double) i);
            yValues.add(i / 2.0);
        }
        DtmPiecewiseConstantFunction pcf = new DtmPiecewiseConstantFunction(xValues, yValues);
        for (int i = 0; i < xValues.size(); i++) {
            assertTrue(xValues.get(i).compareTo(pcf.xValues.get(i)) == 0 && yValues.get(i).compareTo(pcf.yValues.get(i)) == 0, "An initial and actual lists of x or y values are different.");
        }
    }

    @Test
    public void DtmPiecewiseConstantFunctionEmptyParamtersTest() {
        List<Double> xValues = new ArrayList<>();
        List<Double> yValues = new ArrayList<>();

        DtmPiecewiseConstantFunction pcf1 = new DtmPiecewiseConstantFunction(xValues, yValues);
        assertTrue(pcf1.xValues.size() == 0 && pcf1.yValues.size() == 0, "Actual lists of x or y values are not empty.");

        DtmPiecewiseConstantFunction pcf2 = new DtmPiecewiseConstantFunction();
        assertTrue(pcf2.xValues.size() == 0 && pcf2.yValues.size() == 0, "Actual lists of x or y values are not empty.");
    }

    @Test
    public void DtmPiecewiseConstantFunctionIllegalArgumentExceptionTest() {
        List<Double> xValues = new ArrayList<>();
        List<Double> yValues = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            xValues.add((double) i);
        }
        for (int i = 0; i < 4; i++) {
            yValues.add((double) i);
        }

        try {
            new DtmPiecewiseConstantFunction(xValues, yValues);
            assertTrue(false, "IllegalArgumentException not thrown for lists of x and y values of different sizes.");
        }
        catch (IllegalArgumentException ex) {
            //No action - the exception is expected and required
        }
    }

    @Test
    public void DtmPiecewiseConstantFunctionNullPointerExceptionTest() {
        List<Double> xValues = new ArrayList<>();
        List<Double> yValues = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            xValues.add((double) i);
            yValues.add((double) i);
        }
        List<List<Double>> inputXValues = new ArrayList<>();
        List<List<Double>> inputYValues = new ArrayList<>();
        inputXValues.add(xValues);
        inputYValues.add(null);
        inputXValues.add(null);
        inputYValues.add(yValues);
        inputXValues.add(null);
        inputYValues.add(null);

        for (int i = 0; i < inputXValues.size(); i++) {
            try {
                new DtmPiecewiseConstantFunction(inputXValues.get(i), inputYValues.get(i));
                assertTrue(false, "NullPointerException not thrown for one or both null lists of x and y values.");
            }
            catch (NullPointerException ex) {
                //No action - the exception is expected and required
            }
        }
    }

    @Test
    public void addPointCommonParametersTest() {
        DtmPiecewiseConstantFunction pcf1 = new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5));
        pcf1.addPoint(2.00000000000001, 4.0);
        assertTrue(pcf1.xValues.get(pcf1.xValues.size() - 1) == 2.00000000000001 && pcf1.yValues.get(pcf1.yValues.size() - 1) == 4.0, "The last x and y values do not match the last added values.");
        pcf1.addPoint(2.00000000000002, 3.0);
        assertTrue(pcf1.xValues.get(pcf1.xValues.size() - 1) == 2.00000000000002 && pcf1.yValues.get(pcf1.yValues.size() - 1) == 3.0, "The last x and y values do not match the last added values.");

        DtmPiecewiseConstantFunction pcf2 = new DtmPiecewiseConstantFunction();
        pcf2.addPoint(-2.0, 1.0);
        assertTrue(pcf2.xValues.get(pcf2.xValues.size() - 1) == -2.0 && pcf2.yValues.get(pcf2.yValues.size() - 1) == 1.0, "The last x and y values do not match the last added values.");
    }

    @Test
    public void addPointInfiniteParametersIllegalArgumentExceptionTest() {
        DtmPiecewiseConstantFunction pcf = new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5));
        List<Double> inputXValues = List.of(Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NaN, 1.0, 1.0, 1.0, Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NaN, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NaN, Double.NaN);
        List<Double> inputYValues = List.of(1.0, 1.0, 1.0, Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NaN, Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NaN, Double.NEGATIVE_INFINITY, Double.NaN, Double.NEGATIVE_INFINITY, Double.NaN, Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY);

        for (int i = 0; i < inputXValues.size(); i++) {
            try {
                pcf.addPoint(inputXValues.get(i), inputYValues.get(i));
                assertTrue(false, "IllegalArgumentException not thrown for one or both infinite x and y values.");
            }
            catch (IllegalArgumentException ex) {
                //No action - the exception is expected and required
            }
        }
    }

    @Test
    public void addPointInvalidXValueIllegalArgumentExceptionTest() {
        DtmPiecewiseConstantFunction pcf = new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5));

        try {
            pcf.addPoint(1.99999999999999, 4.0);
            assertTrue(false, "IllegalArgumentException not thrown for invalid x value.");
        }
        catch (IllegalArgumentException ex) {
            //No action - the exception is expected and required
        }
    }

    @Test
    public void updatePointCommonParametersTest() {
        DtmPiecewiseConstantFunction pcf1 = new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5));
        pcf1.updatePoint(1, 0.2, 4.0);
        assertTrue(pcf1.xValues.get(1) == 0.2 && pcf1.yValues.get(1) == 4.0, "The actual x and/or y values of the updated point are different from the update values.");
        pcf1.updatePoint(0, -6.0, 3.0);
        assertTrue(pcf1.xValues.get(0) == -6.0 && pcf1.yValues.get(0) == 3.0, "The actual x and/or y values of the updated point are different from the update values.");
        pcf1.updatePoint(pcf1.xValues.size() - 1, 6.0, 3.0);
        assertTrue(pcf1.xValues.get(pcf1.xValues.size() - 1) == 6.0 && pcf1.yValues.get(pcf1.yValues.size() - 1) == 3.0, "The actual x and/or y values of the updated point are different from the update values.");

        DtmPiecewiseConstantFunction pcf2 = new DtmPiecewiseConstantFunction(List.of(1.0), List.of(0.5));
        pcf2.updatePoint(0, 0.2, 4.0);
        assertTrue(pcf2.xValues.get(0) == 0.2 && pcf2.yValues.get(0) == 4.0, "The actual x and/or y values of the updated point are different from the update values.");
    }

    @Test
    public void updatePointIndexOutOfRangeIllegalArgumentExceptionTest() {
        DtmPiecewiseConstantFunction pcf = new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5));
        int[] indices = {-1, pcf.xValues.size()};

        for (int i = 0; i < indices.length; i++) {
            try {
                pcf.updatePoint(indices[i], 0.2, 4.0);
                assertTrue(false, "IllegalArgumentException not thrown for negative index the updated point out of range.");
            }
            catch (IllegalArgumentException ex) {
                //No action - the exception is expected and required
            }
        }
    }

    @Test
    public void updatePointInvalidXValueIllegalArgumentExceptionTest() {
        DtmPiecewiseConstantFunction pcf = new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5));
        int[] indices = {0, 1, 1, 2, 2, 3, 3, 4};
        double[] xValues = {0.6, -0.6, 0.6, 0.05, 1.05, 0.4, 3.0, 0.96};

        for (int i = 0; i < xValues.length; i++) {
            try {
                pcf.updatePoint(indices[i], xValues[i], 4.0);
                assertTrue(false, "IllegalArgumentException not thrown for x value exceeding neighboring x values.");
            }
            catch (IllegalArgumentException ex) {
                //No action - the exception is expected and required
            }
        }
    }

    /**
     * Compares points of the specified functions with specified comparison tolerance
     * @param pcf1 first function, which shall be compared to the second one
     * @param pcf2 second function, which shall be compared to the first one
     * @param comparisonEpsilon the tolerance for the comparison of individual <i>x</i> and <i>y</i> values
     * @return <code>true</code> if both functions have identical point (with the specified tolerance). Returns <code>false</code> otherwise
     */
    public boolean compareFunctions(DtmFunction pcf1, DtmFunction pcf2, double comparisonEpsilon) {
        if (pcf1.getXValues().size() != pcf2.getXValues().size() || pcf1.getYValues().size() != pcf2.getYValues().size() || pcf1.getXValues().size() != pcf2.getYValues().size()) {
            return false;
        }
        else {
            boolean error = false;
            for (int i = 0; i < pcf1.getXValues().size(); i++) {
                if (!DtmMathUtils.isEqualWithEpsilon(pcf1.getXValues().get(i), pcf2.getXValues().get(i), comparisonEpsilon) || !DtmMathUtils.isEqualWithEpsilon(pcf1.getYValues().get(i), pcf2.getYValues().get(i), comparisonEpsilon)) {
                    error = true;
                    break;
                }
            }
            return !error;
        }
    }

    @Test
    public void plusCommonParametersTest() {
        DtmPiecewiseConstantFunction[] pcfs1 = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5))
        };

        DtmPiecewiseConstantFunction[] pcfs2 = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.7, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5))
        };

        DtmPiecewiseConstantFunction[] expectedResults = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 2.0, 1.0, 3.0, 1.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 0.7, 1.0, 2.0), List.of(0.0, 2.0, 1.5, 1.0, 3.0, 1.0))
        };

        DtmPiecewiseConstantFunction actualResult = null;
        for (int i = 0; i < pcfs1.length; i++) {
            actualResult = pcfs1[i].plus(pcfs2[i]);
            assertTrue(compareFunctions(expectedResults[i], actualResult, DtmMathUtils.DEFAULT_COMPARISON_EPSILON), "Expected and actual resulting piecewise constant functions are too different.");
        }
    }

    @Test
    public void plusUnsupportedFunctionTest() {
        DtmPiecewiseConstantFunction pcf = new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5));
        DtmPiecewiseLinearFunction plf = new DtmPiecewiseLinearFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5));

        try {
            pcf.plus(plf);
            assertTrue(false, "IllegalArgumentException not thrown for piecewise linear function.");
        }
        catch (IllegalArgumentException ex) {
            //No action - the exception is expected and required
        }
    }

    @Test
    public void minusCommonParametersTest() {
        DtmPiecewiseConstantFunction[] pcfs1 = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5))
        };

        DtmPiecewiseConstantFunction[] pcfs2 = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.7, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5))
        };

        DtmPiecewiseConstantFunction[] expectedResults = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 0.0, 0.0, 0.0, 0.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 0.7, 1.0, 2.0), List.of(0.0, 0.0, -0.5, 0.0, 0.0, 0.0))
        };

        DtmPiecewiseConstantFunction actualResult = null;
        for (int i = 0; i < pcfs1.length; i++) {
            actualResult = pcfs1[i].minus(pcfs2[i]);
            assertTrue(compareFunctions(expectedResults[i], actualResult, DtmMathUtils.DEFAULT_COMPARISON_EPSILON), "Expected and actual resulting piecewise constant functions are too different.");
        }
    }

    @Test
    public void minusUnsupportedFunctionTest() {
        DtmPiecewiseConstantFunction pcf = new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5));
        DtmPiecewiseLinearFunction plf = new DtmPiecewiseLinearFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5));

        try {
            pcf.minus(plf);
            assertTrue(false, "IllegalArgumentException not thrown for piecewise linear function.");
        }
        catch (IllegalArgumentException ex) {
            //No action - the exception is expected and required
        }
    }

    @Test
    public void timesCommonParameterTest() {
        DtmPiecewiseConstantFunction[] pcfs = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5))
        };

        double[] coefficients = {0.0, 1.0, 1000.0, -1.0};

        DtmPiecewiseConstantFunction[] expectedResults = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 0.0, 0.0, 0.0, 0.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1000.0, 500.0, 1500.0, 500.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, -1.0, -0.5, -1.5, -0.5)),
        };

        DtmPiecewiseConstantFunction actualResult = null;
        for (int i = 0; i < pcfs.length; i++) {
            actualResult = pcfs[i].times(coefficients[i]);
            assertTrue(compareFunctions(expectedResults[i], actualResult, DtmMathUtils.DEFAULT_COMPARISON_EPSILON), "Expected and actual resulting piecewise constant functions are too different.");
        }
    }

    @Test
    public void timesCommonParametersTest() {
        DtmPiecewiseConstantFunction[] pcfs1 = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5))
        };

        DtmPiecewiseConstantFunction[] pcfs2 = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.7, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5))
        };

        DtmPiecewiseConstantFunction[] expectedResults = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.25, 2.25, 0.25)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 0.7, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 0.25, 2.25, 0.25))
        };

        DtmPiecewiseConstantFunction actualResult = null;
        for (int i = 0; i < pcfs1.length; i++) {
            actualResult = pcfs1[i].times(pcfs2[i]);
            assertTrue(compareFunctions(expectedResults[i], actualResult, DtmMathUtils.DEFAULT_COMPARISON_EPSILON), "Expected and actual resulting piecewise constant functions are too different.");
        }
    }

    @Test
    public void timesUnsupportedFunctionTest() {
        DtmPiecewiseConstantFunction pcf = new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5));
        DtmPiecewiseLinearFunction plf = new DtmPiecewiseLinearFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5));

        try {
            pcf.times(plf);
            assertTrue(false, "IllegalArgumentException not thrown for piecewise linear function.");
        }
        catch (IllegalArgumentException ex) {
            //No action - the exception is expected and required
        }
    }

    @Test
    public void dividedByCommonParametersTest() {
        DtmPiecewiseConstantFunction[] pcfs1 = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5))
        };

        DtmPiecewiseConstantFunction[] pcfs2 = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.7, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5))
        };

        DtmPiecewiseConstantFunction[] expectedResults = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(1.0, 1.0, 1.0, 1.0, 1.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 0.7, 1.0, 2.0), List.of(1.0, 1.0, 0.5, 1.0, 1.0, 1.0))
        };

        DtmPiecewiseConstantFunction actualResult = null;
        for (int i = 0; i < pcfs1.length; i++) {
            actualResult = pcfs1[i].dividedBy(pcfs2[i]);
            assertTrue(compareFunctions(expectedResults[i], actualResult, DtmMathUtils.DEFAULT_COMPARISON_EPSILON), "Expected and actual resulting piecewise constant functions are too different.");
        }
    }

    @Test
    public void dividedByUnsupportedFunctionTest() {
        DtmPiecewiseConstantFunction pcf = new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5));
        DtmPiecewiseLinearFunction plf = new DtmPiecewiseLinearFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5));

        try {
            pcf.dividedBy(plf);
            assertTrue(false, "IllegalArgumentException not thrown for piecewise linear function.");
        }
        catch (IllegalArgumentException ex) {
            //No action - the exception is expected and required
        }
    }

    @Test
    public void performOperationCommonParametersTest() {
        DtmPiecewiseConstantFunction[] pcfs1 = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.3, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 1.0), List.of(1.0, 0.5, 1.0, 0.5, 1.0, 0.5, 1.0, 2.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.7, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5))
        };

        DtmPiecewiseConstantFunction[] pcfs2 = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.1 + 0.2, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 3.0), List.of(0.1, 1.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.7, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.7, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.7, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction()
        };

        String[] operators = {"+", "-", "+", "+", "/", "*", "*", "*"};

        DtmPiecewiseConstantFunction[] expectedResults = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.2, 2.0, 1.0, 3.0, 1.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.0, 0.0, 0.0, 0.0, 0.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.3, 1.0, 2.0), List.of(0.2, 2.0, 1.0, 3.0, 1.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 1.0), List.of(1.1, 0.6, 1.1, 0.6, 1.1, 0.6, 1.1, 2.1)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 0.7, 1.0, 2.0), List.of(1.0, 1.0, 0.5, 1.0, 1.0, 1.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 0.7, 1.0, 2.0), List.of(0.0, 1.0, 0.5, 0.25, 2.25, 0.25)),
                new DtmPiecewiseConstantFunction(),
                new DtmPiecewiseConstantFunction()
        };

        DtmPiecewiseConstantFunction actualResult = null;
        for (int i = 0; i < pcfs1.length; i++) {
            actualResult = pcfs1[i].performOperation(pcfs2[i], operators[i]);
            assertTrue(compareFunctions(expectedResults[i], actualResult, DtmMathUtils.DEFAULT_COMPARISON_EPSILON), "Expected and actual resulting piecewise constant functions are too different.");
        }
    }

    @Test
    public void performOperationIllegalArgumentExceptionTest() {
        DtmPiecewiseConstantFunction pcf1 = new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5));
        DtmPiecewiseConstantFunction pcf2 = new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5));

        String[] operators = {"^", "", "sdfasdgas", " + "};


        for (int i = 0; i < operators.length; i++) {
            try {
                pcf1.performOperation(pcf2, operators[i]);
                assertTrue(false, "IllegalArgumentException not thrown for invalid operator.");
            }
            catch (IllegalArgumentException ex) {
                //No action - the exception is expected and required
            }
        }
    }

    @Test
    public void performOperationNullPointerExceptionTest() {
        DtmPiecewiseConstantFunction pcf1 = new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5));
        DtmPiecewiseConstantFunction pcf2 = new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5));
        try {
            pcf1.performOperation(pcf2, null);
            assertTrue(false, "IllegalArgumentException not thrown for invalid operator.");
        }
        catch (NullPointerException ex) {
            //No action - the exception is expected and required
        }
    }

    @Test
    public void integrateCommonParameterTest() {
        DtmPiecewiseConstantFunction[] pcfs = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, -0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(-5.0, 0.0, 0.1, 0.5, 1.0, 2.0), List.of(-1.0, 0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(-5.0, 0.0, 0.1, 0.5, 1.0, 2.0), List.of(-1.0, 0.1, 1.0, -0.5, 1.5, 0.5))
        };

        double[] expectedResults = {2.16, 2.16, 0.0, 0.0, 1.66, 2.16, -2.84, 7.16, -3.34, 7.16};

        double actualResult = 0.0;
        for (int i = 0; i < pcfs.length; i++) {
            actualResult = pcfs[i].integrate(false);
            assertTrue(DtmMathUtils.isEqualWithEpsilon(expectedResults[2 * i], actualResult, DtmMathUtils.DEFAULT_COMPARISON_EPSILON), "Expected and actual resulting integral value are too different.");
            actualResult = pcfs[i].integrate(true);
            assertTrue(DtmMathUtils.isEqualWithEpsilon(expectedResults[2 * i + 1], actualResult, DtmMathUtils.DEFAULT_COMPARISON_EPSILON), "Expected and actual resulting integral value are too different.");
        }
    }

    @Test
    public void valueAtCommonParameterTest() {
        DtmPiecewiseConstantFunction[] pcfs = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(-5.0, 5.0), List.of(0.1, -3.0)),
                new DtmPiecewiseConstantFunction(List.of(-5.0, -0.1, 5.0), List.of(0.1, 2.0, -3.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(5.0), List.of(-1.0))
        };

        double[][] xValues = {
                {0.0, 0.1, 0.5, 1.0, 2.0, 0.2 - 0.1}, //To test rounding errors, but even if the rounding error would move the x to the previous interval, it would be probably counterproductive to use EPSILON
                {-5.0, 0.0, 5.0},
                {-0.1, 0.1 - 0.2}, //To test rounding errors, but even if the rounding error would move the x to the previous interval, it would be probably counterproductive to use EPSILON
                {0.05, 0.2, 0.6, 1.1, 2.5},
                {5.0, 6.0}
        };

        double[][] expectedResults = {
                {0.1, 1.0, 0.5, 1.5, 0.5, 1.0},
                {0.1, 0.1, -3.0},
                {2.0, 2.0},
                {0.1, 1.0, 0.5, 1.5, 0.5},
                {-1.0, -1.0}
        };

        double actualResult = 0.0;
        for (int i = 0; i < xValues.length; i++) {
            for (int j = 0; j < xValues[i].length; j++) {
                actualResult = pcfs[i].valueAt(xValues[i][j]);
                assertTrue(DtmMathUtils.isEqualWithEpsilon(expectedResults[i][j], actualResult, DtmMathUtils.DEFAULT_COMPARISON_EPSILON), "Expected and actual resulting integral value at specified x is too different.");
            }
        }
    }

    @Test
    public void valueAtIllegalStateExceptionTest() {
        DtmPiecewiseConstantFunction pcf = new DtmPiecewiseConstantFunction();
        try {
            pcf.valueAt(0.0);
            assertTrue(false, "IllegalStateException not thrown for empty function (i.e., with no points).");
        }
        catch (IllegalStateException ex) {
            //No action - the exception is expected and required
        }
    }

    @Test
    public void valueAtIllegalArgumentExceptionTest() {
        DtmPiecewiseConstantFunction pcf = new DtmPiecewiseConstantFunction(List.of(-1.5, 0.0, 1.0, 2.0), List.of(-5.0, 1.0, 1.0, 3.0));
        try {
            pcf.valueAt(-2.0);
            assertTrue(false, "IllegalArgumentException not thrown for x lower than lowest x value of the function.");
        }
        catch (IllegalArgumentException ex) {
            //No action - the exception is expected and required
        }
    }

    @Test
    public void minimumCommonParametersTest() {
        DtmPiecewiseConstantFunction[] pcfs = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, -0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(-5.0, 0.0, 0.1, 0.5, 1.0, 2.0), List.of(-1.0, 0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(-5.0, 0.0, 0.1, 0.5, 1.0, 2.0), List.of(-1.0, 0.1, 1.0, -0.5, 1.5, -5.5))
        };

        double[] expectedResults = {0.1, -0.5, -1.0, -5.5};

        double actualResult = 0.0;
        for (int i = 0; i < pcfs.length; i++) {
            actualResult = pcfs[i].minimum();
            assertTrue(DtmMathUtils.isEqualWithEpsilon(expectedResults[i], actualResult, DtmMathUtils.DEFAULT_COMPARISON_EPSILON), "Expected and actual minimum are too different.");
        }
    }

    @Test
    public void minimumIllegalStateExceptionTest() {
        DtmPiecewiseConstantFunction pcf = new DtmPiecewiseConstantFunction();
        try {
            pcf.minimum();
            assertTrue(false, "IllegalStateException not thrown for empty function (i.e., with no points).");
        }
        catch (IllegalStateException ex) {
            //No action - the exception is expected and required
        }
    }

    @Test
    public void maximumCommonParametersTest() {
        DtmPiecewiseConstantFunction[] pcfs = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(1.1, 1.0, -0.5, -1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(-5.0, 0.0, 0.1, 0.5, 1.0, 2.0), List.of(-1.0, -0.1, -1.0, -0.5, -1.5, -0.05)),
                new DtmPiecewiseConstantFunction(List.of(-5.0, 0.0, 0.1, 0.5, 1.0, 2.0), List.of(-1.0, 0.1, 1.0, -0.5, 1.5, 5.5))
        };

        double[] expectedResults = {1.5, 1.1, -0.05, 5.5};

        double actualResult = 0.0;
        for (int i = 0; i < pcfs.length; i++) {
            actualResult = pcfs[i].maximum();
            assertTrue(DtmMathUtils.isEqualWithEpsilon(expectedResults[i], actualResult, DtmMathUtils.DEFAULT_COMPARISON_EPSILON), "Expected and actual minimum are too different.");
        }
    }

    @Test
    public void maximumIllegalStateExceptionTest() {
        DtmPiecewiseConstantFunction pcf = new DtmPiecewiseConstantFunction();
        try {
            pcf.maximum();
            assertTrue(false, "IllegalStateException not thrown for empty function (i.e., with no points).");
        }
        catch (IllegalStateException ex) {
            //No action - the exception is expected and required
        }
    }

    @Test
    public void subFunctionCommonParametersTest() {
        DtmPiecewiseConstantFunction[] pcfs = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.3, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5))
        };

        double[] as = {0.0, 0.1, 0.1 + 0.2, 0.05, 0.6};
        double[] bs = {2.0, 1.0, 2.0, 1.9, 0.7};

        DtmPiecewiseConstantFunction[] expectedResults = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.1, 0.5, 1.0), List.of(1.0, 0.5, 1.5)),
                new DtmPiecewiseConstantFunction(List.of(0.3, 0.5, 1.0, 2.0), List.of(1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.05, 0.1, 0.5, 1.0, 1.9), List.of(0.1, 1.0, 0.5, 1.5, 1.5)),
                new DtmPiecewiseConstantFunction(List.of(0.6, 0.7), List.of(0.5, 0.5))
        };

        DtmPiecewiseConstantFunction actualResult = null;
        for (int i = 0; i < pcfs.length; i++) {
            actualResult = pcfs[i].subFunction(as[i], bs[i]);
            assertTrue(compareFunctions(expectedResults[i], actualResult, DtmMathUtils.DEFAULT_COMPARISON_EPSILON), "Expected and actual sunfunctions are too different.");
        }
    }

    @Test
    public void subFunctionIllegalStateExceptionTest() {
        DtmPiecewiseConstantFunction[] pcfs = {
                new DtmPiecewiseConstantFunction(),
                new DtmPiecewiseConstantFunction(List.of(0.0), List.of(0.0)),
                new DtmPiecewiseConstantFunction(List.of(-5.0), List.of(0.0)),
                new DtmPiecewiseConstantFunction(List.of(10.0), List.of(-6.0)),
        };

        double[] as = {0.0, 0.0, -5.0, 10.0};
        double[] bs = {0.0, 0.0, -5.0, 10.0};


        for (int i = 0; i < pcfs.length; i++) {
            try {
                pcfs[i].subFunction(as[i], bs[i]);
                assertTrue(false, "IllegalStateException not thrown for function with less than two points.");
            }
            catch (IllegalStateException ex) {
                //No action - the exception is expected and required
            }
        }
    }

    @Test
    public void subFunctionIllegalArgumentExceptionTest() {
        DtmPiecewiseConstantFunction[] pcfs = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(1.0, 2.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(1.0, 2.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0), List.of(1.0, 2.0)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 1.0, 2.0), List.of(0.0, 0.0, 1.0)),
                new DtmPiecewiseConstantFunction(List.of(-6.0, 1.0, 9.0, 9.5), List.of(0.5, 1.5, 2.5, 1.0)),
                new DtmPiecewiseConstantFunction(List.of(-6.0, 1.0, 9.0, 9.5), List.of(0.5, 1.5, 2.5, 1.0)),
                new DtmPiecewiseConstantFunction(List.of(-6.0, 1.0, 9.0, 9.5), List.of(0.5, 1.5, 2.5, 1.0))
        };

        double[] as = {-0.5, 0.0, -5.0, 1.0, -10.0, 1.0, 1.0};
        double[] bs = {0.5, 1.0001, 5.0, 2.1, 0.5, 1.0, 0.5};


        for (int i = 0; i < pcfs.length; i++) {
            try {
                pcfs[i].subFunction(as[i], bs[i]);
                assertTrue(false, "IllegalArgumentException not thrown for invalid interval borders.");
            }
            catch (IllegalArgumentException ex) {
                //No action - the exception is expected and required
            }
        }
    }

    @Test
    public void simplifyCommonParametersTest() {
        DtmPiecewiseConstantFunction[] pcfs = {
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 1.0, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 0.10001, 0.5, 1.5, 0.5)),
                new DtmPiecewiseConstantFunction(List.of(0.0, 0.1, 0.5, 1.0, 2.0), List.of(0.1, 0.10001, 0.10002, 0.10003, 0.10004)),
                new DtmPiecewiseConstantFunction(List.of(0.0), List.of(0.0))
        };

        double[] maximalErrors = {1E-3, 0.5, 1E-3, 1E-3, 1E-3};

        DtmPiecewiseConstantFunction actualResult = null;
        for (int i = 0; i < pcfs.length; i++) {
            actualResult = pcfs[i].simplify(maximalErrors[i]);

            boolean withinError = true;
            for (int j = 0; j < pcfs[i].getLength(); j++) {
                if (!DtmMathUtils.isEqualWithEpsilon(pcfs[i].getY(j), actualResult.valueAt(pcfs[i].getX(j)), maximalErrors[i])) {
                    withinError = false;
                    break;
                }
            }
            assertTrue(withinError && DtmMathUtils.isEqualWithEpsilon(pcfs[i].getFirstX(), actualResult.getFirstX(), DtmMathUtils.DEFAULT_COMPARISON_EPSILON) && DtmMathUtils.isEqualWithEpsilon(pcfs[i].getLastX(), actualResult.getLastX(), DtmMathUtils.DEFAULT_COMPARISON_EPSILON) && actualResult.getLength() <= pcfs[i].getLength(), "Simplified and the original functions are too different.");
        }
    }

    @Test
    public void simplifyEmptyFunctionTest() {
        DtmPiecewiseConstantFunction pcf = new DtmPiecewiseConstantFunction();
        DtmPiecewiseConstantFunction actualResult = pcf.simplify(1.0);
        assertTrue(compareFunctions(pcf, actualResult, DtmMathUtils.DEFAULT_COMPARISON_EPSILON), "Simplified and the original functions are too different.");
    }
}
