/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

public class DtmLoaderTest extends PostgresTestBase {
    @Disabled
    @Test
    public void load() throws Exception {
        String modelName = "dtm_nguyen";
        DtmModelLoader loader = new DtmModelLoader(dbUrl);
        DtmOriginDestinationMatrix odm = loader.readModel(modelName, 0);
        DtmGraph graph = odm.getGraph();
        System.out.println("numOfEdges = "+graph.getEdges().size());
        System.out.println("numOfNodes = "+graph.getNodes().size());

        System.out.println("numOfODPairs = "+odm.getPairs().size());
        List<DtmNode> originNodes =  odm.originZones.stream().map(x -> x.node).collect(Collectors.toList());
        System.out.println("numOfOrigins = "+originNodes.size());
        List<DtmNode> destinationNodes =  odm.destinationZones.stream().map(x -> x.node).collect(Collectors.toList());
        System.out.println("numOfDestinations = "+destinationNodes.size());
        System.out.println("isCoherent = "+graph.isCoherent(originNodes, destinationNodes));
        DtmModelLoader.allOrNothingAssignment(odm, 3.0, false, 0);

        DtmWRICNodeModel model5 = (DtmWRICNodeModel) graph.getNodeById(5).nodeModel;
        DtmWRICNodeModel model8 = (DtmWRICNodeModel) graph.getNodeById(8).nodeModel;

        for (int i = 0; i < model5.mutualRestrictionIntervals.length; i++){
            for (int jj = 0; jj < model5.mutualRestrictionIntervals[i].length; jj++){
                for (int j = 0; j < model5.mutualRestrictionIntervals[i][jj].length; j++){
                    DtmPairElement<Double, Double> mri = model5.mutualRestrictionIntervals[i][jj][j];
                    System.out.println(mri.first+" "+mri.second);
                }
            }
        }

        double[][] res = model5.solveUniversal(new double[][]{{900, 900}, {900, 900}}, new double[]{850, 850});
        NodeTest.printMatrix(res);

        System.out.println(model5.capacityModel);
        System.out.println(model8.capacityModel);
    }
}
