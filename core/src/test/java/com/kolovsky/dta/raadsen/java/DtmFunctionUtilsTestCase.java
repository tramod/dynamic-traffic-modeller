/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tomas Potuzak
 */
public class DtmFunctionUtilsTestCase {


    /**
     * Checks whether the condition is <code>true</code> and does nothing if so. Throws an assertion exception with specified message otherwise.
     * @param condition the condition which shall be <code>true</code>
     * @param notTrueMessage the message of the assertion exception in case that the condition if <code>false</code>
     * @throws AssertionError if the condition is <code>false</code>
     */
    public static void assertTrue(boolean condition, String notTrueMessage) throws AssertionError {
        if (!condition) {
            throw new AssertionError(notTrueMessage);
        }
    }

    /**
     *
     * @param xValuesArray
     * @param yValuesArray
     * @param inputFunction
     * @return
     */
    protected DtmFunction constructFunction(double[] xValuesArray, double[] yValuesArray, boolean inputFunction) {
        List<Double> xValues = new ArrayList<>();
        List<Double> yValues = new ArrayList<>();

        for (int i = 0; i < xValuesArray.length; i++) {
            xValues.add(xValuesArray[i]);
        }
        for (int i = 0; i < yValuesArray.length; i++) {
            yValues.add(yValuesArray[i]);
        }

        if (inputFunction) {
            return new DtmNonDecreasingPiecewiseLinearFunction(xValues, yValues);
        }
        else {
            return new DtmPiecewiseLinearFunction(xValues, yValues);
        }
    }

    /**
     *
     * @param function1
     * @param function2
     * @param comparisonEpsilon
     * @return
     */
    protected boolean areEqualWithEpsilon(DtmFunction function1, DtmFunction function2, double comparisonEpsilon) {
        if (function1 == function2) {
            return true;
        }
        else {
            if (function1.getLength() != function2.getLength()) {
                return false;
            }
            else  {
                boolean equal = true;

                for (int i = 0; i < function1.getLength(); i++) {
                    if (!DtmMathUtils.isEqualWithEpsilon(function1.getX(i), function2.getX(i), comparisonEpsilon)) {
                        equal = false;
                        break;
                    }
                    else if (!DtmMathUtils.isEqualWithEpsilon(function1.getY(i), function2.getY(i), comparisonEpsilon)) {
                        equal = false;
                        break;
                    }
                }

                return equal;
            }
        }
    }

    /**
     *
     * @param function1XValues
     * @param function1YValues
     * @param function2XValues
     * @param function2YValues
     * @param expectedResultXValues
     * @param expectedResultYValues
     * @param minimalDifference
     * @return
     */
    protected DtmFunction[] performHorizontalDifferenceTest(double[] function1XValues, double[] function1YValues, double[] function2XValues, double[] function2YValues, double[] expectedResultXValues, double[] expectedResultYValues, double minimalDifference) {
        DtmFunction[] functions = new DtmFunction[4];

        functions[0] = constructFunction(function1XValues, function1YValues, true);
        functions[1] = constructFunction(function2XValues, function2YValues, true);
        functions[2] = constructFunction(expectedResultXValues, expectedResultYValues, false);
        functions[3] = DtmFunctionUtils.calculateHorizontalDifference((DtmNonDecreasingPiecewiseLinearFunction) functions[0], (DtmNonDecreasingPiecewiseLinearFunction) functions[1], minimalDifference);
        assertTrue(areEqualWithEpsilon(functions[2], functions[3], DtmMathUtils.DEFAULT_COMPARISON_EPSILON / 4.0), "The expected and actual results are too different.");

        return functions;
    }

    @Test
    public void calculateHorizontalDifferenceChartTests() {
        performHorizontalDifferenceTest(
                new double[] {0.0, 1.5, 3.0, 4.0}, new double[] {0.0, 0.75, 0.75, 1.0},
                new double[] {0.0, 2.0, 2.5, 4.0}, new double[] {0.0, 0.0, 0.5, 1.0},
                new double[] {0.0, 2 * DtmMathUtils.DEFAULT_COMPARISON_EPSILON, 1.0, 1.5, 3.0, 4.0}, new double[] {0.1, 2.0, 1.5, 1.75, 0.25, 0.1},
                0.1);

        performHorizontalDifferenceTest(
                new double[] {0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5}, new double[] {0.0, 1.0, 1.0, 1.0, 2.0, 2.0, 3.0, 3.0, 3.0, 3.0},
                new double[] {0.5, 1.5, 2.5, 2.75, 3.0, 3.5, 4.0, 4.5, 5.0}, new double[] {0.0, 0.0, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 4.0},
                new double[] {0.0, 2 * DtmMathUtils.DEFAULT_COMPARISON_EPSILON, 0.25, 0.25 + 2 * DtmMathUtils.DEFAULT_COMPARISON_EPSILON, 0.5, 1.5, 2.0, 2.5, 3.0, 4.5}, new double[] {0.5, 1.5, 2.25, 2.5, 2.5, 3.0, 2.6666666666667, 2.1666666666667, 1.8333333333333, 0.3333333333333},
                0.1);

        performHorizontalDifferenceTest(
                new double[] {0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5}, new double[] {0.0, 1.0, 1.0, 1.0, 2.0, 2.0, 3.0, 3.0, 3.0, 3.0},
                new double[] {2.5, 2.75, 3.0, 3.5, 4.0, 4.5, 5.0}, new double[] {0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 2.5},
                new double[] {0.0, 0.25, 0.25 + 2 * DtmMathUtils.DEFAULT_COMPARISON_EPSILON, 0.5, 1.5, 2.0, 2.5, 2.75, 3.0, 4.5}, new double[] {0.1, 2.25, 2.5, 2.5, 3.0, 2.8333333333333, 2.3333333333333, 2.25, 0.1, 0.1},
                0.1);
    }
}