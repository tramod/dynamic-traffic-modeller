/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DtmDueTestCase extends PostgresTestBase {

    @Test
    public void generatedGrid() {
        int intervals = 32;
        int rowsCount = 3;
        int columnsCount = 8;

        DtmOriginDestinationMatrix odm = DtmSubgraphParallelDueTestCase.generateGrid(rowsCount, columnsCount, 2.0, 2.0);
        DtmGraph graph = odm.getGraph();
        DtmModelLoader.allOrNothingAssignment(odm, 3.0, true, intervals);

        DtmEventGeneralLinkTransmissionModel dnl = new DtmEventGeneralLinkTransmissionModel(graph, odm.getOriginZones(), true);
        dnl.setFanStep(10);
        dnl.setFlowThreshold(1.0);
        dnl.setMixtureFlowThreshold(0.01);

        //DNL
//		dnl.run(4.0, EventGeneralLinkTransmissionModel.DEFAULT_RELEASE_EVENT_EPSILON);

        //Dynamic User Equilibrium (DUE)
        DtmQuasiGradientProjection ue = new DtmQuasiGradientProjection(1.0, 0.5, 1.0);
        DtmDiscreteDestinationDynamicUserEquilibrium due = new DtmDiscreteDestinationDynamicUserEquilibrium(dnl, 4.0, odm, ue, 0.0, 3.0, intervals);
        due.run(20, 0.0);

        System.out.println(dnl.statistics.getBackwardEdgeEventsCount());
        System.out.println(dnl.statistics.getForwardEdgeEventsCount());
        System.out.println(dnl.statistics.getMixtureEdgeEventsCount());
    }

    @Test
    public void braessDiscrete() {
        DtmGraph graph = new DtmGraph();
        DtmFundamentalDiagram fundamentalDiagram = new DtmFundamentalDiagram(120.0, 2000.0, 180.0, 61.0, -23.0);
        graph.addEdge(0, 0, 1, 1.0, fundamentalDiagram);
        graph.addEdge(1, 0, 2, 1.0, fundamentalDiagram);
        graph.addEdge(2, 1, 2, 1.0, fundamentalDiagram);
        graph.addEdge(3, 1, 3, 1.0, fundamentalDiagram);
        graph.addEdge(4, 2, 3, 1.0, fundamentalDiagram);
        graph.initializeStructures();
        graph.constructDualGraph();

        for (int i = 0; i < graph.getNodes().size(); i++) {
            System.out.println("" + graph.getNodes().get(i) + ",  in: " + graph.getNodes().get(i).getIncomingEdges() + ", out: " + graph.getNodes().get(i).getOutgoingEdges());
        }

        DtmPiecewiseConstantFunction inflow = new DtmPiecewiseConstantFunction();
        double[] times = {0.0, 50.0, 100.0, 200.0};
        double[] flows = {0.0, 3000, 500, 1};
        for (int i = 0; i < times.length; i++) {
            inflow.addPoint(times[i] / 3600, flows[i]);
        }
        List<DtmTripleElement<Integer, Integer, DtmPiecewiseConstantFunction>> pairsData = new ArrayList<>();
        pairsData.add(new DtmTripleElement<>(0, 3, inflow));
        DtmOriginDestinationMatrix originDestinationMatrix = new DtmOriginDestinationMatrix(graph, pairsData);
        originDestinationMatrix.initializeOrigins();

        //Initialization of node models
        DtmDistributionMatrix distributionMatrix = null;
        HashMap<DtmIdentifiable, DtmDistributionMatrix> destinationNodeDistribution = null;

        //Node 0
        DtmDiscreteFunction dist1 = new DtmDiscreteFunction(500, 0.5,0.0, 1.0);
        DtmDiscreteFunction dist2 = new DtmDiscreteFunction(500, 0.5,0.0, 1.0);
        distributionMatrix = new DtmDistributionMatrix(new DtmFunction[][] {{dist1, dist2}});
        destinationNodeDistribution = new HashMap<>();
        destinationNodeDistribution.put(originDestinationMatrix.getDestinationZones().get(0), distributionMatrix);
        graph.getNodeById(0).setNodeModel(new DtmNoFifoNodeModel(graph.getNodeById(0), destinationNodeDistribution, new double[] {1.0}));

        //Node 1
        DtmDiscreteFunction dist11 = new DtmDiscreteFunction(500, 0.5,0.0, 1.0);
        DtmDiscreteFunction dist21 = new DtmDiscreteFunction(500, 0.5,0.0, 1.0);
        distributionMatrix = new DtmDistributionMatrix(new DtmFunction[][] {{dist11, dist21}});
        destinationNodeDistribution = new HashMap<>();
        destinationNodeDistribution.put(originDestinationMatrix.getDestinationZones().get(0), distributionMatrix);
        graph.getNodeById(1).setNodeModel(new DtmNoFifoNodeModel(graph.getNodeById(1), destinationNodeDistribution, new double[] {1.0}));

        //Node 2
        DtmDiscreteFunction dist12 = new DtmDiscreteFunction(500, 1.0,0.0, 1.0);
        DtmDiscreteFunction dist22 = new DtmDiscreteFunction(500, 1.0,0.0, 1.0);
        distributionMatrix = new DtmDistributionMatrix(new DtmFunction[][] {{dist12} , {dist22}});
        destinationNodeDistribution = new HashMap<>();
        destinationNodeDistribution.put(originDestinationMatrix.getDestinationZones().get(0), distributionMatrix);
        graph.getNodeById(2).setNodeModel(new DtmNoFifoNodeModel(graph.getNodeById(2), destinationNodeDistribution, new double[] {1000.0, 1000.0}));

        //Node 3
        graph.getNodeById(3).setNodeModel(new DtmNoFifoNodeModel(graph.getNodeById(3), null, null));

        DtmEventGeneralLinkTransmissionModel dynamicNetworkLoading = new DtmEventGeneralLinkTransmissionModel(graph, originDestinationMatrix.getOriginZones(), true);

        dynamicNetworkLoading.setFanStep(10);
        dynamicNetworkLoading.setFlowThreshold(2.0);
        dynamicNetworkLoading.setMixtureFlowThreshold(0.0000001);

        // Dynamic User Equilibrium (DUE)
        DtmQuasiGradientProjection ue = new DtmQuasiGradientProjection(1.0, 0.1, 1.0);
        DtmDiscreteDestinationDynamicUserEquilibrium due = new DtmDiscreteDestinationDynamicUserEquilibrium(dynamicNetworkLoading, 1.5,
                originDestinationMatrix, ue, 0.0, 1.0, 500);

        due.run(100, 1E-6);
    }

    @Disabled @Test
    public void braess() throws Exception {
        String modelName = "dtm_braess";
        DtmModelLoader loader = new DtmModelLoader(dbUrl);
        DtmOriginDestinationMatrix odm = loader.readModel(modelName, 0);
        System.out.println(odm.pairs.get(0).getFlow().xValues);
        System.out.println(odm.pairs.get(0).getFlow().yValues);
        DtmModelLoader.allOrNothingAssignment(odm, 0.2, true, 24);

        DtmEventGeneralLinkTransmissionModel dnl = new DtmEventGeneralLinkTransmissionModel(odm.getGraph(), odm.getOriginZones(), true);
        dnl.setFanStep(100.0);
        dnl.setFlowThreshold(0.5);

        // Dynamic User Equilibrium (DUE)
        DtmQuasiGradientProjection ue = new DtmQuasiGradientProjection(1.0, 0.4, 1.0);
        DtmDiscreteDestinationDynamicUserEquilibrium due = new DtmDiscreteDestinationDynamicUserEquilibrium(dnl, 0.5,
                odm, ue, 0.0, 0.2, 24);
        due.setNumberSearchPerInterval(1);

        due.run(100, 0.0);
    }

    @Disabled @Test
    public void nguyen() throws Exception {
        String modelName = "dtm_nguyen";
        DtmModelLoader loader = new DtmModelLoader(dbUrl);

        int intervals = 32;

        DtmOriginDestinationMatrix odm = loader.readModel(modelName, 0);
        DtmGraph graph = odm.getGraph();

        DtmModelLoader.allOrNothingAssignment(odm, 3.0, true, intervals);

        DtmEventGeneralLinkTransmissionModel dnl = new DtmEventGeneralLinkTransmissionModel(graph, odm.getOriginZones(), true);
        dnl.setFanStep(10);
        dnl.setFlowThreshold(0.0);
        dnl.setMixtureFlowThreshold(0.000000);

        // Dynamic User Equilibrium (DUE)
        DtmQuasiGradientProjection ue = new DtmQuasiGradientProjection(1.0, 0.5, 1.0);
        DtmDiscreteDestinationDynamicUserEquilibrium due = new DtmDiscreteDestinationDynamicUserEquilibrium(dnl, 4.0,
                odm, ue, 0.0, 3.0, intervals);

        due.run(100, 0.0);
        System.out.println(dnl.statistics.getBackwardEdgeEventsCount());
        System.out.println(dnl.statistics.getForwardEdgeEventsCount());
        System.out.println(dnl.statistics.getMixtureEdgeEventsCount());
    }

    @Disabled
    @Test
    public void anaheim() throws Exception {
        String modelName = "dtm_anaheim";
        DtmModelLoader loader = new DtmModelLoader(dbUrl);

        int intervals = 16;

        DtmOriginDestinationMatrix odm = loader.readModel(modelName, 0);
        DtmGraph graph = odm.getGraph();
        DtmModelLoader.allOrNothingAssignment(odm, 2.0, true, intervals);

        DtmEventGeneralLinkTransmissionModel dnl = new DtmEventGeneralLinkTransmissionModel(graph, odm.getOriginZones(), true);
        dnl.setFanStep(100000);
        dnl.setFlowThreshold(2.0);
        dnl.setMixtureFlowThreshold(0.02);

        // Dynamic User Equilibrium (DUE)
        DtmQuasiGradientProjection ue = new DtmQuasiGradientProjection(1.0, 0.08, 10.0);
        DtmDiscreteDestinationDynamicUserEquilibrium due = new DtmDiscreteDestinationDynamicUserEquilibrium(dnl, 6.0,
                odm, ue, 0.0, 2.0, intervals);
        due.setNumberSearchPerInterval(1);

        due.run(100, 0.0);

        System.out.println(dnl.statistics.getBackwardEdgeEventsCount());
        System.out.println(dnl.statistics.getForwardEdgeEventsCount());
        System.out.println(dnl.statistics.getMixtureEdgeEventsCount());
    }
}
