/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import org.junit.jupiter.api.BeforeAll;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

abstract class PostgresTestBase {
    static String dbUrl;

    @BeforeAll
    static public void init(){
        try{
            File file = new File("../api/src/main/resources/application.properties");
            Properties prop = new Properties();
            FileInputStream stream = new FileInputStream(file);
            prop.load(stream);
            dbUrl = prop.getProperty("dtm.db.url");
        } catch (IOException exception){
            System.out.println("Cannot load settings");
        }
    }
}
