/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * The origin-destination matrix representing the flows between the origin and destination zones for the dynamic traffic assignment. The flows can change in time.
 * @author Tomas Potuzak
 */
public class DtmOriginDestinationMatrix {
    /** The graph representing the road traffic network, with which this matrix is associated */
    protected DtmGraph graph;
    /** All the origin-destination pairs of this matrix */
    protected List<DtmOriginDestinationPair> pairs;
    /** All the zones of this matrix */
    protected List<DtmZone> zones;
    /** Fast zones access table utilized for direct access to zones based on their nodes */
    protected HashMap<DtmNode, DtmZone> zonesAccessTable;
    /** The origin zones of this matrix */
    protected List<DtmZone> originZones;
    /** The destination zones of this matrix */
    protected List<DtmZone> destinationZones;
    /** Fast origin destination pairs access table utilized for direct access to pairs based on their origin zones */
    protected HashMap<DtmZone, List<DtmOriginDestinationPair>> originZonesPairs;

    /**
     *
     * @param graph the graph, with which this matrix is associated
     * @throws NullPointerException if the graph is <code>null</code>
     */
    public DtmOriginDestinationMatrix(DtmGraph graph) throws NullPointerException {
        if (graph == null) {
            throw new NullPointerException("Graph cannot be null.");
        }
        else {
            this.graph = graph;

            pairs = new ArrayList<>();
            zones = new ArrayList<>();
            zonesAccessTable = new HashMap<>();
            originZones = new ArrayList<>();
            destinationZones = new ArrayList<>();
            originZonesPairs = new HashMap<>();
        }
    }

    /**
     *
     * @param graph the graph, with which this matrix is associated
     * @param originDestinationPairsData the triples of origin node ID, destination node ID, and flow. Each triple represents a single origin-destination pair
     * @throws NullPointerException if the graph and/or the origin-destination pairs data are <code>null</code>
     */
    public DtmOriginDestinationMatrix(DtmGraph graph, List<DtmTripleElement<Integer, Integer, DtmPiecewiseConstantFunction>> originDestinationPairsData) throws NullPointerException {
        this(graph);

        DtmZone originZone = null;
        DtmZone destinationZone = null;
        DtmNode originNode = null;
        DtmNode destinationNode = null;
        DtmOriginDestinationPair pair = null;
        HashSet<DtmZone> originZonesSet = new HashSet<>();
        HashSet<DtmZone> destinationZonesSet = new HashSet<>();
        List<DtmOriginDestinationPair> originZonePairs = null;

        for (DtmTripleElement<Integer, Integer, DtmPiecewiseConstantFunction> element: originDestinationPairsData) {
            originNode = graph.getNodeById(element.getFirst());
            destinationNode = graph.getNodeById(element.getSecond());

            if (zonesAccessTable.containsKey(originNode)) {
                originZone = zonesAccessTable.get(originNode);

                if (!originZonesSet.contains(originZone)) { //The zone is already known, but is not in the set of origin zones, its former occurrence must have been as a destination zone
                    originZonesSet.add(originZone);
                    originZones.add(originZone);
                }
            }
            else {
                originZone = new DtmZone(zones.size(), originNode);
                zones.add(originZone);
                zonesAccessTable.put(originNode, originZone);

                originZonesSet.add(originZone); //The zone is not yet contained in the access table, this must be its first occurrence
                originZones.add(originZone);
            }

            if (zonesAccessTable.containsKey(destinationNode)) {
                destinationZone = zonesAccessTable.get(destinationNode);

                if (!destinationZonesSet.contains(destinationZone)) { //The zone is already known, but is not in the set of destination zones, its former occurrence must have been as an origin zone
                    destinationZonesSet.add(destinationZone);
                    destinationZones.add(destinationZone);
                }
            }
            else {
                destinationZone = new DtmZone(zones.size(), destinationNode);
                zones.add(destinationZone);
                zonesAccessTable.put(destinationNode, destinationZone);

                destinationZonesSet.add(destinationZone); //The zone is not yet contained in the access table, this must be its first occurrence
                destinationZones.add(destinationZone);
            }

            pair = new DtmOriginDestinationPair(originZone, destinationZone, element.getThird());
            pairs.add(pair);

            if (originZonesPairs.containsKey(originZone)) {
                originZonesPairs.get(originZone).add(pair);
            }
            else {
                originZonePairs = new ArrayList<>();
                originZonePairs.add(pair);
                originZonesPairs.put(originZone, originZonePairs);
            }
        }
    }

    /**
     * Initializes origins and stores them to corresponding origin zones.
     */
    public void initializeOrigins() {
        List<DtmOriginDestinationPair> pairs = null;
        DtmPairElement<DtmPiecewiseConstantFunction, List<DtmMixture<DtmIdentifiable>>> combination = null;
        List<DtmMixture<DtmIdentifiable>> pairMixtures = null;
        List<DtmPairElement<DtmIdentifiable, Double>> pairMixturePoints = null;
        DtmOrigin origin = null;

        for (DtmZone originZone: originZones) {
            pairs = getOriginZonePairs(originZone);
            combination = new DtmPairElement<>(null, null);

            for (DtmOriginDestinationPair pair: pairs) {
                pairMixtures = new ArrayList<>();
                for (int i = 0; i < pair.getFlow().getLength(); i++) {
                    pairMixturePoints = new ArrayList<>();
                    pairMixturePoints.add(new DtmPairElement<>(pair.getDestination(), 1.0));
                    pairMixtures.add(new DtmMixture<DtmIdentifiable>(pairMixturePoints));
                }

                combination = DtmFunctionUtils.sumFlowFunctions(combination.getFirst(), pair.getFlow(), combination.getSecond(), pairMixtures);
            }

            origin = new DtmOrigin(combination.getFirst(), combination.getSecond(), originZone.getNode());
            originZone.setOrigin(origin);
        }
    }

    /**
     * Returns the zone associated with the specified node. If there is no zone associated to the specified node, <code>null</code> value is returned.
     * @param node the node, with which the searched zone shall be associated
     * @return the zone associated with the specified node. If there is no zone associated to the specified node, <code>null</code> value is returned.
     */
    public DtmZone getZoneByNode(DtmNode node) {
        return zonesAccessTable.get(node);
    }

    /**
     * Returns all the origin-destination pairs leading from the specified origin zone
     * @param originZone the origin zone, whose pairs should be returned
     * @return all the origin-destination pairs leading from the specified origin zone
     */
    public List<DtmOriginDestinationPair> getOriginZonePairs(DtmZone originZone) {
        return originZonesPairs.get(originZone);
    }

    //Getters and setters

    /**
     * Returns the graph, with which this matrix is associated
     * @return the graph, with which this matrix is associated
     */
    public DtmGraph getGraph() {
        return graph;
    }

    /**
     * Returns all the origin-destination pairs of this matrix
     * @return all the origin-destination pairs of this matrix
     */
    public List<DtmOriginDestinationPair> getPairs() {
        return pairs;
    }

    /**
     * Returns all the zones of this matrix
     * @return all the zones of this matrix
     */
    public List<DtmZone> getZones() {
        return zones;
    }

    /**
     * Returns the origin zones of this matrix
     * @return the origin zones of this matrix
     */
    public List<DtmZone> getOriginZones() {
        return originZones;
    }

    /**
     * Returns the destination zones of this matrix
     * @return the destination zones of this matrix
     */
    public List<DtmZone> getDestinationZones() {
        return destinationZones;
    }

    public HashMap<DtmNode, DtmZone> getZonesAccessTable() {
        return zonesAccessTable;
    }

    public HashMap<DtmZone, List<DtmOriginDestinationPair>> getOriginZonesPairs() {
        return originZonesPairs;
    }
}
