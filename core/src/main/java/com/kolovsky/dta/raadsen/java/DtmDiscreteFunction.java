/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represent discrete function (classic grid-based approach)
 * @author kolovsky
 */
public class DtmDiscreteFunction implements DtmFunction{
    protected double[] values;
    protected double minX;
    protected double maxX;

    /**
     * @param values Y values
     * @param minX minimal X value
     * @param maxX maximal X value
     */
    public DtmDiscreteFunction(double[] values, double minX, double maxX) {
        this.values = values;
        this.minX = minX;
        this.maxX = maxX;
    }

    /**
     * Init function
     * @param numBins number of time intervals
     * @param initValue initial Y value for every interval
     * @param minX minimal X value
     * @param maxX maximal X value
     */
    public DtmDiscreteFunction(int numBins, double initValue, double minX, double maxX){
        this.values = new double[numBins];
        if (initValue != 0.0){
            Arrays.fill(this.values, initValue);
        }
        this.minX = minX;
        this.maxX = maxX;
    }

    private DtmDiscreteFunction performOperation(DtmDiscreteFunction other, String operation){
        if (operation == null) {
            throw new NullPointerException("Arithmetic operation must not be null.");
        }
        if (getLength() == other.getLength() && maxX == other.getLastX() && minX == other.getFirstX()){
            double[] out = new double[getLength()];
            for (int i = 0; i < getLength(); i++){
                out[i] = DtmMathUtils.performOperation(values[i], other.getValues()[i], operation);
            }
            return new DtmDiscreteFunction(out, minX, maxX);
        }
        throw new IllegalArgumentException("Functions have not same parameters as length, maxX, minX.");
    }

    @Override
    public DtmFunction plus(DtmFunction other) throws IllegalArgumentException {
        if (other instanceof DtmDiscreteFunction) {
            return performOperation((DtmDiscreteFunction) other, "+");
        }
        else {
            throw new IllegalArgumentException("Function type is not supported.");
        }
    }

    @Override
    public DtmFunction minus(DtmFunction other) throws IllegalArgumentException {
        if (other instanceof DtmDiscreteFunction) {
            return performOperation((DtmDiscreteFunction) other, "-");
        }
        else {
            throw new IllegalArgumentException("Function type is not supported.");
        }
    }

    /**
     * Performs multiplication of this function by a specified constant
     * @param k the constant, with which this function shall be multiplied
     * @return this function times <code>k</code>
     */
    public DtmDiscreteFunction times(double k) throws IllegalArgumentException {
        double[] out = new double[getLength()];
        for (int i = 0; i < getLength(); i++) {
            out[i] = values[i] * k;
        }
        return new DtmDiscreteFunction(out, minX, maxX);
    }

    /**
     * Performs multiplication with other function
     * @param other other function, with which this function shall be multiplied
     * @return this function times other function
     * @throws IllegalArgumentException if the other is not an instance of the <code>DtmDiscreateFunction</code>
     */
    public DtmDiscreteFunction times(DtmFunction other) throws IllegalArgumentException {
        if (other instanceof DtmDiscreteFunction) {
            return performOperation((DtmDiscreteFunction) other, "*");
        }
        else {
            throw new IllegalArgumentException("Function type is not supported.");
        }
    }

    /**
     * Performs division by other function
     * @param other other function, by which this function shall be divided
     * @return this function divided by other function
     * @throws IllegalArgumentException if the other is not an instance of the <code>DtmDiscreateFunction</code>
     */
    public DtmDiscreteFunction dividedBy(DtmFunction other) throws IllegalArgumentException {
        if (other instanceof DtmDiscreteFunction) {
            return performOperation((DtmDiscreteFunction) other, "/");
        }
        else {
            throw new IllegalArgumentException("Function type is not supported.");
        }
    }

    @Override
    public double integrate(boolean useAbsoluteValues) {
        double sum = 0;
        for (int i = 0; i < getLength(); i++) {
            if (useAbsoluteValues){
                sum += Math.abs(values[i]);
            }
            else{
                sum += values[i];
            }
        }
        return sum / getLength() * (maxX - minX);
    }

    @Override
    public double valueAt(double x) {
        if (x >= maxX){
            return values[getLength() - 1];
        }
        if (x < minX){
            throw new IllegalArgumentException("Domain error. The value is lower than lower bound.");
        }

        double step = (maxX - minX) / getLength();
        int i = (int)((x - minX)/step);
        return values[i];
    }

    @Override
    public double minimum() {
        double minValue = Double.POSITIVE_INFINITY;
        for (int i = 0; i < getLength(); i++) {
            if (minValue > values[i]){
                minValue = values[i];
            }
        }
        return minValue;
    }

    @Override
    public double maximum() {
        double maxValue = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < getLength(); i++) {
            if (maxValue < values[i]){
                maxValue = values[i];
            }
        }
        return maxValue;
    }

    @Override
    public DtmFunction subFunction(double a, double b) {
        return null;
    }

    @Override
    public List<Double> getXValues() {
        ArrayList<Double> out = new ArrayList<>();
        double step = (maxX - minX) / getLength();
        for (int i = 0; i < getLength(); i++) {
            out.add(minX + i*step);
        }
        out.add(minX + getLength()*step);
        return out;
    }

    @Override
    public List<Double> getYValues() {
        ArrayList<Double> out = new ArrayList<>();
        for (int i = 0; i < getLength(); i++) {
            out.add(values[i]);
        }
        out.add(values[getLength()-1]);
        return out;
    }

    @Override
    public double getX(int index) throws IndexOutOfBoundsException {
        double step = (maxX - minX) / getLength();
        return minX + index*step;
    }

    @Override
    public double getY(int index) throws IndexOutOfBoundsException {
        return values[index];
    }

    @Override
    public int getLength() {
        return values.length;
    }

    @Override
    public double getFirstX() throws IndexOutOfBoundsException {
        return minX;
    }

    @Override
    public double getFirstY() throws IndexOutOfBoundsException {
        return values[0];
    }

    @Override
    public double getLastX() throws IndexOutOfBoundsException {
        return maxX;
    }

    @Override
    public double getLastY() throws IndexOutOfBoundsException {
        return values[getLength() - 1];
    }

    /**
     * Convert to piecewise-constant function
     */
    public DtmPiecewiseConstantFunction toPiecewiseConstant(){
        return new DtmPiecewiseConstantFunction(getXValues(), getYValues());
    }

    /**
     * Get Y values as array
     */
    public double[] getValues() {
        return values;
    }

    @Override
    public String toString() {
        return Arrays.toString(values);
    }

    @Override
    public DtmDiscreteFunction toDiscreteFunction(int numBins, double startX, double endX, double positionInInterval) {
        return new DtmDiscreteFunction(values.clone(), minX, maxX);
    }
}
