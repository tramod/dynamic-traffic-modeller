/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * A forward edge event for Dynamic Traffic Assignment - forward kinematic wave (movement of vehicles toward the end of the edge)
 * @author Tomas Potuzak
 */
public class DtmForwardEdgeEvent extends DtmForwardBackwardEdgeEvent {
    /** The comparison tolerance */
    protected double epsilon;
    /** The comparison tolerance for cumulative values */
    protected double cumulativeEpsilon;

    /**
     * Creates a new forward edge event
     * @param edge the edge of this event
     * @param flowTime the time of the current inflow
     * @param flow the current inflow
     * @param cumulativeFlow the current cumulative inflow
     * @param fanEvent the value of whether or not this event is a fan event
     * @param lastFanEvent the value of whether or not this event is the last fan event
     * @param epsilon the comparison tolerance
     * @param cumulativeEpsilon the comparison tolerance for cumulative values
     */
    public DtmForwardEdgeEvent(DtmEdge edge, double flowTime, double flow, double cumulativeFlow, boolean fanEvent, boolean lastFanEvent, double epsilon, double cumulativeEpsilon) {
        super(edge, flowTime, flow, cumulativeFlow, fanEvent, lastFanEvent);

        this.epsilon = epsilon;
        this.cumulativeEpsilon = cumulativeEpsilon;

        minimalTime = 0.0;
        maximalTime = Double.POSITIVE_INFINITY;
        predictedTime = calculatePredictedTime();
    }

    @Override
    public double calculateMinimalTime() {
        DtmForwardEdgeEvent previousEvent = (DtmForwardEdgeEvent) getPrevious();

        if (fanEvent) {
            if (previousEvent == null) {
                minimalTime = flowTime + edge.getLength() / edge.getFundamentalDiagram().calculateEtaI(0.0, flow);
            }
            else {
                minimalTime = flowTime + edge.getLength() / edge.getFundamentalDiagram().calculateEtaI(previousEvent.getFlow(), flow);
            }
        }
        else {
            minimalTime = flowTime + edge.getLength() / edge.getFundamentalDiagram().calculateGammaI(flow);
        }

        return minimalTime;
    }

    @Override
    public double calculateMaximalTime() {
        DtmForwardEdgeEvent nextEvent = (DtmForwardEdgeEvent) getNext();

        if (nextEvent != null) {
            if (fanEvent && !lastFanEvent) {
                maximalTime = flowTime + edge.getLength() / edge.getFundamentalDiagram().calculateEtaI(flow, nextEvent.getFlow());
            }
            else {
                maximalTime = nextEvent.getFlowTime() + edge.getLength() / edge.getFundamentalDiagram().calculateGammaI(flow);
            }
        }
        else {
            maximalTime = Double.POSITIVE_INFINITY;
        }

        return maximalTime;
    }

    @Override
    public double calculatePredictedTime() {
        if (edge.getOutflow().getYValues().get(edge.getOutflow().getYValues().size() - 1) != flow) {
            double lastCumulativeOutflowY = edge.getCumulativeOutflow().getYValues().get(edge.getCumulativeOutflow().getYValues().size() - 1);
            double lastOutflowX = edge.getOutflow().getXValues().get(edge.getOutflow().getXValues().size() - 1);
            double lastOutflowY = edge.getOutflow().getYValues().get(edge.getOutflow().getYValues().size() - 1);
            return (cumulativeFlow - lastCumulativeOutflowY - flow * (flowTime + edge.getLength() / edge.getFundamentalDiagram().calculateGammaI(flow)) + lastOutflowY * lastOutflowX + edge.getFundamentalDiagram().calculateXiI(flow, edge.getLength())) / (lastOutflowY - flow);
        }
        else {
            return -1.0;
        }
    }

    @Override
    public DtmNode perform(double time) {
        if (processed) {
            return null;
        }
        else {
            double newPredictedTime = calculatePredictedTime();

            if (Math.abs(time - newPredictedTime) < epsilon && epsilon + maximalTime >= newPredictedTime && newPredictedTime >= minimalTime - epsilon) {
                //Set demand (s)
                DtmPairElement<Double, Double> projection = edge.getCumulativeInflowDownstreamProjection(time, this, epsilon);
                double cumulativeDownstream = projection.getFirst();
                double flow = projection.getSecond();

                if (cumulativeDownstream - edge.getCumulativeOutflow(time) < cumulativeEpsilon) { //Number of vehicles unsuccessful in leaving the edge at time time equal to zero
                    edge.setQueuePresent(false);
                    edge.setDemand(flow);
                }
                else {
                    edge.setQueuePresent(true);
                    edge.setDemand(edge.getFundamentalDiagram().getFlowCapacity());
                }
                edge.setDemandLastUpdateTime(time);
                processed = true;
                DtmLogger.info(time, "event processed", edge);

                return edge.getEndNode();
            }
            else {
                DtmLogger.warning(time, "invalid forward event - new predicted time " + newPredictedTime, edge);
                return null;
            }
        }
    }

    /**
     * Calculates and returns the downstream projection of the cumulative inflow (U)
     * @param time the time, for which the down projection of the cumulative inflow (U) shall be calculated
     * @return the downstream projection of the cumulative inflow (U)
     */
    public double calculateCumulativeInflowDownstreamProjection(double time) {
        if (minimalTime <= time && maximalTime >= time) {
            return cumulativeFlow + flow * ((time - flowTime) - edge.getLength() / edge.getFundamentalDiagram().calculateGammaI(flow)) + edge.getFundamentalDiagram().calculateXiI(flow, edge.getLength());
        }
        else {
            return Double.POSITIVE_INFINITY;
        }
    }

    @Override
    public String toString() {
        return "ForwardEdgeEvent - time(min, pred, max): (" + minimalTime + ", " + predictedTime + ", " + maximalTime + "), flow: " + flow + ", processed: " + ((isProcessed()) ? "Y ": "N") + ", " + edge;
    }
}