/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.List;

/**
 * Increasing piecewise linear function for Dynamic Traffic Assignment
 * @author Tomas Potuzak
 */
public class DtmIncreasingPiecewiseLinearFunction extends DtmNonDecreasingPiecewiseLinearFunction {

    /**
     * Creates a new non-decreasing piecewise linear function with specified <i>x</i> and <i>y</i> values
     * @param xValues the <i>x</i> values of the function
     * @param yValues the <i>y</> = <i>f</i>(<i>x</i>) values of the function
     * @throws IllegalArgumentException if the numbers of <i>x</i> and <i>y</i> values are different or the <i>x</i> values are not increasing or the <i>y</i> values are not increasing
     * @throws NullPointerException if <i>x</i> and/or <i>y</i> values are <code>null</code>
     */
    public DtmIncreasingPiecewiseLinearFunction(List<Double> xValues, List<Double> yValues) throws IllegalArgumentException, NullPointerException {
        super(xValues, yValues);

        for (int i = 1; i < yValues.size(); i++) {
            if (yValues.get(i - 1) >= yValues.get(i)) {
                throw new IllegalArgumentException("The function is not increasing.");
            }
        }
    }

    /**
     * Creates a new increasing piecewise linear function with empty <i>x</i> and <i>y</i> values
     */
    public DtmIncreasingPiecewiseLinearFunction() {
        super();
    }

    @Override
    public void addPoint(double x, double y) throws IllegalArgumentException {
        super.addPoint(x, y);

        if (y <= yValues.get(yValues.size() - 1)) {
            throw new IllegalArgumentException("The function must be increasing. The added value is lower or equal to the last value.");
        }
    }

    /**
     * Creates and returns the inverse function to this function
     * @return the inverse function to this function
     */
    public DtmNonDecreasingPiecewiseLinearFunction inverseFuction() {
        return new DtmNonDecreasingPiecewiseLinearFunction(xValues, yValues);
    }

}

