/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.Arrays;

/**
 * Based on:
 *
 * G. Gentile, “Solving a Dynamic User Equilibrium model based on splitting rates with Gradient Projection algorithms,”
 * Transportation Research Part B: Methodological, vol. 92, pp. 120–147, Oct. 2016, doi: 10.1016/j.trb.2016.02.005.
 */
public class DtmQuasiGradientProjection implements DtmGradientProjection {
    protected double maximalShift;
    protected double generalGradient;
    protected double alpha;

    /**
     * Update proportions
     * @param generalGradient common gradient value
     * @param alpha step size
     */
    public DtmQuasiGradientProjection(double generalGradient, double alpha, double maximalShift) {
        this.generalGradient = generalGradient;
        this.alpha = alpha;
        this.maximalShift = maximalShift;
    }

    /**
     * Checks if the sum of values is one.
     */
    private void check(double[] values, double epsilon){
        double sum = 0.0;
        for (double v : values) {
            if (v != 0) {
                sum += v;
            }
        }
        if (Math.abs(1 - sum) > epsilon){
            throw new RuntimeException("The sum must be 1.0 not " + sum);
        }
        for (double v: values){
            if (v < 0){
                throw new RuntimeException("The value must be positive not "+v);
            }
        }
    }

    /**
     * Maintain the shift of proportion
     * @param proportions proportion for alternatives
     * @param cost travel time for alternatives
     */
    public double[] run(double[] proportions, double[] cost) {
        if (proportions.length != cost.length){
            throw new IllegalArgumentException("All arrays must have the same length");
        }

        // set of used alternatives
        boolean[] B = new boolean[cost.length];
        Arrays.fill(B, true);
        // shifts
        double[] shift = new double[cost.length];
        // new proportion
        double[] outP = new double[cost.length];

        boolean wasEliminated = true;
        while (wasEliminated){
            wasEliminated = false;

            // compute average cost from set B
            double sum = 0;
            int count = 0;
            for (int a = 0; a < cost.length; a++){
                if(B[a]){
                    sum += cost[a];
                    count++;
                }
            }
            double avgCost = sum/count;

            // compute shift
            for (int a = 0; a < cost.length; a++){
                if (B[a]){
                    //shift[a] = (avgCost - cost[a])/generalGradient * alpha;
                    shift[a] = (avgCost - cost[a])/avgCost * alpha;
                }
                else {
                    shift[a] = 0.0;
                }

                // eliminate non used alternative
                if (proportions[a] == 0.0 && shift[a] < 0.0){
                    B[a] = false;
                    wasEliminated = true;
                }
            }
        }

        // non-negatively correlation
        // compute beta (shortening coefficient)
        double beta = 1.0;
        for(int a = 0; a < cost.length; a++){
            if (B[a] && shift[a] < 0.0){
                double coef = - (proportions[a]/shift[a]);
                if (beta > coef){
                    beta = coef;
                }
            }
            if (B[a] && shift[a] > maximalShift){
                double coef = maximalShift/shift[a];
                if (beta > coef){
                    beta = coef;
                }
            }
            if (B[a] && shift[a] < - maximalShift){
                double coef = -(maximalShift/shift[a]);
                if (beta > coef){
                    beta = coef;
                }
            }
        }

        // compute updated proportion
        for(int a = 0; a < cost.length; a++){
            outP[a] = proportions[a] + beta*shift[a];
            if (outP[a] < 0.0 && - outP[a] < 10E-12){
                outP[a] = 0.0;
            }
            if(outP[a] < 10E-12){
                outP[a] = 0.0;
            }
        }
        makeSumOne(outP);
        check(outP, 1E-12);
        return outP;
    }

    private void makeSumOne(double[] p) {
        double sum = 0.0;
        int count = 0;
        for (double v : p) {
            if (v != 0) {
                sum += v;
                count++;
            }
        }
        double k = (1.0 - sum)/count;

        for (int a = 0; a < p.length; a++){
            if (p[a] != 0.0){
                p[a] = p[a] + k;
            }
        }

        var isBad = false;
        for (int a = 0; a < p.length; a++){
            if (p[a] < 0.0){
                p[a] = 0.0;
                isBad = true;
            }
        }
        if (isBad){
            makeSumOne(p);
        }
    }
}
