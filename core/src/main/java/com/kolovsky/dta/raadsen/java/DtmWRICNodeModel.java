/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Model according:
 *
 * Kolovský, F., Kolingerová, I., Martolos, J., & Potužák, T. (2021). Capacity based
 * first order node model for dynamic traffic loading.
 *
 * @author kolovsky
 */
public class DtmWRICNodeModel extends DtmDestinationNodeModel{
    double[] priorities;
    DtmPairElement<Double, Double>[][][] mutualRestrictionIntervals;
    // precision od the solution in veh/h
    final double SOL_PRECISION = 1.0;
    public DtmNodeCapacity capacityModel;

    class Rectangle{
        DtmPairElement<Double, Double> a;
        DtmPairElement<Double, Double> b;
        public Rectangle(DtmPairElement<Double, Double> a, DtmPairElement<Double, Double> b){
            this.a = a;
            this.b = b;
        }

        /**
         * Area of intersection
         */
        double intersectArea(Rectangle other){
            DtmPairElement<Double, Double> da = intersect(this.a, other.a);
            DtmPairElement<Double, Double> db = intersect(this.b, other.b);
            return abs(da) * abs(db);
        }

        /**
         * Area of the rectangle
         */
        double area(){
            return abs(a) * abs(b);
        }
    }

    /**
     * @param node          associated node for node model
     * @param distributions node distribution for each destination
     * @param mutualRestrictionIntervals [ij'j] how flow on movement ij' influence flow on movement ij
     */
    public DtmWRICNodeModel(DtmNode node, HashMap<DtmIdentifiable, DtmDistributionMatrix> distributions,
                            double[] priorities, DtmPairElement<Double, Double>[][][] mutualRestrictionIntervals, DtmNodeCapacity capacityModel) {
        super(node, distributions);
        this.priorities = priorities;
        this.mutualRestrictionIntervals = mutualRestrictionIntervals;
        this.capacityModel = capacityModel;
    }

    @Override
    public double[][] solveUniversal(double[][] demand, double[] supply){
        if (capacityModel instanceof DtmFixedCapacity){
            double[][] dem = constrainedDemand(demand, capacityModel.capacity(null));
            return solveWRI(dem, supply);
        }
        return steffensen(demand, supply);
    }

    /**
     * Solve fixed-point problem using Steffensen’s method and Aitken’s delta squared method
     */
    private double[][] steffensen(double[][] demand, double[] supply){
        double[][] q = demand;
        double[][] qOld;
        int k = 0;
        double d = Double.POSITIVE_INFINITY;
        while (d > SOL_PRECISION){
            qOld = q;
            double[][] cap = capacityModel.capacity(q);
            double[][] dem = constrainedDemand(demand, cap);
            double[][] p1 = solveWRI(dem, supply);

            cap = capacityModel.capacity(p1);
            dem = constrainedDemand(demand, cap);
            double[][] p2 = solveWRI(dem, supply);

            double[][] o = new double[demand.length][supply.length];
            for (int i = 0; i < demand.length; i++){
                for (int j = 0; j < supply.length; j++){
                    double denominator = p2[i][j] - 2 * p1[i][j] + q[i][j];
                    if (denominator != 0){
                        double shift = (p1[i][j] - q[i][j])*(p1[i][j] - q[i][j]) / denominator;
                        o[i][j] = q[i][j] - shift;
                    }
                    else{
                        o[i][j] = q[i][j];
                    }
                }
            }
            q = o;
            k += 1;
            d = diffMatrix(q, qOld);
        }
        return q;
    }

    private double diffMatrix(double[][] m, double[][] mo){
        double sum = 0;
        for (int i = 0; i < m.length; i++){
            for(int j = 0; j < m[i].length; j++){
                sum += Math.abs(m[i][j] - mo[i][j]);
            }
        }
        return sum;
    }

    /**
     * Solve model by
     *
     * WRIGHT, Matthew A., et al. On node models for high-dimensional road networks.
     * Transportation Research Part B: Methodological, 2017, 105: 212-234.
     */
    public double[][] solveWRI(double[][] demand, double[] supply) {
        // shortcut
        DtmPairElement<Double, Double>[][][] mri = mutualRestrictionIntervals;
        // INITIALIZATION
        double[] Rw = supply.clone();
        double[][] q = new double[demand.length][demand[0].length];

        // init sets U_j and set V
        @SuppressWarnings("unchecked")
        Set<Integer>[] U = (Set<Integer>[]) Array.newInstance(Set.class, supply.length);
        for (int j = 0; j < supply.length; j++){
            U[j] = new HashSet<>();
        }

        double[][] S = new double[demand.length][supply.length];
        @SuppressWarnings("unchecked")
        DtmPairElement<Double, Double>[][] ni = (DtmPairElement<Double, Double>[][]) Array.newInstance(DtmPairElement.class, demand.length, supply.length);

        for (int i = 0; i < demand.length; i++){
            for (int j = 0; j < demand[i].length; j++){
                if (demand[i][j] > 0.0){
                    U[j].add(i);
                }
                S[i][j] = demand[i][j];
                ni[i][j] = new DtmPairElement<>(0.0, 0.0);
            }
        }

        Set<Integer> V = new HashSet<>();
        for (int j = 0; j < supply.length; j++){
            if (!U[j].isEmpty()){
                V.add(j);
            }
        }

        // 3. ORIENTED PRIORITIES
        double[] incomingDemand = new double[demand.length];
        for (int i = 0; i < incomingDemand.length; i++){
            incomingDemand[i] = 0;
            for (int j = 0; j < demand[i].length; j++){
                incomingDemand[i] += demand[i][j];
            }
        }
        double[][] p = new double[demand.length][supply.length];
        for (int i = 0; i < p.length; i++){
            for (int j = 0; j < p[i].length; j++){
                p[i][j] = priorities[i] * demand[i][j]/incomingDemand[i];
            }
        }

        int k = 0;
        while (!V.isEmpty()){
            // 4. FACTORS
            double minA = Double.POSITIVE_INFINITY;
            int minJ = -1;
            for (int j: V){
                double sumP = 0.0;
                for (int i: U[j]){
                    sumP += p[i][j];
                }
                double a = Rw[j]/sumP;

                if (a < minA){
                    minA = a;
                    minJ = j;
                }
            }

            // 5. ....
            Set<Integer> Uw = new HashSet<>();
            for (int i = 0; i < S.length; i++){
                double Si = 0.0;
                for (int j = 0; j < S[i].length; j++){
                    Si += S[i][j];
                }
                // only for "i" in U[minJ]
                if (U[minJ].contains(i) && Si <= minA * priorities[i]){
                    Uw.add(i);
                }
            }

            // case U(k) != 0
            if (!Uw.isEmpty()){
                for (int j: V){
                    double sum = 0.0;
                    for(int i: Uw){
                        q[i][j] = S[i][j];
                        sum += S[i][j];
                        U[j].remove(i);
                    }
                    Rw[j] -= sum;
                }
            }
            else {
                for (int i: U[minJ]){
                    S[i][minJ] = p[i][minJ] * minA;
                }

                for (int i: U[minJ]){
                    for (int j: V){
                        if(j != minJ && U[j].contains(i)){
                            double inter = abs(mri[i][minJ][j]) - abs(intersect(ni[i][j], mri[i][minJ][j]));
                            S[i][j] -= demand[i][j] * inter * (1 - S[i][minJ] / demand[i][minJ]);
                        }
                    }
                }

                for (int i: new HashSet<>(U[minJ])){
                    for (int j: V){
                        ni[i][j] = union(ni[i][j], mri[i][minJ][j]);
                        if (abs(ni[i][j]) > 1.0 - 1E-10){
                            if (U[j].contains(i)){
                                q[i][j] = S[i][j];
                            }
                            Rw[j] -= q[i][j];
                            U[j].remove(i);
                        }
                    }
                }
            }

            V.clear();
            for (int j = 0; j < supply.length; j++){
                if (!U[j].isEmpty()){
                    V.add(j);
                }
            }
            k += 1;
        }

        return q;
    }

    /**
     * Union two intervals TODO do not reflect the situation when the intervals do not intersect e.g (0, 0.3) and (0.5, 0.8)
     */
    private DtmPairElement<Double, Double> union(DtmPairElement<Double, Double> a, DtmPairElement<Double, Double> b){
        if (a.first == 0.0 && a.second == 0.0){ return b; }
        if (b.first == 0.0 && b.second == 0.0){ return a; }
        return new DtmPairElement<>(Math.min(a.first, b.first), Math.max(a.second, b.second));
    }

    private double abs(DtmPairElement<Double, Double> a){
        return a.second - a.first;
    }

    private DtmPairElement<Double, Double> intersect(DtmPairElement<Double, Double> a, DtmPairElement<Double, Double> b){
        if ((a.first == 0.0 && a.second == 0.0) || (b.first == 0.0 && b.second == 0.0)){
            return new DtmPairElement<>(0.0, 0.0);
        }
        if (a.second < b.first || b.second < a.first){
            return new DtmPairElement<>(0.0, 0.0);
        }
        return new DtmPairElement<>(Math.max(a.first, b.first), Math.min(a.second, b.second));
    }

    /**
     * Return constrained demand by internal node supply constraints represented by capacity
     * @param demand original demand
     * @param capacity internal node supply constraints
     * @return constrained demand
     */
    double[][] constrainedDemand(double[][] demand, double[][] capacity){
        double[][] d = new double[demand.length][demand[0].length];
        for (int i = 0; i < d.length; i++){
            for (int j = 0; j < d[0].length; j++){
                ArrayList<Rectangle> rectangles = new ArrayList<>();
                for (int jj = 0; jj < d[0].length; jj++){
                    if (capacity[i][jj] < demand[i][jj]){
                        DtmPairElement<Double, Double> b = new DtmPairElement<>(capacity[i][jj]/demand[i][jj]
                                * demand[i][j], demand[i][j]);
                        rectangles.add(new Rectangle(mutualRestrictionIntervals[i][jj][j], b));
                    }
                }
                d[i][j] = demand[i][j] - union(rectangles);
            }
        }
        return d;
    }

    /**
     * Area of union of rectangles
     */
    private double union(ArrayList<Rectangle> rectangles){
        double sum = 0.0;
        int i = 0;
        for (Rectangle rectangle: rectangles){
            sum += rectangle.area();
            for (int j = i + 1; j < rectangles.size(); j++){
                sum -= rectangle.intersectArea(rectangles.get(j));
            }
            i += 1;
        }
        return sum;
    }
}
