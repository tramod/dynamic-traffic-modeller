/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.List;

/**
 * Piecewise linear function for Dynamic Traffic Assignment
 * @author Tomas Potuzak
 */
public class DtmPiecewiseLinearFunction implements DtmFunction {
    /** The <i>x</i> values of the function */
    protected List<Double> xValues;
    /** The <i>y</> = <i>f</i>(<i>x</i>) values of the function */
    protected List<Double> yValues;

    /**
     * Creates a new piecewise linear function with specified <i>x</i> and <i>y</i> values
     * @param xValues the <i>x</i> values of the function
     * @param yValues the <i>y</> = <i>f</i>(<i>x</i>) values of the function
     * @throws IllegalArgumentException if the numbers of <i>x</i> and <i>y</i> values are different or the <i>x</i> values are not increasing
     * @throws NullPointerException if <i>x</i> and/or <i>y</i> values are <code>null</code>
     */
    public DtmPiecewiseLinearFunction(List<Double> xValues, List<Double> yValues) throws IllegalArgumentException, NullPointerException {
        this.xValues = new ArrayList<>();
        this.yValues = new ArrayList<>();

        if (xValues != null && yValues != null && xValues.size() == yValues.size()) {
            this.xValues.addAll(xValues);
            this.yValues.addAll(yValues);
        }
        else if (xValues.size() != yValues.size()) {
            throw new IllegalArgumentException("Number of x and y values must be identical.");
        }
        else {
            throw new NullPointerException("Both x and y values must not be null.");
        }

        for (int i = 1; i < xValues.size(); i++) {
            if (xValues.get(i - 1) >= xValues.get(i)) {
                throw new IllegalArgumentException("x[i - 1] is not smaller than x[i] : " + xValues.get(i - 1) + " >= " + xValues.get(i));
            }
        }

    }

    /**
     * Creates a new piecewise linear function with empty <i>x</i> and <i>y</i> values
     */
    public DtmPiecewiseLinearFunction() {
        xValues = new ArrayList<>();
        yValues = new ArrayList<>();
    }

    /**
     * Adds a point to the end of this function
     * @param x the <i>x</i> value
     * @param y the <i>y</i> value at <code>x</code> (<i>f</i>(<i>x</i>))
     * @throws IllegalArgumentException if the <i>x</i> and/or <i>x</i> value is infinite or NaN or the <i>x</i> value is not larger that the last value of this function
     */
    public void addPoint(double x, double y) throws IllegalArgumentException {
        if (Double.isInfinite(x) || Double.isInfinite(y) || Double.isNaN(x) || Double.isNaN(y)) {
            throw new IllegalArgumentException("Both x and y values must be regular numbers (i.e., finite)");
        }
        else {
            if (xValues.isEmpty() || xValues.get(xValues.size() - 1) < x) {
                xValues.add(x);
                yValues.add(y);
            }
            else {
                //TODO Zakomentovano stejne jako v puvodni implementaci - vede k chybe pri volani FunctionUtils.calculateHorizontalDifferene()
                //throw new IllegalArgumentException("The x value must be larger than last value of the function " + xValues.get(xValues.size() - 1));
            }
        }
    }

    /**
     * Adds a point to the start of this function
     * @param x the <i>x</i> value
     * @param y the <i>y</i> value at <code>x</code> (<i>f</i>(<i>x</i>))
     * @throws IllegalArgumentException if the <i>x</i> and/or <i>x</i> value is infinite or NaN or the <i>x</i> value is larger that the first value of this function
     */
    public void addPointToStart(double x, double y) throws IllegalArgumentException {
        if (Double.isInfinite(x) || Double.isInfinite(y) || Double.isNaN(x) || Double.isNaN(y)) {
            throw new IllegalArgumentException("Both x and y values must be regular numbers (i.e., finite)");
        }
        else {
            if (xValues.isEmpty()) {
                xValues.add(x);
                yValues.add(y);
            }
            else if (x < xValues.get(0)) {
                xValues.add(0, x);
                yValues.add(0, y);
            }
            else {
                throw new IllegalArgumentException("The x value must be lower than the first value of the function " + xValues.get(0));
            }
        }
    }

    /**
     * Updates the point of specified index of this function
     * @param index the index of the point
     * @param x the <i>x</i> value
     * @param y the <i>y</i> value at <code>x</code> (<i>f</i>(<i>x</i>))
     * @throws IllegalArgumentException if the <code>index</code> is out of range or the <code>x</code> does not correspond to the <code>index</code>
     */
    public void updatePoint(int index, double x, double y) throws IllegalArgumentException {
        if (index < 0 || index >= xValues.size()) {
            throw new IllegalArgumentException("The index of the point is out of range.");
        }
        else if (index > 0 && index < xValues.size() - 1) { //A point in inner parts of the function - not at the ends
            if (x <= xValues.get(index - 1) || x >= xValues.get(index + 1)) {
                throw new IllegalArgumentException("The x value must be between the neighboring values.");
            }
            else { //Everything ok, setting the x and y values
                xValues.set(index, x);
                yValues.set(index, y);
            }
        }
        else if (index == 0 && xValues.size() == 1) { //There is just one point (this should probably not happen)
            xValues.set(0, x);
            yValues.set(0, y);
        }
        else if (index == 0) {
            if (x >= xValues.get(index + 1)) { //The point at the start
                throw new IllegalArgumentException("The x value must be between the neighboring values.");
            }
            else { //Everything ok, setting the x and y values
                xValues.set(index, x);
                yValues.set(index, y);
            }
        }
        else if (index == xValues.size() - 1) { //The point at the end
            if (x <= xValues.get(index - 1)) {
                throw new IllegalArgumentException("The x value must be between the neighboring values.");
            }
            else { //Everything ok, setting the x and y values
                xValues.set(index, x);
                yValues.set(index, y);
            }
        }
    }

    /**
     * Returns the slope of the specified line segment
     * @param index the index of the end point of the line segment in range <1; xValues.size() - 1>
     * @return the slope of the specified line segment
     */
    public double getSlope(int index) {
        if (index <= 0 || index >= xValues.size()) {
            System.out.println(index);
            throw new IllegalArgumentException("The index of end point of the segment must be in range <1; " + (xValues.size() - 1) + ">");
        }
        else {
            return (yValues.get(index) - yValues.get(index - 1)) / (xValues.get(index) - xValues.get(index - 1));
        }
    }

    /**
     * Returns the vertical shift (of the linear function) of the specified line segment
     * @param index the index of the end point of the line segment in range <1; xValues.size() - 1>
     * @return the vertical shift (of the linear function) of the specified line segment
     * @throws IllegalArgumentException if the index is out of range
     */
    public double getVerticalShift(int index) throws IllegalArgumentException {
        if (index <= 0 || index >= xValues.size()) {
            throw new IllegalArgumentException("The index of end point of the segment must be in range <1; " + (xValues.size() - 1) + ">");
        }
        else {
            return yValues.get(index - 1) - (xValues.get(index - 1) * getSlope(index));
        }
    }

    @Override
    public DtmPiecewiseLinearFunction plus(DtmFunction other) throws IllegalArgumentException {
        if (other instanceof DtmPiecewiseLinearFunction) {
            return performOperation((DtmPiecewiseLinearFunction) other, "+");
        }
        else {
            throw new IllegalArgumentException("Function type is not supported.");
        }
    }

    @Override
    public DtmPiecewiseLinearFunction minus(DtmFunction other) throws IllegalArgumentException {
        if (other instanceof DtmPiecewiseLinearFunction) {
            return performOperation((DtmPiecewiseLinearFunction) other, "-");
        }
        else {
            throw new IllegalArgumentException("Function type is not supported.");
        }
    }

    /**
     * Performs multiplication of this function by a specified constant
     * @param k the constant, with which this function shall be multiplied
     * @return this function times <code>k</code>
     */
    public DtmPiecewiseLinearFunction times(double k) throws IllegalArgumentException {
        List<Double> resultXValues = new ArrayList<>(xValues);
        List<Double> resultYValues = new ArrayList<>();

        for (int i = 0; i < resultXValues.size(); i++) {
            resultYValues.add(k * yValues.get(i));
        }

        return new DtmPiecewiseLinearFunction(resultYValues, resultYValues);
    }

    /**
     * Performs a specified arithmetic operation of this and other functions
     * @param other the function with which the arithmetic operation shall be performed
     * @param operation the operator of the arithmetic operation
     * @return the resulting function
     * @throws IllegalArgumentException if the specified operation is not supported or
     * @throws NullPointerException if the specified operation is <code>null</code>
     */
    protected DtmPiecewiseLinearFunction performOperation(DtmPiecewiseLinearFunction other, String operation) throws IllegalArgumentException, NullPointerException {
        List<Double> resultXValues = new ArrayList<>();
        List<Double> resultYValues = new ArrayList<>();
        double x = 0.0;
        double y = 0.0;
        int i = 0;
        int j = 0;

        while (i < xValues.size() && j < other.xValues.size()) {
            if (xValues.get(i) == other.xValues.get(j)) {
                x = xValues.get(i);
                y = DtmMathUtils.performOperation(yValues.get(i), other.yValues.get(j), operation);

                i++;
                j++;
            }
            else if (xValues.get(i) < other.xValues.get(j)) {
                x = xValues.get(i);
                y = DtmMathUtils.performOperation(yValues.get(i), DtmMathUtils.interpolateLine(j, other.xValues, other.yValues, x), operation);

                i++;
            }
            else {
                x = other.xValues.get(j);
                y = DtmMathUtils.performOperation(DtmMathUtils.interpolateLine(i, xValues, yValues, x), other.yValues.get(j), operation);

                j++;
            }

            resultXValues.add(x);
            resultYValues.add(y);
        }


        return new DtmPiecewiseLinearFunction(resultXValues, resultYValues);
    }



    @Override
    public double integrate(boolean useAbsoluteValues) {
        double integral = 0.0;
        double dx = 0.0;

        for (int i = 1; i < xValues.size(); i++) {
            dx = xValues.get(i) - xValues.get(i - 1);
            if (yValues.get(i - 1) >= 0 && yValues.get(i) >= 0) {
                integral += DtmMathUtils.calculateTrapezoidArea(yValues.get(i - 1), yValues.get(i), dx);
            }
            else if (yValues.get(i - 1) < 0 && yValues.get(i) < 0) {
                if (useAbsoluteValues) {
                    integral -= DtmMathUtils.calculateTrapezoidArea(yValues.get(i - 1), yValues.get(i), dx);
                }
                else {
                    integral += DtmMathUtils.calculateTrapezoidArea(yValues.get(i - 1), yValues.get(i), dx);
                }
            }
            else if (yValues.get(i - 1) < 0) {
                double denominator = 2 * (yValues.get(i) - yValues.get(i - 1));
                double negativePart = (dx * DtmMathUtils.square(yValues.get(i - 1))) / denominator;
                double positivePart = (dx * DtmMathUtils.square(yValues.get(i))) / denominator;

                if (useAbsoluteValues) {
                    integral += positivePart + negativePart;
                }
                else {
                    integral += positivePart - negativePart;
                }
            }
            else {
                double denominator = 2 * (yValues.get(i - 1) - yValues.get(i));
                double negativePart = (dx * DtmMathUtils.square(yValues.get(i))) / denominator;
                double positivePart = (dx * DtmMathUtils.square(yValues.get(i - 1))) / denominator;

                if (useAbsoluteValues) {
                    integral += positivePart + negativePart;
                }
                else {
                    integral += positivePart - negativePart;
                }
            }
        }

        return integral;
    }

    @Override
    public double valueAt(double x) {
        if (x == xValues.get(xValues.size() - 1)) {
            return yValues.get(yValues.size() - 1);
        }
        else {
            int index = DtmFunctionUtils.binarySearch(xValues, x); //TODO overit, ze se to chova stejne jako Scala implementace
            return DtmMathUtils.interpolateLine(index + 1, xValues, yValues, x);
        }
    }

    @Override
    public double minimum() {
        double minimum = yValues.get(0);

        for (int i = 1; i < yValues.size(); i++) {
            if (yValues.get(i) < minimum) {
                minimum = yValues.get(i);
            }
        }

        return minimum;
    }

    @Override
    public double maximum() {
        double maximum = yValues.get(0);

        for (int i = 1; i < yValues.size(); i++) {
            if (yValues.get(i) > maximum) {
                maximum = yValues.get(i);
            }
        }

        return maximum;
    }

    @Override
    public DtmPiecewiseLinearFunction subFunction(double a, double b) {
        int startIndex = DtmFunctionUtils.binarySearch(xValues, a);
        int endIndex = DtmFunctionUtils.binarySearch(xValues, b);
        if (xValues.get(endIndex) < b) {
            endIndex++;
        }

        List<Double> resultXValues = new ArrayList<>();
        List<Double> resultYValues = new ArrayList<>();

        resultXValues.add(a);
        resultYValues.add(yValues.get(startIndex));
        for (int i = startIndex + 1; i < endIndex; i++) {
            resultXValues.add(xValues.get(i));
            resultYValues.add(yValues.get(i));
        }
        resultXValues.add(b);
        resultYValues.add(yValues.get(endIndex));

        return new DtmPiecewiseLinearFunction(resultXValues, resultYValues);
    }

    /**
     *
     * @param firstX
     * @param firstY
     * @return
     * @throws IllegalArgumentException
     */
    public DtmPiecewiseLinearFunction precede(double firstX, double firstY) throws IllegalArgumentException {
        if (Double.isInfinite(firstX) || Double.isInfinite(firstY) || Double.isNaN(firstX) || Double.isNaN(firstY)) {
            throw new IllegalArgumentException("Both x and y values must be regular numbers (i.e., finite)");
        }
        else {
            if (xValues.isEmpty() || firstX < xValues.get(0)) {
                List<Double> resultXValues = new ArrayList<>();
                List<Double> resultYValues = new ArrayList<>();
                resultXValues.add(firstX);
                resultYValues.add(firstY);

                if (!xValues.isEmpty()) {
                    resultXValues.addAll(xValues);
                    resultYValues.addAll(yValues);
                }

                return new DtmPiecewiseLinearFunction(resultXValues, resultYValues);
            }
            else {
                throw new IllegalArgumentException("The x value must be lower than the first value of the function " + xValues.get(0));
            }
        }
    }

    /**
     *
     * @param bound
     * @param index
     * @param slope
     * @param maximalAbsoluteError
     * @return
     */
    protected double calculateXForBound(double bound, int index, double slope, double maximalAbsoluteError) {
        return xValues.get(index - 1) + (bound - yValues.get(index - 1) + maximalAbsoluteError) / slope;
    }

    public DtmPiecewiseConstantFunction toPiecewiseConstantFunctionHeuristic(double maximalAbsoluteError, double positionInterval){
        int numBins = (int) ((getLastX() - getFirstX())/maximalAbsoluteError);
        return this.toDiscreteFunction(numBins, getFirstX(), getLastX(), positionInterval).toPiecewiseConstant().simplify(maximalAbsoluteError);
    }

    /**
     * Approximates this piecewise linear by a new piecewise constant function with specified maximal absolute error and returns it
     * Inspired by H. Konno and T. Kuno, "Best piecewise constant approximation of a function of single variable," Operations Research Letters, vol. 7, no. 4, pp. 205–210, Aug. 1988, DOI: 10.1016/0167-6377(88)90030-2.
     * @return piecewise constant approximation of this function
     */
    public DtmPiecewiseConstantFunction toPiecewiseConstantFunction(double maximalAbsoluteError) {
        if (yValues.isEmpty()) {
            return new DtmPiecewiseConstantFunction();
        }
        else {
            double upperBound = yValues.get(0) + maximalAbsoluteError;
            double lowerBound = yValues.get(0) - maximalAbsoluteError;
            double lastPointX = xValues.get(0);
            double slope = 0.0;
            double newPointX = 0.0;
            double newPointY = 0.0;
            double newPointXRelative = 0.0;
            int i = 1;
            DtmPiecewiseConstantFunction piecewiseConstantFunction = new DtmPiecewiseConstantFunction();

            while (i < xValues.size()) {
                slope = getSlope(i);

                if (slope > 0) { //Increasing segment
                    newPointXRelative = (upperBound - lowerBound) / slope;
                    newPointY = lowerBound + slope * newPointXRelative;
                    newPointX = newPointXRelative + calculateXForBound(lowerBound, i, slope, maximalAbsoluteError);

                    if (newPointX < xValues.get(i)) {
                        piecewiseConstantFunction.addPoint(lastPointX, newPointY);
                        lowerBound = newPointY;
                        upperBound = lowerBound + 2 * maximalAbsoluteError;
                        lastPointX =  newPointX;
                    }
                    else {
                        lowerBound += slope * (xValues.get(i) - calculateXForBound(lowerBound, i, slope, maximalAbsoluteError));
                        i++;
                    }
                }
                else if (slope < 0) { //Decreasing segment
                    newPointXRelative = (lowerBound - upperBound) / slope;
                    newPointY = upperBound + slope * newPointXRelative;
                    newPointX = newPointXRelative + calculateXForBound(upperBound, i, slope, -maximalAbsoluteError);

                    if (newPointX < xValues.get(i)) {
                        piecewiseConstantFunction.addPoint(lastPointX, newPointY);
                        upperBound = newPointY;
                        lowerBound = lowerBound - 2 * maximalAbsoluteError;
                        lastPointX =  newPointX;
                    }
                    else {
                        upperBound += slope * (xValues.get(i) - calculateXForBound(upperBound, i, slope, -maximalAbsoluteError));
                        i++;
                    }
                }
                else { //Constant segment - unlikely to occur due to the double comparison issue
                    i++;
                }
            }
            piecewiseConstantFunction.addPoint(lastPointX, (lowerBound + upperBound) / 2);
            if (lastPointX != xValues.get(xValues.size() - 1)) { //TODO potential problem due to the double comparison issue
                piecewiseConstantFunction.addPoint(xValues.get(xValues.size() - 1), (lowerBound + upperBound) / 2);
            }
            return piecewiseConstantFunction;
        }
    }

    /**
     * Returns this function as a new non-decreasing piecewise linear function
     * @return this function as a new non-decreasing piecewise linear function
     * @throws IllegalArgumentException if this function is not non-decreasing
     */
    public DtmNonDecreasingPiecewiseLinearFunction toNonDecreasingPiecewiseLinearFunction() throws IllegalArgumentException {
        return new DtmNonDecreasingPiecewiseLinearFunction(xValues, yValues);
    }

    /**
     * Returns this function as a new increasing piecewise linear function
     * @return this function as a new increasing piecewise linear function
     * @throws IllegalArgumentException if this function is not increasing
     */
    public DtmIncreasingPiecewiseLinearFunction toIncreasingPiecewiseLinearFunction() throws IllegalArgumentException {
        return new DtmIncreasingPiecewiseLinearFunction(xValues, yValues);
    }

    /**
     * Recursively determines and returns the indices for the <code>simplify()</code> method
     * @param lower lower bound
     * @param upper upper bound
     * @param errors
     * @return the indices for the <code>simplify()</code> method
     */
    protected List<Integer> getPointIndices(int lower, int upper, List<Double> errors) {
        if (upper - lower == 1) {
            return new ArrayList<>();
        }
        else {
            double alpha = 0.0;
            double maximalAlpha = 0.0;
            int maximalAlphaIndex = 0;

            for (int index = lower + 1; index < upper; index++) {
                alpha = Math.abs(getY(index) - (((getY(upper) - getY(lower)) / (getX(upper) - getX(lower))) * (getX(index) - getX(lower)) + getY(lower)));

                if (alpha > maximalAlpha && alpha > errors.get(index)) {
                    maximalAlpha = alpha;
                    maximalAlphaIndex = index;
                }
            }

            if (maximalAlpha == 0.0) {
                return new ArrayList<>();
            }
            else {
                List<Integer> result = new ArrayList<>();
                result.addAll(getPointIndices(lower, maximalAlphaIndex, errors));
                result.add(maximalAlphaIndex);
                result.addAll(getPointIndices(maximalAlphaIndex, upper, errors));
                return result;
            }
        }
    }

    /**
     * Approximates this function with a simpler piecewise linear function with specified error and returns it. The error can be relative to the individual <i>y</i> values or can be absolute based on the <code>useAbsoluteError</code> flag
     * The implementation utilizes Dougles-Peulker algorithm
     * @param error the maximal allowed error for each <i>y</i> value of this function. The value can be the same (absolute) for all <i>y</i> values or can be relative to each <i>y</i> value (i.e., multiplied by each <i>y</i> value)
     * @param useAbsoluteError determines whether the specified error will be uses as absolute for each <i>y</i> value or will be relative to each <i>y</i> value (i.e., multiplied by each <i>y</i> value)
     * @return a simpler piecewise linear function with specified error
     */
    public DtmPiecewiseLinearFunction simplify(double error, boolean useAbsoluteError) {
        List<Double> errors = new ArrayList<>();
        if (useAbsoluteError) {
            for (int i = 0; i < getLength(); i++) {
                errors.add(error);
            }
        }
        else {
            for (int i = 0; i < getLength(); i++) {
                errors.add(getY(i) * error);
            }
        }

        return simplify(errors);
    }

    /**
     * Approximates this function with a simpler piecewise linear function with specified errors for individual points of the the function and returns it
     * The implementation utilizes Dougles-Peulker algorithm
     * @param errors the maximal allowed error for each <i>y</i> value of this function
     * @return a simpler piecewise linear function with specified errors for individual points of the function
     */
    public DtmPiecewiseLinearFunction simplify(List<Double> errors) {
        List<Integer> indices = new ArrayList<>();
        indices.add(0);
        indices.addAll(getPointIndices(0, getLength() - 1, errors));
        indices.add(getLength() - 1);
        indices.sort(null);

        DtmPiecewiseLinearFunction simplifiedFunction = new DtmPiecewiseLinearFunction();
        for (int i: indices) {
            simplifiedFunction.addPoint(getX(i), getY(i));
        }

        return simplifiedFunction;
    }

    /**
     * Creates and returns the maximum function of this function and the constant function with specified <i>y</i> value on the specified interval
     * @param xInterval the interval, on which the constant function is defined. For <code>null</code> value, the entire range of this function is considered.
     * @param y the function value of the constant function
     * @return the maximum function of this function
     * and the constant function with specified <i>y</i> value on the specified interval
     */
    public DtmPiecewiseLinearFunction maximumFunction(DtmPairElement<Double, Double> xInterval, double y) {
        DtmPiecewiseLinearFunction maximumFunction = new DtmPiecewiseLinearFunction();

        if (xInterval != null) {  //TODO nechybi neco podobneho i pro pravou hranici def. oboru?
            if (xInterval.getFirst() < getX(0)) {
                maximumFunction.addPoint(xInterval.getFirst(), y);
            }
        }

        int lastPosition = 0;

        if (getFirstY() > y) {
            maximumFunction.addPoint(getFirstX(), getFirstY());
            lastPosition = 2;
        }
        else if (getFirstY() == y) {
            maximumFunction.addPoint(getFirstX(), getFirstY());
            lastPosition = 1;
        }
        else {
            maximumFunction.addPoint(getFirstX(), y);
            lastPosition = 0;
        }

        for (int i = 1; i < getLength(); i++) {
            if (getY(i) > y) {
                if (lastPosition == 0) {
                    maximumFunction.addPoint(DtmMathUtils.interpolateLine(i, yValues, xValues, y), y);
                    maximumFunction.addPoint(getX(i), getY(i));
                }
                else {
                    maximumFunction.addPoint(getX(i), getY(i));
                }
                lastPosition = 2;
            }
            else if (getY(i) == y) {
                maximumFunction.addPoint(getX(i), getY(i));
                lastPosition = 1;
            }
            else {
                if (lastPosition == 2) {
                    maximumFunction.addPoint(getX(i - 1) + (getY(i - 1) - y) / (getY(i - 1) - getY(i)) * (getX(i) - getX(i - 1)), y);
                }
                lastPosition = 0;
            }
        }

        if (maximumFunction.getLastX() != getLastX()) {
            maximumFunction.addPoint(getLastX(), maximumFunction.getLastY());
        }

        return maximumFunction;
    }

    /**
     * Creates and returns the maximum function of this and the specified function. The resulting function is in every point the maximum of both functions.
     * @param other the other function to be compared with this function and used for the resulting maximum function
     * @return the maximum function of this and the specified function
     */
    public DtmPiecewiseLinearFunction maximumFunction(DtmPiecewiseLinearFunction other) {
        return times(-1).minimumFunction(other.times(-1)).times(-1);
    }

    /**
     * Adds intersection of two line segments specified by the <code>i</code> and <code>j</code> indices if such intersection exists
     * @param i index of the end point of the segment of this function
     * @param j index of the end point of the segment of the other function
     * @param currentMinimal index specifying the currently minimal of this two functions (0 - equal, 1 - this function minimal, 2 - the other function minimal)
     * @param newMinimal index specifying the new minimal of this two functions (0 - equal, 1 - this function minimal, 2 - the other function minimal)
     * @param minimumFunction the minimum function of this and the specified function
     * @param other the other function to be compared with this function and used for the resulting minimum function
     */
    protected void addIntersection(int i, int j, int currentMinimal, int newMinimal, DtmPiecewiseLinearFunction minimumFunction, DtmPiecewiseLinearFunction other) {
        if (currentMinimal != newMinimal && currentMinimal > 0 && newMinimal > 0) {
            try {
                double[] intersection = DtmMathUtils.determineIntersection(xValues.get(i - 1), yValues.get(i - 1), xValues.get(i), yValues.get(i), other.xValues.get(j - 1), other.yValues.get(j - 1), other.xValues.get(j), other.yValues.get(j));
                minimumFunction.addPoint(intersection[0], intersection[1]);
            }
            catch (ArithmeticException ex) {
                //ex.printStackTrace();
            }
        }
    }

    /**
     * Creates and returns the minimum function of this and the specified function. The resulting function is in every point the minimum of both functions.
     * @param other the other function to be compared with this function and used for the resulting minimum function
     * @return the minimum function of this and the specified function
     */
    public DtmPiecewiseLinearFunction minimumFunction(DtmPiecewiseLinearFunction other) {
        DtmPiecewiseLinearFunction minimumFunction = new DtmPiecewiseLinearFunction();
        int i = 0;
        int j = 0;
        int currentMinimal = 0;
        double x = 0.0;
        double y1 = 0.0;
        double y2 = 0.0;

        while (i < xValues.size() && j < other.xValues.size()) {
            if (xValues.get(i).equals(other.xValues.get(j))) {
                x = xValues.get(i);
                y1 = yValues.get(i);
                y2 = other.yValues.get(j);

                if (y1 < y2) {
                    addIntersection(i, j, currentMinimal, 1, minimumFunction, other);
                    currentMinimal = 1;
                    minimumFunction.addPoint(x, y1);
                }
                else if (y1 == y2) { //TODO potential issue due to the double comparison
                    addIntersection(i, j, currentMinimal, 0, minimumFunction, other);
                    currentMinimal = 0;
                    minimumFunction.addPoint(x, y1);
                }
                else {
                    addIntersection(i, j, currentMinimal, 2, minimumFunction, other);
                    currentMinimal = 2;
                    minimumFunction.addPoint(x, y2);
                }
                i++;
                j++;
            }
            else if (xValues.get(i) < other.xValues.get(j)) {
                x = xValues.get(i);
                y1 = yValues.get(i);
                y2 = DtmMathUtils.interpolateLine(j, other.xValues, other.yValues, x);

                if (y1 < y2) {
                    addIntersection(i, j, currentMinimal, 1, minimumFunction, other);
                    currentMinimal = 1;
                    minimumFunction.addPoint(x, y1);
                }
                else if (y1 == y2) { //TODO potential issue due to the double comparison
                    addIntersection(i, j, currentMinimal, 0, minimumFunction, other);
                    currentMinimal = 0;
                    minimumFunction.addPoint(x, y1);
                }
                else {
                    addIntersection(i, j, currentMinimal, 2, minimumFunction, other);
                    currentMinimal = 2;
                    if (i == xValues.size() - 1) {
                        minimumFunction.addPoint(x, y2);
                    }
                }
                i++;
            }
            else {
                x = other.xValues.get(j);
                y1 = DtmMathUtils.interpolateLine(i, xValues, yValues, x);
                y2 = other.yValues.get(j);

                if (y1 < y2) {
                    addIntersection(i, j, currentMinimal, 1, minimumFunction, other);
                    currentMinimal = 1;
                    if (j == other.xValues.size() - 1) {
                        minimumFunction.addPoint(x, y1);
                    }
                }
                else if (y1 == y2) { //TODO potential issue due to the double comparison
                    addIntersection(i, j, currentMinimal, 0, minimumFunction, other);
                    currentMinimal = 0;
                    minimumFunction.addPoint(x, y2);
                }
                else {
                    addIntersection(i, j, currentMinimal, 2, minimumFunction, other);
                    currentMinimal = 2;
                    minimumFunction.addPoint(x, y2);
                }
                j++;
            }
        }
        //TODO throwing exception is missing and should not be here in production version -> test the method for correct functionality
        return minimumFunction;
    }

    private static void checkChange(DtmPiecewiseLinearFunction actualMin, DtmPiecewiseLinearFunction oldActualMin,
                                    DtmPiecewiseLinearFunction a, DtmPiecewiseLinearFunction b, DtmPiecewiseLinearFunction f, int i, int j){
        if (actualMin != null && oldActualMin != null){
            if (actualMin != oldActualMin){
                try{
                    double[] intersection = DtmMathUtils.determineIntersection(a.xValues.get(i - 1),
                            a.yValues.get(i - 1), a.xValues.get(i), a.yValues.get(i), b.xValues.get(j - 1),
                            b.yValues.get(j - 1), b.xValues.get(j), b.yValues.get(j));
                    addBreakPoint(f, intersection[0], intersection[1]);
                } catch (ArithmeticException ex) {
                    //ex.printStackTrace();
                }
            }
        }
    }

    private static void addBreakPoint(DtmPiecewiseLinearFunction f, double x, double y){
        double EPS = 1E-10;
        if(f.getLength() != 0){
            double lastX = f.getLastX();
            if (x == lastX){
                return;
            }
            if (x < lastX){
                if (Math.abs(x - lastX) < EPS){
                    return;
                }
                throw new IllegalArgumentException("cannot add breakpoint: x <= lastX " + lastX + " " + x);
            }
            if (x - lastX < EPS){
                return;
            }
            double lastY = f.getLastY();
            if (y < lastY){
                if (Math.abs(y - lastY) < EPS){
                    return;
                }
                throw new IllegalArgumentException("cannot add breakpoint: y < yo.last " + y);
            }
        }
        f.addPoint(x, y);
    }

    private double valueAt(double x, int index) {
        if (x == xValues.get(xValues.size() - 1)) {
            return yValues.get(yValues.size() - 1);
        }
        else {
            return DtmMathUtils.interpolateLine(index, xValues, yValues, x);
        }
    }

    /**
     * Alternative implementation of minimum function based on scala Operators
     * @param other other function for minimum
     * @return minimum function min(this, other)
     */
    public DtmPiecewiseLinearFunction minimumFunctionOperators(DtmPiecewiseLinearFunction other){
        DtmPiecewiseLinearFunction a = this;
        DtmPiecewiseLinearFunction b = other;
        DtmPiecewiseLinearFunction minimumFunction = new DtmPiecewiseLinearFunction();

        int i = 0;
        int j = 0;
        DtmPiecewiseLinearFunction actualMin = null;
        DtmPiecewiseLinearFunction oldActualMin = null;

        while (i < a.getLength() && j < b.getLength()){
            oldActualMin = actualMin;

            if (a.getX(i) == b.getX(j)){
                if (a.getY(i) < b.getY(j)){
                    actualMin = a;
                    checkChange(actualMin, oldActualMin, a, b, minimumFunction, i, j);
                    addBreakPoint(minimumFunction, a.getX(i), a.getY(i));
                }
                else if (a.getY(i) == b.getY(j)){
                    actualMin = null;
                    addBreakPoint(minimumFunction, a.getX(i), a.getY(i));
                }
                else {
                    actualMin = b;
                    checkChange(actualMin, oldActualMin, a, b, minimumFunction, i, j);
                    addBreakPoint(minimumFunction, b.getX(j), b.getY(j));
                }
                i += 1;
                j += 1;
            }
            else if (a.getX(i) < b.getX(j)){
                if (a.getY(i) < b.valueAt(a.getX(i), j)){
                    actualMin = a;
                    checkChange(actualMin, oldActualMin, a, b, minimumFunction, i, j);
                    addBreakPoint(minimumFunction, a.getX(i), a.getY(i));
                }
                else if (a.getY(i) == b.valueAt(a.getX(i), j)){
                    actualMin = null;
                    addBreakPoint(minimumFunction, a.getX(i), a.getY(i));
                }
                else {
                    actualMin = b;
                    checkChange(actualMin, oldActualMin, a, b, minimumFunction, i, j);
                    if (i == a.getLength() - 1 ){
                        addBreakPoint(minimumFunction, a.getX(i), b.valueAt(a.getX(i), j));
                    }
                }
                i += 1;
            }
            else if (b.getX(j) < a.getX(i)){
                if (b.getY(j) < a.valueAt(b.getX(j), i)){
                    actualMin = b;
                    checkChange(actualMin, oldActualMin, a, b, minimumFunction, i, j);
                    addBreakPoint(minimumFunction, b.getX(j), b.getY(j));
                }
                else if (b.getY(j) == a.valueAt(b.getX(j), i)){
                    actualMin = null;
                    addBreakPoint(minimumFunction, b.getX(j), b.getY(j));
                }
                else {
                    actualMin = a;
                    checkChange(actualMin, oldActualMin, a, b, minimumFunction, i, j);
                    if (j == b.getLength() - 1 ){
                        addBreakPoint(minimumFunction, b.getX(j), a.valueAt(b.getX(j), i));
                    }
                }
                j += 1;
            }
        }

        double loLim = Math.max(a.getFirstX(), b.getFirstX());
        double hiLim = Math.min(a.getLastX(), b.getLastX());
        if (minimumFunction.getFirstX() > loLim + 1E-10 || minimumFunction.getLastX() < hiLim - 1E-10){
            throw new RuntimeException("Bad limits in min operation: expect ("+loLim+", "+hiLim+"), res ("
                    +minimumFunction.getFirstX()+", "+minimumFunction.getLastX()+")");
        }
        return minimumFunction;
    }

    /**
     * Approximate this function by discrete function
     * @param numBins number of intervals
     * @param startX lower bound
     * @param endX upper bound
     * @return discrete function
     */
    public DtmDiscreteFunction toDiscreteFunction(int numBins, double startX, double endX, double positionInInterval){
        double[] values = new double[numBins];
        double step = (endX - startX)/numBins;

        int index = 1;
        for(int i = 0; i < numBins; i++){
            double x = startX + i * step + positionInInterval * step;
            if (x >= getLastX()){
                break;
            }
            if (x < getFirstX()) {
                continue;
            }
            while (xValues.get(index) <= x){
                index++;
            }
            double y = DtmMathUtils.interpolateLine(index, xValues, yValues, x);
            values[i] = y;
        }
        return new DtmDiscreteFunction(values, startX, endX);
    }

    @Override
    public List<Double> getXValues() {
        return xValues;
    }

    @Override
    public List<Double> getYValues() {
        return yValues;
    }

    @Override
    public double getX(int index) throws IndexOutOfBoundsException {
        return xValues.get(index);
    }

    @Override
    public double getY(int index) throws IndexOutOfBoundsException {
        return yValues.get(index);
    }

    @Override
    public int getLength() {
        return xValues.size();
    }

    @Override
    public double getFirstX() throws IndexOutOfBoundsException {
        return xValues.get(0);
    }

    @Override
    public double getFirstY() throws IndexOutOfBoundsException {
        return yValues.get(0);
    }

    @Override
    public double getLastX() throws IndexOutOfBoundsException {
        return xValues.get(xValues.size() - 1);
    }

    @Override
    public double getLastY() throws IndexOutOfBoundsException {
        return yValues.get(yValues.size() - 1);
    }
}