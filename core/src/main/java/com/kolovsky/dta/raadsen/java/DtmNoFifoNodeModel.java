/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.Arrays;
import java.util.HashMap;

/**
 * The simplest node model that does not reflect FIFO at incoming edges and distributes the supply according priorities.
 * The model is invariant. This model should be applied to the non-congested intersections.
 * @author kolovsky
 */
public class DtmNoFifoNodeModel extends DtmDestinationNodeModel {
    double[] incomingPriorities;

    /**
     * @param node          associated node for node model
     * @param distributions node distribution for each destination
     * @param incomingPriorities priorities of the incoming edges
     */
    public DtmNoFifoNodeModel(DtmNode node, HashMap<DtmIdentifiable, DtmDistributionMatrix> distributions, double[] incomingPriorities) {
        super(node, distributions);
        this.incomingPriorities = incomingPriorities;
    }

    @Override
    public double[][] solveUniversal(double[][] demand, double[] supply) {

        double[][] q = new double[demand.length][supply.length];

        for (int j = 0; j < supply.length; j++){
            double outgoingDemand = 0;
            for (double[] doubles : demand) {
                outgoingDemand += doubles[j];
            }

            if (outgoingDemand <= supply[j]){
                for(int i = 0; i < demand.length; i++){
                    q[i][j] += demand[i][j];
                }
            }
            else {

                boolean[] mask = new boolean[demand.length];
                Arrays.fill(mask, true);
                int numDetermined = 0;
                double r = supply[j];

                while (numDetermined < demand.length){
                    double prioritiesSum = 0;
                    for(int i = 0; i < demand.length; i++){
                        if (mask[i]){
                            prioritiesSum += incomingPriorities[i];
                        }
                    }

                    boolean allConstrained = true;
                    for(int i = 0; i < demand.length; i++){
                        if (mask[i]){
                            if (demand[i][j] <= incomingPriorities[i]/prioritiesSum * r){
                                allConstrained = false;
                                q[i][j] = demand[i][j];
                                r -= demand[i][j];
                                mask[i] = false;
                                numDetermined += 1;
                            }
                        }
                    }

                    if (allConstrained){
                        for(int i = 0; i < demand.length; i++){
                            if (mask[i]){
                                q[i][j] = incomingPriorities[i]/prioritiesSum * r;
                            }
                        }
                        break;
                    }
                }
            }
        }
        return q;
    }
}
