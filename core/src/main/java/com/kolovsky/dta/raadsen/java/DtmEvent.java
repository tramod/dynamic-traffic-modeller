/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * A general event for dynamic traffic assignment
 * @author Tomas Potuzak
 */
public abstract class DtmEvent {
    /** The predicted time of this event (key for a priority queue) */
    protected double predictedTime;


    /**
     * Performs an important procedure at the specified time
     * @param time the time, at which the procedure shall be performed
     * @return the node
     */
    public abstract DtmNode perform(double time);


    /**
     * Returns the predicted time of this event (key for a priority queue)
     * @return the predicted time of this event (key for a priority queue)
     */
    public double getPredictedTime() {
        return predictedTime;
    }


    /**
     * Sets a new value of the predicted time of this event
     * @param predictedTime the new value of the predicted time of this event
     */
    public void setPredictedTime(double predictedTime) {
        this.predictedTime = predictedTime;
    }
}