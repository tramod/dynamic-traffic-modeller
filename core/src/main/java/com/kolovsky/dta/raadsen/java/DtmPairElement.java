/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * A tuple of two values
 * @param <F> first value
 * @param <S> second value
 * @author Tomas Potuzak
 */
public class DtmPairElement<F, S> {
    /** First value */
    protected F first;
    /** Second value */
    protected S second;

    /**
     * Creates new pair of values
     * @param first first value
     * @param second second value
     */
    public DtmPairElement(F first, S second) {
        this.first = first;
        this.second = second;
    }

    /**
     * Returns the first value
     * @return the first value
     */
    public F getFirst() {
        return first;
    }

    /**
     * Sets the first value
     * @param first the new first value
     */
    public void setFirst(F first) {
        this.first = first;
    }

    /**
     * Returns the second value
     * @return the second value
     */
    public S getSecond() {
        return second;
    }

    /**
     * Sets the second value
     * @param second the new second value
     */
    public void setSecond(S second) {
        this.second = second;
    }

    //Inherited methods

    @Override
    public String toString() {
        return "(" + first + ", " + second + ")";
    }
}

