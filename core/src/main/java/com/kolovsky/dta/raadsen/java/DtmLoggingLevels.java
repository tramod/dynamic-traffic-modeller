/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * Logging levels for the logger
 * @author Tomas Potuzak
 */
public enum DtmLoggingLevels {
    /** Information level representing standard behavior */
    INFO(0),
    /** Warning level representing problematic behavior */
    WARNING(1),
    /** Error level representing erroneous behavior  */
    ERROR(2);

    /** The value of the level used for comparison of the levels */
    private int levelValue;

    /**
     * Creates a value of this enumeration
     * @param levelValue the value of the level used for comparison of the levels
     */
    DtmLoggingLevels(int levelValue) {
        this.levelValue = levelValue;
    }

    /**
     * Returns the value of the level used for comparison of the levels
     * @return the value of the level used for comparison of the levels
     */
    public int getLevelValue() {
        return levelValue;
    }
}
