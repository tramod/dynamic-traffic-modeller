/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * Types of border edges queue element
 * @author Tomas Potuzak
 */
public enum DtmSubgraphParallelBorderEdgesQueueElementTypes {
    /** Incoming edge flow to process */
    INCOMING_EDGE_FLOW(/*1*/6), //TODO Values 6, 5, 3, 0 was discovered by an mistake in switch, but shows better and deterministic results for nguyen even when non-border event queues are ordered only using time. It is possible that there will be problems in other networks - in that case return the 0..3 values and change non-border event queues to be ordered also using id of edges ids and maybe even nodes ids
    /** Outgoing edge flow to process */
    OUTGOING_EDGE_FLOW(/*2*/5),
    /** Outgoing edge mixture to process */
    OUTGOING_EDGE_MIXTURE(/*3*/3),
    /** Event created during border edges processing to process */
    EVENT(0);

    /** The value, according which the values of this enum can be ordered */
    private int orderingValue;

    /**
     * Creates a new value of this enum based on the specified ordering value
     * @param orderingValue the value, according which the values of this enum can be ordered
     */
    private DtmSubgraphParallelBorderEdgesQueueElementTypes(int orderingValue) {
        this.orderingValue = orderingValue;
    }

    //Getters and setters

    /**
     * Returns the value, according which the values of this enum can be sorted
     * @return the value, according which the values of this enum can be sorted
     */
    public int getOrderingValue() {
        return orderingValue;
    }
}
