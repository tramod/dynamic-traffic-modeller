/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.Arrays;

/**
 *
 * @author Tomas Potuzak
 */
public class DtmReleasableCyclicBarrier extends DtmCyclicBarrier {
    /**  */
    public boolean released;

    /**
     *
     * @param totalThreadsCount
     */
    public DtmReleasableCyclicBarrier(int totalThreadsCount) {
        super(totalThreadsCount);

        released = false;
    }

    @Override
    public synchronized void synchronize(int threadIndex) {
        if (!released) {
            super.synchronize(threadIndex);
        }
    }

    /**
     *
     */
    public synchronized void release() {
        released = true;
        waitingThreadsCount = 0;
        Arrays.fill(mustThreadsWait, false);
        notifyAll();
    }

    /**
     *
     */
    public synchronized void restore() {
        released = false;
        waitingThreadsCount = 0;
        Arrays.fill(mustThreadsWait, true);
    }
}
