/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * An interface for an item identifiable by an integer id. This item also carry index information for fast access via (non-hash) tables.
 * @author Tomas Potuzak
 */
public interface DtmIdentifiable {

    /**
     * Returns the ID of this identifiable item
     * @return the ID of this identifiable item
     */
    public int getId();

    /**
     * Sets the ID of this identifiable item
     * @param id the new id of this identifiable item
     */
    public void setId(int id);

    /**
     * Returns the index of this identifiable item
     * @return the index of this identifiable item
     */
    public int getIndex();

    /**
     * Sets the index of this identifiable item
     * @param index the new index of this identifiable item
     */
    public void setIndex(int index);
}

