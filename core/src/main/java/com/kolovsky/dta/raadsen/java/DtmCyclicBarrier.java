/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.Arrays;

/**
 *
 * @author Tomas Potuzak
 */
public class DtmCyclicBarrier {
    /**  */
    protected int totalThreadsCount;
    /**  */
    protected int waitingThreadsCount;
    /**  */
    protected boolean[] mustThreadsWait;

    /**
     *
     * @param totalThreadsCount
     */
    public DtmCyclicBarrier(int totalThreadsCount) {
        this.totalThreadsCount = totalThreadsCount;
        mustThreadsWait = new boolean[totalThreadsCount];
        Arrays.fill(mustThreadsWait, true);
    }

    /**
     *
     * @param threadIndex
     */
    public synchronized void synchronize(int threadIndex) {
        waitingThreadsCount++;

        if (waitingThreadsCount == totalThreadsCount) {
            waitingThreadsCount = 0;
            Arrays.fill(mustThreadsWait, false);
            notifyAll();
        }
        else {
            mustThreadsWait[threadIndex] = true;
            while (mustThreadsWait[threadIndex]) {
                try {
                    wait();
                }
                catch (InterruptedException ex) {
                    //No action - fall asleep again when woke up unintentionally
                }
            }
        }
    }

}
