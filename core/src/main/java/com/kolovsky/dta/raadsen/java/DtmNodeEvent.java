/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * A node event for dynamic traffic assignment
 * @author Tomas Potuzak
 */
public class DtmNodeEvent extends DtmEvent {
    /** The node of this event */
    protected DtmNode node;
    /** The change time of this event */
    protected double changeTime;

    /**
     * Creates a new node event for specified node and with specified change time
     * @param node the node of this event
     * @param changeTime the change time of this event
     */
    public DtmNodeEvent(DtmNode node, double changeTime) {
        this.node = node;
        this.changeTime = changeTime;

        predictedTime = changeTime;
    }

    @Override
    public DtmNode perform(double time) {
        return node;
    }
}