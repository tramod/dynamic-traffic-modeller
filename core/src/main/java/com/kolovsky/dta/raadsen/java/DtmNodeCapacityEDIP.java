/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.*;

/**
 * Capacity of the intersection by Czech technical norms (TP188) and
 * German Highway Capacity Manual (Forschungsgesellschaft für Strassen und Verkehrswesen, 2001)
 * @author kolovsky
 */
public class DtmNodeCapacityEDIP implements DtmNodeCapacity{
    /**
     * Represent intersection movement
     */
    class Mov{
        public int i;
        public int j;
        public Mov(int i, int j){
            this.i = i;
            this.j = j;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Mov mov = (Mov) o;
            return i == mov.i && j == mov.j;
        }

        @Override
        public int hashCode() {
            return Objects.hash(i, j);
        }

        @Override
        public String toString() {
            return "Mov{" + "i=" + i + ", j=" + j + '}';
        }
    }

    class INode{
        public Set<Mov> in = new HashSet<>();
        public Set<Mov> out = new HashSet<>();
    }

    private final double SPEED = 50.0;
    private final double BASIC_FLOW = 3600.0;

    int numIncoming;
    int numOutgoing;
    final private Mov[] order;
    final private Map<Mov, List<DtmBeta>> dependence;

    DtmPairElement<Double, Double>[][] criticalTimeInterval;
    double[][] nextTimeInterval;

    /**
     *
     * @param betas dependence coefficients (ij) depends on (kl)
     * @param numIncoming number of incoming edges
     * @param numOutgoing number of outgoing edges
     * @param criticalTimeInterval critical time intervals (see to TP188)
     * @param nextTimeInterval next time intervals (see to TP188)
     */
    public DtmNodeCapacityEDIP(List<DtmBeta> betas, int numIncoming, int numOutgoing,
                                DtmPairElement<Double, Double>[][] criticalTimeInterval, double[][] nextTimeInterval) throws Exception {
        this.numIncoming = numIncoming;
        this.numOutgoing = numOutgoing;
        order = topologyOrder(betas);
        dependence = createDependence(betas);
        this.criticalTimeInterval = criticalTimeInterval;
        this.nextTimeInterval = nextTimeInterval;
    }

    @Override
    public double[][] capacity(double[][] flow) {
        double[][] c = new double[numIncoming][numOutgoing];
        double[][] q = new double[numIncoming][];

        // copy demand to flow
        for (int i = 0; i < numIncoming; i++){
            q[i] = flow[i].clone();
        }

        // copy demand to flow
        for (Mov m: order){
            // compute superior flow
            double superiorFlow = 0.0;
            if (dependence.containsKey(m)){
                for (DtmBeta b: dependence.get(m)){
                    superiorFlow += b.dependence * q[b.k][b.l];
                }
            }

            // compute capacity
            double cti = criticalTimeInterval[m.i][m.j].first + SPEED * criticalTimeInterval[m.i][m.j].second;
            double nti = nextTimeInterval[m.i][m.j];
            c[m.i][m.j] = BASIC_FLOW/nti * Math.pow(Math.E, - superiorFlow/BASIC_FLOW * (cti - nti/2));

            // compute flow
            q[m.i][m.j] = Math.min(c[m.i][m.j], q[m.i][m.j]);
        }
        return c;
    }

    private Mov[] topologyOrder(List<DtmBeta> betas) throws RuntimeException {
        INode[][] nodes = new INode[numIncoming][numOutgoing];
        for (int i = 0; i < numIncoming; i++){
            for (int j = 0; j < numOutgoing; j++){
                nodes[i][j] = new INode();
            }
        }

        for (DtmBeta b: betas){
            nodes[b.k][b.l].out.add(new Mov(b.i, b.j));
            nodes[b.i][b.j].in.add(new Mov(b.k, b.l));
        }

        Mov[] order = new Mov[numIncoming * numOutgoing];
        int index = 0;
        Set<Mov> processedMov = new HashSet<>();
        while (index < numIncoming * numOutgoing){
            boolean existStart = false;
            for (int i = 0; i < numIncoming; i++){
                for (int j = 0; j < numOutgoing; j++){
                    Mov currentMov = new Mov(i, j);
                    if (nodes[i][j].in.isEmpty() && !processedMov.contains(currentMov)){
                        order[index] = currentMov;
                        processedMov.add(currentMov);
                        index += 1;
                        for (Mov e: nodes[i][j].out){
                            nodes[e.i][e.j].in.remove(currentMov);
                        }
                        nodes[i][j].out.clear();
                        existStart = true;
                    }
                }
            }
            if (!existStart){
                throw new RuntimeException("The graph is not acyclic!");
            }
        }
        return order;
    }

    private Map<Mov, List<DtmBeta>> createDependence(List<DtmBeta> betas){
        Map<Mov, List<DtmBeta>> map = new HashMap<>(numIncoming * numOutgoing);
        for (DtmBeta b: betas){
            Mov currentMov = new Mov(b.i, b.j);
            if (!map.containsKey(currentMov)){
                map.put(currentMov, new ArrayList<>());
            }
            map.get(currentMov).add(b);
        }
        return map;
    }
}
