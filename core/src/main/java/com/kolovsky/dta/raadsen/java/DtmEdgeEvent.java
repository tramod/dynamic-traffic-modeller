/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * An edge event for Dynamic Traffic Assignment
 * @author Tomas Potuzak
 */
public abstract class DtmEdgeEvent extends DtmEvent {
    /** The edge of this event */
    protected DtmEdge edge;
    /** The previous edge event */
    protected DtmEdgeEvent previous;
    /** The next edge event */
    protected DtmEdgeEvent next;

    /**
     * Calculates and returns the predicted time of this event
     * @return the predicted time of this event
     */
    public abstract double calculatePredictedTime();

    //Getters and setters

    /**
     * Returns the edge of this event
     * @return the edge of this event
     */
    public DtmEdge getEdge() {
        return edge;
    }

    /**
     * Sets the edge of this event
     * @param edge the new edge of this event
     */
    public void setEdge(DtmEdge edge) {
        this.edge = edge;
    }

    /**
     * Returns the previous edge event
     * @return the previous edge event
     */
    public DtmEdgeEvent getPrevious() {
        return previous;
    }

    /**
     * Sets the previous edge event
     * @param previous the new previous edge event
     */
    public void setPrevious(DtmEdgeEvent previous) {
        this.previous = previous;
    }

    /**
     * Returns the next edge event
     * @return the next edge event
     */
    public DtmEdgeEvent getNext() {
        return next;
    }

    /**
     * Sets the next edge event
     * @param next the new next edge event
     */
    public void setNext(DtmEdgeEvent next) {
        this.next = next;
    }
}
