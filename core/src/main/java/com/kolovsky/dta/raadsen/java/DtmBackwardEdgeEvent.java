/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * A backward edge event for Dynamic Traffic Assignment - backward kinematic wave
 * @author Tomas Potuzak
 */
public class DtmBackwardEdgeEvent extends DtmForwardBackwardEdgeEvent {
    /** The comparison tolerance */
    protected double epsilon;
    /** The comparison tolerance for cumulative values */
    protected double cumulativeEpsilon;

    /**
     * Creates a new forward edge event
     * @param edge the edge of this event
     * @param flowTime the time of the current inflow
     * @param flow the current inflow
     * @param cumulativeFlow the current inflow
     * @param fanEvent the value of whether or not this event is a fan event
     * @param lastFanEvent the value of whether or not this event is the last fan event
     * @param epsilon the comparison tolerance
     * @param cumulativeEpsilon the comparison tolerance for cumulative values
     */
    public DtmBackwardEdgeEvent(DtmEdge edge, double flowTime, double flow, double cumulativeFlow, boolean fanEvent, boolean lastFanEvent, double epsilon, double cumulativeEpsilon) {
        super(edge, flowTime, flow, cumulativeFlow, fanEvent, lastFanEvent);

        this.epsilon = epsilon;
        this.cumulativeEpsilon = cumulativeEpsilon;

        minimalTime = 0.0;
        maximalTime = Double.POSITIVE_INFINITY;
        predictedTime = calculatePredictedTime();
    }

    @Override
    public double calculateMinimalTime() {
        DtmBackwardEdgeEvent previous = (DtmBackwardEdgeEvent) getPrevious();

        if (fanEvent) {
            if (previous == null) {
                minimalTime = flowTime - edge.getLength() / edge.getFundamentalDiagram().calculateEtaII(0.0, flow);
            }
            else {
                minimalTime = flowTime - edge.getLength() / edge.getFundamentalDiagram().calculateEtaII(previous.getFlow(), flow);
            }
        }
        else {
            minimalTime = flowTime - edge.getLength() / edge.getFundamentalDiagram().calculateGammaII(flow);
        }

        return minimalTime;
    }

    @Override
    public double calculateMaximalTime() {
        DtmBackwardEdgeEvent next = (DtmBackwardEdgeEvent) getNext();

        if (next != null) {
            if (fanEvent && !lastFanEvent) {
                maximalTime = flowTime - edge.getLength() / edge.getFundamentalDiagram().calculateEtaII(flow, next.getFlow());
            }
            else {
                maximalTime = next.getFlowTime() - edge.getLength() / edge.getFundamentalDiagram().calculateGammaII(flow);
            }
        }
        else {
            maximalTime = Double.POSITIVE_INFINITY;
        }

        return maximalTime;
    }

    @Override
    public double calculatePredictedTime() {
        if (edge.getCurrentInflow() != flow) {
            double lastCumulativeInflowY = edge.getCumulativeInflow().getLastY();
            //double lastOutflowX = edge.getOutflow().getLastX();
            double lastOutflowX = edge.getInflow().getLastX();
            double lastInflowY = edge.getInflow().getLastY();
            return (lastCumulativeInflowY - cumulativeFlow - lastInflowY * lastOutflowX + flow * (flowTime - edge.getLength() / edge.getFundamentalDiagram().calculateGammaII(flow)) - edge.getFundamentalDiagram().calculateXiII(flow, edge.getLength())) / (flow - lastInflowY);
        }
        else {
            return -1.0;
        }
    }

    @Override
    public DtmNode perform(double time) {
        if (processed) {
            return null;
        }

        double newPredictedTime = calculatePredictedTime();

        if (DtmMathUtils.isEqualWithEpsilon(newPredictedTime, time, epsilon) && newPredictedTime >= minimalTime - epsilon && newPredictedTime <= maximalTime + epsilon) {
            DtmPairElement<Double, Double> projection = edge.getCumulativeOutflowUpstreamProjection(time, this, epsilon);
            double supply = projection.getFirst() - edge.getCumulativeInflow(time);

            if (supply < cumulativeEpsilon) {
                edge.setSupply(projection.getSecond());
            }
            else {
                edge.setSupply(edge.getFundamentalDiagram().getFlowCapacity());
            }
            DtmLogger.info(time, "Valid backward event - supply " + edge.getSupply(), edge);
            processed = true;
            return edge.getStartNode();
        }
        else { //Invalid event
            return null;
        }
    }

    /**
     * Calculates and returns the upstream projection of the cumulative outflow (V)
     * @param time the time, for which the down projection of the cumulative outflow (V) shall be calculated
     * @return the downstream projection of the cumulative outflow (V)
     */
    public double calculateCumulativeOutflowUpstreamProjection(double time) {
        if (minimalTime <= time && maximalTime > time) {
            return cumulativeFlow + flow * ((time - flowTime) + edge.getLength() / edge.getFundamentalDiagram().calculateGammaII(flow)) + edge.getFundamentalDiagram().calculateXiII(flow, edge.getLength());
        }
        else {
            return Double.POSITIVE_INFINITY;
        }
    }

    @Override
    public String toString() {
        return "BackwardEdgeEvent - time(min, pred, max): (" + minimalTime + ", " + predictedTime + ", " + maximalTime + "), flow: " + flow + ", processed: " + ((isProcessed()) ? "Y ": "N") + ", " + edge;
    }
}