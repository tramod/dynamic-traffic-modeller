/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * A source of a flow for the dynamic traffic assignment
 * @author Tomas Potuzak
 */
public abstract class DtmSource implements DtmIdentifiable {
    /** The ID of this source */
    protected int id;
    /** The index of this source for access via non-hash tables */
    protected int index;
    /** The mixture of the outflow of this source */
    protected DtmMixture<DtmIdentifiable> outflowMixture;
    /** The demand of this source (s) */
    protected double demand;

    /**
     * Reset this source to its initial state
     */
    public abstract void reset();

    //Getters and setters

    /**
     * Returns the outflow mixture of this source
     * @return the outflow mixture of this source
     */
    public DtmMixture<DtmIdentifiable> getOutflowMixture() {
        return outflowMixture;
    }

    /**
     * Sets the outflow mixture of this source
     * @param outflowMixture the new outflow mixture of this source
     */
    public void setOutflowMixture(DtmMixture<DtmIdentifiable> outflowMixture) {
        this.outflowMixture = outflowMixture;
    }

    /**
     * Returns the demand of this source
     * @return the demand of this source
     */
    public double getDemand() {
        return demand;
    }

    /**
     * Sets the demand of this source
     * @param demand the new demand of this source
     */
    public void setDemand(double demand) {
        this.demand = demand;
    }

    //Inherited methods

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
