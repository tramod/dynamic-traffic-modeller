/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * A mixture edge event for Dynamic Traffic Assignment - change of the mixture of the vehicles
 * @author Tomas Potuzak
 */
public class DtmMixtureEdgeEvent extends DtmEdgeEvent {
    /** Time of the current inflow */
    protected double inflowTime;
    /** The current inflow */
    protected double inflow;
    /** The current cumulative inflow */
    protected double cumulativeInflow;
    /** The time of this event */
    protected double time;
    /** The mixture of the vehicles */
    protected DtmMixture<DtmIdentifiable> mixture;
    /** The comparison tolerance */
    protected double epsilon;
    /** was the event already performed? **/
    protected boolean isPerformed = false;

    /**
     * Creates a new mixture edge event
     * @param edge the edge of this event
     * @param inflowTime the time of the current inflow
     * @param inflow the current inflow
     * @param cumulativeInflow the current cumulative inflow
     * @param time the time of this event
     * @param mixture the mixture of the vehicles
     * @param epsilon the comparison tolerance
     */
    public DtmMixtureEdgeEvent(DtmEdge edge, double inflowTime, double inflow, double cumulativeInflow, double time, DtmMixture<DtmIdentifiable> mixture, double epsilon) {
        this.edge = edge;
        this.inflowTime = inflowTime;
        this.inflow = inflow;
        this.cumulativeInflow = cumulativeInflow;
        this.time = time;
        this.mixture = mixture;
        this.epsilon = epsilon;

        predictedTime = calculatePredictedTime();
    }

    @Override
    public double calculatePredictedTime() {
        if (edge.getOutflow().getYValues().get(edge.getOutflow().getYValues().size() - 1) != 0.0) {
            double lastCumulativeOutflowY = edge.getCumulativeOutflow().getLastY();
            double lastOutflowX = edge.getOutflow().getLastX();
            double lastOutflowY = edge.getOutflow().getLastY();
            return (cumulativeInflow - lastCumulativeOutflowY + inflow * (time - inflowTime) + lastOutflowY * lastOutflowX) / lastOutflowY;
        }
        else {
            return -1.0;
        }
    }

    @Override
    public DtmNode perform(double currentTime) {
        double difference = Math.abs(edge.getCumulativeInflow(time) - edge.getCumulativeOutflow(predictedTime));
        if (difference < epsilon && !isPerformed) {
            edge.setOutflowMixture(mixture);
            edge.getMixtureEventsStorage().remove(new DtmFilter<DtmMixtureEdgeEvent>() {

                @Override
                public boolean isAccepted(DtmMixtureEdgeEvent object) {
                    return object == DtmMixtureEdgeEvent.this;
                }
            });

            isPerformed = true;
            return edge.getEndNode();
        }
        else {
            return null;
        }
    }

    /**
     * Returns the mixture of this event
     * @return the mixture of this event
     */
    public DtmMixture<DtmIdentifiable> getMixture() {
        return mixture;
    }

    @Override
    public String toString() {
        return "MixtureEdgeEvent - time(pred): (" + predictedTime + "), inflow: " + inflow + ", " + edge;
    }
}