/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DtmContinuousDynamicUserEquilibrium implements DtmDynamicUserEquilibrium {
    /** Dynamic network loading */
    protected DtmEventGeneralLinkTransmissionModel dnl;
    /** maximal time for the simulation */
    protected double maximalTime;
    /** Origin-Destination matrix */
    protected DtmOriginDestinationMatrix odm;
    /** maintain  gradient projection algorithm */
    protected DtmGradientProjection equilibrator;
    /** start time for node distribution */
    protected double minimalNodeTime;
    /** end time for node distribution */
    protected double maximalNodeTime;
    /** number of time intervals for the discrete functions */
    protected int numberIntervals;
    /** the best result */
    protected List<DtmEdge> bestResultEdges;
    /** maximum proportion error during simplification of proportions */
    protected double proportionsError = 0.001;
    /** maximal error for discretization in hours */
    protected double discretizationError = 1E-3;
    /** position of value in discretization */
    protected double positionInterval = 0.0;

    public DtmContinuousDynamicUserEquilibrium(DtmEventGeneralLinkTransmissionModel dnl, double maximalTime,
                                               DtmOriginDestinationMatrix odm, DtmGradientProjection gradientProjection,
                                               double minimalNodeTime, double maximalNodeTime, int numberIntervals){
        equilibrator = gradientProjection;
        this.dnl = dnl;
        this.odm = odm;
        this.maximalTime = maximalTime;
        this.minimalNodeTime = minimalNodeTime;
        this.maximalNodeTime = maximalNodeTime;
        this.numberIntervals = numberIntervals;
    }

    @Override
    public void run(int maxIter, double targetGap) {
        DtmGraph graph = dnl.graph;
        int iter = 0;
        long time = System.currentTimeMillis();
        double bestGap = Double.POSITIVE_INFINITY;
        while (iter < maxIter){
            for (DtmEdge edge: dnl.graph.getEdges()){
                edge.reset();
            }
            for (DtmZone originZone: odm.getOriginZones()){
                originZone.getOrigin().reset();
            }
            dnl.run(maximalTime, DtmEventGeneralLinkTransmissionModel.DEFAULT_RELEASE_EVENT_EPSILON);

            if(iter == maxIter - 1){
                return;
            }

            double cumRelativeError = 0.0;
            double countRelativeError = 0;
            for (DtmZone destination: odm.getDestinationZones()){

                // SHORTEST PATH SEARCH
                List<DtmArrivalFunction> afs = graph.performBackProfileSearch(destination.getNode(), minimalNodeTime, maximalTime, 1E-6);
                DtmArrivalFunction[] dist = new DtmArrivalFunction[afs.size()];
                for (int i = 0; i < dist.length; i++){
                    dist[i] = afs.get(i);
                    if (afs.get(i).isInfinite()){
                        dist[i] = new DtmArrivalFunction(List.of(minimalNodeTime, maximalTime), List.of(10_000.0, 20_000.0));
                    }
                }

                for (DtmNode node: graph.getNodes()){
                    DtmDestinationNodeModel nodeModel = (DtmDestinationNodeModel) node.getNodeModel();
                    if (nodeModel.getDistributions() != null && destination.getNode() != node
                            && nodeModel.getDistributions().containsKey(destination)){

                        DtmPiecewiseLinearFunction[] travelTimeFunctions = new DtmPiecewiseLinearFunction[node.getOutgoingEdges().size()];
                        for (int i = 0; i < travelTimeFunctions.length; i++){
                            DtmEdge edge = node.getOutgoingEdges().get(i);
                            travelTimeFunctions[i] = composition(edge.getArrivalFunction(), dist[edge.getEndNode().getIndex()]);
                        }
                        DtmDistributionMatrix distributionMatrix = nodeModel.getDistributions().get(destination);

                        for (int i = 0; i < distributionMatrix.getMatrix().length; i++){
                            DtmPairElement<Double, Double> re = relativeErrorFlow(travelTimeFunctions, flows(node, i, destination),
                                    minimalNodeTime, maximalNodeTime/2.0);
                            if (re.first > 0.0){
                                cumRelativeError += re.first*re.second;
                                countRelativeError += re.second;
                            }
                        }

                        DtmDistributionMatrix updatedDistributionMatrix = updateDistributionMatrix(distributionMatrix, travelTimeFunctions);
                        nodeModel.getDistributions().put(destination, updatedDistributionMatrix);
                    }
                }
            }
            double averageRelativeGap = cumRelativeError/countRelativeError;
            if (bestGap > averageRelativeGap){
                bestGap = averageRelativeGap;
                bestResultEdges = new ArrayList<>(graph.edges.size());
                for (DtmEdge edge: graph.getEdges()){
                    bestResultEdges.add(edge.shallowCopy());
                }
            }
            System.out.println(""+(System.currentTimeMillis() - time)+" "+averageRelativeGap);
            if (averageRelativeGap <= targetGap){
                return;
            }
            iter++;
        }
    }

    /**
     * Perform composition of two arrival function and control the minimal and maximal times.
     * @param edgeArrivalFunction function connected with edge
     * @param nodeDist arrival function from end of edge to destination node
     * @return travel time function from source node of the edge to destination node
     */
    private DtmPiecewiseLinearFunction composition(DtmArrivalFunction edgeArrivalFunction, DtmArrivalFunction nodeDist){
        DtmPiecewiseLinearFunction ttf = nodeDist.performComposition(edgeArrivalFunction, 0.0)
                .getFirst().toTravelTimeFunction();
        if (ttf.getLastX() < maximalNodeTime){
            ttf.addPoint(maximalNodeTime, ttf.getLastY());
        }
        return ttf;
    }

    /**
     * Update distribution matrix based on travel time functions
     * @param distributionMatrix original distribution matrix
     * @param travelTimeFunctions list if travel time functions for all alternative
     * @return updated distribution matrix
     */
    private DtmDistributionMatrix updateDistributionMatrix(DtmDistributionMatrix distributionMatrix,
                                                           DtmPiecewiseLinearFunction[] travelTimeFunctions){
        DtmFunction[][] matrix = distributionMatrix.getMatrix();
        DtmFunction[][] newMatrix = new DtmFunction[matrix.length][];

        for (int i = 0; i < matrix.length; i++){
            DtmPiecewiseConstantFunction[] ttfs = new DtmPiecewiseConstantFunction[travelTimeFunctions.length];
            DtmPiecewiseConstantFunction[] props = new DtmPiecewiseConstantFunction[travelTimeFunctions.length];

            for (int j = 0; j < matrix[i].length; j++){
                ttfs[j] = travelTimeFunctions[j].toPiecewiseConstantFunctionHeuristic(discretizationError, positionInterval);
                props[j] = (DtmPiecewiseConstantFunction) matrix[i][j];
            }

            newMatrix[i] = shiftFlow(props, ttfs);
        }
        return new DtmDistributionMatrix(newMatrix);
    }

    private static void fixFunc(List<Double> x, List<List<Double>> ys, int k, int xSavePointIndex, double toXValue, double newYValue){
        int j = xSavePointIndex;
        while (x.get(j) < toXValue){
            double originalValueU = ys.get(k+1).get(j);
            double originalValueA = ys.get(k).get(j);
            ys.get(k+1).set(j, originalValueU + originalValueA - newYValue);
            j++;
        }
    }

    /**
     * Simplifies the set of functions and maintain the sum equal to 1.0
     * @param x X values
     * @param ys Y values for all function in box
     * @param maximalAbsoluteError maximal allowed error
     * @return simplified functions
     */
    public static DtmPiecewiseConstantFunction[] boxSimplification(List<Double> x, List<List<Double>> ys, double maximalAbsoluteError){
        // check input
        for (List<Double> f: ys){
            if (!f.get(f.size() - 1).equals(f.get(f.size() - 2))){
                throw new IllegalArgumentException("The last value must be equal to penultimate value " + f.get(f.size() - 1) + " " + f.get(f.size() - 2));
            }
        }

        int k = 0;
        DtmPiecewiseConstantFunction[] out = new DtmPiecewiseConstantFunction[ys.size()];
        for (List<Double> y: ys){
            if (k == ys.size() - 1){
                maximalAbsoluteError = 1E-15;
            }
            double upperBound = y.get(0) + maximalAbsoluteError;
            double lowerBound = y.get(0) - maximalAbsoluteError;
            double newUpperBound = 0.0;
            double newLowerBound = 0.0;
            double lastPointX = x.get(0);
            DtmPiecewiseConstantFunction simplifiedFunction = new DtmPiecewiseConstantFunction();

            int xSavePointIndex = 0;

            for (int i = 1; i < x.size(); i++) {
                newUpperBound = y.get(i) + maximalAbsoluteError;
                newLowerBound = y.get(i) - maximalAbsoluteError;

                if (newUpperBound >= lowerBound && newLowerBound <= upperBound) {
                    if (newUpperBound < upperBound) {
                        upperBound = newUpperBound;
                    }
                    if (newLowerBound > lowerBound) {
                        lowerBound = newLowerBound;
                    }
                }
                else {
                    double newYValue = Math.max((upperBound + lowerBound) / 2, 0.0);
                    double fromXValue = lastPointX;
                    double toXValue = x.get(i);
                    simplifiedFunction.addPoint(fromXValue, newYValue);
                    lastPointX = toXValue;
                    lowerBound = newLowerBound;
                    upperBound = newUpperBound;

                    // fix second function
                    if (k < ys.size() - 1){
                        fixFunc(x, ys, k, xSavePointIndex, toXValue, newYValue);
                        xSavePointIndex = i;
                    }

                }
            }
            double newYValue = Math.max((upperBound + lowerBound) / 2, 0.0);
            simplifiedFunction.addPoint(lastPointX, newYValue);
            if (k < ys.size() - 1){
                fixFunc(x, ys, k, xSavePointIndex, x.get(x.size() - 1), newYValue);
                // set the last point to right value
                List<Double> f = ys.get(k + 1);
                f.set(f.size() - 1, f.get(f.size() - 2));
            }

            if (!x.get(x.size() - 1).equals(simplifiedFunction.xValues.get(simplifiedFunction.xValues.size() - 1))) {
                simplifiedFunction.addPoint(x.get(x.size() - 1), newYValue);
            }

            out[k] = simplifiedFunction;
            k++;
        }
        return out;
    }

    /**
     * Shifts flow based on travel time functions
     * @param props proportions for alternatives
     * @param ttfs travel time functions for alternative
     * @return updated proportions
     */
    protected DtmPiecewiseConstantFunction[] shiftFlow(DtmPiecewiseConstantFunction[] props, DtmPiecewiseConstantFunction[] ttfs){
        // current index for every function (init to 0)
        int[] currentIndex = new int[props.length + ttfs.length];
        Arrays.fill(currentIndex,0);

        // all functions
        DtmPiecewiseConstantFunction[] functions = new DtmPiecewiseConstantFunction[props.length + ttfs.length];
        System.arraycopy(props, 0, functions, 0, props.length);
        System.arraycopy(ttfs, 0, functions, props.length, ttfs.length);

        // actual function with minimal X value
        DtmPiecewiseConstantFunction actualFunction = functions[0];
        int actualFunctionIndex = 0;

        // old minx
        double oldMinx = Double.POSITIVE_INFINITY;

        // costs and proportions for alternatives
        double[] costsA = new double[ttfs.length];
        double[] propsA = new double[props.length];

        // out
        ArrayList<Double> xOut = new ArrayList<>();
        List<List<Double>> box = new ArrayList<>();
        for (int i = 0; i < props.length; i++){
            box.add(new ArrayList<>());
        }

        // main loop
        while (true){
            // select minimal X breakpoint
            double minx = Double.POSITIVE_INFINITY;
            for (int i = 0; i < functions.length; i++){
                double x = functions[i].getX(currentIndex[i]);
                if (minx > x){
                    minx = x;
                    actualFunction = functions[i];
                    actualFunctionIndex = i;
                }
            }
            if (minx == oldMinx){
                currentIndex[actualFunctionIndex] += 1;
                if (currentIndex[actualFunctionIndex] >= functions[actualFunctionIndex].getLength()){
                    break;
                }
                continue;
            }

            // fill propsA
            for (int i = 0; i < props.length; i++){
                if (props[i] != actualFunction){
                    if (functions[i].getX(currentIndex[i]) == minx){
                        propsA[i] = functions[i].getY(currentIndex[i]);
                    }
                    else {
                        propsA[i] = functions[i].getY(currentIndex[i] - 1);
                    }
                }
                else {
                    propsA[i] = actualFunction.getY(currentIndex[i]);
                }
            }
            // fill costsA
            for (int i = 0; i < ttfs.length; i++){
                if (ttfs[i] != actualFunction){
                    if (ttfs[i].getX(currentIndex[props.length + i]) == minx){
                        costsA[i] = ttfs[i].getY(currentIndex[props.length + i]);
                    }
                    else {
                        costsA[i] = ttfs[i].getY(currentIndex[props.length + i] - 1);
                    }

                }
                else {
                    costsA[i] = actualFunction.getY(currentIndex[props.length + i]);
                }
            }

            // compute shift
            // minimum function
            xOut.add(minx);

            double[] newPropsA = equilibrator.run(propsA, costsA);

            for (int i = 0; i < props.length; i++){
                box.get(i).add(newPropsA[i]);
            }

            // increase index at current function
            currentIndex[actualFunctionIndex] += 1;
            if (currentIndex[actualFunctionIndex] >= functions[actualFunctionIndex].getLength()){
                break;
            }
            oldMinx = minx;
        }

        // fix the last point in box
        for (int i = 0; i < ttfs.length; i++){
            List<Double> y = box.get(i);
            y.set(y.size() - 1, y.get(y.size() - 2));
        }

        return boxSimplification(xOut, box, proportionsError);
    }

    private DtmPairElement<Double, Double> relativeErrorFlow(DtmPiecewiseLinearFunction[] travelTimeFunctions, DtmDiscreteFunction[] flows, double minTime, double maxTime){
        int timeLength = numberIntervals;
        double startTime = travelTimeFunctions[0].getFirstX();
        double timeStep = (travelTimeFunctions[0].getLastX() - startTime)/timeLength;

        double sumNumerator = 0.0;
        double sumDenominator  = 0.0;
        double weight = 0.0;
        // travel time for all alternative for given time
        double[] ttfs = new double[travelTimeFunctions.length];

        for (int t = 0; t < timeLength; t++){
            double time = startTime + t*timeStep + timeStep*positionInterval;
            if (minTime <= time && time <= maxTime){
                double minValue = Double.POSITIVE_INFINITY;
                for (int j = 0; j < travelTimeFunctions.length; j++){
                    ttfs[j] = travelTimeFunctions[j].valueAt(time);
                    if (minValue > ttfs[j]){
                        minValue = ttfs[j];
                    }
                }

                for (int j = 0; j < travelTimeFunctions.length; j++){
                    double flow = flows[j].valueAt(time);
                    if (flow > 1E-10){
                        sumNumerator += flow * minValue;
                        sumDenominator += flow * ttfs[j];
                        weight += flow;
                    }
                }

            }
        }
        if (sumDenominator > 0.0){
            return new DtmPairElement<>(1 - sumNumerator/sumDenominator, weight);
        }
        return new DtmPairElement<>(-1.0, -1.0);
    }

    private DtmDiscreteFunction[] flows(DtmNode node, int incomeEdgeIndex, DtmZone destination){
        DtmDestinationNodeModel nodeModel = (DtmDestinationNodeModel) node.getNodeModel();
        DtmDistributionMatrix distributionMatrix = nodeModel.getDistributions().get(destination);

        DtmDiscreteFunction[] flows = new DtmDiscreteFunction[node.getOutgoingEdges().size()];
        for (int j = 0; j < node.getOutgoingEdges().size(); j++){
            DtmDiscreteFunction dist = distributionMatrix.getMatrix()[incomeEdgeIndex][j].toDiscreteFunction(numberIntervals,
                    minimalNodeTime, maximalNodeTime, 0.5);
            DtmEdge outgoingEdge = node.getOutgoingEdges().get(j);
            List<DtmMixture<DtmIdentifiable>> mixtures = outgoingEdge.inflowMixtureFunction
                    .toDiscreteFunction(numberIntervals, minimalNodeTime, maximalNodeTime, 0.5);
            DtmDiscreteFunction inflow = outgoingEdge.getInflow()
                    .toDiscreteFunction(numberIntervals, minimalNodeTime, maximalNodeTime, 0.5);
            double[] values = new double[numberIntervals];
            for (int t = 0; t < numberIntervals; t++){
                if (mixtures.get(t) != null){
                    double portion = mixtures.get(t).getPortion(destination);
                    double inflowToDestination = portion * inflow.getY(t);
                    if (inflowToDestination > 1.0 && dist.getY(t) > 1E-2){ //&& dist.getY(t) > 1E-3
                        values[t] = inflowToDestination;
                    }
                }
            }
            flows[j] = new DtmDiscreteFunction(values, minimalNodeTime, maximalNodeTime);
        }
        return flows;
    }

    public void setProportionsError(double proportionsError) {
        this.proportionsError = proportionsError;
    }

    public void setDiscretizationError(double discretizationError) {
        this.discretizationError = discretizationError;
    }

    public void setPositionInterval(double positionInterval) {
        this.positionInterval = positionInterval;
    }
}
