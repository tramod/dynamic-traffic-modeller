/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.lang.reflect.Array;
import java.sql.*;
import java.util.*;

/**
 * Load model from PostGIS database
 * @author kolovsky, tpotuzak
 */
public class DtmModelLoader {
    /** Driver of the database */
    public static final String DATABASE_DRIVER = "org.postgresql.Driver";

    /** time of day */
    public static final String TABLE_TOD = "tod";
    public static final String COLUMN_TOD_TOD_ID = "tod_id";
    public static final String COLUMN_TOD_DAY = "day";
    public static final String COLUMN_TOD_START_TIME = "start_time";
    public static final String COLUMN_TOD_END_TIME = "end_time";

    /** node */
    public static final String TABLE_NODE = "node";
    public static final String COLUMN_NODE_NODE_ID = "node_id";
    public static final String COLUMN_NODE_CTRL_TYPE = "ctrl_type";
    public static final String COLUMN_NODE_USE_MOVEMENT = "use_movement";

    /** edge */
    public static final String TABLE_EDGE = "edge";
    public static final String COLUMN_EDGE_EDGE_ID = "edge_id";
    public static final String COLUMN_EDGE_FROM_NODE_ID = "from_node_id";
    public static final String COLUMN_EDGE_TO_NODE_ID = "to_node_id";
    public static final String COLUMN_EDGE_CAPACITY = "capacity";
    public static final String COLUMN_EDGE_LANES = "lanes";
    public static final String COLUMN_EDGE_FREE_SPEED = "free_speed";
    public static final String COLUMN_EDGE_CRITICAL_SPEED = "critical_speed";
    public static final String COLUMN_EDGE_LENGTH = "length";

    /** zone */
    public static final String TABLE_ZONE = "zone";
    public static final String COLUMN_ZONE_INCOMING_TRIPS = "incoming_trips";
    public static final String COLUMN_ZONE_OUTGOING_TRIPS = "outgoing_trips";
    public static final String COLUMN_ZONE_NODE_ID = "node_id";
    public static final String COLUMN_ZONE_ZONE_ID = "zone_id";

    /** origin-destination matrix */
    public static final String TABLE_ODM = "odm";
    public static final String COLUMN_ODM_ORIGIN_ZONE_ID = "origin_zone_id";
    public static final String COLUMN_ODM_DESTINATION_ZONE_ID = "destination_zone_id";
    public static final String COLUMN_ODM_TOD_ID = "tod_id";
    public static final String COLUMN_ODM_FLOW = "flow";

    /** movements */
    public static final String TABLE_MVMT = "mvmt";
    public static final String COLUMN_MVMT_MVMT_ID = "mvmt_id";
    public static final String COLUMN_MVMT_NODE_ID = "node_id";
    public static final String COLUMN_MVMT_IB_EDGE_ID = "ib_edge_id";
    public static final String COLUMN_MVMT_START_IB_LANE = "start_ib_lane";
    public static final String COLUMN_MVMT_END_IB_LANE = "end_ib_lane";
    public static final String COLUMN_MVMT_OB_EDGE_ID = "ob_edge_id";
    public static final String COLUMN_MVMT_START_OB_LANE = "start_ob_lane";
    public static final String COLUMN_MVMT_END_OB_LANE = "end_ob_lane";
    public static final String COLUMN_MVMT_TYPE = "type";
    public static final String COLUMN_MVMT_CAPACITY = "capacity";

    /** movement conflicts */
    public static final String TABLE_MVMT_CONFLICT = "mvmt_conflict";
    public static final String COLUMN_MVMT_CONFLICT_MINOR_MVMT_ID = "minor_mvmt_id";
    public static final String COLUMN_MVMT_CONFLICT_MAJOR_MVMT_ID = "major_mvmt_id";
    public static final String COLUMN_MVMT_CONFLICT_COEFICIENT = "coeficient";

    /** The URL of the database */
    protected String dbUrl;
    /** The JDBC connection to the database */
    protected Connection connection;
    /** The statement for performing of SQL queries */
    protected Statement statement;

    public DtmModelLoader(String dbUrl) throws ClassNotFoundException, SQLException {
        this.dbUrl = dbUrl;
        createConnection();
    }

    /**
     * Read and construct model from database
     * @param modelName name of the model
     * @param day day in week (0 - monday, 6 - sunday)
     * @return origin-destination matrix that includes graph
     */
    public DtmOriginDestinationMatrix readModel(String modelName, int day) throws Exception {
        DtmPairElement<DtmGraph, Map<DtmEdge, Integer>> g = readGraph(modelName);
        DtmGraph graph = g.first;
        Map<DtmEdge, Integer> edgeLanes = g.second;

        DtmOriginDestinationMatrix odm = readOriginDestinationMatrix(modelName, graph, day);
        odm.initializeOrigins();
        includeIntersectionsToGraph(modelName, graph, edgeLanes);
        return odm;
    }

    /**
     * Read simple graph from database (without intersection)
     * @param modelName name of the model
     * @return (graph, number of lanes for edges)
     */
    public DtmPairElement<DtmGraph, Map<DtmEdge, Integer>> readGraph(String modelName) throws Exception {
        String edgeFields = String.join(", ", List.of(COLUMN_EDGE_EDGE_ID, COLUMN_EDGE_FROM_NODE_ID,
                COLUMN_EDGE_TO_NODE_ID, COLUMN_EDGE_CAPACITY, COLUMN_EDGE_LANES, COLUMN_EDGE_FREE_SPEED, COLUMN_EDGE_LENGTH,
                COLUMN_EDGE_CRITICAL_SPEED));
        ResultSet resultSet = performSQLQuery("SELECT "+edgeFields+" FROM "+modelName+"."+TABLE_EDGE);
        DtmGraph graph = new DtmGraph();

        Map<DtmEdge, Integer> edgeLanes = new HashMap<>();

        while (resultSet.next()){
            int edgeId = resultSet.getInt(1);
            int source = resultSet.getInt(2);
            int target = resultSet.getInt(3);
            double capacity = resultSet.getDouble(4);
            int lanes = resultSet.getInt(5);
            double speed = resultSet.getDouble(6);
            double length = resultSet.getDouble(7);
            double criticalSpeed = resultSet.getDouble(8);

            double maximalDensity = 167 * lanes; // TODO 6m per vehicle

            /* compute according:
             * Smulders (1990). Control of freeway traffic ﬂow by variable speed signs.
             * Reduce the fundamental diagram to Quadratic-Linear form */
            double waveSpeed = (capacity*criticalSpeed)/(capacity-maximalDensity*criticalSpeed) - 1E-10;

            DtmFundamentalDiagram fd = new DtmFundamentalDiagram(speed, capacity, maximalDensity, criticalSpeed, waveSpeed);
            graph.addEdge(edgeId, source, target, length, fd);
            edgeLanes.put(graph.getEdgeById(edgeId), lanes);
        }

        graph.initializeStructures();
        graph.constructDualGraph();

        return new DtmPairElement<>(graph, edgeLanes);
    }

    /**
     * Includes intersections to the graph.
     * @param modelName name of the model
     * @param graph graph
     * @param edgeLanes number of lanes for the edges
     */
    public void includeIntersectionsToGraph(String modelName, DtmGraph graph, Map<DtmEdge, Integer> edgeLanes) throws Exception {
        // intersections
        Map<DtmNode, Intersection> intersections = readIntersections(modelName, graph);
        System.out.println(intersections.keySet());

        String nodeFields = String.join(", ", List.of(COLUMN_NODE_NODE_ID, COLUMN_NODE_CTRL_TYPE, COLUMN_NODE_USE_MOVEMENT));
        ResultSet resultSet = performSQLQuery("SELECT "+nodeFields+" FROM "+modelName+"."+TABLE_NODE);
        while (resultSet.next()){
            int nodeId = resultSet.getInt(1);
            CtrlType ctrlType = CtrlType.valueOf(resultSet.getString(2));
            boolean useMovements = resultSet.getBoolean(3);
            if (!graph.nodesAccessTable.containsKey(nodeId)){
                continue;
            }
            DtmNode node = graph.getNodeById(nodeId);

            int incomingLength = node.incomingEdges.size();
            if (node.isOrigin()){
                incomingLength += 1;
            }

            // priorities according the capacities
            double[] priorities = new double[incomingLength];
            for (int i = 0; i < node.incomingEdges.size(); i++){
                priorities[i] = node.getIncomingEdges().get(i).fundamentalDiagram.getFlowCapacity();
            }
            if (node.isOrigin()){
                priorities[incomingLength - 1] = 10000.0;
            }

            if (ctrlType == CtrlType.none){
                //DtmNodeModel nodeModel = new DtmTampereNodeModel(node, new HashMap<>(), priorities);
                DtmNodeModel nodeModel = new DtmNoFifoNodeModel(node, new HashMap<>(), priorities);
                node.setNodeModel(nodeModel);
                continue;
            }

            if (useMovements){
                Intersection intersection = intersections.get(node);
                if (intersection == null){
                    DtmNodeModel nodeModel = new DtmNoFifoNodeModel(node, new HashMap<>(), priorities);
                    node.setNodeModel(nodeModel);
                    System.out.println("Field node.use_movement = true but there are no movements for the node_id = "+node.getId());
                    continue;
                }
                DtmPairElement<Double, Double>[][][] mri = constructMutualRestrictionIntervals(intersection, edgeLanes);

                boolean useFixed = true;
                // only fixed capacities for now

                // for (Movement[] movements: intersection.movements){
                //     for (Movement movement: movements){
                //         if (movement.dependOn.size() > 0){
                //             useFixed = false;
                //             break;
                //         }
                //     }
                // }

                DtmNodeCapacity capacityModel;
                if (useFixed){
                    double[][] capacity = new double[incomingLength][node.outgoingEdges.size()];
                    for (int i = 0; i < node.incomingEdges.size(); i++){
                        for (int j = 0; j < node.outgoingEdges.size(); j++){
                            if (intersection.movements[i][j] != null){
                                capacity[i][j] = intersection.movements[i][j].capacity;
                            }
                            else {
                                capacity[i][j] = 0.0;
                            }

                            // remove the movement (dual graph)
                            if (capacity[i][j] < 1E-10){
                                DtmEdge in = node.incomingEdges.get(i);
                                DtmEdge out = node.outgoingEdges.get(j);
                                DtmDualEdge movement = node.getDualEdge(in, out);
                                if (movement == null){
                                    System.out.println("The movement (node, in, out) = ("+node.getId()+", "+in.getId()+", "+out.getId()+")");
                                }
                                node.removeDualEdge(movement);
                            }
                        }
                    }
                    if(node.isOrigin()){
                        for (int j = 0; j < node.outgoingEdges.size(); j++){
                            capacity[incomingLength - 1][j] = 10000.0;
                        }
                    }
                    capacityModel = new DtmFixedCapacity(capacity);
                }
                else{
                    List<DtmBeta> betas = new ArrayList<>();
                    double[][] nti = new double[incomingLength][node.outgoingEdges.size()];
                    @SuppressWarnings("unchecked")
                    DtmPairElement<Double, Double>[][] cti = (DtmPairElement<Double, Double>[][]) Array.newInstance(
                            DtmPairElement.class, incomingLength, node.outgoingEdges.size());

                    for (int i = 0; i < node.incomingEdges.size(); i++){
                        for (int j = 0; j < node.outgoingEdges.size(); j++){
                            Movement movement = intersection.movements[i][j];
                            for (DtmPairElement<Movement, Double> mov: movement.dependOn) {
                                int k = node.incomingEdges.indexOf(mov.first.fromEdge);
                                int l = node.outgoingEdges.indexOf(mov.first.toEdge);
                                betas.add(new DtmBeta(i, j, k, l, mov.getSecond()));
                            }
                            nti[i][j] = movement.type.getNextTimeInterval(ctrlType);
                            cti[i][j] = movement.type.getCriticalTimeInterval();
                        }
                    }
                    if (node.isOrigin()){
                        int i = incomingLength - 1;
                        for (int j = 0; j < node.outgoingEdges.size(); j++){
                            nti[i][j] = MovType.other.getNextTimeInterval(ctrlType);
                            cti[i][j] = MovType.other.getCriticalTimeInterval();
                        }
                    }
                    capacityModel = new DtmNodeCapacityEDIP(betas, node.incomingEdges.size(), node.outgoingEdges.size(), cti, nti);
                }
                node.setNodeModel(new DtmWRICNodeModel(node, new HashMap<>(), priorities, mri, capacityModel));
            }
        }
    }

    private DtmPairElement<Double, Double>[][][] constructMutualRestrictionIntervals(Intersection intersection, Map<DtmEdge, Integer> edgeLanes){
        DtmNode node = intersection.node;

        int numIncoming = node.getIncomingEdges().size();
        if (node.isOrigin()){
            numIncoming += 1;
        }

        @SuppressWarnings("unchecked")
        DtmPairElement<Double, Double>[][][] mri = (DtmPairElement<Double, Double>[][][]) Array.newInstance(
                DtmPairElement.class, numIncoming, node.getOutgoingEdges().size(), node.getOutgoingEdges().size());

        for (int i = 0; i < node.getIncomingEdges().size(); i++){
            DtmEdge inEdge = node.incomingEdges.get(i);
            int lanes = edgeLanes.get(inEdge);
            for (int j = 0; j < node.getOutgoingEdges().size(); j++){
                for (int jj = 0; jj < node.getOutgoingEdges().size(); jj++){
                    if (j == jj || lanes == 1){
                        mri[i][jj][j] = new DtmPairElement<>(0.0, 1.0);
                    }
                    else {
                        Movement affectedMov = intersection.movements[i][j];
                        Movement affectingMov = intersection.movements[i][jj];
                        if (affectedMov != null && affectingMov != null){
                            double step = 1.0 / (affectedMov.toLane - affectedMov.fromLane + 1);
                            int lo = Math.max(affectedMov.fromLane, affectingMov.fromLane);
                            int up = Math.min(affectedMov.toLane, affectingMov.toLane);
                            // lanes overlapped
                            if (lo <= up){
                                double mriLo = (lo - affectedMov.fromLane) * step;
                                double mriUp;
                                if (up == affectedMov.toLane){
                                    mriUp = 1.0;
                                } else {
                                    mriUp = (up - affectedMov.fromLane + 1) * step;
                                }
                                mri[i][jj][j] = new DtmPairElement<>(mriLo, mriUp);
                            }
                            else {
                                mri[i][jj][j] = new DtmPairElement<>(0.0, 0.0);
                            }
                        }
                        else {
                            mri[i][jj][j] = new DtmPairElement<>(0.0, 0.0);
                        }
                    }
                }
            }
        }

        // for source coming form zone
        for (int j = 0; j < node.getOutgoingEdges().size(); j++){
            for (int jj = 0; jj < node.getOutgoingEdges().size(); jj++){
                if (j == jj){
                    mri[numIncoming - 1][jj][j] = new DtmPairElement<>(0.0, 1.0);
                }
                else{
                    mri[numIncoming - 1][jj][j] = new DtmPairElement<>(0.0, 0.0);
                }
            }
        }

        return mri;
    }

    /**
     * @param time time
     * @param offset time offset in hours
     * @return number of hours from midnight
     */
    private double toHoursFromMidnight(Time time, double offset){
        return toHoursFromMidnight(time) - offset;
    }

    private double toHoursFromMidnight(Time time){
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(time);
        return calendar.get(Calendar.HOUR) + calendar.get(Calendar.MINUTE)/60.0 + calendar.get(Calendar.SECOND)/3600.0;
    }

    /**
     * @param modelName name of the model
     * @param day day number (Monday - Sunday, 0 - 6)
     * @return origin-destination matrix
     * @throws SQLException SQL problem
     */
    public DtmOriginDestinationMatrix readOriginDestinationMatrix(String modelName, DtmGraph graph, int day) throws SQLException {
        DtmOriginDestinationMatrix odm = new DtmOriginDestinationMatrix(graph);

        Map<Integer, DtmZone> idToZone = new HashMap<>();
        Map<Integer, DtmPairElement<Time, Time>> timesOfDay = new HashMap<>();
        Set<DtmZone> origins = new HashSet<>();
        Set<DtmZone> destinations = new HashSet<>();

        // Zones
        ResultSet resultSet = performSQLQuery("SELECT "+COLUMN_ZONE_ZONE_ID+", "+COLUMN_ZONE_NODE_ID+" FROM "+modelName+"."+TABLE_ZONE);
        while (resultSet.next()){
            int zoneId = resultSet.getInt(1);
            int nodeId = resultSet.getInt(2);
            DtmNode node = graph.getNodeById(nodeId);
            DtmZone zone = new DtmZone(zoneId, node);

            odm.zones.add(zone);
            odm.zonesAccessTable.put(node, zone);
            idToZone.put(zoneId, zone);
        }

        // Time of day
        String todFields = String.join(", ", List.of(COLUMN_TOD_START_TIME, COLUMN_TOD_END_TIME, COLUMN_TOD_TOD_ID));
        resultSet = performSQLQuery("SELECT "+todFields+" FROM "+modelName+"."+TABLE_TOD+" " +
                "WHERE "+COLUMN_TOD_DAY+" = "+day+" ORDER BY "+COLUMN_TOD_START_TIME);
        while (resultSet.next()){
            Time startTime = resultSet.getTime(1);
            Time endTime = resultSet.getTime(2);
            int todId = resultSet.getInt(3);
            timesOfDay.put(todId, new DtmPairElement<>(startTime, endTime));
        }
        Time simulationStartTime = timesOfDay.values().stream().min(Comparator.comparing(x -> x.first)).get().first;
        Time simulationEndTime = timesOfDay.values().stream().min(Comparator.comparing(x -> x.second)).get().second;
        double offset = toHoursFromMidnight(simulationStartTime);

        // Origin-Destination matrix
        String odmFields = String.join(", ", List.of(COLUMN_ODM_ORIGIN_ZONE_ID, COLUMN_ODM_DESTINATION_ZONE_ID,
                COLUMN_ODM_TOD_ID, COLUMN_ODM_FLOW));
        resultSet = performSQLQuery("SELECT "+odmFields+" FROM "+modelName+"."+TABLE_ODM+" ORDER BY "
                +COLUMN_ODM_ORIGIN_ZONE_ID+", "+COLUMN_ODM_DESTINATION_ZONE_ID);

        List<DtmTripleElement<Time, Time, Double>> onePairPieces = new ArrayList<>(timesOfDay.size());
        DtmZone currentOrigin = null;
        DtmZone currentDestination = null;
        DtmZone origin = null;
        DtmZone destination = null;

        while (resultSet.next()){

            int originZoneId = resultSet.getInt(1);
            int destinationZoneId = resultSet.getInt(2);
            origin = idToZone.get(originZoneId);
            destination = idToZone.get(destinationZoneId);

            if (currentOrigin == null){
                currentOrigin = origin;
                currentDestination = destination;
            }

            if (currentOrigin != origin || currentDestination != destination){
                if (onePairPieces.size() != timesOfDay.size()){
                    System.out.println("The function is not fully described.");
                }
                onePairPieces.sort(Comparator.comparing(a -> a.first));

                // construct function
                DtmPiecewiseConstantFunction flowFunc = new DtmPiecewiseConstantFunction();
                for (DtmTripleElement<Time, Time, Double> piece: onePairPieces){
                    flowFunc.addPoint(toHoursFromMidnight(piece.first, offset), piece.third);
                }
                DtmTripleElement<Time, Time, Double> piece = onePairPieces.get(onePairPieces.size() - 1);
                flowFunc.addPoint(toHoursFromMidnight(piece.second, offset), 0.0); // TODO 1 veh/s really?

                DtmOriginDestinationPair pair = new DtmOriginDestinationPair(currentOrigin, currentDestination, flowFunc);
                odm.pairs.add(pair);

                if (!odm.originZonesPairs.containsKey(origin)) {
                    odm.originZonesPairs.put(origin, new ArrayList<>());
                }
                odm.originZonesPairs.get(origin).add(pair);

                origins.add(origin);
                destinations.add(destination);

                onePairPieces.clear();
                currentOrigin = origin;
                currentDestination = destination;
            }

            int todId = resultSet.getInt(3);
            double flow = resultSet.getDouble(4);
            onePairPieces.add(new DtmTripleElement<>(timesOfDay.get(todId).first, timesOfDay.get(todId).second, flow));
        }

        if (!onePairPieces.isEmpty()){
            if (onePairPieces.size() != timesOfDay.size()){
                System.out.println("The function is not fully described.");
            }
            onePairPieces.sort(Comparator.comparing(a -> a.first));

            // construct function
            DtmPiecewiseConstantFunction flowFunc = new DtmPiecewiseConstantFunction();
            for (DtmTripleElement<Time, Time, Double> piece: onePairPieces){
                flowFunc.addPoint(toHoursFromMidnight(piece.first, offset), piece.third);
            }
            DtmTripleElement<Time, Time, Double> piece = onePairPieces.get(onePairPieces.size() - 1);
            flowFunc.addPoint(toHoursFromMidnight(piece.second, offset), 1.0);

            DtmOriginDestinationPair pair = new DtmOriginDestinationPair(currentOrigin, currentDestination, flowFunc);
            odm.pairs.add(pair);

            if (!odm.originZonesPairs.containsKey(origin)) {
                odm.originZonesPairs.put(origin, new ArrayList<>());
            }
            odm.originZonesPairs.get(origin).add(pair);

            origins.add(origin);
            destinations.add(destination);
        }


        odm.originZones.addAll(origins);
        odm.destinationZones.addAll(destinations);
        return odm;
    }

    /**
     * Performs an SQL query (i.e. a SELECT) and returns the result of the query
     * @param sqlQuery a SELECT SQL query
     * @return the result of the query
     * @throws SQLException if there was a problem with the query
     */
    private ResultSet performSQLQuery(String sqlQuery) throws SQLException {
        return statement.executeQuery(sqlQuery);
    }

    /**
     * Connects to the database using JDBC driver
     * @throws ClassNotFoundException if the database driver is not found
     * @throws SQLException if there was a problem with establishing the connection (e.g., invalid user of password)
     */
    private void createConnection() throws ClassNotFoundException, SQLException {
        Class.forName(DATABASE_DRIVER);
        connection = DriverManager.getConnection(dbUrl);
        statement = connection.createStatement();
    }

    /**
     * Initialize the node distribution according all or nothing
     * The node model with empty distribution must initialized before call this function
     * @param odm origin-destination matrix
     * @param maxTime maximal time for function initialization
     * @param discrete if true, the distribution will be initialized using DtmDiscreteFunction instead of DtmPiecewiseConstantFunction
     * @param numBins number of intervals for discrete functions
     */
    public static void allOrNothingAssignment(DtmOriginDestinationMatrix odm, double maxTime, boolean discrete, int numBins){
        DtmGraph graph = odm.getGraph();
        for(DtmNode node: graph.getNodes()){
            if (node.nodeModel == null){
                System.out.println("First, please initialize the node models with empty distribution");
                return;
            }
            DtmDestinationNodeModel nodeModel = (DtmDestinationNodeModel) node.nodeModel;
            nodeModel.distributions.clear();

            DtmPairElement<List<Double>, List<DtmEdge>> searchResult = graph.performShortestPathSearchDual(node);
            List<Double> dist = searchResult.getFirst();
            List<DtmEdge> prev = searchResult.getSecond();

            int numIncoming = node.incomingEdges.size();
            if (node.isOrigin()){
                numIncoming += 1;
            }
            int numOutgoing = node.outgoingEdges.size();

            for (DtmZone destination: odm.getDestinationZones()){
                if (node.getId() != destination.node.getId()){
                    if (dist.get(destination.node.getIndex()) != Double.POSITIVE_INFINITY){
                        DtmEdge firstEdge = graph.extractPath(prev, destination.node).get(0);
                        int index = node.outgoingEdges.indexOf(firstEdge);

                        DtmFunction[][] distMatrix = new DtmFunction[numIncoming][numOutgoing];
                        for (int i = 0; i < numIncoming; i++){
                            for (int j = 0; j < numOutgoing; j++){
                                if (index == j){
                                    if (discrete){
                                        distMatrix[i][index] = new DtmDiscreteFunction(numBins, 1.0, 0.0, maxTime);
                                    }
                                    else{
                                        distMatrix[i][index] = new DtmPiecewiseConstantFunction(List.of(0.0, maxTime), List.of(1.0, 1.0));
                                    }
                                }
                                else{
                                    if (discrete){
                                        distMatrix[i][j] = new DtmDiscreteFunction(numBins, 0.0, 0.0, maxTime);
                                    }
                                    else{
                                        distMatrix[i][j] = new DtmPiecewiseConstantFunction(List.of(0.0, maxTime), List.of(0.0, 0.0));
                                    }
                                }
                            }
                        }
                        nodeModel.distributions.put(destination, new DtmDistributionMatrix(distMatrix));
                    }
                    else {
                        System.out.println("WARN there is no path from "+node.getId()+" to "+destination.node.getId());
                    }
                }
            }
        }
    }

    // INTERSECTIONS
    class Intersection{
        DtmNode node;
        Movement[][] movements;

        public Intersection(DtmNode node) {
            this.node = node;
            this.movements = new Movement[node.incomingEdges.size()][node.outgoingEdges.size()];
        }
    }

    class Movement{
        List<DtmPairElement<Movement, Double>> dependOn = new ArrayList<>();
        DtmEdge fromEdge;
        DtmEdge toEdge;
        double capacity;
        int fromLane;
        int toLane;
        MovType type;

        public Movement(DtmEdge fromEdge, DtmEdge toEdge, double capacity, int fromLane, int toLane, MovType type) {
            this.fromEdge = fromEdge;
            this.toEdge = toEdge;
            this.capacity = capacity;
            this.fromLane = fromLane;
            this.toLane = toLane;
            this.type = type;
        }
    }

    enum CtrlType{none, yield, stop, signal, constant}

    enum MovType{
        left_from_major(2.6, 2.6, 3.4, 0.021),
        right_from_minor(3.1, 3.7, 2.8, 0.038),
        through_from_minor(3.3, 3.9, 4.4, 0.036),
        left_from_minor(3.5, 4.1, 5.2, 0.022),
        other(1.0, 1.0, 1.0, 0.0);

        private final Double nextTimeIntervalYield;
        private final Double nextTimeIntervalStop;
        private final DtmPairElement<Double, Double> criticalTimeInterval;

        /**
         *  cti = ctiC + speed * ctiL
         * @param ntiYield next time interval [s] for yield
         * @param ntiStop next time interval [s] for stop
         * @param ctiC critical time interval [s], constant part
         * @param ctiL critical time interval [s], linear part
         */
        MovType(Double ntiYield, Double ntiStop, Double ctiC, Double ctiL){
            nextTimeIntervalYield = ntiYield;
            nextTimeIntervalStop = ntiStop;
            criticalTimeInterval = new DtmPairElement<>(ctiC, ctiL);
        }

        public Double getNextTimeInterval(CtrlType ctrlType) {
            if(ctrlType == CtrlType.stop){
                return nextTimeIntervalStop;
            }
            return nextTimeIntervalYield;
        }

        public DtmPairElement<Double, Double> getCriticalTimeInterval() {
            return criticalTimeInterval;
        }
    }

    /**
     * Read and return the intersection data from database
     * @param modelName name of the model
     * @param graph graph
     * @return intersection data
     * @throws SQLException database problem
     */
    private Map<DtmNode, Intersection> readIntersections(String modelName, DtmGraph graph) throws SQLException {
        String movFields = String.join(", ", List.of(COLUMN_MVMT_NODE_ID, COLUMN_MVMT_IB_EDGE_ID,
                COLUMN_MVMT_OB_EDGE_ID, COLUMN_MVMT_CAPACITY, COLUMN_MVMT_START_IB_LANE, COLUMN_MVMT_END_IB_LANE, COLUMN_MVMT_MVMT_ID, COLUMN_MVMT_TYPE));
        ResultSet resultSet = performSQLQuery("SELECT "+movFields+" FROM "+modelName+"."+TABLE_MVMT+" ORDER BY "+COLUMN_MVMT_NODE_ID);
        Map<DtmNode, Intersection> intersections = new HashMap<>();
        Map<Integer, Movement> idToMovement = new HashMap<>();
        Intersection intersection = null;
        while (resultSet.next()){
            DtmNode node = graph.getNodeById(resultSet.getInt(1));
            if (intersection == null){
                intersection = new Intersection(node);
            }
            if (intersection.node != node){
                intersections.put(intersection.node, intersection);
                intersection = new Intersection(node);
            }
            DtmEdge fromEdge = graph.getEdgeById(resultSet.getInt(2));
            DtmEdge toEdge = graph.getEdgeById(resultSet.getInt(3));
            double capacity = resultSet.getDouble(4);
            int fromLane = resultSet.getInt(5);
            int toLane = resultSet.getInt(6);
            if (toLane == 0){
                toLane = fromLane;
            }
            int movementId = resultSet.getInt(7);
            MovType type = MovType.valueOf(resultSet.getString(8));
            Movement movement = new Movement(fromEdge, toEdge, capacity, fromLane, toLane, type);
            idToMovement.put(movementId, movement);
            int i = node.incomingEdges.indexOf(fromEdge);
            int j = node.outgoingEdges.indexOf(toEdge);
            intersection.movements[i][j] = movement;
        }
        if (intersection != null){
            intersections.put(intersection.node, intersection);
        }


        String conFields = String.join(", ", List.of(COLUMN_MVMT_CONFLICT_MINOR_MVMT_ID, COLUMN_MVMT_CONFLICT_MAJOR_MVMT_ID, COLUMN_MVMT_CONFLICT_COEFICIENT));
        resultSet = performSQLQuery("SELECT "+conFields+" FROM "+modelName+"."+TABLE_MVMT_CONFLICT);
        while (resultSet.next()){
            int minor = resultSet.getInt(1);
            int major = resultSet.getInt(2);
            double coef = resultSet.getDouble(3);
            idToMovement.get(minor).dependOn.add(new DtmPairElement<>(idToMovement.get(major), coef));
        }
        return intersections;
    }

    public void saveResult(String modelName, String resultName, boolean overwrite, List<DtmEdge> edges,
                           int numIntervals, double startTime, double endTime) throws SQLException {
        List<String> columns = new ArrayList<>();
        columns.add("edge_id int");
        for (int i = 0; i<numIntervals; i++){
            columns.add("inflow_"+i+" real");
        }
        for (int i = 0; i<numIntervals; i++){
            columns.add("outflow_"+i+" real");
        }
        for (int i = 0; i<numIntervals; i++){
            columns.add("travel_"+i+" real");
        }
        String sql = "CREATE TABLE "+modelName+".result_"+resultName+" ";
        sql = sql + "("+String.join(", ", columns)+");";
        statement.executeUpdate(sql);

        for (DtmEdge edge: edges){
            double[] inflow = edge.getInflow().toDiscreteFunction(numIntervals, startTime, endTime, 0.5).values;
            double[] outflow = edge.getOutflow().toDiscreteFunction(numIntervals, startTime, endTime, 0.5).values;
            double[] travel = edge.getArrivalFunction().toTravelTimeFunction()
                    .toDiscreteFunction(numIntervals, startTime, endTime, 0.5).values;
            List<String> values = new ArrayList<>(numIntervals+1);
            values.add(edge.getId()+"");
            for (int i = 0; i<numIntervals; i++){
                values.add(inflow[i]+"");
            }
            for (int i = 0; i<numIntervals; i++){
                values.add(outflow[i]+"");
            }
            for (int i = 0; i<numIntervals; i++){
                values.add(travel[i]+"");
            }
            sql = "INSERT INTO "+modelName+".result_"+resultName+" VALUES("+String.join(", ", values)+")";
            statement.executeUpdate(sql);
        }
    }
}
