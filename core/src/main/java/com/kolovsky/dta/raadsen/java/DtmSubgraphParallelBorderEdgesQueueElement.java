/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * Element for a queue used for border edges processing
 * @author Tomas Potuzak
 */
public class DtmSubgraphParallelBorderEdgesQueueElement {
    /** The type of this element */
    protected DtmSubgraphParallelBorderEdgesQueueElementTypes type;
    /** The edge, whose flows or mixture shall be processed. It is set to <code>null</code> if the type of the element is <code>SubgraphParallelBorderEdgesQueueElementTypes.EVENT</code>.*/
    protected DtmEdge edge;
    /** The new inflow or outflow of the edge. It is set to 0.0 if the type of the element is <code>SubgraphParallelBorderEdgesQueueElementTypes.EVENT</code> or <code>SubgraphParallelBorderEdgesQueueElementTypes.OUTGOING_EDGE_MIXTURE</code>.*/
    protected double flow;
    /** The new mixture of the edge. It is set to <code>null</code> if the type of the element is not <code>SubgraphParallelBorderEdgesQueueElementTypes.OUTGOING_EDGE_MIXTURE</code>.*/
    protected DtmMixture<DtmIdentifiable> mixture;
    /** The event created during the border edges processing, which shall be processed. It is set to <code>null</code> of the type of the element is not <code>SubgraphParallelBorderEdgesQueueElementTypes.EVENT</code>.*/
    protected DtmEvent event;

    /**
     * Creates a new element representing processing of flow of an incoming or an outgoing edge
     * @param type the type of the element, the allowed values are <code>SubgraphParallelBorderEdgesQueueElementTypes.INCONING_EDGE_FLOW</code> or <code>SubgraphParallelBorderEdgesQueueElementTypes.OUTGOING_EDGE_FLOW</code> only
     * @param edge the edge whose flows shall be processed
     * @param flow the new inflow or outflow of the edge
     */
    public DtmSubgraphParallelBorderEdgesQueueElement(DtmSubgraphParallelBorderEdgesQueueElementTypes type, DtmEdge edge, double flow) {
        this.type = type;
        this.edge = edge;
        this.flow = flow;
    }

    /**
     * Creates a new element representing processing of mixture of an outgoing edge
     * @param edge the edge whose mixture shall be processed
     * @param mixture the new mixture of the edge
     */
    public DtmSubgraphParallelBorderEdgesQueueElement(DtmEdge edge, DtmMixture<DtmIdentifiable> mixture) {
        this.edge = edge;
        this.mixture = mixture;

        type = DtmSubgraphParallelBorderEdgesQueueElementTypes.OUTGOING_EDGE_MIXTURE;
    }

    /**
     * Creates a new element representing processing of an event created during the border edges processing
     * @param event the event, which shall be processed
     */
    public DtmSubgraphParallelBorderEdgesQueueElement(DtmEvent event) {
        this.event = event;

        type = DtmSubgraphParallelBorderEdgesQueueElementTypes.EVENT;
    }

    //Getters and setters

    /**
     * Returns the type of this element
     * @return the type of this element
     */
    public DtmSubgraphParallelBorderEdgesQueueElementTypes getType() {
        return type;
    }

    /**
     * Returns the edge, whose flows or mixture shall be processed. It should return <code>null</code> if the type of the element is <code>SubgraphParallelBorderEdgesQueueElementTypes.EVENT</code>.
     * @return the edge, whose flows or mixture shall be processed. It should return <code>null</code> if the type of the element is <code>SubgraphParallelBorderEdgesQueueElementTypes.EVENT</code>.
     */
    public DtmEdge getEdge() {
        return edge;
    }

    /**
     * Returns the new inflow or outflow of the edge. It should return 0.0 if the type of the element is <code>SubgraphParallelBorderEdgesQueueElementTypes.EVENT</code> or <code>SubgraphParallelBorderEdgesQueueElementTypes.OUTGOING_EDGE_MIXTURE</code>.
     * @return the new inflow or outflow of the edge. It should return 0.0 if the type of the element is <code>SubgraphParallelBorderEdgesQueueElementTypes.EVENT</code> or <code>SubgraphParallelBorderEdgesQueueElementTypes.OUTGOING_EDGE_MIXTURE</code>.
     */
    public double getFlow() {
        return flow;
    }

    /**
     * Returns the new mixture of the edge. It is set to <code>null</code> if the type of the element is not <code>SubgraphParallelBorderEdgesQueueElementTypes.OUTGOING_EDGE_MIXTURE</code>.
     * @return the new mixture of the edge. It is set to <code>null</code> if the type of the element is not <code>SubgraphParallelBorderEdgesQueueElementTypes.OUTGOING_EDGE_MIXTURE</code>.
     */
    public DtmMixture<DtmIdentifiable> getMixture() {
        return mixture;
    }

    /**
     * Returns the event created during the border edges processing, which shall be processed. It should return <code>null</code> of the type of the element is not <code>SubgraphParallelBorderEdgesQueueElementTypes.EVENT</code>.
     * @return the event created during the border edges processing, which shall be processed. It should return <code>null</code> of the type of the element is not <code>SubgraphParallelBorderEdgesQueueElementTypes.EVENT</code>.
     */
    public DtmEvent getEvent() {
        return event;
    }
}
