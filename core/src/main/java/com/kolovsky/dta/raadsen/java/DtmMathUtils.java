/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.List;

/**
 * A utility class for efficient calculation of powers with integer exponents and other mathematic functions
 * @author Tomas Potuzak
 */
public class DtmMathUtils {
    /** Default epsilon for comparison of double values */
    public static final double DEFAULT_COMPARISON_EPSILON = 1E-10;

    /**
     * Calculates and returns the power of the specified integer base with specified integer exponent. The power is calculated in a cycle and is more efficient than general <code>Math.pow()</code>
     * @param base the base of the power
     * @param exponent the exponent of the power
     * @return the power of the specified integer base with specified integer exponent
     * @throws IllegalArgumentException if the exponent is negative
     */
    public static int power(int base, int exponent) throws IllegalArgumentException {
        if (exponent < 0) {
            throw new IllegalArgumentException("Exponent cannot be negative.");
        }
        else if (exponent == 0) {
            return 1;
        }
        else {
            int result = 1;

            for (int i = 0; i < exponent; i++) {
                result *= base;
            }

            return result;
        }
    }

    /**
     * Calculates and returns the power of the specified doube base with specified integer exponent. The power is calculated in a cycle and is more efficient than general <code>Math.pow()</code>
     * @param base the base of the power
     * @param exponent the exponent of the power
     * @return the power of the specified integer base with specified integer exponent
     * @throws IllegalArgumentException if the exponent is negative
     */
    public static double power(double base, int exponent) throws IllegalArgumentException {
        if (exponent < 0) {
            throw new IllegalArgumentException("Exponent cannot be negative.");
        }
        else if (exponent == 0) {
            return 1;
        }
        else {
            double result = 1;

            for (int i = 0; i < exponent; i++) {
                result *= base;
            }

            return result;
        }
    }

    /**
     * Calculates and return the second power of the specified integer base
     * @param base the base of the second power
     * @return the second power of the specified integer base
     */
    public static int square(int base) {
        return power(base, 2);
    }

    /**
     * Calculates and return the second power of the specified double base
     * @param base the base of the second power
     * @return the second power of the specified double base
     */
    public static double square(double base) {
        return power(base, 2);
    }

    /**
     * Performs a specified arithmetic operation between two operands
     * Supported arithmetic operations: +, -, *, /
     * @param x1 first operand
     * @param x2 second operand
     * @param operation the operator of he arithmetic operation
     * @return the result of the arithmetic operation
     * @throws IllegalArgumentException if the specified operation is not supported or
     * @throws NullPointerException if the specified operation is <code>null</code>
     */
    public static double performOperation(double x1, double x2, String operation) throws IllegalArgumentException, NullPointerException {
        if (operation == null) {
            throw new NullPointerException("Arithmetic operation must not be null.");
        }

        switch (operation) {
            case "+":
                return x1 + x2;
            case "-":
                return x1 - x2;
            case "*":
                return x1 * x2;
            case "/":
                return x1 / x2;
            default:
                throw new IllegalArgumentException("Unsupported arithmetic operation: " + operation);
        }
    }

    /**
     * Performs interpolation on a line segment of a piecewise linear function described by <i>x</i> and <i>y</i> values
     * @param index the index of the end point of the line segment in range <1; <code>xValues.size()</code> - 1>
     * @param xValues the <i>x</i> values of the points of the piecewise linear function
     * @param yValues the <i>y</i> values of the points of the piecewise linear function
     * @param x the <i>x</i> value, for which the <i>y</i> = <i>f</i>(<i>x</i>) value shall be interpolated
     * @return the interpolated <i>y</i> = <i>f</i>(<i>x</i>) value
     * @throws IllegalArgumentException if the <code>x</code> value is not in the line segment specified by the index of its end point or if the index is out of range or if <code>xValues</code> and <code>xValues</code> have different lengths
     * @throws NullPointerException if <code>xValues</code> and/or <code>xValues</code> are <code>null</code>
     */
    public static double interpolateLine(int index, List<Double> xValues, List<Double> yValues, double x) throws IllegalArgumentException, NullPointerException {
        if (xValues == null || yValues == null) {
            throw new NullPointerException("The x and y values of the piecewise linear function cannot be null.");
        }
        else if (xValues.size() != yValues.size()) {
            throw new IllegalArgumentException("The x and y values of the piecewise linear function must have the same lengths.");
        }
        else if (index <= 0 || index >= xValues.size()) {
            throw new IllegalArgumentException("The index of end point of the segment must be in range <1; " + (xValues.size() - 1) + ">");
        }
        else if (x < xValues.get(index - 1) || x >= xValues.get(index)) {
            throw new IllegalArgumentException("The x value does not correspond to the the segment specified by the index of its endpoint.");
        }
        else {
            return yValues.get(index - 1) + (x - xValues.get(index - 1)) / (xValues.get(index) - xValues.get(index - 1)) * (yValues.get(index) - yValues.get(index - 1));
        }
    }

    /**
     * Calculates intersection of a linear function given by its two points and a horizontal line given by its y value (level) and returns the x value of the intersection
     * @param x1 the x value of the first point of the linear function
     * @param y1 the y value of the first point of the linear function
     * @param x2 the x value of the second point of the linear function
     * @param y2 the y value of the second point of the linear function
     * @param level the y value of the linear function
     * @return the x value of the intersection
     */
    public static double calculateIntersectionX(double x1, double y1, double x2, double y2, double level) throws IllegalArgumentException {
        double k = (y2 - y1) / (x2 - x1);
        double q = y1 - k * x1;

        return (level - q) / k;
    }

    /**
     * Calculates and returns the area of the trapezoid specified by its parallel sides and its height (i.e., distance of its parallel sides)
     * @param sideA parallel side <i>a</i>
     * @param sideC parallel side <i>b</i>
     * @param height the distance of the parallel sides <i>a</i> and <i>b</i>
     * @return the area of the specified trapezoid
     */
    public static double calculateTrapezoidArea(double sideA, double sideC, double height) {
        return ((sideA + sideC) / 2) * height;
    }

    /**
     * Divides the specified interval into required number of equally-sized sub-intervals (steps) and returns the boundaries of sub-intervals
     * @param start the start of the interval
     * @param end the end of the interval
     * @param stepsCount the number of steps
     * @return the boundaries of sub-intervals
     * @throws IllegalArgumentException if the end of the interval is lower or equal to its start or the number of steps is non-positive
     */
    public static double[] divideInterval(double start, double end, int stepsCount) throws IllegalArgumentException {
        if (start >= end) {
            throw new IllegalArgumentException("Start of the interval cannot be higher or equal to its end.");
        }
        else if (stepsCount <= 0) {
            throw new IllegalArgumentException("Number of steps must be positive.");
        }
        else {
            double step = (end - start) / stepsCount;
            double[] result = new double[stepsCount + 1];

            result[0] = start;
            for (int i = 1; i < stepsCount; i++) {
                result[i] = result[i - 1] + step;
            }
            result[result.length - 1] = end;

            return result;
        }
    }

    /**
     * Calculates and returns the number of steps, into which the specified interval shall be divide based on the specified step length
     * @param start the start of the interval
     * @param end the end of the interval
     * @param step the step length
     * @return the number of steps, into which the specified interval shall be divide based on the specified step length
     * @throws IllegalArgumentException if the end of the interval is lower or equal to its start or the step length is non-positive
     */
    public static int stepToStepsCount(double start, double end, double step) throws IllegalArgumentException {
        if (start >= end) {
            throw new IllegalArgumentException("Start of the interval cannot be higher or equal to its end.");
        }
        else if (step <= 0) {
            throw new IllegalArgumentException("Step must be positive.");
        }
        else {
            return Math.max((int) Math.round((end - start) / step), 1);
        }
    }

    /**
     * Divides the specified interval into number of equally-sized sub-intervals (steps) of the (approximately) specified length and returns the boundaries of sub-intervals
     * @param start the start of the interval
     * @param end the end of the interval
     * @param step the step length
     * @return the boundaries of sub-intervals
     * @throws IllegalArgumentException if the end of the interval is lower or equal to its start or the step length is non-positive
     */
    public static double[] divideInterval(double start, double end, double step) throws IllegalArgumentException {
        return divideInterval(start, end, stepToStepsCount(start, end, step));
    }

    /**
     * Compares two double values with specified comparison tolerance
     * @param value1 first value to be compared
     * @param value2 second value to be compared
     * @param comparisonEpsilon the comparison tolerance
     * @return <code>true</code> if the two values are equal with the specified comparison tolerance. Returns <code>false</code> otherwise
     */
    public static boolean isEqualWithEpsilon(double value1, double value2, double comparisonEpsilon) {
        return Math.abs(value1 - value2) <= comparisonEpsilon;
    }

    /**
     * Compares two double values with default comparison tolerance
     * @param value1 first value to be compared
     * @param value2 second value to be compared
     * @return <code>true</code> if the two values are equal with the default comparison tolerance. Returns <code>false</code> otherwise
     */
    public static boolean isEqualWithEpsilon(double value1, double value2) {
        return Math.abs(value1 - value2) <= DEFAULT_COMPARISON_EPSILON;
    }

    /**
     * Compares two double values with the specified comparison tolerance
     * @param value1 first value to be compared
     * @param value2 second value to be compared
     * @return <code>true</code> if the first value is lower than the second value with the specified comparison tolerance. Returns <code>false</code> otherwise
     */
    public static boolean isLowerThanWithEpsilon(double value1, double value2, double comparisonEpsilon) {
        return (value1 <= value2 - comparisonEpsilon);
    }

    /**
     * Compares two double values with default comparison tolerance
     * @param value1 first value to be compared
     * @param value2 second value to be compared
     * @return <code>true</code> if the first value is lower than the second value with the default comparison tolerance. Returns <code>false</code> otherwise
     */
    public static boolean isLowerThanWithEpsilon(double value1, double value2) {
        return isLowerThanWithEpsilon(value1, value2, DEFAULT_COMPARISON_EPSILON);
    }

    /**
     * Compares two double values with the specified comparison tolerance
     * @param value1 first value to be compared
     * @param value2 second value to be compared
     * @return <code>true</code> if the first value is lower than or equal to the second value with the specified comparison tolerance. Returns <code>false</code> otherwise
     */
    public static boolean isLowerThanOrEqualWithEpsilon(double value1, double value2, double comparisonEpsilon) {
        return (value1 <= value2 + comparisonEpsilon);
    }

    /**
     * Compares two double values with default comparison tolerance
     * @param value1 first value to be compared
     * @param value2 second value to be compared
     * @return <code>true</code> if the first value is lower than or equal to the second value with the default comparison tolerance. Returns <code>false</code> otherwise
     */
    public static boolean isLowerThanOrEqualWithEpsilon(double value1, double value2) {
        return isLowerThanWithEpsilon(value1, value2, DEFAULT_COMPARISON_EPSILON);
    }

    /**
     * Determines whether the specified value is within specified bounds (bounds-inclusive). The bounds do not have to be ordered.
     * @param value the value, whose presence within the specified bounds is investigated
     * @param bound1 first bound, not necessarily the lower one
     * @param bound2 second bound, not necessarily the upper one
     * @return <code>true</code> if the value is within the bounds. Returns <code>false</code> otherwise
     */
    public static boolean isBetween(double value, double bound1, double bound2) {
        double lowerBound = Math.min(bound1, bound2);
        double upperBound = Math.max(bound1, bound2);

        return (value >= lowerBound && value <= upperBound);
    }


    /**
     * Determines whether the specified value is within specified bounds (bounds-inclusive) with the specified comparison tolerance. The bounds do not have to be ordered.
     * @param value the value, whose presence within the specified bounds is investigated
     * @param bound1 first bound, not necessarily the lower one
     * @param bound2 second bound, not necessarily the upper one
     * @param comparisonEpsilon the comparison tolerance
     * @return <code>true</code> if the value is within the bounds with the specified comparison tolerance. Returns <code>false</code> otherwise
     */
    public static boolean isBetweenWithEpsilon(double value, double bound1, double bound2, double comparisonEpsilon) {
        double lowerBound = Math.min(bound1 - comparisonEpsilon, bound2 - comparisonEpsilon);
        double upperBound = Math.max(bound1 + comparisonEpsilon, bound2 + comparisonEpsilon);

        return (value >= lowerBound && value <= upperBound);
    }

    /**
     * Determines whether the specified value is within specified bounds (bounds-inclusive) with the default comparison tolerance. The bounds do not have to be ordered.
     * @param value the value, whose presence within the specified bounds is investigated
     * @param bound1 first bound, not necessarily the lower one
     * @param bound2 second bound, not necessarily the upper one
     * @return <code>true</code> if the value is within the bounds with the default comparison tolerance. Returns <code>false</code> otherwise
     */
    public static boolean isBetweenWithEpsilon(double value, double bound1, double bound2) {
        return isBetweenWithEpsilon(value, bound1, bound2, DEFAULT_COMPARISON_EPSILON);
    }

    /**
     * Determines and returns the intersection point of two line segments <i>a</i> = AB and <i>b</i> = CD if such intersection point exists. Parallel lines are considered not to intersect even when they are overlapping or touching in one point.
     * Based on https://stackoverflow.com/questions/16314069/calculation-of-intersections-between-line-segments
     * @param x1 the x coordinate of the A point of the line segment <i>a</i>
     * @param y1 the y coordinate of the A point of the line segment <i>a</i>
     * @param x2 the x coordinate of the B point of the line segment <i>a</i>
     * @param y2 the y coordinate of the B point of the line segment <i>a</i>
     * @param x3 the x coordinate of the C point of the line segment <i>b</i>
     * @param y3 the y coordinate of the C point of the line segment <i>b</i>
     * @param x4 the x coordinate of the D point of the line segment <i>b</i>
     * @param y4 the y coordinate of the D point of the line segment <i>b</i>
     * @return the intersection point of two line segments <i>a</i> = AB and <i>b</i> = CD if such intersection point exists
     * @throws ArithmeticException if the intersection point does not exist
     */
    public static double[] determineIntersection(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4) throws ArithmeticException {
        double denominator = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
        if (isEqualWithEpsilon(denominator, 0.0)) {
            throw new ArithmeticException("Intersection does not exist.");
        }
        else {
            double slopeLineSegment1 = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denominator;
            double slopeLineSegment2 = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denominator;

            if (isBetweenWithEpsilon(slopeLineSegment1, 0.0, 1.0) && isBetweenWithEpsilon(slopeLineSegment2, 0.0, 1.0)) {
                return new double[] {(x1 + slopeLineSegment1 * (x2 - x1)), (y1 + slopeLineSegment1 * (y2 - y1))};
            }
            else {
                throw new ArithmeticException("Intersection does not exist.");
            }
        }
    }
}
