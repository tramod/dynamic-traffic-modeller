/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.List;

/**
 * The node model for dynamic traffic assignment
 * @author Tomas Potuzak
 */
public interface DtmNodeModel {

    /**
     * Solves the node model using the relevant parameters of node and the neighboring incoming and outgoing edges
     * @param time the time of the solving
     * @return the outflow for incoming edges, the inflow to outgoing edges, and the mixture to outgoing edges
     */
    public DtmTripleElement<List<Double>, List<Double>, List<DtmMixture<DtmIdentifiable>>> solve(double time);

    /**
     * Returns all the node events that occurred during the entire computation
     * @return all the node events that occurred during the entire computation
     */
    public List<DtmNodeEvent> getAllEvents();

    /**
     * Returns the next scheduled node event for the specified time
     * @param time the time, for which the next scheduled node event shall be determined
     * @return the next scheduled node event for the specified time and
     */
    public DtmPairElement<DtmNodeEvent, Double> nextSheduledEvent(double time);
}
