/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of
 *
 * TAMPERE, Chris MJ, et al. A generic class of first order node models for dynamic macroscopic simulation of traffic flows.
 * Transportation Research Part B: Methodological, 2011, 45.1: 289-309.
 *
 * @author kolovsky
 */
public class DtmTampereNodeModel extends DtmDestinationNodeModel {
    double[] incomingPriorities;

    public DtmTampereNodeModel(DtmNode node, HashMap<DtmIdentifiable, DtmDistributionMatrix> distributions, double[] incomingPriorities){
        super(node, distributions);
        this.incomingPriorities = incomingPriorities;
    }

    @Override
    public double[][] solveUniversal(double[][] demand, double[] supply) {
        // INITIALIZATION
        double[] Rw = supply.clone();
        double[][] q = new double[demand.length][demand[0].length];

        // init sets U_j and set J
        ArrayList<Set<Integer>> U = new ArrayList<>(supply.length);
        for(int j = 0; j < supply.length; j++){
            U.add(new HashSet<>());
        }
        HashSet<Integer> J = new HashSet<>();

        for (int i = 0; i< demand.length; i++){
            for(int j = 0; j < demand[i].length; j++){
                if (demand[i][j] > 0){
                    U.get(j).add(i);
                    J.add(j);
                }
            }
        }

        // init "a"
        double[] a = new double[supply.length];

        // 2. Determine oriented capacities
        double[][] capacity = new double[demand.length][supply.length];
        double[] incomingDemand = new double[incomingPriorities.length];

        for (int i = 0; i< demand.length; i++){
            for(int j = 0; j < demand[i].length; j++){
                incomingDemand[i] += demand[i][j];
            }
        }

        for (int i = 0; i< demand.length; i++){
            for(int j = 0; j < demand[i].length; j++){
                if (incomingDemand[i] > 0){
                    capacity[i][j] = demand[i][j]/incomingDemand[i] * incomingPriorities[i];
                }
            }
        }

        // MAIN LOOP
        while(!J.isEmpty()){
            // 3. Determine most restrictive constraint
            double minA = Double.POSITIVE_INFINITY;
            int minJ = -1;
            for(int j: J){
                double capSum = 0.0;
                for (int i: U.get(j)){
                    capSum += capacity[i][j];
                }
                double aa = Rw[j]/capSum;

                if (aa < minA){
                    minA = aa;
                    minJ = j;
                }
            }

            // 4. Determine flows of corresponding set U_minJ and recalculate Rw
            boolean ifA = false;
            for (int i: U.get(minJ)){
                if (incomingDemand[i] <= minA * incomingPriorities[i]) {
                    ifA = true;
                    break;
                }
            }

            // (a)
            if(ifA){
                for (int i: new HashSet<>(U.get(minJ))){
                    if (incomingDemand[i] <= minA * incomingPriorities[i]){
                        for (int j = 0; j < demand[i].length; j++){
                            q[i][j] = demand[i][j];
                        }
                        HashSet<Integer> forRemoving = new HashSet<>();
                        for(int j: J){
                            Rw[j] -= demand[i][j];
                            U.get(j).remove(i);
                            if (U.get(j).isEmpty()){
                                forRemoving.add(j);
                                a[j] = 1.0;
                            }
                        }
                        for (int j: forRemoving){
                            J.remove(j);
                        }
                    }
                }
            } // (b)
            else {
                for (int i: U.get(minJ)){
                    for(int j = 0; j < demand[i].length; j++){
                        q[i][j] = minA*capacity[i][j];
                    }
                    HashSet<Integer> forRemoving = new HashSet<>();
                    for (int j: J){
                        Rw[j] -= minA * capacity[i][j];
                        if (j != minJ){
                            for (int jj: U.get(minJ)){
                                U.get(j).remove(jj);
                            }
                            if(U.get(j).isEmpty()){
                                forRemoving.add(j);
                                a[j] = 1;
                            }
                        }
                        else{
                            forRemoving.add(minJ);
                            a[j] = minA;
                        }
                    }
                    for (int j: forRemoving){
                        J.remove(j);
                    }
                }
            }
        }
        return q;
    }
}
