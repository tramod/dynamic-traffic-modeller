/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * A zone associated with a node, which can serve as an origin, destination, or both
 * @author Tomas Potuzak
 */
public class DtmZone implements DtmIdentifiable {
    /** The ID of the zone */
    protected int id;
    /** The index of this zone for fast access */
    protected int index;
    /** The node, with which this zone is associated */
    protected DtmNode node;
    /** The origin part of this zone. Can be <code>null</code> if this zone is not an origin */
    protected DtmOrigin origin;
    /** The destination part of this zone */
    //protected Destination destination; //TODO implement when necessary

    /**
     * Creates a new zone with specified ID and the node, with which this zone is associated
     * @param id the ID of the zone
     * @param node the node, with which this zone is associated
     */
    public DtmZone(int id, DtmNode node) {
        this.id = id;
        this.node = node;
    }

    /**
     * Returns the node, with which this zone is associated
     * @return the node, with which this zone is associated
     */
    public DtmNode getNode() {
        return node;
    }

    /**
     * Sets the node, with which this zone is associated
     * @param node the new node, with which this zone is associated
     */
    public void setNode(DtmNode node) {
        this.node = node;
    }

    /**
     * Returns the origin part of this zone
     * @return the origin part of this zone
     */
    public DtmOrigin getOrigin() {
        return origin;
    }

    /**
     * Sets the origin part of this zone
     * @param origin the new origin part of this zone
     */
    public void setOrigin(DtmOrigin origin) {
        this.origin = origin;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof DtmZone) {
            DtmZone other = (DtmZone) o;
            return id == other.id;
        }
        else {
            return false;
        }
    }
}