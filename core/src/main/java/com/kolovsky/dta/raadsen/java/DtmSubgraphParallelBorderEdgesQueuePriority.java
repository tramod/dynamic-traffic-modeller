/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * The priority of the border edges priority queue enabling correct ordering of the border elements
 * @author Tomas Potuzak
 */
public class DtmSubgraphParallelBorderEdgesQueuePriority implements Comparable<DtmSubgraphParallelBorderEdgesQueuePriority> {
    /**	The time - primary attribute, according which the elements are ordered in the queue */
    protected double time;
    /** The secondary ordering value - secondary attribute ensuring the correct ordering of the elements with the same time - this attributes includes the ID of the corresponding edge and the type of the border element (in this order) */
    protected int secondaryOrderingValue;

    /**
     * Creates a new priority for border priority queue based on specified time, edge, and border element type
     * @param time the time of the border element
     * @param edge the edge corresponding to the element or <code>null</code> if there is no such edge (e.g., for node events)
     * @param type the type of the border element
     */
    public DtmSubgraphParallelBorderEdgesQueuePriority(double time, DtmEdge edge, DtmSubgraphParallelBorderEdgesQueueElementTypes type) {
        this.time = time;
        this.secondaryOrderingValue = calculateSecondaryOrderingValue(edge, type);
    }

    /**
     * Calculates and returns the secondary ordering value based on the specified edge and border element type
     * @param edge the edge corresponding to the element of <code>null</code> if there is no such edge (e.g., for node events)
     * @param type the type of the border element
     * @return the secondary ordering value based on the specified edge and border element type
     */
    public int calculateSecondaryOrderingValue(DtmEdge edge, DtmSubgraphParallelBorderEdgesQueueElementTypes type) {
        int secondaryOrderingValue = 0;

        if (edge != null) {
            secondaryOrderingValue += edge.getId();
            secondaryOrderingValue <<= 2;
        }
        if (type != null) {
            secondaryOrderingValue += type.getOrderingValue();
        }
        return secondaryOrderingValue;
    }

    //Getters and setters

    /**
     * Returns the time of this priority
     * @return the time of this priority
     */
    public double getTime() {
        return time;
    }

    /**
     * Returns the secondary ordering value
     * @return the secondary ordering value
     */
    public int getSecondaryOrderingValue() {
        return secondaryOrderingValue;
    }

    //Inherited methods

    @Override
    public int compareTo(DtmSubgraphParallelBorderEdgesQueuePriority o) {
        if (time < o.time) {
            return -1;
        }
        else if (time > o.time) {
            return 1;
        }
        else {
            if (secondaryOrderingValue < o.secondaryOrderingValue) {
                return -1;
            }
            else if (secondaryOrderingValue > o.secondaryOrderingValue) {
                return 1;
            }
            else {
                return 0;
            }
        }
    }

    @Override
    public String toString() {
        return "(" + time + " " + secondaryOrderingValue + ")";
    }
}