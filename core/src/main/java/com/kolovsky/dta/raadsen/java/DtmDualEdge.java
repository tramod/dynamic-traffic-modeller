/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.Objects;

public class DtmDualEdge {
    final private DtmEdge startEdge;
    final private DtmEdge endEdge;
    private DtmNode node;

    public DtmDualEdge(DtmEdge startEdge, DtmEdge endEdge, DtmNode node) {
        this.startEdge = startEdge;
        this.endEdge = endEdge;
        this.node = node;
    }

    public DtmEdge getStartEdge() {
        return startEdge;
    }

    public DtmEdge getEndEdge() {
        return endEdge;
    }

    public DtmNode getNode() {
        return node;
    }

    public void setNode(DtmNode node) {
        this.node = node;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DtmDualEdge dualEdge = (DtmDualEdge) o;
        return Objects.equals(startEdge, dualEdge.startEdge) && Objects.equals(endEdge, dualEdge.endEdge) && Objects.equals(node, dualEdge.node);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startEdge, endEdge, node);
    }
}
