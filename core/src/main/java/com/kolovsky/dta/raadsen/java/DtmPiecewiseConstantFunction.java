/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.List;

/**
 * Piecewise constant function for Dynamic Traffic Assignment
 * @author Tomas Potuzak
 */
public class DtmPiecewiseConstantFunction implements DtmFunction {
    /** The <i>x</i> values of the function */
    protected List<Double> xValues;
    /** The <i>y</> = <i>f</i>(<i>x</i>) values of the function */
    protected List<Double> yValues;

    /**
     * Creates a new piecewise constant function with specified <i>x</i> and <i>y</i> values
     * @param xValues the <i>x</i> values of the function
     * @param yValues the <i>y</> = <i>f</i>(<i>x</i>) values of the function
     * @throws IllegalArgumentException if the numbers of <i>x</i> and <i>y</i> values are different
     * @throws NullPointerException if <i>x</i> and/or <i>y</i> values are <code>null</code>
     */
    public DtmPiecewiseConstantFunction(List<Double> xValues, List<Double> yValues) throws IllegalArgumentException, NullPointerException {
        this.xValues = new ArrayList<>();
        this.yValues = new ArrayList<>();

        if (xValues != null && yValues != null && xValues.size() == yValues.size()) {
            this.xValues.addAll(xValues);
            this.yValues.addAll(yValues);
        }
        else if (xValues == null || yValues == null) {
            throw new NullPointerException("Both x and y values must not be null.");
        }
        else {
            throw new IllegalArgumentException("Number of x and y values must be identical.");
        }
    }

    /**
     * Creates a new piecewise constant function with empty <i>x</i> and <i>y</i> values
     */
    public DtmPiecewiseConstantFunction() {
        xValues = new ArrayList<>();
        yValues = new ArrayList<>();
    }

    /**
     * Adds a point to the end of this function
     * @param x the <i>x</i> value
     * @param y the <i>y</i> value at <code>x</code> (<i>f</i>(<i>x</i>))
     * @throws IllegalArgumentException if the <i>x</i> and/or <i>x</i> value is infinite or NaN or the <i>x</i> value is not larger that the last value of this function
     */
    public void addPoint(double x, double y) throws IllegalArgumentException {
        if (Double.isInfinite(x) || Double.isInfinite(y) || Double.isNaN(x) || Double.isNaN(y)) {
            throw new IllegalArgumentException("Both x and y values must be regular numbers (i.e., finite)");
        }
        else {
            if (xValues.isEmpty() || xValues.get(xValues.size() - 1) < x) {
                xValues.add(x);
                yValues.add(y);
            }
            else {
                throw new IllegalArgumentException("The x value must be larger than last value of the function " + xValues.get(xValues.size() - 1));
            }
        }
    }

    /**
     * Updates the point of specified index of this function
     * @param index the index of the point
     * @param x the <i>x</i> value
     * @param y the <i>y</i> value at <code>x</code> (<i>f</i>(<i>x</i>))
     * @throws IllegalArgumentException if the <code>index</code> is out of range or the <code>x</code> does not correspond to the <code>index</code>
     */
    public void updatePoint(int index, double x, double y) throws IllegalArgumentException {
        if (index < 0 || index >= xValues.size()) {
            throw new IllegalArgumentException("The index of the point is out of range.");
        }
        else if (index > 0 && index < xValues.size() - 1) { //A point in inner parts of the function - not at the ends
            if (x <= xValues.get(index - 1) || x >= xValues.get(index + 1)) {
                throw new IllegalArgumentException("The x value must be between the neighboring values.");
            }
            else { //Everything ok, setting the x and y values
                xValues.set(index, x);
                yValues.set(index, y);
            }
        }
        else if (index == 0 && xValues.size() == 1) { //There is just one point (this should probably not happen)
            xValues.set(0, x);
            yValues.set(0, y);
        }
        else if (index == 0) {
            if (x >= xValues.get(index + 1)) { //The point at the start
                throw new IllegalArgumentException("The x value must be between the neighboring values.");
            }
            else { //Everything ok, setting the x and y values
                xValues.set(index, x);
                yValues.set(index, y);
            }
        }
        else if (index == xValues.size() - 1) { //The point at the end
            if (x <= xValues.get(index - 1)) {
                throw new IllegalArgumentException("The x value must be between the neighboring values.");
            }
            else { //Everything ok, setting the x and y values
                xValues.set(index, x);
                yValues.set(index, y);
            }
        }
    }

    @Override
    public DtmPiecewiseConstantFunction plus(DtmFunction other) throws IllegalArgumentException {
        if (other instanceof DtmPiecewiseConstantFunction) {
            return performOperation((DtmPiecewiseConstantFunction) other, "+");
        }
        else {
            throw new IllegalArgumentException("Function type is not supported.");
        }
    }

    @Override
    public DtmPiecewiseConstantFunction minus(DtmFunction other) throws IllegalArgumentException {
        if (other instanceof DtmPiecewiseConstantFunction) {
            return performOperation((DtmPiecewiseConstantFunction) other, "-");
        }
        else {
            throw new IllegalArgumentException("Function type is not supported.");
        }
    }

    /**
     * Performs multiplication of this function by a specified constant
     * @param k the constant, with which this function shall be multiplied
     * @return this function times <code>k</code>
     */
    public DtmPiecewiseConstantFunction times(double k) {
        List<Double> resultXValues = new ArrayList<>(xValues);
        List<Double> resultYValues = new ArrayList<>();

        for (int i = 0; i < resultXValues.size(); i++) {
            resultYValues.add(k * yValues.get(i));
        }

        return new DtmPiecewiseConstantFunction(resultXValues, resultYValues);
    }

    /**
     * Performs multiplication with other function
     * @param other other function, with which this function shall be multiplied
     * @return this function times other function
     * @throws IllegalArgumentException if the other is not an instance of the <code>PieceWiseConstantFunction</code>
     */
    public DtmPiecewiseConstantFunction times(DtmFunction other) throws IllegalArgumentException {
        if (other instanceof DtmPiecewiseConstantFunction) {
            return performOperation((DtmPiecewiseConstantFunction) other, "*");
        }
        else {
            throw new IllegalArgumentException("Function type is not supported.");
        }
    }

    /**
     * Performs division by other function
     * @param other other function, by which this function shall be divided
     * @return this function divided by other function
     * @throws IllegalArgumentException if the other is not an instance of the <code>PieceWiseConstantFunction</code>
     */
    public DtmPiecewiseConstantFunction dividedBy(DtmFunction other) throws IllegalArgumentException {
        if (other instanceof DtmPiecewiseConstantFunction) {
            return performOperation((DtmPiecewiseConstantFunction) other, "/");
        }
        else {
            throw new IllegalArgumentException("Function type is not supported.");
        }
    }

    /**
     * Performs a specified arithmetic operation of this and other functions
     * @param other the function with which the arithmetic operation shall be performed
     * @param operation the operator of the arithmetic operation
     * @return the resulting function
     * @throws IllegalArgumentException if the specified operation is not supported or
     * @throws NullPointerException if the specified operation is <code>null</code>
     */
    protected DtmPiecewiseConstantFunction performOperation(DtmPiecewiseConstantFunction other, String operation) throws IllegalArgumentException, NullPointerException {
        List<Double> resultXValues = new ArrayList<>();
        List<Double> resultYValues = new ArrayList<>();
        double x1 = 0.0;
        double x2 = 0.0;
        double x = 0.0;
        double y = 0.0;
        int i = 0;
        int j = 0;

        while (i < xValues.size() && j < other.xValues.size()) {
            x1 = getX(i);
            x2 = other.getX(j);

            if (DtmMathUtils.isEqualWithEpsilon(x1, x2, DtmMathUtils.DEFAULT_COMPARISON_EPSILON)) {
                x = xValues.get(i);
                y = DtmMathUtils.performOperation(getY(i), other.getY(j), operation);

                i++;
                j++;
            }
            else if (DtmMathUtils.isLowerThanWithEpsilon(x1, x2, DtmMathUtils.DEFAULT_COMPARISON_EPSILON)) {
                x = xValues.get(i);
                y = DtmMathUtils.performOperation(getY(i), other.getY(j - 1), operation);

                i++;
            }
            else {
                x = other.xValues.get(j);
                y = DtmMathUtils.performOperation(getY(i - 1), other.getY(j), operation);

                j++;
            }

            resultXValues.add(x);
            resultYValues.add(y);
        }


        return new DtmPiecewiseConstantFunction(resultXValues, resultYValues);
    }

    @Override
    public double integrate(boolean useAbsoluteValues) {
        double integral = 0.0;

        for (int i = 1; i < xValues.size(); i++) {
            if (useAbsoluteValues) {
                integral += Math.abs((xValues.get(i) - xValues.get(i - 1)) * yValues.get(i - 1));
            }
            else {
                integral += (xValues.get(i) - xValues.get(i - 1)) * yValues.get(i - 1);
            }
        }

        return integral;
    }

    @Override
    public double valueAt(double x) throws IllegalStateException {
        if (xValues.isEmpty()) {
            throw new IllegalStateException("Function value cannot be determined for function with no point.");
        }
        else if (x >= xValues.get(xValues.size() - 1)) {
            return yValues.get(yValues.size() - 1);
        }
        else {
            int index = DtmFunctionUtils.binarySearch(xValues, x); //TODO overit, ze se to chova stejne jako Scala implementace
            return yValues.get(index);
        }
    }

    @Override
    public double minimum() throws IllegalStateException {
        if (xValues.isEmpty()) {
            throw new IllegalStateException("Function minimum cannot be determined for function with no point.");
        }
        else {
            double minimum = yValues.get(0);

            for (int i = 1; i < yValues.size(); i++) {
                if (yValues.get(i) < minimum) {
                    minimum = yValues.get(i);
                }
            }

            return minimum;
        }
    }

    @Override
    public double maximum() throws IllegalStateException {
        if (xValues.isEmpty()) {
            throw new IllegalStateException("Function maximum cannot be determined for function with no point.");
        }
        else {
            double maximum = yValues.get(0);

            for (int i = 1; i < yValues.size(); i++) {
                if (yValues.get(i) > maximum) {
                    maximum = yValues.get(i);
                }
            }

            return maximum;
        }
    }

    @Override
    public DtmPiecewiseConstantFunction subFunction(double a, double b) throws IllegalStateException, IllegalArgumentException {
        if (xValues.size() <= 1) {
            throw new IllegalStateException("Subfunction cannot be created from a function with one or no point.");
        }
        else if (a >= b) {
            throw new IllegalArgumentException("Start (left) border of the interval of the subfunction must be lower than end (right) border of the interval.");
        }
        else if (a < getFirstX() || b > getLastX()) {
            throw new IllegalArgumentException("The border of the interval of the subfunction must not be outside the interval of the original function.");
        }
        else {
            int startIndex = DtmFunctionUtils.binarySearch(xValues, a);
            int endIndex = DtmFunctionUtils.binarySearch(xValues, b);
            List<Double> resultXValues = new ArrayList<>();
            List<Double> resultYValues = new ArrayList<>();

            resultXValues.add(a);
            resultYValues.add(yValues.get(startIndex));
            for (int i = startIndex + 1; i <= endIndex; i++) {
                resultXValues.add(xValues.get(i));
                resultYValues.add(yValues.get(i));
            }
            if (xValues.get(endIndex) < b) {
                resultXValues.add(b);
                resultYValues.add(yValues.get(endIndex));
            }

            return new DtmPiecewiseConstantFunction(resultXValues, resultYValues);
        }
    }

    /**
     * Approximates this function with a simpler piecewise linear function with specified maximal absolute error and returns it
     * Inspired by H. Konno and T. Kuno, "Best piecewise constant approximation of a function of single variable," Operations Research Letters, vol. 7, no. 4, pp. 205–210, Aug. 1988, DOI: 10.1016/0167-6377(88)90030-2.
     * @param maximalAbsoluteError maximal acceptable absolute difference between the original and the resulting simpler piecewise constant function
     * @return a simpler piecewise linear function with specified maximal absolute error
     */
    public DtmPiecewiseConstantFunction simplify(double maximalAbsoluteError) {
        if (yValues.isEmpty()) {
            return new DtmPiecewiseConstantFunction();
        }
        else {
            double upperBound = yValues.get(0) + maximalAbsoluteError;
            double lowerBound = yValues.get(0) - maximalAbsoluteError;
            double newUpperBound = 0.0;
            double newLowerBound = 0.0;
            double lastPointX = xValues.get(0);
            DtmPiecewiseConstantFunction simplifiedFunction = new DtmPiecewiseConstantFunction();

            for (int i = 1; i < xValues.size(); i++) {
                newUpperBound = yValues.get(i) + maximalAbsoluteError;
                newLowerBound = yValues.get(i) - maximalAbsoluteError;

                if (newUpperBound >= lowerBound && newLowerBound <= upperBound) {
                    if (newUpperBound < upperBound) {
                        upperBound = newUpperBound;
                    }
                    if (newLowerBound > lowerBound) {
                        lowerBound = newLowerBound;
                    }
                }
                else {
                    simplifiedFunction.addPoint(lastPointX, (upperBound + lowerBound) / 2);
                    lastPointX = xValues.get(i);
                    lowerBound = newLowerBound;
                    upperBound = newUpperBound;
                }
            }
            simplifiedFunction.addPoint(lastPointX, (upperBound + lowerBound) / 2);
            if (!xValues.get(xValues.size() - 1).equals(simplifiedFunction.xValues.get(simplifiedFunction.xValues.size() - 1))) { //TODO potential problem due to the double comparison issue
                simplifiedFunction.addPoint(xValues.get(xValues.size() - 1), (upperBound + lowerBound) / 2);
            }

            return simplifiedFunction;
        }
    }

    /**
     * Approximate this function by discrete function
     * @param numBins number of intervals
     * @param startX lower bound
     * @param endX upper bound
     * @return discrete function
     */
    public DtmDiscreteFunction toDiscreteFunction(int numBins, double startX, double endX, double positionInInterval){
        double[] values = new double[numBins];
        double step = (endX - startX)/numBins;

        int index = 1;
        for(int i = 0; i < numBins; i++){
            double x = startX + i * step + positionInInterval * step;
            if (x >= getLastX()){
                values[i] = getLastY();
            }
            else {
                while (xValues.get(index) <= x){
                    index++;
                }
                double y = yValues.get(index - 1);
                values[i] = y;
            }
        }
        return new DtmDiscreteFunction(values, startX, endX);
    }

    @Override
    public List<Double> getXValues() {
        return xValues;
    }

    @Override
    public List<Double> getYValues() {
        return yValues;
    }

    @Override
    public double getX(int index) throws IndexOutOfBoundsException {
        return xValues.get(index);
    }

    @Override
    public double getY(int index) throws IndexOutOfBoundsException {
        return yValues.get(index);
    }

    @Override
    public int getLength() {
        return xValues.size();
    }

    @Override
    public double getFirstX() throws IndexOutOfBoundsException {
        return xValues.get(0);
    }

    @Override
    public double getFirstY() throws IndexOutOfBoundsException {
        return yValues.get(0);
    }

    @Override
    public double getLastX() throws IndexOutOfBoundsException {
        return xValues.get(xValues.size() - 1);
    }

    @Override
    public double getLastY() throws IndexOutOfBoundsException {
        return yValues.get(yValues.size() - 1);
    }

    @Override
    public String toString() {
        return "Piecewise constant function:\n" + xValues.toString() + "\n" + yValues.toString();
    }
}