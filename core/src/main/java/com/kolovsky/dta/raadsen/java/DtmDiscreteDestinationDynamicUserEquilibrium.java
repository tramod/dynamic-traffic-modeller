/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.List;

/**
 * Compute route-choice dynamic user equilibrium (RC-DUE) using grid-based gradient algorithm.
 * Use destination-based formulation of the RC-DUE.
 * @author kolovsky
 */
public class DtmDiscreteDestinationDynamicUserEquilibrium implements DtmDynamicUserEquilibrium{
    /** Dynamic network loading */
    protected DtmEventGeneralLinkTransmissionModel dnl;
    /** maximal time for the simulation */
    protected double maximalTime;
    /** Origin-Destination matrix */
    protected DtmOriginDestinationMatrix odm;
    /** maintain  gradient projection algorithm */
    protected DtmGradientProjection equilibrator;
    /** start time for node distribution */
    protected double minimalNodeTime;
    /** end time for node distribution */
    protected double maximalNodeTime;
    /** number of time intervals for the discrete functions */
    protected int numberIntervals;
    /** the best result */
    protected List<DtmEdge> bestResultEdges;
    /** number of shortest path search per time interval */
    protected int numberSearchPerInterval = 3;
    /** travel time representation */
    protected double positionInInterval = 1.0;
    /** use continuous shortest path search */
    protected boolean useContinuousShortestPathSearch = false;

    public DtmDiscreteDestinationDynamicUserEquilibrium(DtmEventGeneralLinkTransmissionModel dnl, double maximalTime,
                                                        DtmOriginDestinationMatrix odm, DtmGradientProjection equilibrator,
                                                        double minimalNodeTime, double maximalNodeTime, int numberIntervals) {
        this.dnl = dnl;
        this.maximalTime = maximalTime;
        this.odm = odm;
        this.equilibrator = equilibrator;
        this.minimalNodeTime = minimalNodeTime;
        this.maximalNodeTime = maximalNodeTime;
        this.numberIntervals = numberIntervals;
    }

    @Override
    public void run(int maxIter, double targetGap) {
        DtmGraph graph = dnl.graph;
        int iter = 0;
        long time = System.currentTimeMillis();
        double bestGap = Double.POSITIVE_INFINITY;
        while (iter < maxIter){
            for (DtmEdge edge: dnl.graph.getEdges()){
                edge.reset();
            }
            for (DtmZone originZone: odm.getOriginZones()){
                originZone.getOrigin().reset();
            }
            dnl.run(maximalTime, DtmEventGeneralLinkTransmissionModel.DEFAULT_RELEASE_EVENT_EPSILON);

            if(iter == maxIter - 1){
                return;
            }

            double cumRelativeError = 0.0;
            double countRelativeError = 0;
            for (DtmZone destination: odm.getDestinationZones()){

                // shortest path search
                DtmPiecewiseLinearFunction[] dist;
                if (useContinuousShortestPathSearch){
                    List<DtmArrivalFunction> afs = graph.performBackProfileSearch(destination.getNode(), minimalNodeTime, maximalTime, 1E-6);
                    dist = new DtmPiecewiseLinearFunction[afs.size()];
                    for (int i = 0; i < dist.length; i++){
                        dist[i] = afs.get(i).toTravelTimeFunction();
                        if (afs.get(i).isInfinite()){
                            dist[i] = new DtmPiecewiseLinearFunction(List.of(minimalNodeTime, maximalTime), List.of(10000.0, 10000.0));
                        }
                    }
                } else {
                    dist = graph.performDiscreteBackProfileSearchLin(destination.getNode(),
                            minimalNodeTime, maximalNodeTime, numberIntervals*numberSearchPerInterval, maximalTime);
                }

                for (DtmNode node: graph.getNodes()){
                    DtmDestinationNodeModel nodeModel = (DtmDestinationNodeModel) node.getNodeModel();
                    if (nodeModel.getDistributions() != null && destination.getNode() != node
                            && nodeModel.getDistributions().containsKey(destination)){

                        DtmDiscreteFunction[] travelTimeFunctions = new DtmDiscreteFunction[node.getOutgoingEdges().size()];
                        for (int i = 0; i < travelTimeFunctions.length; i++){
                            DtmEdge edge = node.getOutgoingEdges().get(i);
                            travelTimeFunctions[i] = composition(edge.getArrivalFunction(), dist[edge.getEndNode().getIndex()], positionInInterval);
                        }
                        DtmDistributionMatrix distributionMatrix = nodeModel.getDistributions().get(destination);

                        for (int i = 0; i < distributionMatrix.getMatrix().length; i++){
                            DtmPairElement<Double, Double> re = relativeErrorFlow(travelTimeFunctions, flows(node, i, destination),
                                    minimalNodeTime, maximalNodeTime/2.0);
                            if (re.first > 0.0){
                                cumRelativeError += re.first*re.second;
                                countRelativeError += re.second;
                            }
                        }

                        DtmDistributionMatrix updatedDistributionMatrix = updateDistributionMatrix(distributionMatrix, travelTimeFunctions);
                        nodeModel.getDistributions().put(destination, updatedDistributionMatrix);
                    }
                }
            }
            double averageRelativeGap = cumRelativeError/countRelativeError;
            if (bestGap > averageRelativeGap){
                bestGap = averageRelativeGap;
                bestResultEdges = new ArrayList<>(graph.edges.size());
                for (DtmEdge edge: graph.getEdges()){
                    bestResultEdges.add(edge.shallowCopy());
                }
            }
            System.out.println(""+(System.currentTimeMillis() - time)+" "+averageRelativeGap);
            if (averageRelativeGap <= targetGap){
                return;
            }
            iter++;
        }
    }

    private DtmDiscreteFunction[] flows(DtmNode node, int incomeEdgeIndex, DtmZone destination){
        DtmDestinationNodeModel nodeModel = (DtmDestinationNodeModel) node.getNodeModel();
        DtmDistributionMatrix distributionMatrix = nodeModel.getDistributions().get(destination);

        DtmDiscreteFunction[] flows = new DtmDiscreteFunction[node.getOutgoingEdges().size()];
        for (int j = 0; j < node.getOutgoingEdges().size(); j++){
            DtmDiscreteFunction dist = (DtmDiscreteFunction) distributionMatrix.getMatrix()[incomeEdgeIndex][j];
            DtmEdge outgoingEdge = node.getOutgoingEdges().get(j);
            List<DtmMixture<DtmIdentifiable>> mixtures = outgoingEdge.inflowMixtureFunction
                    .toDiscreteFunction(numberIntervals, minimalNodeTime, maximalNodeTime, positionInInterval);
            DtmDiscreteFunction inflow = outgoingEdge.getInflow()
                    .toDiscreteFunction(numberIntervals, minimalNodeTime, maximalNodeTime, positionInInterval);
            double[] values = new double[numberIntervals];
            for (int t = 0; t < numberIntervals; t++){
                if (mixtures.get(t) != null){
                    double portion = mixtures.get(t).getPortion(destination);
                    double inflowToDestination = portion * inflow.getY(t);
                    if (inflowToDestination > 1.0 && dist.getY(t) > 1E-2){ //&& dist.getY(t) > 1E-3
                        values[t] = inflowToDestination;
                    }
                }
            }
            flows[j] = new DtmDiscreteFunction(values, minimalNodeTime, maximalNodeTime);
        }
        return flows;
    }

    /**
     * Compute relative error for the alternatives
     * @param proportion radio of flow split (node distribution)
     * @param travelTimeFunctions travel time function for each outgoing edge
     * @param minTime minimal time for interval where the error should be computed
     * @param maxTime maximal time for interval where the error should be computed
     * @return average relative error
     */
    private double relativeError(DtmDiscreteFunction[] proportion, DtmDiscreteFunction[] travelTimeFunctions,
                                 boolean[][] usedToDestination, double minTime, double maxTime){
        int timeLength = travelTimeFunctions[0].getLength();
        double startTime = travelTimeFunctions[0].getFirstX();
        double timeStep = (travelTimeFunctions[0].getLastX() - startTime)/timeLength;

        double sum = 0.0;
        int count = 0;
        for (int t = 0; t < timeLength; t++){
            double time = startTime + t*timeStep;
            if (minTime <= time && time <= maxTime){
                double maxValue = Double.NEGATIVE_INFINITY;
                double minValue = Double.POSITIVE_INFINITY;
                for (int j = 0; j < proportion.length; j++){
                    if (proportion[j].getY(t) > 1E-3 && usedToDestination[j][t]){
                        if (maxValue < travelTimeFunctions[j].getY(t)){
                            maxValue = travelTimeFunctions[j].getY(t);
                        }
                    }
                }
                for (int j = 0; j < proportion.length; j++){
                    if (minValue > travelTimeFunctions[j].getY(t)){
                        minValue = travelTimeFunctions[j].getY(t);
                    }
                }
                if (maxValue != Double.NEGATIVE_INFINITY){
                    sum += (maxValue - minValue);
                    count++;
                }
            }
        }
        if (count > 0){
            return sum/count;
        }
        return -1;
    }

    private DtmPairElement<Double, Double> relativeErrorFlow(DtmDiscreteFunction[] travelTimeFunctions, DtmDiscreteFunction[] flows, double minTime, double maxTime){
        int timeLength = travelTimeFunctions[0].getLength();
        double startTime = travelTimeFunctions[0].getFirstX();
        double timeStep = (travelTimeFunctions[0].getLastX() - startTime)/timeLength;

        double sumNumerator = 0.0;
        double sumDenominator  = 0.0;
        double weight = 0.0;
        for (int t = 0; t < timeLength; t++){
            double time = startTime + t*timeStep;
            if (minTime <= time && time <= maxTime){
                double minValue = Double.POSITIVE_INFINITY;
                for (int j = 0; j < travelTimeFunctions.length; j++){
                    if (minValue > travelTimeFunctions[j].getY(t)){
                        minValue = travelTimeFunctions[j].getY(t);
                    }
                }

                for (int j = 0; j < travelTimeFunctions.length; j++){
                    if (flows[j].getY(t) > 1E-10){
                        sumNumerator += flows[j].getY(t) * minValue;
                        sumDenominator += flows[j].getY(t) * travelTimeFunctions[j].getY(t);
                        weight += flows[j].getY(t);
                    }
                }

            }
        }
        if (sumDenominator > 0.0){
            return new DtmPairElement<>(1 - sumNumerator/sumDenominator, weight);
        }
        return new DtmPairElement<>(-1.0, -1.0);
    }

    /**
     * Update node distribution matrix by travel time functions.
     * @param distributionMatrix distribution matrix
     * @param travelTimeFunctions travel time functions for every outgoing edge
     * @return updated distribution matrix
     */
    private DtmDistributionMatrix updateDistributionMatrix(DtmDistributionMatrix distributionMatrix, DtmDiscreteFunction[] travelTimeFunctions){
        DtmFunction[][] matrix = distributionMatrix.getMatrix();
        int timeLength = travelTimeFunctions[0].getLength();
        double[][][] newMatrix = new double[matrix.length][matrix[0].length][timeLength];

        double minTime = matrix[0][0].getFirstX();
        double maxTime = matrix[0][0].getLastX();

        for (int i = 0; i < matrix.length; i++){
            if (matrix[i].length != travelTimeFunctions.length){
                throw new IllegalArgumentException("The length of TTFs must be same as number of outgoing edges");
            }
            for(int t = 0; t < timeLength; t++){
                double[] cost = new double[travelTimeFunctions.length];
                double[] proportions = new double[travelTimeFunctions.length];
                for (int j = 0; j < travelTimeFunctions.length; j++){
                    cost[j] = travelTimeFunctions[j].getY(t);
                    proportions[j] = matrix[i][j].getY(t);
                }
                // new proportions
                double[] newProportions = equilibrator.run(proportions, cost);

                for (int j = 0; j < travelTimeFunctions.length; j++){
                    newMatrix[i][j][t] = newProportions[j];
                }
            }
        }

        DtmFunction[][] newDistributionMatrix = new DtmFunction[matrix.length][travelTimeFunctions.length];
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < travelTimeFunctions.length; j++){
                newDistributionMatrix[i][j] = new DtmDiscreteFunction(newMatrix[i][j], minTime, maxTime);
            }
        }
        return new DtmDistributionMatrix(newDistributionMatrix);
    }

    /**
     * Compute nodeDist(edgeArrivalFunction(whole_time_interval)).
     * @param edgeArrivalFunction arrival function associated with edge
     * @param nodeDist travel time function to destination
     * @return discrete travel time function
     */
    private DtmDiscreteFunction composition(DtmArrivalFunction edgeArrivalFunction, DtmFunction nodeDist, double positionInInterval){
        double step = (maximalNodeTime - minimalNodeTime)/numberIntervals;
        double[] out = new double[numberIntervals];
        for (int i = 0; i < numberIntervals; i++){
            double departureTime = nodeDist.getFirstX() + i * step + positionInInterval * step;
            double arrivalTime = edgeArrivalFunction.getArrival(departureTime);
            if (arrivalTime <= maximalNodeTime){
                out[i] = (arrivalTime - departureTime) + nodeDist.valueAt(arrivalTime);
            } else {
                out[i] = (arrivalTime - departureTime) + nodeDist.getLastY();
            }
        }
        return new DtmDiscreteFunction(out, nodeDist.getFirstX(), nodeDist.getLastX());
    }

    // GET and SET

    public void setNumberSearchPerInterval(int numberSearchPerInterval) {
        this.numberSearchPerInterval = numberSearchPerInterval;
    }

    public void setPositionInInterval(double positionInInterval) {
        if (positionInInterval < 0.0 || 1.0 < positionInInterval){
            throw new IllegalArgumentException("The position in interval must be in <0.0, 1.0>");
        }
        this.positionInInterval = positionInInterval;
    }

    public DtmEventGeneralLinkTransmissionModel getDnl() {
        return dnl;
    }

    public double getMaximalTime() {
        return maximalTime;
    }

    public DtmOriginDestinationMatrix getOdm() {
        return odm;
    }

    public DtmGradientProjection getEquilibrator() {
        return equilibrator;
    }

    public double getMinimalNodeTime() {
        return minimalNodeTime;
    }

    public double getMaximalNodeTime() {
        return maximalNodeTime;
    }

    public int getNumberIntervals() {
        return numberIntervals;
    }

    public List<DtmEdge> getBestResultEdges() {
        return bestResultEdges;
    }

    public int getNumberSearchPerInterval() {
        return numberSearchPerInterval;
    }

    public double getPositionInInterval() {
        return positionInInterval;
    }

    public boolean isUseContinuousShortestPathSearch() {
        return useContinuousShortestPathSearch;
    }

    public void setUseContinuousShortestPathSearch(boolean useContinuousShortestPathSearch) {
        this.useContinuousShortestPathSearch = useContinuousShortestPathSearch;
    }
}
