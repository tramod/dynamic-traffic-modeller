/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.List;

/**
 * General function used in Dynamic Traffic Assignment
 * @author Tomas Potuzak
 */
public interface DtmFunction {

    /**
     * Performs addition with other function
     * @param other other function, which shall be added to this one
     * @return this function plus other function
     * @throws IllegalArgumentException if the other function is of unsupported type
     */
    public DtmFunction plus(DtmFunction other) throws IllegalArgumentException;

    /**
     * Performs subtraction from this function
     * @param other other function, which shall be subtracted from this one
     * @return this function minus other function
     * @throws IllegalArgumentException if the other function is of unsupported type
     */
    public DtmFunction minus(DtmFunction other) throws IllegalArgumentException;

    /**
     * Calculates and returns definite integral of this function from its start to its end point
     * @param useAbsoluteValues indicates whether absolute values of function values shall be used for the computation
     * @return definite integral of this function from its start to its end point
     */
    public double integrate(boolean useAbsoluteValues);

    /**
     * Returns the function value (<i>f</i>(<i>x</i>)) for the specified <i>x<i> value
     * @param x the <i>x</i> value, for which the function value (<i>f</i>(<i>x</i>)) shall be returned
     * @return the function value (<i>f</i>(<i>x</i>)) for the specified <i>x<i> value
     * @throws IllegalStateException if the function has no points (i.e., is empty)
     */
    public double valueAt(double x) throws IllegalStateException;

    /**
     * Returns the minimum of this function
     * @return the minimum of this function
     * @throws IllegalStateException if the function has no points (i.e., is empty)
     */
    public double minimum() throws IllegalStateException;

    /**
     * Returns the maximum of this function
     * @return the maximum of this function
     * @throws IllegalStateException if the function has no points (i.e., is empty)
     */
    public double maximum() throws IllegalStateException;

    /**
     * Returns a sub-function of this function on interval <a, b>
     * @param a the start point of the sub-function
     * @param b the end point of the sub-function
     * @return a sub-function of this function on interval <a, b>
     * @throws IllegalStateException if the function has less than two points
     * @throws IllegalArgumentException if a is greater or equal to b or a and/or b are outside the interval of the function
     */
    public DtmFunction subFunction(double a, double b) throws IllegalStateException, IllegalArgumentException;

    /**
     * Returns the <i>x</i> values of this function
     * @return the <i>x</i> values of this function
     */
    public List<Double> getXValues();

    /**
     * Returns the <i>y</i> = <i>f</i>(<i>x</i>) values of this function
     * @return the <i>y</i> = <i>f</i>(<i>x</i>) values of this function
     */
    public List<Double> getYValues();

    /**
     * Returns the <i>x</i> value of the point at the specified index
     * @param index the index of the point, whose <i>x</i> value shall be returned
     * @return the <i>x</i> value of the point at the specified index
     * @throws IndexOutOfBoundsException if the point at the specified index does not exist
     */
    public double getX(int index) throws IndexOutOfBoundsException;

    /**
     * Returns the <i>y</i> value of the point at the specified index
     * @param index the index of the point, whose <i>y</i> value shall be returned
     * @return the <i>y</i> value of the point at the specified index
     * @throws IndexOutOfBoundsException if the point at the specified index does not exist
     */
    public double getY(int index) throws IndexOutOfBoundsException;

    /**
     * Returns the number of points of this function
     * @return the number of points of this function
     */
    public int getLength();

    /**
     * Returns the <i>x</i> value of the first point of the function (i.e., at index 0)
     * @return the <i>x</i> value of the first point of the function (i.e., at index 0)
     * @throws IndexOutOfBoundsException if the point at index 0 does not exist (i.e., the function is empty, its length is 0)
     */
    public double getFirstX() throws IndexOutOfBoundsException;

    /**
     * Returns the <i>y</i> value of the first point of the function (i.e., at index 0)
     * @return the <i>y</i> value of the first point of the function (i.e., at index 0)
     * @throws IndexOutOfBoundsException if the point at index 0 does not exist (i.e., the function is empty, its length is 0)
     */
    public double getFirstY() throws IndexOutOfBoundsException;

    /**
     * Returns the <i>x</i> value of the last point of the function (i.e., at index (length - 1))
     * @return the <i>x</i> value of the last point of the function (i.e., at index (length - 1))
     * @throws IndexOutOfBoundsException if the point at index (length - 1) does not exist (i.e., the function is empty, its length is 0)
     */
    public double getLastX() throws IndexOutOfBoundsException;

    /**
     * Returns the <i>y</i> value of the last point of the function (i.e., at index (length - 1))
     * @return the <i>y</i> value of the last point of the function (i.e., at index (length - 1))
     * @throws IndexOutOfBoundsException if the point at index (length - 1) does not exist (i.e., the function is empty, its length is 0)
     */
    public double getLastY() throws IndexOutOfBoundsException;

    /**
     *
     * @param numBins number of intervals
     * @param startX lower bound
     * @param endX upper bound
     * @param positionInInterval representative value in the interval <0.0, 1.0>
     *                           0.0 - start of the interval
     *                           1.0 - end of the interval
     * @return discrete function
     */
    public DtmDiscreteFunction toDiscreteFunction(int numBins, double startX, double endX, double positionInInterval);
}
