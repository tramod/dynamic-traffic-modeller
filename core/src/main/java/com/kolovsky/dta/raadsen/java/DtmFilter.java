/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * A filter for distinguishing object with acceptable features from the object with unacceptable features
 * @param <T> the type of the investigated objects
 * @author Tomas Potuzak
 */
public interface DtmFilter<T> {

    /**
     * Returns <code>true</code> if the specified object has acceptable features and this filter accepts it. Returns <code>false</code> otherwise
     * @param object the object, whose features are investigated
     * @return <code>true</code> if the specified object has acceptable features and this filter accepts it. Returns <code>false</code> otherwise
     */
    public boolean isAccepted(T object);
}
