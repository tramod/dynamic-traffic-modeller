/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;

/**
 * The graph representing the road traffic network for dynamic traffic assignment
 * @author Tomas Potuzak
 */
public class DtmGraph {
    /** Default capacity of (priority) queues used for search algorithms */
    public static final int DEFAULT_QUEUE_CAPACITY = 100;
    /** The nodes of this graph */
    protected List<DtmNode> nodes;
    /** Fast nodes access table utilized for direct access to nodes based on their indices */
    protected HashMap<Integer, DtmNode> nodesAccessTable;
    /** The edges of this graph */
    protected List<DtmEdge> edges;
    /** Fast edges access table utilized for direct access to edges based on their indices */
    protected HashMap<Integer, DtmEdge> edgesAccessTable;
    /** The minimal time required for the events of the border edges to start to influence the neighboring subgraph - used for the parallel dynamic traffic assignment */
    protected double lookAhead;

    // DUAL GRAPH
    protected List<DtmDualEdge> dualEdges = new ArrayList<>();

    /**
     * Creates new graph
     */
    public DtmGraph() {
        nodes = new ArrayList<>();
        nodesAccessTable = new HashMap<>();
        edges = new ArrayList<>();
        edgesAccessTable = new HashMap<>();
    }

    /**
     * Adds a new edge with specified ID, specified start and end node ID, specified length and fundamental diagram. If the edge already exists, it is overwritten by the new edge
     * The nodes of the specified indices are added, if they are not already present.
     * The edge is added to the list of outgoing edges of the start node and to the list of incoming edges of the end node
     * @param edgeId the ID of the edge
     * @param startNodeId the ID of the start node of the edge
     * @param endNodeId the ID of the end node of the edge
     * @param edgeLength the length of the edge
     * @param fundamentalDiagram the fundamental diagram of the edge
     */
    public void addEdge(int edgeId, int startNodeId, int endNodeId, double edgeLength, DtmFundamentalDiagram fundamentalDiagram) {
        DtmNode startNode = nodesAccessTable.get(startNodeId);
        if (startNode == null) {
            startNode = new DtmNode(startNodeId);
            nodesAccessTable.put(startNodeId, startNode);
            nodes.add(startNode);
        }

        DtmNode endNode = nodesAccessTable.get(endNodeId);
        if (endNode == null) {
            endNode = new DtmNode(endNodeId);
            nodesAccessTable.put(endNodeId, endNode);
            nodes.add(endNode);
        }

        DtmEdge edge = edgesAccessTable.get(edgeId);
        if (edge == null) {
            edge = new DtmEdge(edgeId, edgeLength, startNode, endNode, fundamentalDiagram);
            edge.setFundamentalDiagram(fundamentalDiagram);
            edgesAccessTable.put(edgeId, edge);
            edges.add(edge);
        }
        else { //If the edge with the same id is already present, its attribute values are overwritten
            edge.setLength(edgeLength);
            edge.setStartNode(startNode);
            edge.setEndNode(endNode);
            edge.setFundamentalDiagram(fundamentalDiagram);
        }
        startNode.addOutgoingEdge(edge);
        endNode.addIncomingEdge(edge);
    }

    /**
     * Add movement to the graph
     */
    public void addDualEdge(DtmDualEdge dualEdge){
        dualEdges.add(dualEdge);
    }

    /**
     * Add node to the graph
     */
    public void addNode(DtmNode node){
        nodesAccessTable.put(node.id, node);
        nodes.add(node);
    }

    /**
     * Initialize all not yet initialized data structures of this graph.
     * This method should be called after all edges (and, with them, nodes) were added to this graph.
     */
    public void initializeStructures() {
        for (int i = 0; i < nodes.size(); i++) {
            nodes.get(i).setIndex(i);
        }

        for (int i = 0; i < edges.size(); i++) {
            edges.get(i).setIndex(i);
        }
    }

    /**
     * Returns the node corresponding to the specified ID.
     * @param id the ID of the searched node
     * @return the node corresponding to the specified ID.
     * @throws IllegalArgumentException if the node with specified ID does not exists in this graph
     */
    public DtmNode getNodeById(int id) throws IllegalArgumentException {
        DtmNode node = nodesAccessTable.get(id);
        if (node == null) {
            throw new IllegalArgumentException("No such node in this graph (ID: " + id + ")");
        }
        else {
            return node;
        }
    }

    /**
     * Returns the node at specified index of the list of nodes
     * @param index the index of the node in the list of nodes
     * @return the node at specified index of the list of nodes
     * @throws IndexOutOfBoundsException if the index is out of bounds of the list of nodes
     */
    public DtmNode getNodeByIndex(int index) throws IndexOutOfBoundsException {
        return nodes.get(index);
    }

    /**
     * Returns the edge corresponding to the specified ID.
     * @param id the ID of the searched edge
     * @return the edge corresponding to the specified ID.
     * @throws IllegalArgumentException if the edge with specified ID does not exists in this graph
     */
    public DtmEdge getEdgeById(int id) throws IllegalArgumentException {
        DtmEdge edge = edgesAccessTable.get(id);
        if (edge == null) {
            throw new IllegalArgumentException("No such edge in this graph (ID: " + id + ")");
        }
        else {
            return edge;
        }
    }

    /**
     * Returns the edge at specified index of the list of nodes
     * @param index the index of the edge in the list of nodes
     * @return the edge at specified index of the list of nodes
     * @throws IndexOutOfBoundsException if the index is out of bounds of the list of edges
     */
    public DtmEdge getEdgeByIndex(int index) throws IndexOutOfBoundsException {
        return edges.get(index);
    }

    /**
     * Perform back search using time-dependent Dijkstra algorithm
     * @param startNode start node for searching (destination node)
     * @param startTime start time of node distribution function
     * @param endTime end time of node distribution function
     * @param numIntervals number of interval in discrete function
     * @param maxTimeQuery maximal arrival time in destination (start node)
     * @return distance node map (travel time function for every node)
     */
    public DtmDiscreteFunction[] performDiscreteBackProfileSearch(DtmNode startNode, double startTime, double endTime,
                                                                  int numIntervals, double maxTimeQuery){

        DtmDiscreteFunction[] dist = new DtmDiscreteFunction[nodes.size()];
        for(DtmNode node: nodes){
            dist[node.getIndex()] = new DtmDiscreteFunction(numIntervals, 0.0, startTime, endTime);
        }

        double timeStep = (endTime - startTime)/numIntervals;
        int addArrivalIntervals = (int) ((maxTimeQuery - endTime)/timeStep);
        boolean[] isReachable = new boolean[nodes.size()];
        for (int i = 0; i < (numIntervals + addArrivalIntervals); i++){
            // start from the end
            double arrivalTime = maxTimeQuery - i*timeStep;
            double[] departureTimes = backSearchDual(startNode, arrivalTime);
            for(DtmNode node: nodes){
                double departureTime = departureTimes[node.getIndex()];
                if (departureTime != Double.NEGATIVE_INFINITY){
                    isReachable[node.getIndex()] = true;
                }
                int departureIndex = (int) ((departureTime - startTime)/timeStep);
                if (departureIndex >= 0 && departureIndex < numIntervals){
                    dist[node.getIndex()].getValues()[departureIndex] = arrivalTime - departureTime;
                }
            }
        }

        // non reachable
        for(DtmNode node: nodes){
            if (!isReachable[node.getIndex()]){
                Arrays.fill(dist[node.getIndex()].getValues(), 1E6);
            }
        }

        // fill the gaps in functions
        for(DtmNode node: nodes){
            double[] ttf = dist[node.getIndex()].getValues();
            double lastValue = 0.0;
            for (int i = ttf.length - 1; i >= 0; i--){
                if (ttf[i] == 0.0 && lastValue > 0.0){
                    ttf[i] = lastValue;
                }
                lastValue = ttf[i];
            }
        }
        return dist;
    }

    /**
     * Perform back search using time-dependent Dijkstra algorithm
     * @param startNode start node for searching (destination node)
     * @param startTime start time of node distribution function
     * @param endTime end time of node distribution function
     * @param numIntervals number of interval in discrete function
     * @param maxTimeQuery maximal arrival time in destination (start node)
     * @return distance node map (travel time function for every node)
     */
    public DtmPiecewiseLinearFunction[] performDiscreteBackProfileSearchLin(DtmNode startNode, double startTime, double endTime,
                                                                            int numIntervals, double maxTimeQuery){

        DtmPiecewiseLinearFunction[] dist = new DtmPiecewiseLinearFunction[nodes.size()];
        for(DtmNode node: nodes){
            dist[node.getIndex()] = new DtmPiecewiseLinearFunction();
        }

        double timeStep = (endTime - startTime)/numIntervals;
        int addArrivalIntervals = (int) ((maxTimeQuery - endTime)/timeStep);
        boolean[] isReachable = new boolean[nodes.size()];
        int numSearch = numIntervals + addArrivalIntervals;
        for (int i = 0; i < numSearch; i++){
            double arrivalTime = startTime + i*timeStep;
            double[] departureTimes = backSearchDual(startNode, arrivalTime);
            for(DtmNode node: nodes){
                DtmPiecewiseLinearFunction ttf = dist[node.getIndex()];
                double departureTime = departureTimes[node.getIndex()];
                if (departureTime != Double.NEGATIVE_INFINITY){
                    isReachable[node.getIndex()] = true;
                }
                if (startTime <= departureTime && departureTime <= endTime){
                    if (ttf.getLength() == 0 && startTime != departureTime){
                        ttf.addPoint(startTime, arrivalTime - departureTime);
                    }
                    ttf.addPoint(departureTime, arrivalTime - departureTime);
                }
            }
        }

        // non reachable
        for(DtmNode node: nodes){
            if (!isReachable[node.getIndex()]){
                dist[node.getIndex()].addPoint(startTime, 1E6);
                dist[node.getIndex()].addPoint(endTime, 1E6);
            }
        }

        // add last point
        for(DtmNode node: nodes){
            DtmPiecewiseLinearFunction ttf = dist[node.getIndex()];
            if (ttf.getLastX() < endTime){
                ttf.addPoint(endTime, ttf.getLastY());
            }
        }
        return dist;
    }

    /**
     * Static time-dependent back shortest path search for given arrival time to destination node (startNode)
     * @param startNode start node for searching (destination node)
     * @param arrivalTime arrival time at start node
     * @return departure time for every node
     */
    public double[] backSearch(DtmNode startNode, double arrivalTime){
        // priority queue in opposite order -> time with minus sign
        PriorityQueue<DtmPriorityQueueElement<Double, DtmNode>> priorityQueue = new PriorityQueue<>(DEFAULT_QUEUE_CAPACITY);
        double[] dist = new double[nodes.size()];
        for (int i = 0; i < nodes.size(); i++){
            dist[i] = Double.NEGATIVE_INFINITY;
        }

        dist[startNode.getIndex()] = arrivalTime;
        priorityQueue.offer(new DtmPriorityQueueElement<>(-arrivalTime, startNode));
        while (!priorityQueue.isEmpty()){
            DtmPriorityQueueElement<Double, DtmNode> element = priorityQueue.poll();
            DtmNode node = element.getValue();
            for (DtmEdge edge: node.getIncomingEdges()){
                DtmArrivalFunction af = edge.getArrivalFunction();
                if (af.getFirstY() <= dist[node.getIndex()] && dist[node.getIndex()] <= af.getLastY()){
                    double departureTime = af.getDeparture(dist[node.getIndex()]);
                    // check maximal speed on the edge
                    double minimalTravelTime = edge.getLength()/edge.getFundamentalDiagram().getMaximalSpeed();
                    if (arrivalTime - departureTime < minimalTravelTime){
                        departureTime = arrivalTime - minimalTravelTime;
                    }
                    if (departureTime > dist[edge.getStartNode().getIndex()]){
                        dist[edge.getStartNode().getIndex()] = departureTime;
                        priorityQueue.offer(new DtmPriorityQueueElement<>(- departureTime, edge.getStartNode()));
                    }
                }
            }
        }
        return dist;
    }

    /**
     * Static time-dependent back shortest path search for given arrival time to destination node (startNode)
     * Use Dual graph
     * @param startNode start node for searching (destination node)
     * @param arrivalTime arrival time at start node
     * @return departure time for every node
     */
    public double[] backSearchDual(DtmNode startNode, double arrivalTime){
        // priority queue in opposite order -> time with minus sign
        PriorityQueue<DtmPriorityQueueElement<Double, DtmEdge>> priorityQueue = new PriorityQueue<>(DEFAULT_QUEUE_CAPACITY);
        double[] dist = new double[edges.size()];
        for (int i = 0; i < edges.size(); i++){
            dist[i] = Double.NEGATIVE_INFINITY;
        }

        for (DtmEdge startEdge: startNode.getIncomingEdges()){
            DtmPriorityQueueElement<Double, DtmEdge> element = new DtmPriorityQueueElement<>(-arrivalTime, startEdge);
            priorityQueue.offer(element);
            dist[startEdge.getIndex()] = arrivalTime;
        }

        while (!priorityQueue.isEmpty()){
            DtmPriorityQueueElement<Double, DtmEdge> element = priorityQueue.poll();
            DtmEdge edge = element.getValue();
            DtmArrivalFunction af = edge.getArrivalFunction();

            if (af.getFirstY() <= dist[edge.getIndex()] && dist[edge.getIndex()] <= af.getLastY()){
                double departureTime = af.getDeparture(dist[edge.getIndex()]);
                // check maximal speed on the edge
                double minimalTravelTime = edge.getLength()/edge.getFundamentalDiagram().getMaximalSpeed();

                if (arrivalTime - departureTime < minimalTravelTime){
                    departureTime = arrivalTime - minimalTravelTime;
                }

                for (DtmDualEdge dualEdge: edge.getIncomingDualEdges()){
                    DtmEdge startEdge = dualEdge.getStartEdge();
                    if (departureTime > dist[startEdge.getIndex()]){
                        dist[startEdge.getIndex()] = departureTime;
                        priorityQueue.offer(new DtmPriorityQueueElement<>(- departureTime, startEdge));
                    }
                }
            }
        }

        // transform back
        double[] distT = new double[edges.size()];
        for (DtmNode node: nodes) {
            double d = Double.NEGATIVE_INFINITY;
            for (DtmEdge outEdge: node.getOutgoingEdges()){
                double c = dist[outEdge.getIndex()];
                if (c > d){
                    d = c;
                }
            }
            distT[node.getIndex()] = d;
        }

        return distT;
    }

    /**
     * Performs all to all profile search (TDSP) and returns arrival function for each node from the node to the start node
     * @param startNode the starting node of the search
     * @param startTime the lower bound of the time interval
     * @param endTime the upper bound of the time interval
     * @param epsilon the maximal allowed relative error
     * @return arrival function for each node from the node to start node
     */
    public List<DtmArrivalFunction> performBackProfileSearch(DtmNode startNode, double startTime, double endTime, double epsilon) {
        List<DtmArrivalFunction> distances = new ArrayList<>();
        for (int i = 0; i < nodes.size(); i++) {
            distances.add(new DtmArrivalFunction());
        }

        PriorityQueue<DtmPriorityQueueElement<DtmArrivalFunction, DtmNode>> priorityQueue = new PriorityQueue<>();
        DtmPriorityQueueElement<DtmArrivalFunction, DtmNode> element = null;
        DtmPairElement<DtmArrivalFunction, List<Double>> compositionResult = null;

        List<Double> xValues = new ArrayList<>();
        xValues.add(startTime);
        xValues.add(endTime);
        List<Double> yValues = new ArrayList<>(xValues);
        distances.set(startNode.getIndex(), new DtmArrivalFunction(xValues, yValues));
        element = new DtmPriorityQueueElement<>(distances.get(startNode.getIndex()), startNode);
        priorityQueue.offer(element);

        while (!priorityQueue.isEmpty()) {
            element = priorityQueue.poll();

            for (DtmEdge edge: element.getValue().getIncomingEdges()) {
                compositionResult = distances.get(element.getValue().getIndex()).performComposition(edge.getArrivalFunction(), epsilon);
                if (epsilon != 0.0) {
                    element.setPriority(compositionResult.getFirst().keepEpsilon(compositionResult.getSecond()));
                }

                if (element.getPriority().isLowerWithEpsilon(distances.get(edge.getStartNode().getIndex()), epsilon)) {
                    distances.set(edge.getStartNode().getIndex(), element.getPriority().minimumFunction(distances.get(edge.getStartNode().getIndex())));
                    priorityQueue.offer(new DtmPriorityQueueElement<>(distances.get(edge.getStartNode().getIndex()), edge.getStartNode()));
                }
            }
        }

        return distances;
    }

    /**
     * Performs the shortest path search using the Dijkstra algorithm from the start node and returns the distances and previous edges of the nodes
     * @param startNode the node, from which the searching starts
     * @return the distances and previous edges of the nodes
     */
    public DtmPairElement<List<Double>, List<DtmEdge>> performShortestPathSearch(DtmNode startNode) {
		/*PriorityQueue<DtmPriorityQueueElement<Double, DtmNode>> priorityQueue = new PriorityQueue<>(DEFAULT_QUEUE_CAPACITY, new Comparator<DtmPriorityQueueElement<Double, DtmNode>>() {
			@Override
			public int compare(DtmPriorityQueueElement<Double, DtmNode> element1, DtmPriorityQueueElement<Double, DtmNode> element2) {
				return -element1.compareTo(element2); //Reversed priority
			}
		});
		*/
        PriorityQueue<DtmPriorityQueueElement<Double, DtmNode>> priorityQueue = new PriorityQueue<>(DEFAULT_QUEUE_CAPACITY);
        List<Double> distances = new ArrayList<>();
        for (int i = 0; i < nodes.size(); i++) {
            distances.add(Double.POSITIVE_INFINITY);
        }

        List<DtmEdge> previousEdges = new ArrayList<>();
        for (int i = 0; i < nodes.size(); i++) {
            previousEdges.add(null);
        }

        double cost = 0.0;
        DtmPriorityQueueElement<Double, DtmNode> element = new DtmPriorityQueueElement<>(0.0, startNode);
        priorityQueue.offer(element);
        distances.set(startNode.getIndex(), 0.0);

        while (!priorityQueue.isEmpty()) {
            element = priorityQueue.poll();

            for (DtmEdge edge: element.getValue().getOutgoingEdges()) {
                cost = edge.getLength() / edge.getFundamentalDiagram().getMaximalSpeed(); //Result is in hours

                if (distances.get(element.getValue().getIndex()) + cost < distances.get(edge.getEndNode().getIndex())) {
                    distances.set(edge.getEndNode().getIndex(), distances.get(element.getValue().getIndex()) + cost);
                    previousEdges.set(edge.getEndNode().getIndex(), edge);
                    DtmPriorityQueueElement<Double, DtmNode> element2 = new DtmPriorityQueueElement<>(distances.get(edge.getEndNode().getIndex()), edge.getEndNode());
                    priorityQueue.offer(element2);
                }
            }
        }

        return new DtmPairElement<>(distances, previousEdges);
    }

    /**
     * Performs the shortest path search using the Dijkstra algorithm from the start node and returns the distances and previous edges of the nodes
     * Search on dual graph.
     * @param startNode the node, from which the searching starts
     * @return the distances and previous edges of the nodes
     */
    public DtmPairElement<List<Double>, List<DtmEdge>> performShortestPathSearchDual(DtmNode startNode){
        PriorityQueue<DtmPriorityQueueElement<Double, DtmEdge>> priorityQueue = new PriorityQueue<>(DEFAULT_QUEUE_CAPACITY);
        List<Double> distances = new ArrayList<>();
        for (int i = 0; i < edges.size(); i++) {
            distances.add(Double.POSITIVE_INFINITY);
        }

        for (DtmEdge startEdge: startNode.getOutgoingEdges()){
            Double cost = startEdge.getLength() / startEdge.getFundamentalDiagram().getMaximalSpeed();
            DtmPriorityQueueElement<Double, DtmEdge> element = new DtmPriorityQueueElement<>(cost, startEdge);
            priorityQueue.offer(element);
            distances.set(startEdge.getIndex(), cost);
        }

        while (!priorityQueue.isEmpty()) {
            DtmPriorityQueueElement<Double, DtmEdge> element = priorityQueue.poll();
            DtmEdge edge = element.getValue();

            for (DtmDualEdge dualEdge: edge.getOutgoingDualEdges()) {
                DtmEdge targetEdge = dualEdge.getEndEdge();
                Double cost = targetEdge.getLength() / targetEdge.getFundamentalDiagram().getMaximalSpeed();

                if (distances.get(edge.getIndex()) + cost < distances.get(dualEdge.getEndEdge().getIndex())) {
                    distances.set(targetEdge.getIndex(), distances.get(edge.getIndex()) + cost);
                    DtmPriorityQueueElement<Double, DtmEdge> element2 = new DtmPriorityQueueElement<>(distances.get(targetEdge.getIndex()), targetEdge);
                    priorityQueue.offer(element2);
                }
            }
        }

        // transform back
        List<Double> nodeDistances = new ArrayList<>();
        List<DtmEdge> nodePrev = new ArrayList<>();
        for (DtmNode node: nodes) {
            Double dist = Double.POSITIVE_INFINITY;
            DtmEdge prev = null;
            for (DtmEdge inEdge: node.getIncomingEdges()){
                Double c = distances.get(inEdge.getIndex());
                if (c < dist){
                    dist = c;
                    prev = inEdge;
                }
            }
            nodeDistances.add(dist);
            nodePrev.add(prev);
        }
        nodeDistances.set(startNode.getIndex(), 0.0);
        nodePrev.set(startNode.getIndex(), null);

        return new DtmPairElement<>(nodeDistances, nodePrev);
    }

    /**
     * Construct dual graph on top of graph.
     */
    public void constructDualGraph(){
        for (DtmNode node: nodes){
            for (DtmEdge in: node.getIncomingEdges()){
                for (DtmEdge out: node.getOutgoingEdges()){
                    DtmDualEdge dualEdge = new DtmDualEdge(in, out, node);
                    dualEdges.add(dualEdge);
                    in.addOutgoingDualEdge(dualEdge);
                    out.addIncomingDualEdge(dualEdge);
                    node.addDualEdge(dualEdge);
                }
            }
        }
    }

    /**
     * Extracts and returns the path from the start node to the specified end node from the list of previous edges. The start node is specified prior the creation of the list of previous edges as the parameter of the <code>performShortestPathSearch()</code> method.
     * @param previousEdges the list of previous edged created as a part of the output of the <code>performShortestPathSearch()</code> method
     * @param endNode the end node, to which the path is leading
     * @return the path from the start node to the specified end node from the list of previous edges
     */
    public List<DtmEdge> extractPath(List<DtmEdge> previousEdges, DtmNode endNode) {
        List<DtmEdge> reversedPath = new ArrayList<>();
        DtmEdge edge = previousEdges.get(endNode.getIndex());

        while (edge != null) {
            reversedPath.add(edge);
            edge = previousEdges.get(edge.getStartNode().getIndex());
        }

        List<DtmEdge> path = new ArrayList<>(); //Reverse the path
        for (int i = reversedPath.size() - 1; i >= 0; i--) {
            path.add(reversedPath.get(i));
        }

        return path;
    }

    /**
     * Returns <code>true</code>, if this graph is coherent (i.e., there is a path between each origin and each destination). Returns <code>false</code> otherwise.
     * @param origins the origins
     * @param destinations the destinations
     * @return <code>true</code>, if this graph is coherent (i.e., there is a path between each origin and each destination). Returns <code>false</code> otherwise.
     */
    public boolean isCoherent(List<DtmNode> origins, List<DtmNode> destinations) {
        boolean coherent = true;
        for (DtmNode origin: origins) {
            DtmPairElement<List<Double>, List<DtmEdge>> searchResult = performShortestPathSearch(origin);

            for (DtmNode destination: destinations) {
                if (Double.isInfinite(searchResult.getFirst().get(destination.getIndex()))) {
                    coherent = false;
                    break;
                }
            }
        }

        return coherent;
    }


    /**
     * Resets all edges to initial state
     */
    public void resetEdges() {
        for (DtmEdge edge: edges) {
            edge.reset();
        }
    }

    /**
     * Determines the minimal time required for the events of the border edges to start to influence the neighboring subgraph.
     * The look ahead is calculated from the lengths and maximal speed of the border edges
     */
    public void determineLookAhead() {
        lookAhead = Double.NaN;
        double edgeLookAhead = 0.0;

        for (DtmEdge edge: edges) {
            if (edge.isOnBorder()) {
                edgeLookAhead = edge.getLength() / edge.getFundamentalDiagram().getMaximalSpeed();
                System.out.println("edge look ahead " + edgeLookAhead);
                if (Double.isNaN(lookAhead) || edgeLookAhead < lookAhead) {
                    lookAhead = edgeLookAhead;
                }
            }
        }

    }

    //Getters and setters

    /**
     * Returns the nodes of this graph
     * @return the nodes of this graph
     */
    public List<DtmNode> getNodes() {
        return nodes;
    }

    /**
     * Returns the edges of this graph
     * @return the edges of this graph
     */
    public List<DtmEdge> getEdges() {
        return edges;
    }

    /**
     * Returns the minimal time required for the events of the border edges to start to influence the neighboring subgraph
     * @return the minimal time required for the events of the border edges to start to influence the neighboring subgraph
     */
    public double getLookAhead() {
        return lookAhead;
    }
}