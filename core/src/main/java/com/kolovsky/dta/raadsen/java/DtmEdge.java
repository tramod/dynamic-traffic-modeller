/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.List;

/**
 * An edge of the graph (i.e., a one-way road in the road traffic network) for dynamic traffic assignment
 * @author Tomas Potuzak
 */
public class DtmEdge extends DtmSource {
    /** The node from which this edge is outgoing */
    protected DtmNode startNode;
    /** The node to which this edge is incoming */
    protected DtmNode endNode;
    /** The length of this edge in kilometers */
    protected double length;
    /** The fundamental diagram of this edge */
    protected DtmFundamentalDiagram fundamentalDiagram;
    /** The flow incoming to this edge (u) in vehicles per hour */
    protected DtmPiecewiseConstantFunction inflow;
    /** The flow outgoing from this edge (v) */
    protected DtmPiecewiseConstantFunction outflow;
    /** The cumulative inflow of this edge (U) */
    protected DtmNonDecreasingPiecewiseLinearFunction cumulativeInflow;
    /** The cumulative outflow from this edge (V) */
    protected DtmNonDecreasingPiecewiseLinearFunction cumulativeOutflow;
    /** Indicates whether or not there is a vehicle queue in this edge */
    protected boolean queuePresent;
    /** The time of the last update of the demand */
    protected double demandLastUpdateTime;
    /** Current valid forward event */
    protected DtmForwardEdgeEvent currentForwardEvent;
    /** The supply of this edge (r) */
    protected double supply;
    /** The mixture of the inflow */
    protected DtmMixture<DtmIdentifiable> inflowMixture;
    /** Function of the mixture of the inflow */
    protected DtmGeneralFunction<DtmMixture<DtmIdentifiable>> inflowMixtureFunction;
    /** The arrival function */
    protected DtmArrivalFunction arrivalFunction;
    /** Forward events of this edge */
    protected DtmEdgeEventsStorage<DtmForwardEdgeEvent> forwardEventsStorage;
    /** Backward events of this edge */
    protected DtmEdgeEventsStorage<DtmBackwardEdgeEvent> backwardEventsStorage;
    /** Forward events of this edge */
    protected DtmEdgeEventsStorage<DtmMixtureEdgeEvent> mixtureEventsStorage;
    /** ID of the subgraph, to which this edge belongs */
    protected int subgraphId;
    /** Determines whether this edge is on the border of the subgraphs or not */
    protected boolean onBorder;
    /** ID of the subgraph, to which this edge continue (for the border edges only) */
    protected int neighboringSubgraphId;

    // DUAL GRAPH
    protected List<DtmDualEdge> incomingDualEdges = new ArrayList<>();
    protected List<DtmDualEdge> outgoingDualEdges = new ArrayList<>();

    public List<DtmDualEdge> getIncomingDualEdges() {
        return incomingDualEdges;
    }

    public List<DtmDualEdge> getOutgoingDualEdges() {
        return outgoingDualEdges;
    }

    public void addIncomingDualEdge(DtmDualEdge dualEdge){
        incomingDualEdges.add(dualEdge);
    }

    public void addOutgoingDualEdge(DtmDualEdge dualEdge){
        outgoingDualEdges.add(dualEdge);
    }

    /**
     * Creates a new edge with specified parameters
     * @param id the ID of the edge
     * @param length the length of this edge in kilometers
     * @param startNode the node from which this edge is outgoing
     * @param endNode the node to which this edge is incoming
     * @param fundamentalDiagram the fundamental diagram of this edge
     */
    public DtmEdge(int id, double length, DtmNode startNode, DtmNode endNode, DtmFundamentalDiagram fundamentalDiagram) {
        this.id = id;
        this.length = length;
        this.startNode = startNode;
        this.endNode = endNode;
        this.fundamentalDiagram = fundamentalDiagram;

        forwardEventsStorage = new DtmEdgeEventsStorage<>();
        backwardEventsStorage = new DtmEdgeEventsStorage<>();
        mixtureEventsStorage = new DtmEdgeEventsStorage<>();

        reset();
    }

    /**
     * Reset this edge to its initial state
     */
    public void reset() {
        inflow = new DtmPiecewiseConstantFunction();
        inflow.addPoint(0.0, 0.0);
        outflow = new DtmPiecewiseConstantFunction();
        outflow.addPoint(0.0, 0.0);

        cumulativeInflow = new DtmNonDecreasingPiecewiseLinearFunction();
        cumulativeInflow.addPoint(0.0, 0.0);
        cumulativeOutflow = new DtmNonDecreasingPiecewiseLinearFunction();
        cumulativeOutflow.addPoint(0.0, 0.0);

        queuePresent = false;
        currentForwardEvent = null;
        supply = fundamentalDiagram.getFlowCapacity();
        inflowMixture = null;
        inflowMixtureFunction = new DtmGeneralFunction<>();
        inflowMixtureFunction.addPoint(0.0, null);
        outflowMixture = null;
        arrivalFunction = null;

        forwardEventsStorage = new DtmEdgeEventsStorage<>();
        backwardEventsStorage = new DtmEdgeEventsStorage<>();
        mixtureEventsStorage = new DtmEdgeEventsStorage<>();

        demand = 0.0;
        demandLastUpdateTime = 0.0;
    }

    /**
     * Adds an inflow point for the specified time
     * @param time the time of the point
     * @param inflowValue the inflow of the point
     * @throws IllegalArgumentException if the inflow is greater than this edge capacity
     */
    public void addInflowPoint(double time, double inflowValue) throws IllegalArgumentException {
        if (inflowValue > fundamentalDiagram.getFlowCapacity()) {
            throw new IllegalArgumentException("Added inflow must be lower or equal to the capacity of this edge.");
        }
        else {
            int index = inflow.getXValues().size() - 1;
            double cumulativeInflowValue = cumulativeInflow.getYValues().get(cumulativeInflow.getYValues().size() - 1) + inflow.getYValues().get(index) * (time - inflow.getXValues().get(index));

            if (time == inflow.getXValues().get(inflow.getXValues().size() - 1)) {
                inflow.updatePoint(index, time, inflowValue);
                cumulativeInflow.updatePoint(index, time, cumulativeInflowValue);
            }
            else {
                inflow.addPoint(time, inflowValue);
                cumulativeInflow.addPoint(time, cumulativeInflowValue);
            }
        }
    }

    /**
     * Adds an outflow point for the specified time
     * @param time the time of the point
     * @param outflowValue the outflow of the point
     * @throws IllegalArgumentException if the outflow is greater than this edge capacity
     */
    public void addOutflowPoint(double time, double outflowValue) throws IllegalArgumentException {
        if (outflowValue > fundamentalDiagram.getFlowCapacity()) {
            if (outflowValue - 1 < fundamentalDiagram.getFlowCapacity()) {
                outflowValue = fundamentalDiagram.getFlowCapacity();
            }
            else {
                throw new IllegalArgumentException("Added inflow must be lower or equal to the capacity of this edge.");
            }
        }

        int index = outflow.getYValues().size() - 1;
        double cumulativeOutflowValue = cumulativeOutflow.getYValues().get(cumulativeOutflow.getYValues().size() - 1) + outflow.getYValues().get(index) * (time - outflow.getXValues().get(index));

        if (time == outflow.getXValues().get(outflow.getXValues().size() - 1)) {
            outflow.updatePoint(index, time, outflowValue);
            cumulativeOutflow.updatePoint(index, time, cumulativeOutflowValue);
        }
        else {
            outflow.addPoint(time, outflowValue);
            cumulativeOutflow.addPoint(time, cumulativeOutflowValue);
        }
    }

    /**
     * Returns the cumulative inflow (U) value for the specified time
     * @param time the time, whose cumulative inflow value shall be returned
     * @return the cumulative inflow (U) value for the specified time
     */
    public double getCumulativeInflow(double time) {
        if (time > cumulativeInflow.getLastX()) {
            return cumulativeInflow.getLastY() + (time - cumulativeInflow.getLastX()) * inflow.getLastY();
        }
        else {
            return cumulativeInflow.valueAt(time);
        }
    }

    /**
     * Returns the cumulative outflow (U) value for the specified time
     * @param time the time, whose cumulative outflow value shall be returned
     * @return the cumulative outflow (U) value for the specified time
     */
    public double getCumulativeOutflow(double time) {
        if (time > cumulativeOutflow.getLastX()) {
            return cumulativeOutflow.getLastY() + (time - cumulativeOutflow.getLastX()) * outflow.getLastY();
        }
        else {
            return cumulativeOutflow.valueAt(time);
        }
    }

    /**
     * Calculates and returns the downstream projection of the cumulative inflow (U) for the specified time and event
     * @param time the time, for which the down projection of the cumulative inflow (U) shall be calculated
     * @param currentEvent the event, which triggered this method invocation
     * @param epsilon numerical precision of the calculation
     * @return the downstream projection of the cumulative inflow (U) for the specified time and event
     * @throws IllegalStateException
     */
    public DtmPairElement<Double, Double> getCumulativeInflowDownstreamProjection(double time, DtmForwardEdgeEvent currentEvent, double epsilon) throws IllegalStateException {
        double minimum = Double.POSITIVE_INFINITY;
        DtmForwardEdgeEvent minimumEvent = null;
        double cumulativeInflowDownstreamProjection = 0.0;
        for (DtmForwardEdgeEvent event: forwardEventsStorage) {
            if (event.getMinimalTime() <= time && event.getMaximalTime() > time) {
                if (minimum >= (cumulativeInflowDownstreamProjection = event.calculateCumulativeInflowDownstreamProjection(time))) {
                    minimum = cumulativeInflowDownstreamProjection;
                    minimumEvent = event;
                }
            }
        }
        if (minimumEvent == null) {
            if (forwardEventsStorage.isEmpty() || time < forwardEventsStorage.getLast().getMinimalTime()) {
                return new DtmPairElement<>(0.0, 0.0);
            }
            else {
                throw new IllegalStateException("Something is wrong.");
            }
        }

        if (minimumEvent != currentEvent && currentEvent != null) {
            if (minimum > (cumulativeInflowDownstreamProjection = currentEvent.calculateCumulativeInflowDownstreamProjection(time)) - epsilon) {
                minimum = cumulativeInflowDownstreamProjection;
                minimumEvent = currentEvent;
            }
        }
        currentForwardEvent = minimumEvent;

        return new DtmPairElement<>(minimum, minimumEvent.getFlow());
    }

    /**
     * Calculates and returns the upstream projection of the cumulative outflow (V) for the specified time and event
     * @param time the time, for which the down projection of the cumulative outflow (V) shall be calculated
     * @param currentEvent the event, which triggered this method invocation
     * @param epsilon numerical precision of the calculation
     * @return the upstream projection of the cumulative outflow (V) for the specified time and event
     * @throws IllegalStateException
     */
    public DtmPairElement<Double, Double> getCumulativeOutflowUpstreamProjection(double time, DtmBackwardEdgeEvent currentEvent, double epsilon) throws IllegalStateException {
        double minimum = Double.POSITIVE_INFINITY;
        DtmBackwardEdgeEvent minimumEvent = null;
        double cumulativeOutflowUpstreamProjection = 0.0;
        for (DtmBackwardEdgeEvent event: backwardEventsStorage) {
            if (event.getMinimalTime() <= time && event.getMaximalTime() >= time) {
                if (minimum >= (cumulativeOutflowUpstreamProjection = event.calculateCumulativeOutflowUpstreamProjection(time))) {
                    minimum = cumulativeOutflowUpstreamProjection;
                    minimumEvent = event;
                }
            }
        }
        if (minimumEvent == null) {
            //throw new IllegalStateException("Something is wrong.");
            minimumEvent = currentEvent;
        }

        if (minimumEvent != currentEvent) {
            if (minimum > (cumulativeOutflowUpstreamProjection = currentEvent.calculateCumulativeOutflowUpstreamProjection(time)) - epsilon) {
                minimum = cumulativeOutflowUpstreamProjection;
                minimumEvent = currentEvent;
            }
        }

        return new DtmPairElement<>(minimum, minimumEvent.getFlow());
    }

    /**
     * Returns the current (i.e., last) inflow (u) value
     * @return the current (i.e., last) inflow (u) value
     */
    public double getCurrentInflow() {
        return inflow.getYValues().get(inflow.getYValues().size() - 1);
    }

    /**
     * Returns the current (i.e., last) outflow (v) value
     * @return the current (i.e., last) outflow (v) value
     */
    public double getCurrentOutflow() {
        return outflow.getYValues().get(outflow.getYValues().size() - 1);
    }

    /**
     * Updates the mixture of the inflow, stores and returns the corresponding mixture event
     * @param time the time of the update
     * @param mixture the new mixture of the inflow
     * @return the generated mixture event corresponding to the mixture update
     * @throws NullPointerException if the specified mixture is <code>null</code>
     */
    public DtmMixtureEdgeEvent updateMixture(double time, DtmMixture<DtmIdentifiable> mixture) throws NullPointerException {
        if (mixture == null) {
            throw new NullPointerException("Mixture cannot be null.");
        }
        else {
            inflowMixture = mixture;

            if (time == inflowMixtureFunction.getLastX()) {
                inflowMixtureFunction.updatePoint(inflowMixtureFunction.getXValues().size() - 1, time, mixture);
            }
            else {
                inflowMixtureFunction.addPoint(time, mixture);
            }

            if (outflowMixture == null) {
                outflowMixture = mixture;
            }
            DtmMixtureEdgeEvent event = new DtmMixtureEdgeEvent(this, inflow.getLastX(), inflow.getLastY(), cumulativeInflow.getLastY(), time, mixture, DtmMathUtils.DEFAULT_COMPARISON_EPSILON);
            mixtureEventsStorage.add(event);

            return event;
        }
    }

    /**
     * Updates the inflow to this edge
     * @param time the time of the update
     * @param inflow the new value of the inflow
     * @param fanStep the length of the fan steps (not D - the fan steps count)
     * @param epsilon the comparison tolerance
     * @param cumulativeEpsilon the comparison tolerance for cumulative values
     * @return the events generated forward events corresponding to the inflow update
     */
    public List<DtmForwardEdgeEvent> updateInflow(double time, double inflow, double fanStep, double epsilon, double cumulativeEpsilon) {
        double lastInflow = getCurrentInflow();
        addInflowPoint(time, inflow);
        List<DtmForwardEdgeEvent> events = new ArrayList<>();
        DtmForwardEdgeEvent event = null;

        if (lastInflow < inflow) { //Increasing inflow -> with fan effect
            double[] inflowFanValues = DtmMathUtils.divideInterval(lastInflow, inflow, fanStep);

            for (int i = 1; i < inflowFanValues.length; i++) {
                if (i < inflowFanValues.length - 1) {
                    event = new DtmForwardEdgeEvent(this, time, inflowFanValues[i], cumulativeInflow.getYValues().get(cumulativeInflow.getYValues().size() - 1), true, false, cumulativeEpsilon, epsilon);
                }
                else {
                    event = new DtmForwardEdgeEvent(this, time, inflowFanValues[i], cumulativeInflow.getYValues().get(cumulativeInflow.getYValues().size() - 1), true, true, cumulativeEpsilon, epsilon);
                }
                forwardEventsStorage.add(event);
                event.calculateMinimalTime();

                if (forwardEventsStorage.getLast().getPrevious() != null) {
                    ((DtmForwardEdgeEvent) forwardEventsStorage.getLast().getPrevious()).calculateMaximalTime();
                }

                events.add(event);
            }
        }
        else if (lastInflow > inflow) { //Decreasing iflow -> without fan effect
            event = new DtmForwardEdgeEvent(this, time, inflow, cumulativeInflow.getYValues().get(cumulativeInflow.getYValues().size() - 1), false, false, cumulativeEpsilon, epsilon);
            forwardEventsStorage.add(event);
            event.calculateMinimalTime();

            if (forwardEventsStorage.getLast().getPrevious() != null) {
                ((DtmForwardEdgeEvent) forwardEventsStorage.getLast().getPrevious()).calculateMaximalTime();
            }

            events.add(event);
        }

        return events;
    }

    /**
     * Updates the outflow from this edge
     * @param time the time of the update
     * @param outflow the new value of the outflow
     * @param fanStep the length of the fan steps (not D - the fan steps count)
     * @param epsilon the comparison tolerance
     * @param cumulativeEpsilon the comparison tolerance for cumulative values
     * @return the events generated backward events corresponding to the outflow update
     */
    public List<DtmBackwardEdgeEvent> updateOutflow(double time, double outflow, double fanStep, double epsilon, double cumulativeEpsilon) {
        double lastOutflow = getCurrentOutflow();
        addOutflowPoint(time, outflow);
        List<DtmBackwardEdgeEvent> events = new ArrayList<>();
        DtmBackwardEdgeEvent event = null;

        if (lastOutflow < outflow - epsilon) { //Increasing outflow -> with fan effect
            double[] outflowFanValues = DtmMathUtils.divideInterval(lastOutflow, outflow, fanStep);

            for (int i = 1; i < outflowFanValues.length; i++) {
                if (i < outflowFanValues.length - 1) {
                    event = new DtmBackwardEdgeEvent(this, time, outflowFanValues[i], cumulativeOutflow.getLastY(), true, false, epsilon, cumulativeEpsilon);
                }
                else {
                    event = new DtmBackwardEdgeEvent(this, time, outflowFanValues[i], cumulativeOutflow.getLastY(), true, true, epsilon, cumulativeEpsilon);
                }
                backwardEventsStorage.add(event);
                event.calculateMinimalTime();

                if (backwardEventsStorage.getLast().getPrevious() != null) {
                    ((DtmBackwardEdgeEvent) backwardEventsStorage.getLast().getPrevious()).calculateMaximalTime();
                }

                events.add(event);
            }
        }
        else if (lastOutflow > outflow + epsilon) { //Decreasing iflow -> without fan effect
            event = new DtmBackwardEdgeEvent(this, time, outflow, cumulativeOutflow.getLastY(), false, false, epsilon, cumulativeEpsilon);
            backwardEventsStorage.add(event);
            event.calculateMinimalTime();

            if (backwardEventsStorage.getLast().getPrevious() != null) {
                ((DtmBackwardEdgeEvent) backwardEventsStorage.getLast().getPrevious()).calculateMaximalTime();
            }

            events.add(event);
        }

        return events;
    }

    /**
     * Updates the demand of this edge
     * @param time the time of the update
     * @param cumulativeEpsilon
     */
    public void updateDemand(double time, double cumulativeEpsilon) {
        if (demandLastUpdateTime < time) {
            DtmPairElement<Double, Double> projection = getCumulativeInflowDownstreamProjection(time, currentForwardEvent, cumulativeEpsilon);
            double cumulativeDownstream = projection.getFirst();
            double flow = projection.getSecond();

            //TODO only temporal and not correct - to avoid excess number of events in parallel run - change "if (cumulative..." to "if (true || cumulative..."
            if (cumulativeDownstream - getCumulativeOutflow(time) < cumulativeEpsilon) { //Number of vehicles unsuccessful in leaving the edge at time time equal to zero
                if (queuePresent) {
                    demand = flow;
                }
                queuePresent = false;
            }
            else {
                queuePresent = true;
                demand = fundamentalDiagram.getFlowCapacity();
            }
            demandLastUpdateTime = time;
        }
    }

    /**
     * Calculates and returns the travel time function based on cumulative numbers of vehicles entering and leaving the edge and calculates and stores the arrival function
     * @param maximalTime the <i>x</i> upper bound
     * @param maintainMaximalTime determines whether the resulting function shall be restricted by the upper bound maximalTime</code>
     * @return the travel time function based on cumulative numbers of vehicles entering and leaving the edge
     */
    public DtmPiecewiseLinearFunction calculateTravelTimeFunction(double maximalTime, boolean maintainMaximalTime) {
        //TODO comment fixFunctions()
        //DtmFunctionUtils.fixFunctions(cumulativeInflow, cumulativeOutflow, 1.0);
        DtmPiecewiseLinearFunction travelTimeFunction = DtmFunctionUtils.calculateHorizontalDifference(cumulativeInflow, cumulativeOutflow);
        //DtmPiecewiseLinearFunction travelTimeFunction = DtmFunctionUtils.calculateHorizontalDifference(cumulativeInflow, cumulativeOutflow, length / fundamentalDiagram.getMaximalSpeed());
        DtmPiecewiseLinearFunction maximumFunction = travelTimeFunction.maximumFunction(null, length / fundamentalDiagram.getMaximalSpeed());
        if (maintainMaximalTime && maximumFunction.getLastX() < maximalTime) {
            maximumFunction.addPoint(maximalTime, maximumFunction.getLastY());
        }
        maximumFunction = maximumFunction.simplify(1E-6, false);
        arrivalFunction = new DtmArrivalFunction();
        for (int i = 0; i < maximumFunction.getLength(); i++) {
            arrivalFunction.addPoint(maximumFunction.getX(i), maximumFunction.getY(i) + maximumFunction.getX(i));
        }

        return travelTimeFunction;
    }

    /**
     * Returns the shallow copy of this edge
     * @return the shallow copy of this edge
     */
    public DtmEdge shallowCopy(){
        DtmEdge e = new DtmEdge(id, length, startNode, endNode, fundamentalDiagram);
        e.inflow = inflow;
        e.outflow = outflow;

        e.cumulativeInflow = cumulativeInflow;
        e.cumulativeOutflow = cumulativeOutflow;
        e.arrivalFunction = arrivalFunction;

        return e;
    }

    //Getters and setters

    /**
     * @return the startNode
     */
    public DtmNode getStartNode() {
        return startNode;
    }

    /**
     * @param startNode the startNode to set
     */
    public void setStartNode(DtmNode startNode) {
        this.startNode = startNode;
    }

    /**
     * @return the endNode
     */
    public DtmNode getEndNode() {
        return endNode;
    }

    /**
     * @param endNode the endNode to set
     */
    public void setEndNode(DtmNode endNode) {
        this.endNode = endNode;
    }

    /**
     * @return the length
     */
    public double getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * @return the fundamentalDiagram
     */
    public DtmFundamentalDiagram getFundamentalDiagram() {
        return fundamentalDiagram;
    }



    /**
     * @param fundamentalDiagram the fundamentalDiagram to set
     */
    public void setFundamentalDiagram(DtmFundamentalDiagram fundamentalDiagram) {
        this.fundamentalDiagram = fundamentalDiagram;
    }



    /**
     * @return the inflow
     */
    public DtmPiecewiseConstantFunction getInflow() {
        return inflow;
    }



    /**
     * @param inflow the inflow to set
     */
    public void setInflow(DtmPiecewiseConstantFunction inflow) {
        this.inflow = inflow;
    }



    /**
     * @return the outflow
     */
    public DtmPiecewiseConstantFunction getOutflow() {
        return outflow;
    }



    /**
     * @param outflow the outflow to set
     */
    public void setOutflow(DtmPiecewiseConstantFunction outflow) {
        this.outflow = outflow;
    }



    /**
     * @return the cumulativeInflow
     */
    public DtmNonDecreasingPiecewiseLinearFunction getCumulativeInflow() {
        return cumulativeInflow;
    }



    /**
     * @param cumulativeInflow the cumulativeInflow to set
     */
    public void setCumulativeInflow(DtmNonDecreasingPiecewiseLinearFunction cumulativeInflow) {
        this.cumulativeInflow = cumulativeInflow;
    }



    /**
     * @return the cumulativeOutflow
     */
    public DtmNonDecreasingPiecewiseLinearFunction getCumulativeOutflow() {
        return cumulativeOutflow;
    }



    /**
     * @param cumulativeOutflow the cumulativeOutflow to set
     */
    public void setCumulativeOutflow(DtmNonDecreasingPiecewiseLinearFunction cumulativeOutflow) {
        this.cumulativeOutflow = cumulativeOutflow;
    }

    /**
     * @return the queuePresent
     */
    public boolean isQueuePresent() {
        return queuePresent;
    }

    /**
     * @param queuePresent the queuePresent to set
     */
    public void setQueuePresent(boolean queuePresent) {
        this.queuePresent = queuePresent;
    }

    /**
     * @return the demandLastUpdateTime
     */
    public double getDemandLastUpdateTime() {
        return demandLastUpdateTime;
    }



    /**
     * @param demandLastUpdateTime the demandLastUpdateTime to set
     */
    public void setDemandLastUpdateTime(double demandLastUpdateTime) {
        this.demandLastUpdateTime = demandLastUpdateTime;
    }



    /**
     * @return the currentForwaredEvent
     */
    public DtmForwardEdgeEvent getCurrentForwaredEvent() {
        return currentForwardEvent;
    }



    /**
     * @param currentForwaredEvent the currentForwaredEvent to set
     */
    public void setCurrentForwaredEvent(DtmForwardEdgeEvent currentForwaredEvent) {
        this.currentForwardEvent = currentForwaredEvent;
    }



    /**
     * @return the supply
     */
    public double getSupply() {
        return supply;
    }



    /**
     * @param supply the supply to set
     */
    public void setSupply(double supply) {
        this.supply = supply;
    }



    /**
     * @return the inflowMixture
     */
    public DtmMixture<DtmIdentifiable> getInflowMixture() {
        return inflowMixture;
    }



    /**
     * @param inflowMixture the inflowMixture to set
     */
    public void setInflowMixture(DtmMixture<DtmIdentifiable> inflowMixture) {
        this.inflowMixture = inflowMixture;
    }



    /**
     * @return the inflowMixtureFunction
     */
    public DtmGeneralFunction<DtmMixture<DtmIdentifiable>> getInflowMixtureFunction() {
        return inflowMixtureFunction;
    }



    /**
     * @param inflowMixtureFunction the inflowMixtureFunction to set
     */
    public void setInflowMixtureFunction(DtmGeneralFunction<DtmMixture<DtmIdentifiable>> inflowMixtureFunction) {
        this.inflowMixtureFunction = inflowMixtureFunction;
    }

    /**
     * @return the arrivalFunction
     */
    public DtmArrivalFunction getArrivalFunction() {
        return arrivalFunction;
    }

    /**
     * @param arrivalFunction the arrivalFunction to set
     */
    public void setArrivalFunction(DtmArrivalFunction arrivalFunction) {
        this.arrivalFunction = arrivalFunction;
    }

    /**
     * @return the forwardEventsStorage
     */
    public DtmEdgeEventsStorage<DtmForwardEdgeEvent> getForwardEventsStorage() {
        return forwardEventsStorage;
    }

    /**
     * @param forwardEventsStorage the forwardEventsStorage to set
     */
    public void setForwardEventsStorage(DtmEdgeEventsStorage<DtmForwardEdgeEvent> forwardEventsStorage) {
        this.forwardEventsStorage = forwardEventsStorage;
    }

    /**
     * @return the backwardEventsStorage
     */
    public DtmEdgeEventsStorage<DtmBackwardEdgeEvent> getBackwardEventsStorage() {
        return backwardEventsStorage;
    }

    /**
     * @param backwardEventsStorage the backwardEventsStorage to set
     */
    public void setBackwardEventsStorage(DtmEdgeEventsStorage<DtmBackwardEdgeEvent> backwardEventsStorage) {
        this.backwardEventsStorage = backwardEventsStorage;
    }

    /**
     * @return the mixtureEventsStorage
     */
    public DtmEdgeEventsStorage<DtmMixtureEdgeEvent> getMixtureEventsStorage() {
        return mixtureEventsStorage;
    }

    /**
     * @param mixtureEventsStorage the mixtureEventsStorage to set
     */
    public void setMixtureEventsStorage(DtmEdgeEventsStorage<DtmMixtureEdgeEvent> mixtureEventsStorage) {
        this.mixtureEventsStorage = mixtureEventsStorage;
    }

    /**
     * Returns the ID of the subgraph, to which this edge belongs
     * @return the ID of the subgraph, to which this edge belongs
     */
    public int getSubgraphId() {
        return subgraphId;
    }

    /**
     * Sets the ID of the subgraph, to which this edge belongs
     * @param subgraphId the new ID of the subgraph, to which this edge shall belong
     */
    public void setSubgraphId(int subgraphId) {
        this.subgraphId = subgraphId;
    }

    /**
     * Returns whether this edge is on the border of subgraphs or not
     * @return whether this edge is on the border of subgraphs or not
     */
    public boolean isOnBorder() {
        return onBorder;
    }

    /**
     * Sets whether this edge is on the border of subgraphs or not
     * @param onBorder the new value of whether this edge is on the border of subgraphs or not
     */
    public void setOnBorder(boolean onBorder) {
        this.onBorder = onBorder;
    }

    /**
     * Returns the ID of the subgraph, to which this edge continues (for the border edges only)
     * @return the ID of the subgraph, to which this edge continues (for the border edges only)
     */
    public int getNeighboringSubgraphId() {
        return neighboringSubgraphId;
    }

    /**
     * Sets the ID of the subgraph, to which this edge continues (for the border edges only)
     * @param neighboringSubgraphId the new ID of the subgraph, to which this edge continue
     */
    public void setNeighboringSubgraphId(int neighboringSubgraphId) {
        this.neighboringSubgraphId = neighboringSubgraphId;
    }

    //Inherited methods

    @Override
    public boolean equals(Object o) {
        if (o instanceof DtmEdge) {
            DtmEdge other = (DtmEdge) o;
            return id == other.id;
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Edge ID: " + id;
    }
}