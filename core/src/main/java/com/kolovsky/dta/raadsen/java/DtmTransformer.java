/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * A transformer enabling calculation with general functions
 * @param <T> The type of the first general function
 * @author Tomas Potuzak
 */
public interface DtmTransformer<T> {

    /**
     * Transforms the <i>y</i> value of the general function into a double value
     * @param y <i>y</i> the value of the general function, which shall be transformed into a double value
     * @return the double value of the <i>y</i> value of the general function
     */
    public double getValue(T y);

    /**
     * Performs the specified arithmetic operation with the values of two general functions
     * @param <U> the type of the second general function
     * @param <V> the type of the resulting general function
     * @param y1 the value of the first general function
     * @param y2 the value of the second general function
     * @param operation the operator of the arithmetic operation
     * @return the result of the arithmetic operation
     * @throws IllegalArgumentException if the specified operation is not supported or
     * @throws NullPointerException if the specified operation is <code>null</code>
     */
    public <U, V> V performOperation(T y1, U y2, String operation) throws IllegalArgumentException, NullPointerException;

}
