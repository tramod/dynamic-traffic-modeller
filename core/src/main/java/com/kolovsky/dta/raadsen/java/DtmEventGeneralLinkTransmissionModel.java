/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Event-based dynamic network loading for dynamic traffic assignment - implementation of RAADSEN, Mark PH; BLIEMER, Michiel CJ. Continuous-time general link transmission model with simplified fanning, Part II: Event-based algorithm for networks. Transportation Research Part B: Methodological, 2019, 126: 471-501.
 * @author Tomas Potuzak
 */
public class DtmEventGeneralLinkTransmissionModel implements DtmDynamicNetworkLoading {
    /**  */
    public static final double DEFAULT_CUMULATIVE_EPSILON = 1E-10;
    /**  */
    public static final double DEFAULT_RELEASE_EVENT_EPSILON = 1E-10;
    /**  */
    public static final double DEFAULT_FLOW_THRESHOLD = 0.0;
    /**  */
    public static final double DEFAULT_MIXTURE_FLOW_THRESHOLD = 0.0;
    /**  */
    public static final double DEFAULT_FAN_STEP = 10.0;
    /**  */
    protected double cumulativeEpsilon;
    /**  */
    protected double releaseEventEpsilon;
    /**  */
    protected double flowThreshold;
    /**  */
    protected double mixtureFlowThreshold;
    /**  */
    protected double fanStep;

    /** The graph representing the road traffic network, in which the dynamic network loading is performed */
    protected DtmGraph graph;
    /** The origin zones, in which all the flows are starting */
    protected List<DtmZone> originZones;
    /** Determines whether  */
    protected boolean maintainMaximalTime;
    /** Priority queue of all events of the simulation */
    protected PriorityQueue<DtmPriorityQueueElement<Double, DtmEvent>> eventsPriorityQueue;
    /** The current time of the simulation */
    protected double currentTime;
    /** The number of events performed in the simulation */
    protected int eventsCount;
    /** Statistics of the events of the simulation */
    protected DtmDynamicNetworkLoadingStatistics statistics;

    /**
     * Creates new event-based dynamic network loading
     * @param graph the graph representing the road traffic network, in which the dynamic network loading is performed
     * @param originZones the origin zones, in which all the flows are starting
     * @param maintainMaximalTime
     */
    public DtmEventGeneralLinkTransmissionModel(DtmGraph graph, List<DtmZone> originZones, boolean maintainMaximalTime) {
        this.graph = graph;
        this.originZones = originZones;
        this.maintainMaximalTime = maintainMaximalTime;

        eventsPriorityQueue = new PriorityQueue<>(DtmGraph.DEFAULT_QUEUE_CAPACITY, new Comparator<DtmPriorityQueueElement<Double, DtmEvent>>() {

            @Override
            public int compare(DtmPriorityQueueElement<Double, DtmEvent> element1, DtmPriorityQueueElement<Double,DtmEvent> element2) {
                return element1.compareTo(element2); //Natural ordering used for priority - the least element is at the start of the priority queue (unlike Scala, where the highest element is at the start)
            }
            //TODO can be removed, natural ordering will do the same job (it is used anyway in the compare() method)
        });

        currentTime = 0.0;
        eventsCount = 0;
        statistics = new DtmDynamicNetworkLoadingStatistics();

        cumulativeEpsilon = DEFAULT_CUMULATIVE_EPSILON;
        releaseEventEpsilon = DEFAULT_RELEASE_EVENT_EPSILON;
        flowThreshold = DEFAULT_FLOW_THRESHOLD;
        mixtureFlowThreshold = DEFAULT_MIXTURE_FLOW_THRESHOLD;
        fanStep = DEFAULT_FAN_STEP;
    }

    @Override
    public List<DtmArrivalFunction> run(double maximalTime, double epsilon) {
        eventsPriorityQueue.clear();
        currentTime = 0;
        statistics.reset();

        for (DtmZone originZone: originZones) {
            for (DtmOriginEvent originEvent: originZone.getOrigin().getAllEvents()) {
                eventsPriorityQueue.add(new DtmPriorityQueueElement<>(originEvent.getPredictedTime(), originEvent));
            }
        }

        for (DtmNode node: graph.getNodes()) {
            for (DtmNodeEvent nodeEvent: node.getNodeModel().getAllEvents()) {
                eventsPriorityQueue.add(new DtmPriorityQueueElement<>(nodeEvent.getPredictedTime(), nodeEvent));
            }
        }

        DtmPriorityQueueElement<Double, DtmEvent> currentEvent = null;
        DtmTripleElement<List<Double>, List<Double>, List<DtmMixture<DtmIdentifiable>>> nodeModelUpdate = null;
        DtmNode node = null;

        while (currentTime < maximalTime && !eventsPriorityQueue.isEmpty()) {
            currentEvent = eventsPriorityQueue.poll();

            if (currentEvent.getPriority() < currentTime) {
                throw new IllegalStateException("The time of the event must be higher or equal to the current time");
            }

            currentTime = currentEvent.getPriority();
            node = currentEvent.getValue().perform(currentTime);

            if (node != null) { //The event is valid
                statistics.registerEvent(currentEvent.getValue());

                for (DtmEdge edge: node.getIncomingEdges()) { //Update demand and supply if there is a queue
                    edge.updateDemand(currentTime, cumulativeEpsilon);
                }

                nodeModelUpdate = node.getNodeModel().solve(currentTime);

                DtmLogger.info(currentTime, "[" + currentTime + "] " + node + ", incoming edges outflows: " + nodeModelUpdate.getFirst() + ", outgoing edges inflows: " + nodeModelUpdate.getSecond(), node);

                updateIncomingEdges(node.getIncomingEdges(), nodeModelUpdate.getFirst(), epsilon);
                updateOutgoingEdges(node.getOutgoingEdges(), nodeModelUpdate.getSecond(), epsilon);
                updateMixtures(node.getOutgoingEdges(), nodeModelUpdate.getThird(), epsilon);
            }
        }

        List<DtmArrivalFunction> arrivalFunctions = new ArrayList<>();
        for (DtmEdge edge: graph.getEdges()) {
            edge.calculateTravelTimeFunction(maximalTime, maintainMaximalTime);
            arrivalFunctions.add(edge.getArrivalFunction());
        }

        return arrivalFunctions;
    }

    /**
     * Updates the ouflows from incoming edges of a node and releases new events
     * @param incomingEdges the incoming edges of a node, which shall be updated
     * @param outflows the new outflows of the incoming edges
     * @param epsilon the comparison tolerance
     */
    protected void updateIncomingEdges(List<DtmEdge> incomingEdges, List<Double> outflows, double epsilon) {
        DtmEdge edge = null;
        double outflow = 0.0;
        List<DtmBackwardEdgeEvent> backwardEvents = null;
        DtmPairElement<Double, Double> cumulativeInflowDownStreamProjection = null;
        double demand = 0.0;
        double predictedTime = 0.0;

        for (int i = 0; i < incomingEdges.size(); i++) {
            edge = incomingEdges.get(i);
            outflow = outflows.get(i);

            if (Math.abs(edge.getCurrentOutflow() - outflow) > flowThreshold * 0.5) { //TODO here can be approximation to discard very little changes
                //Release backward events
                if (outflow > edge.getFundamentalDiagram().getFlowCapacity() && outflow - edge.getFundamentalDiagram().getFlowCapacity() < epsilon) {
                    outflow = edge.getFundamentalDiagram().getFlowCapacity();
                }

                backwardEvents = edge.updateOutflow(currentTime, outflow, fanStep, epsilon, cumulativeEpsilon);
                for (DtmBackwardEdgeEvent backwardEvent: backwardEvents) {
                    releaseEdgeEvent(backwardEvent, releaseEventEpsilon);
                }

                //Release node event if there is a queue
                if (edge.isQueuePresent()) {
                    cumulativeInflowDownStreamProjection = edge.getCumulativeInflowDownstreamProjection(currentTime, null, epsilon);

                    if (!outflows.get(i).equals(cumulativeInflowDownStreamProjection.getSecond())) {
                        // demand - Number of vehicles unsuccessful in leaving the edge at time
                        demand = cumulativeInflowDownStreamProjection.getFirst() - edge.getCumulativeOutflow(currentTime);
                        predictedTime = currentTime + demand / (outflows.get(i) - cumulativeInflowDownStreamProjection.getSecond());

                        if (predictedTime > currentTime && demand > flowThreshold) {
                            eventsPriorityQueue.offer(new DtmPriorityQueueElement<>(predictedTime, new DtmNodeEvent(edge.getEndNode(), predictedTime)));
                        }
                    }
                }

                //Update forward events
                for (DtmForwardEdgeEvent forwardEvent: edge.getForwardEventsStorage()) {
                    updateForwardBackwardEdgeEvent(forwardEvent, releaseEventEpsilon);
                }

                //Remove old events
                edge.getForwardEventsStorage().remove(new DtmFilter<>() {

                    @Override
                    public boolean isAccepted(DtmForwardEdgeEvent event) {
                        if (event.getMaximalTime() < currentTime) {
                            if (!event.isProcessed()) {
                                DtmLogger.warning(currentTime, "WARNING! - removing unprocessed " + event, event.getEdge());
                            }
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                });
            }
        }
    }

    /**
     * Updates the inflows to outgoing edges of a node and releases new events
     * @param outgoingEdges the outgoing edges of a node, which shall be updated
     * @param inflows the new inflows to the outgoing edges
     * @param epsilon the comparison tolerance
     */
    protected void updateOutgoingEdges(List<DtmEdge> outgoingEdges, List<Double> inflows, double epsilon) {
        DtmEdge edge = null;
        double inflow = 0.0;
        List<DtmForwardEdgeEvent> events = null;

        for (int i = 0; i < outgoingEdges.size(); i++) {
            edge = outgoingEdges.get(i);
            inflow = inflows.get(i);

            if (Math.abs(edge.getCurrentInflow() - inflow) > flowThreshold || (inflow != edge.getCurrentInflow() && inflow < 1E-12)) { //TODO nahradit nejakou vhodnou konstantou
                //Release forward events
                events = edge.updateInflow(currentTime, inflow, fanStep, releaseEventEpsilon, cumulativeEpsilon);
                for (DtmForwardEdgeEvent event: events) {
                    releaseEdgeEvent(event, releaseEventEpsilon);
                }

                //Update backward events
                for (DtmBackwardEdgeEvent backwardEvent: edge.getBackwardEventsStorage()) {
                    updateForwardBackwardEdgeEvent(backwardEvent, releaseEventEpsilon);
                }

                //Remove old events
                edge.getBackwardEventsStorage().remove(new DtmFilter<>() {

                    @Override
                    public boolean isAccepted(DtmBackwardEdgeEvent event) {
                        if (event.getMaximalTime() < currentTime) {
                            if (!event.isProcessed()) {
                                DtmLogger.warning(currentTime, "WARNING! - removing unprocessed " + event, event.getEdge());
                            }
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                });
            }
        }
    }

    /**
     * Updates the mixtures of the outgoing edges of a node and releases new event
     * @param outgoingEdges the outgoing edges of a node, which shall be updated
     * @param mixtures the new mixtures of the outgoing edges
     * @param epsilon the comparison tolerance
     */
    protected void updateMixtures(List<DtmEdge> outgoingEdges, List<DtmMixture<DtmIdentifiable>> mixtures, double epsilon) {
        double mixtureDifference = 0.0;
        DtmEdge edge = null;
        DtmMixture<DtmIdentifiable> mixture = null;
        DtmMixtureEdgeEvent event = null;

        for (int i = 0; i < outgoingEdges.size(); i++) {
            edge = outgoingEdges.get(i);
            mixture = mixtures.get(i);

            if (mixtures.get(i) != null) {
                //Approximation to discard very little changes
                mixtureDifference = Double.POSITIVE_INFINITY;
                if (edge.getInflowMixture() != null) {
                    mixtureDifference = edge.getInflowMixture().findMaximalDifference(mixture);
                }
                if (mixtureDifference > mixtureFlowThreshold) {
                    event = edge.updateMixture(currentTime, mixture);

                    if (edge.getOutflow().getLastX() <= event.getPredictedTime() && currentTime < event.getPredictedTime()) {
                        eventsPriorityQueue.offer(new DtmPriorityQueueElement<>(event.getPredictedTime(), event));
                    }

                    //Update mixture events
                    for (DtmMixtureEdgeEvent mixtureEvent: edge.getMixtureEventsStorage()) {
                        updateMixtureEdgeEvent(mixtureEvent, releaseEventEpsilon);
                    }
                }

            }
        }
    }

    /**
     * Releases the specified edge event (i.e., adds it to the queue)
     * @param edgeEvent the edge event, which shall be released
     * @param epsilon the comparison tolerance
     */
    protected void releaseEdgeEvent(DtmEdgeEvent edgeEvent, double epsilon) {
        if (edgeEvent instanceof DtmForwardBackwardEdgeEvent) {
            DtmForwardBackwardEdgeEvent event = (DtmForwardBackwardEdgeEvent) edgeEvent;
            if (event.getMinimalTime() <= event.getPredictedTime() && event.getPredictedTime() <= event.getMaximalTime() && currentTime < event.getPredictedTime()) {
                eventsPriorityQueue.offer(new DtmPriorityQueueElement<>(event.getPredictedTime(), event));
                DtmLogger.info(currentTime, "Valid: " + event, event.getEdge());
            }
            else if (event.getMinimalTime() <= event.getPredictedTime() + epsilon && event.getMaximalTime() >= event.getPredictedTime() && currentTime < event.getPredictedTime()) {
                event.setMinimalTime(event.getPredictedTime()); //TODO Potentially problematic
                eventsPriorityQueue.offer(new DtmPriorityQueueElement<>(event.getPredictedTime(), event));
                DtmLogger.info(currentTime, "Valid: " + event, event.getEdge());
            }
            else {
                DtmLogger.info(currentTime, "Invalid during release: " + event, event.getEdge());
            }
        }
        else if (edgeEvent instanceof DtmMixtureEdgeEvent) {
            DtmMixtureEdgeEvent event = (DtmMixtureEdgeEvent) edgeEvent;
            event.getEdge().setInflowMixture(event.getMixture());

            if (event.getEdge().getOutflowMixture() == null) {
                event.getEdge().setOutflowMixture(event.getMixture());
            }
            if (event.getEdge().getOutflow().getLastX() <= event.getPredictedTime() && currentTime < event.getPredictedTime()) { //TODO Potentially problematic
                eventsPriorityQueue.offer(new DtmPriorityQueueElement<>(event.getPredictedTime(), event));
                DtmLogger.info(currentTime, "Valid " + event, event.getEdge());
            }
        }
    }

    /**
     * Updates the specified forward or backward edge event
     * @param event the event, which shall be updated
     * @param epsilon the comparison tolerance
     */
    protected void updateForwardBackwardEdgeEvent(DtmForwardBackwardEdgeEvent event, double epsilon) {
        if (!event.isProcessed()) {
            double predictedTime = event.calculatePredictedTime();

            if (predictedTime != -1.0) {
                event.setPredictedTime(predictedTime);
            }

            if (currentTime < event.getPredictedTime() && event.getMinimalTime() <= predictedTime && predictedTime <= event.getMaximalTime()) {
                eventsPriorityQueue.offer(new DtmPriorityQueueElement<>(predictedTime, event));
            }
            else if (currentTime < event.getPredictedTime() && event.getMinimalTime() - epsilon <= predictedTime && predictedTime <= event.getMaximalTime()) {
                event.setMinimalTime(predictedTime); //TODO Potentially problematic
                eventsPriorityQueue.offer(new DtmPriorityQueueElement<>(predictedTime, event));
            }
            else {
                //Incorrect event - no action
            }
        }
    }

    /**
     * Updates the specified mixture edge event
     * @param event the event, which shall be updated
     * @param epsilon the comparison tolerance
     */
    protected void updateMixtureEdgeEvent(DtmMixtureEdgeEvent event, double epsilon) {
        double predictedTime = event.calculatePredictedTime();

        if (currentTime < predictedTime && predictedTime >= event.getEdge().getInflow().getLastX() && predictedTime != event.getPredictedTime()) {
            event.setPredictedTime(predictedTime);
            eventsPriorityQueue.offer(new DtmPriorityQueueElement<>(predictedTime, event));
        }
    }

    //Getters and setters

    /**
     *
     * @return the cumulativeEpsilon
     */
    public double getCumulativeEpsilon() {
        return cumulativeEpsilon;
    }

    /**
     *
     * @param cumulativeEpsilon the cumulativeEpsilon to set
     */
    public void setCumulativeEpsilon(double cumulativeEpsilon) {
        this.cumulativeEpsilon = cumulativeEpsilon;
    }

    /**
     *
     * @return the releaseEventEpsilon
     */
    public double getReleaseEventEpsilon() {
        return releaseEventEpsilon;
    }

    /**
     *
     * @param releaseEventEpsilon the releaseEventEpsilon to set
     */
    public void setReleaseEventEpsilon(double releaseEventEpsilon) {
        this.releaseEventEpsilon = releaseEventEpsilon;
    }

    /**
     *
     * @return the flowThreshold
     */
    public double getFlowThreshold() {
        return flowThreshold;
    }

    /**
     *
     * @param flowThreshold the flowThreshold to set
     */
    public void setFlowThreshold(double flowThreshold) {
        this.flowThreshold = flowThreshold;
    }

    /**
     *
     * @return the mixtureFlowThreshold
     */
    public double getMixtureFlowThreshold() {
        return mixtureFlowThreshold;
    }

    /**
     *
     * @param mixtureFlowThreshold the mixtureFlowThreshold to set
     */
    public void setMixtureFlowThreshold(double mixtureFlowThreshold) {
        this.mixtureFlowThreshold = mixtureFlowThreshold;
    }

    /**
     *
     * @return the fanStep
     */
    public double getFanStep() {
        return fanStep;
    }

    /**
     *
     * @param fanStep the fanStep to set
     */
    public void setFanStep(double fanStep) {
        this.fanStep = fanStep;
    }
}