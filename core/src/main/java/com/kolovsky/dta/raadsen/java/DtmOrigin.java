/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.List;

/**
 * An origin for the dynamic traffic assignment
 * @author Tomas Potuzak
 */
public class DtmOrigin extends DtmSource {
    /** The flow outgoing from this origin */
    protected DtmPiecewiseConstantFunction outflow;
    /** The mixtures of the outflow of this origin  */
    protected List<DtmMixture<DtmIdentifiable>> outflowMixtures;
    /** The node, to which this origin is connected */
    protected DtmNode node;

    /**
     * Creates a new origin with specified outflow, outflow mixtures and the node
     * @param outflow the flow outgoing from this origin
     * @param outflowMixtures the mixtures of the outflow of this origin
     * @param node the node, to which this origin is connected
     */
    public DtmOrigin(DtmPiecewiseConstantFunction outflow, List<DtmMixture<DtmIdentifiable>> outflowMixtures, DtmNode node) {
        this.outflow = outflow;
        this.outflowMixtures = outflowMixtures;
        this.node = node;

        demand = outflow.getFirstY();
        outflowMixture = outflowMixtures.get(0);
        node.setOrigin(this);
    }

    /**
     * Reset this origin to its initial state
     */
    public void reset(){
        demand = outflow.getFirstY();
        outflowMixture = outflowMixtures.get(0);
    }

    /**
     * Returns all the node events that occurred during the entire computation
     * @return all the node events that occurred during the entire computation
     */
    public List<DtmOriginEvent> getAllEvents() {
        List<DtmOriginEvent> allEvents = new ArrayList<>();

        for (int i = 0; i < outflow.getLength(); i++) {
            allEvents.add(new DtmOriginEvent(node, outflow.getX(i), outflow.getY(i), outflowMixtures.get(i)));
        }

        return allEvents;
    }

    /**
     * Returns the next scheduled origin event (<code>null</code>) and its duration for the specified time
     * @param time the time, for which the next scheduled node event shall be determined
     * @return the next scheduled origin event (<code>null</code>) and its duration
     * @throws IllegalStateException if the resulting duration is negative
     */
    public DtmPairElement<DtmOriginEvent, Double> nextScheduledEvent(double time) throws IllegalStateException {
        if (time >= outflow.getLastX()) {
            return new DtmPairElement<>(null, 1000.0);
        }
        else {
            int index = DtmFunctionUtils.binarySearch(outflow.getXValues(), time);
            double duration = outflow.getX(index) - time;

            if (duration < 0) {
                throw new IllegalStateException("Duration of the origin event cannot be negative.");
            }

            return new DtmPairElement<>(null, duration);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof DtmOrigin) {
            DtmOrigin other = (DtmOrigin) o;
            return id == other.id;
        }
        else {
            return false;
        }
    }
}
