/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * Statistics of the dynamic network loading for the dynamic traffic assignment
 * @author Tomas Potuzak
 */
public class DtmDynamicNetworkLoadingStatistics {
    /** The number of registered forward edge events */
    protected int forwardEdgeEventsCount;
    /** The number of registered backward edge events */
    protected int backwardEdgeEventsCount;
    /** The number of registered mixture edge events */
    protected int mixtureEdgeEventsCount;
    /** The number of registered node events */
    protected int nodeEventsCount;
    /** The number of registered origin events */
    protected int originEventsCount;

    /**
     * Create new statistics of the dynamic network loading for the dynamic traffic assignment with all counters set to zero
     */
    public DtmDynamicNetworkLoadingStatistics() {
        reset();
    }

    /**
     * Registers the specified event (i.e., the counter corresponding to the type of the event is incremented)
     * @param event the event, which shall be registered
     */
    public synchronized void registerEvent(DtmEvent event) {
        if (event instanceof DtmForwardEdgeEvent) {
            forwardEdgeEventsCount++;
        }
        else if (event instanceof DtmBackwardEdgeEvent) {
            backwardEdgeEventsCount++;
        }
        else if (event instanceof DtmMixtureEdgeEvent) {
            mixtureEdgeEventsCount++;
        }
        else if (event instanceof DtmNodeEvent) {
            nodeEventsCount++;
        }
        else if (event instanceof DtmOriginEvent) {
            originEventsCount++;
        }
    }

    /**
     * Sets all counters to zero
     */
    public synchronized void reset() {
        forwardEdgeEventsCount = 0;
        backwardEdgeEventsCount = 0;
        mixtureEdgeEventsCount = 0;
        nodeEventsCount = 0;
        originEventsCount = 0;
    }

    //Getters and setters

    /**
     * Returns the total number of registered messages (regardless of their types)
     * @return the total number of registered messages (regardless of their types)
     */
    public int getEventsCount() {
        return forwardEdgeEventsCount + backwardEdgeEventsCount + mixtureEdgeEventsCount + nodeEventsCount + originEventsCount;
    }

    /**
     * Returns the number of registered forward edge events
     * @return the number of registered forward edge events
     */
    public int getForwardEdgeEventsCount() {
        return forwardEdgeEventsCount;
    }

    /**
     * Returns the number of registered backward edge events
     * @return the number of registered backward edge events
     */
    public int getBackwardEdgeEventsCount() {
        return backwardEdgeEventsCount;
    }

    /**
     * Returns the number of registered mixture edge events
     * @return the number of registered mixture edge events
     */
    public int getMixtureEdgeEventsCount() {
        return mixtureEdgeEventsCount;
    }

    /**
     * Returns the number of registered node events
     * @return the number of registered node events
     */
    public int getNodeEventsCount() {
        return nodeEventsCount;
    }

    /**
     * Returns the number of origin node events
     * @return the number of origin node events
     */
    public int getOriginEventsCount() {
        return originEventsCount;
    }

    //Inherited methods

    @Override
    public String toString() {
        return "Number of events: " + getEventsCount() + "\n  forward edge: " + forwardEdgeEventsCount + "\n  backward edge: " + backwardEdgeEventsCount + "\n  mixture edge: " + mixtureEdgeEventsCount + "\n node: " + nodeEventsCount + "\n origin: " + originEventsCount + "\n";
    }
}
