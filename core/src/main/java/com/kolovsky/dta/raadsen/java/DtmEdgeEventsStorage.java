/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * A container for edge events implemented by a two-directional linked list
 * @param <T> the type of the stored events
 * @author Tomas Potuzak
 */
public class DtmEdgeEventsStorage<T extends DtmEdgeEvent> implements Iterable<T> {
    /** First edge event in this storage */
    protected T first;
    /** Last edge event in this storage */
    protected T last;

    /**
     * Creates a new storage for edge events
     */
    public DtmEdgeEventsStorage() {
        first = null;
        last = null;
    }

    /**
     * Adds an event to this storage
     * @param event the event to be added to this storage
     */
    public void add(T event) {
        if (first == null) {
            first = event;
            last = event;
        }
        else {
            event.setPrevious(last);
            last.setNext(event);
            last = event;  //TODO POZOR! - je to jinak nez v puvodni implementaci, kde se pridava za first (head)
        }
    }

    /**
     * Removes the event, for which the <code>filter</code> method returns <code>true</code>, from this storage
     * @param filter determines (using its <code>isAccepted</code> method), whether the events contained in this storage shall be removed
     */
    public void remove(DtmFilter<T> filter) {
        Iterator<T> iterator = iterator();
        T event = null;

        while (iterator.hasNext()) {
            event = iterator.next();
            if (filter.isAccepted(event)) {
                iterator.remove();
            }
        }
    }

    /**
     * Finds and returns the next schedule event different from the specified trigger event
     * @param currentTime the current time
     * @param triggerEvent the trigger event, which must be different from the searched event
     * @return the next schedule event different from the specified trigger event
     * @throws NoSuchElementException if the searching was not successful
     */
    public DtmPairElement<T, Double> nextScheduledEvent(double currentTime, DtmEvent triggerEvent) throws NoSuchElementException {
        Iterator<T> iterator = iterator();
        T event = null;
        T foundEvent = null;
        double timeLength = Double.POSITIVE_INFINITY;

        while (iterator.hasNext()) {
            event = iterator.next();

            if (event.getPredictedTime() >= currentTime && triggerEvent != event) { //TODO verify that == instance comparison is ok
                if (event.getPredictedTime() - currentTime < timeLength) {
                    timeLength = event.getPredictedTime() - currentTime;
                    foundEvent = event;
                }
            }
        }

        if (timeLength < 0.0) {
            throw new NoSuchElementException("No suitable schedule event found."); //TODO determine whether returning null would not be better than exception
        }

        return new DtmPairElement<>(foundEvent, timeLength);
    }

    /**
     * Returns <code>true</code>, if this storage does not contain any edge event (i.e., is empty). Returns <code>false</code> otherwise.
     * @return <code>true</code>, if this storage does not contain any edge event (i.e., is empty). Returns <code>false</code> otherwise.
     */
    public boolean isEmpty() {
        return first == null;
    }

    /**
     * Returns the first edge event stored in this storage or <code>null</code> if the storage is empty
     * @return the first edge event stored in this storage or <code>null</code> if the storage is empty
     */
    public T getFirst() {
        return first;
    }

    /**
     * Returns the last edge event stored in this storage or <code>null</code> if the storage is empty
     * @return the last edge event stored in this storage or <code>null</code> if the storage is empty
     */
    public T getLast() {
        return last;
    }

    /**
     * Deletes all edge events from this storage. The storage will remain empty.
     */
    public void clear() {
        first = null;
        last = null;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private T currentEdgeEvent = null;
            private boolean nextCalled = false;

            @Override
            public boolean hasNext() {
                if (first == null || currentEdgeEvent == last) {
                    return false;
                }
                else {
                    return true;
                }
            }

            @Override
            @SuppressWarnings("unchecked")
            public T next() {
                if (currentEdgeEvent == null) {
                    currentEdgeEvent = first;
                }
                else {
                    currentEdgeEvent = (T) currentEdgeEvent.getNext();
                }

                nextCalled = true;
                return currentEdgeEvent;
            }

            @Override
            @SuppressWarnings("unchecked")
            public void remove() {
                if (!nextCalled) {
                    throw new IllegalStateException("Cannot perform remove() before next() or after remove().");
                }
                else {
                    if (currentEdgeEvent == last) { //The element is the last one
                        if (currentEdgeEvent == first) { //Only one element in the list
                            first = last = null;
                        }
                        else { //Removing last element
                            last = (T) last.getPrevious();
                            last.setNext(null);
                            currentEdgeEvent = last; //To ensure the iterator is at its end
                        }
                    }
                    else { //The element is not last one
                        if (currentEdgeEvent == first) { //The element is the first one
                            first = (T) first.getNext();
                            first.setPrevious(null);
                            //Reference to removed element still in currentEdgeEvent - will be changed by the next next() invocation
                        }
                        else { //The emement is the inner one
                            currentEdgeEvent.getPrevious().setNext(currentEdgeEvent.getNext());
                            currentEdgeEvent.getNext().setPrevious(currentEdgeEvent.getPrevious());
                            //Reference to removed element still in currentEdgeEvent - will be changed by the next next() invocation
                        }
                    }

                    nextCalled = false;
                }
            }
        };
    }
}