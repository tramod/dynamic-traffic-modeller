/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * Proportion shift by Min-Max strategy.
 * Proportion is shifted from the most expensive alternative to the cheapest alternative.
 * @author kolovsky
 */
public class DtmMinMaxGradientProjection implements DtmGradientProjection {
    /** gradient step */
    protected double alpha;

    /**
     * @param alpha gradient step
     */
    public DtmMinMaxGradientProjection(double alpha){
        this.alpha = alpha;
    }

    @Override
    public double[] run(double[] proportions, double[] cost) {
        if (proportions.length != cost.length){
            throw new IllegalArgumentException("All arrays must have the same length");
        }
        if (proportions.length == 1){
            return proportions.clone();
        }

        // new proportion
        double[] outP = new double[cost.length];

        // find fastest alternative and costest alternative
        int bestIndex = -1;
        int worstIndex = -1;
        double bestCost = Double.POSITIVE_INFINITY;
        double worstCost = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < cost.length; i++){
            if (cost[i] < bestCost){
                bestCost = cost[i];
                bestIndex = i;
            }
            if (cost[i] > worstCost){
                worstCost = cost[i];
                worstIndex = i;
            }
        }

        if (worstIndex == bestIndex){
            return proportions.clone();
        }

        // shift flow
        double shift = (bestCost - worstCost)/bestCost * alpha;
        double totalProp = 0.0;

        for (int i = 0; i < cost.length; i++){
            if (i != worstIndex && i != bestIndex){
                outP[i] = proportions[i];
                totalProp += outP[i];
            }
        }

        // worst alternative
        outP[worstIndex] = Math.max(0.0, proportions[worstIndex] + shift);
        totalProp += outP[worstIndex];

        // best alternative
        outP[bestIndex] = 1.0 - totalProp;

        return outP;
    }
}
