/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Splitting of the flow to portions
 * Path-based: portions correspond to paths
 * Destination-based portions correspond to destinations
 * @author Tomas Potuzak
 * @param <T> the type of key under which the portions are accessible (path or destination)
 */
public class DtmMixture<T extends DtmIdentifiable> {
    /** Precision if the sum of portions equal to 1.0 */
    public static final double SUM_PRECISION = 1E-10;
    /** The portions as the pairs of portion keys and portion values */
    private List<DtmPairElement<T, Double>> portions;
    /** Fast access to the portion values based on the specified portion key */
    private HashMap<T, Double> portionsAccess;

    /**
     * Creates a new mixture based on specified portions
     * @param portions the portions as the pairs of portion keys and portion values
     * @throws IllegalArgumentException if the sum of the portions in to equal to 1.0
     */
    public DtmMixture(List<DtmPairElement<T, Double>> portions) throws IllegalArgumentException {
        this.portions = portions;

        if (!isPortionsSumCorrect()) {
            throw new IllegalArgumentException("The sum of portions is not 1.0!");
        }

        portionsAccess = new HashMap<>();
        for (DtmPairElement<T, Double> portion: portions) {
            portionsAccess.put(portion.getFirst(), portion.getSecond());
        }
    }

    /**
     * Checks whether the sum of the portions is 1.0 with the precision of <code>SUM_PRECISION</code>
     * @return <code>true</code> of the sum of the portions is 1.0 or <code>false</code> otherwise
     */
    public boolean isPortionsSumCorrect() {
        double sum = 0.0;

        for (DtmPairElement<T, Double> portion: portions) {
            sum += portion.getSecond();
        }

        return portions.isEmpty() || (1 - Math.abs(sum)) <= SUM_PRECISION;
    }

    /**
     * Returns the portion value for the specified key if the specified key exists in this mixture, otherwise, it returns 0.0
     * @param portionKey the key under which the portion value is stored
     * @return the portion value for the specified key if the specified key exists in this mixture, otherwise, it returns 0.0
     */
    public double getPortion(T portionKey) {
        if (portionsAccess.containsKey(portionKey)) {
            return portionsAccess.get(portionKey);
        }
        else {
            return 0.0;
        }
    }

    /**
     * Returns set of objects that have non-zero flow share
     * @return set of objects that have non-zero flow share
     */
    public Set<T> getKeys(){
        return portionsAccess.keySet();
    }

    /**
     * Finds and returns the maximal difference between this and the other mixture
     * @param other other mixture to be compared with this one
     * @return the maximal difference between this and the other mixture
     */
    public double findMaximalDifference(DtmMixture<T> other) {
        if (other == null || other.portionsAccess.isEmpty() || portionsAccess.isEmpty()) {
            return Double.POSITIVE_INFINITY;
        }
        else {
            double maximalDifference = 0.0;
            double difference = 0.0;

            for (DtmPairElement<T, Double> portion: portions) {
                if (other.portionsAccess.containsKey(portion.getFirst())) {
                    difference = Math.abs(other.portionsAccess.get(portion.getFirst()) - portion.getSecond());
                }
                else {
                    difference = portion.getSecond();
                }

                if (difference > maximalDifference) {
                    maximalDifference = difference;
                }
            }

            for (DtmPairElement<T, Double> portion: other.portions) {
                if (portionsAccess.containsKey(portion.getFirst())) {
                    difference = Math.abs(portionsAccess.get(portion.getFirst()) - portion.getSecond());
                }
                else {
                    difference = portion.getSecond();
                }

                if (difference > maximalDifference) {
                    maximalDifference = difference;
                }
            }
            return maximalDifference;
        }
    }

    /**
     * Merges this and the other mixture and returns the resulting mixture. Both input mixtures remains unchanged. The keys in both mixtures must be disjunct.
     * @param other the mixture to be merged with this one
     * @param flow the flow corresponding to this mixture
     * @param otherFlow the flow corresponding to the other mixture
     * @return the resulting merged mixture
     */
    public DtmMixture<T> merge(DtmMixture<T> other, double flow, double otherFlow) {
        if (flow == 0.0 && otherFlow == 0.0) {
            return new DtmMixture<T>(new ArrayList<DtmPairElement<T, Double>>());
        }
        else {
            //TODO dodelat kontrolu, zda tam neni vice stejnych klicu
            double ratio = flow / (flow + otherFlow);
            double otherRatio = otherFlow / (flow + otherFlow);
            List<DtmPairElement<T, Double>> resultPortions = new ArrayList<>();

            for (DtmPairElement<T, Double> portion: portions) {
                resultPortions.add(new DtmPairElement<T, Double>(portion.getFirst(), portion.getSecond() * ratio));
            }
            for (DtmPairElement<T, Double> portion: other.portions) {
                resultPortions.add(new DtmPairElement<T, Double>(portion.getFirst(), portion.getSecond() * otherRatio));
            }

            return new DtmMixture<T>(resultPortions);
        }
    }
}
