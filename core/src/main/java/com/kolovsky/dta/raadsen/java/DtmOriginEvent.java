/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * An origin event for dynamic traffic assignment
 * @author Tomas Potuzak
 */
public class DtmOriginEvent extends DtmEvent {
    /** The node of this event */
    protected DtmNode node;
    /** The change time of this event */
    protected double changeTime;
    /** The flow outgoing from the origin */
    protected double outflow;
    /** The mixture of the outflow */
    protected DtmMixture<DtmIdentifiable> outflowMixture;

    /**
     * Creates a new origin event for specified node and with specified change time, outflow and outflow mixture
     * @param node the node of this event
     * @param changeTime the change time of this event
     * @param outflow the flow outgoing from the origin
     * @param outflowMixture the mixture of the outflow
     */
    public DtmOriginEvent(DtmNode node, double changeTime, double outflow, DtmMixture<DtmIdentifiable> outflowMixture) {
        this.node = node;
        this.changeTime = changeTime;
        this.outflow = outflow;
        this.outflowMixture = outflowMixture;

        predictedTime = changeTime;
    }

    @Override
    public DtmNode perform(double time) {
        node.getOrigin().setDemand(outflow);
        node.getOrigin().setOutflowMixture(outflowMixture);

        return node;
    }
}