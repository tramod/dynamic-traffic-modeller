/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Subgraph-parallel event-based dynamic network loading for dynamic traffic assignment - parallel implementation of RAADSEN, Mark PH; BLIEMER, Michiel CJ. Continuous-time general link transmission model with simplified fanning, Part II: Event-based algorithm for networks. Transportation Research Part B: Methodological, 2019, 126: 471-501.
 * The parallelization is bases on division of the graph into subgraphs, which are then processed by individual working threads. The edges on the border of the subgraphs are processed in a sequential manner during each synchronization. The synchronization is regular based on the minimal lookahead determined from the border edges. The assignment of the edges and nodes into the subgraphs must be performed prior the computation itself.
 * @author Tomas Potuzak
 */
public class DtmSubgraphParallelEventGeneralLinkTransmissionModel implements DtmDynamicNetworkLoading {
    /**  */
    public static final double DEFAULT_CUMULATIVE_EPSILON = 1E-10;
    /**  */
    public static final double DEFAULT_RELEASE_EVENT_EPSILON = 1E-10;
    /**  */
    public static final double DEFAULT_FLOW_THRESHOLD = 0.0;
    /**  */
    public static final double DEFAULT_MIXTURE_FLOW_THRESHOLD = 0.0;
    /**  */
    public static final double DEFAULT_FAN_STEP = 10;
    /**  */
    protected double cumulativeEpsilon;
    /**  */
    protected double releaseEventEpsilon;
    /**  */
    protected double flowThreshold;
    /**  */
    protected double mixtureFlowThreshold;
    /**  */
    protected double fanStep;

    /** The graph representing the road traffic network, in which the dynamic network loading is performed */
    protected DtmGraph graph;
    /** The origin zones, in which all the flows are starting */
    protected List<DtmZone> originZones;
    /** Determines whether  */
    protected boolean maintainMaximalTime;
    /** The number of events performed in the simulation */
    protected int eventsCount;
    /** Statistics of the events of the simulation */
    protected DtmDynamicNetworkLoadingStatistics statistics;
    /** The comparison tolerance */
    protected double epsilon;
    /** The thread-safe priority queues for parallel events processing utilized by individual working threads */
    protected List<PriorityBlockingQueue<DtmPriorityQueueElement<Double, DtmEvent>>> eventsPriorityQueues;
    /** The thread-safe priority queue for sequential processing of border edges by the control thread */
    protected PriorityBlockingQueue<DtmPriorityQueueElement<DtmSubgraphParallelBorderEdgesQueuePriority, DtmSubgraphParallelBorderEdgesQueueElement>> borderEdgesPriorityQueue;
    /** The number of working threads */
    protected int threadsCount;
    /** The current times of individual working threads */
    protected double[] currentTimes;
    /** The synchronization times of individual working threads - the threads are synchronized if they have no further events to process or the time of the next event to be processed is higher or equal to the synchronization time of the thread */
    protected double[] synchronizationTimes;
    /** The time, at which the computation is stopped if not stopped earlier due to the lack of events to process */
    protected double maximalTime;
    /** The working threads performing the computation */
    protected DtmSubgraphParallelWorkingThread[] subgraphParallelWorkingThreads;

    /**
     * Creates new parallel event-based dynamic network loading
     * @param graph the graph representing the road traffic network, in which the parallel dynamic network loading is performed
     * @param originZones the origin zones, in which all the flows are starting
     * @param maintainMaximalTime
     */
    public DtmSubgraphParallelEventGeneralLinkTransmissionModel(DtmGraph graph, List<DtmZone> originZones, boolean maintainMaximalTime, int threadsCount) {
        this.graph = graph;
        this.originZones = originZones;
        this.maintainMaximalTime = maintainMaximalTime;
        this.threadsCount = threadsCount;

        eventsPriorityQueues = new ArrayList<>();
        for (int i = 0; i < threadsCount; i++) {
            eventsPriorityQueues.add(new PriorityBlockingQueue<>(DtmGraph.DEFAULT_QUEUE_CAPACITY));
        }
        borderEdgesPriorityQueue = new PriorityBlockingQueue<>();

        currentTimes = new double[threadsCount];
        synchronizationTimes = new double[threadsCount];

        eventsCount = 0;
        statistics = new DtmDynamicNetworkLoadingStatistics();

        cumulativeEpsilon = DEFAULT_CUMULATIVE_EPSILON;
        releaseEventEpsilon = DEFAULT_RELEASE_EVENT_EPSILON;
        flowThreshold = DEFAULT_FLOW_THRESHOLD;
        mixtureFlowThreshold = DEFAULT_MIXTURE_FLOW_THRESHOLD;
        fanStep = DEFAULT_FAN_STEP;
    }

    /**
     * Returns <code>true</code> is all the priority queues are empty. Returns <code>false</code> otherwise.
     * @return <code>true</code> is all the priority queues are empty. Returns <code>false</code> otherwise.
     */
    protected boolean areEventsPriorityQueuesEmpty() {
        boolean empty = true;

        for (PriorityBlockingQueue<DtmPriorityQueueElement<Double, DtmEvent>> epq: eventsPriorityQueues) {
            if (!epq.isEmpty()) {
                empty = false;
                break;
            }
        }

        if (empty) {
            empty = borderEdgesPriorityQueue.isEmpty();
        }

        return empty;
    }

    /**
     * Returns the minimum of current times of all working threads
     * @return the minimum of current times of all working threads
     */
    protected double minimalCurrentTime() {
        double minimalCurrentTime = currentTimes[0];

        for (int i = 0; i < currentTimes.length; i++) {
            if (currentTimes[i] < minimalCurrentTime) {
                minimalCurrentTime = currentTimes[i];
            }
        }

        return minimalCurrentTime;
    }

    @Override
    public List<DtmArrivalFunction> run(double maximalTime, double epsilon) {
        return runParallel(maximalTime, epsilon);
    }

    /**
     * Performs the parallel dynamic network loading and returns arrival functions of individual edges (the order of functions correspond to the order of edges)
     * @param maximalTime maximal time of the simulation in hours
     * @param epsilon the comparison tolerance
     * @return arrival functions of individual edges (the order of functions correspond to the order of edges)
     */
    protected List<DtmArrivalFunction> runParallel(double maximalTime, double epsilon) {
        this.maximalTime = maximalTime;
        this.epsilon = epsilon;

        for (PriorityBlockingQueue<DtmPriorityQueueElement<Double, DtmEvent>> epq: eventsPriorityQueues) {
            epq.clear();
        }

        Arrays.fill(currentTimes, 0.0);
        Arrays.fill(synchronizationTimes, graph.getLookAhead());
        statistics.reset();

        for (DtmZone originZone: originZones) {
            for (DtmOriginEvent originEvent: originZone.getOrigin().getAllEvents()) {
                eventsPriorityQueues.get(originZone.getNode().getSubgraphId()).offer(new DtmPriorityQueueElement<>(originEvent.getPredictedTime(), originEvent));
            }
        }

        for (DtmNode node: graph.getNodes()) {
            for (DtmNodeEvent nodeEvent: node.getNodeModel().getAllEvents()) {
                eventsPriorityQueues.get(node.getSubgraphId()).offer(new DtmPriorityQueueElement<>(nodeEvent.getPredictedTime(), nodeEvent));
            }
        }

        subgraphParallelWorkingThreads = new DtmSubgraphParallelWorkingThread[threadsCount];
        DtmReleasableCyclicBarrier barrier = new DtmReleasableCyclicBarrier(subgraphParallelWorkingThreads.length);

        for (int i = 0; i < subgraphParallelWorkingThreads.length; i++) {
            subgraphParallelWorkingThreads[i] = new DtmSubgraphParallelWorkingThread(i, this, barrier);
        }
        for (int i = 0; i < subgraphParallelWorkingThreads.length; i++) {
            subgraphParallelWorkingThreads[i].start();
        }

        while (subgraphParallelWorkingThreads[0].isAlive()) {
            try {
                subgraphParallelWorkingThreads[0].join();
            }
            catch (InterruptedException ex) {
                //No action
            }
        }

        while (subgraphParallelWorkingThreads[1].isAlive()) {
            try {
                subgraphParallelWorkingThreads[1].join();
            }
            catch (InterruptedException ex) {
                //No action
            }
        }

        List<DtmArrivalFunction> arrivalFunctions = new ArrayList<>();
        for (DtmEdge edge: graph.getEdges()) {
            edge.calculateTravelTimeFunction(maximalTime, maintainMaximalTime);
            arrivalFunctions.add(edge.getArrivalFunction());
        }

		/*
		System.out.print("Edge 13 inflow x");
		for (int i = 0; i < graph.getEdgeById(13).getInflow().getLength(); i++) {
			System.out.print("\t" + graph.getEdgeById(13).getInflow().getX(i));
		}
		System.out.print("\nEdge 13 inflow y");
		for (int i = 0; i < graph.getEdgeById(13).getInflow().getLength(); i++) {
			System.out.print("\t" + graph.getEdgeById(13).getInflow().getY(i));
		}
		System.out.println();

		System.out.print("Edge 13 outflow x");
		for (int i = 0; i < graph.getEdgeById(13).getOutflow().getLength(); i++) {
			System.out.print("\t" + graph.getEdgeById(13).getOutflow().getX(i));
		}
		System.out.print("\nEdge 13 outflow y");
		for (int i = 0; i < graph.getEdgeById(13).getOutflow().getLength(); i++) {
			System.out.print("\t" + graph.getEdgeById(13).getOutflow().getY(i));
		}
		System.out.println();

		System.out.println("-----------------------------------------------------------------------------" + graph.getEdges().size());
		for (int j = 0; j < graph.getEdges().size(); j++) {
			System.out.print("Edge " + graph.getEdges().get(j).getId() + " inflow x");
			for (int i = 0; i < graph.getEdges().get(j).getInflow().getLength(); i++) {
				System.out.print("\t" + graph.getEdges().get(j).getInflow().getX(i));
			}
			System.out.print("\nEdge " + graph.getEdges().get(j).getId() + " inflow y");
			for (int i = 0; i < graph.getEdges().get(j).getInflow().getLength(); i++) {
				System.out.print("\t" + graph.getEdges().get(j).getInflow().getY(i));
			}
			System.out.println();

			System.out.print("Edge " + graph.getEdges().get(j).getId() + " outflow x");
			for (int i = 0; i < graph.getEdges().get(j).getOutflow().getLength(); i++) {
				System.out.print("\t" + graph.getEdges().get(j).getOutflow().getX(i));
			}
			System.out.print("\nEdge " + graph.getEdges().get(j).getId() + " outflow y");
			for (int i = 0; i < graph.getEdges().get(j).getOutflow().getLength(); i++) {
				System.out.print("\t" + graph.getEdges().get(j).getOutflow().getY(i));
			}
			System.out.println("\n");
		}
		System.out.println("-----------------------------------------------------------------------------");
		*/
        return arrivalFunctions;
    }

    /**
     * Returns <code>true</code> if the computation shall continue (i.e., the maximal time was not yet achieved and the at least one priority queue is not empty). Returns <code>false</code> otherwise.
     * @return <code>true</code> if the computation shall continue (i.e., the maximal time was not yet achieved and the at least one priority queue is not empty). Returns <code>false</code> otherwise.
     */
    public synchronized boolean continueComputation() {
        return minimalCurrentTime() < maximalTime && !areEventsPriorityQueuesEmpty();
    }

    /**
     * Return the event at the start of the priority queue of the working thread specified by its index
     * @param threadIndex the index of the working thread
     * @return the event at the start of the priority queue of the working thread specified by its index
     */
    public DtmPriorityQueueElement<Double, DtmEvent> peekCurrentEvent(int threadIndex) {
        return eventsPriorityQueues.get(threadIndex).peek();
    }

    /**
     * Returns the synchronization time of the working thread specified by its index
     * @param threadIndex the index of the working thread
     * @return the synchronization time of the working thread specified by its index
     */
    public double getSynchronizationTime(int threadIndex) {
        return synchronizationTimes[threadIndex];
    }

    /**
     * Increments the synchronization times of all thread by the lookahead
     */
    public void updateSynchronizationTime() {
        for (int i = 0; i < synchronizationTimes.length; i++) {
            synchronizationTimes[i] += graph.getLookAhead();
        }
    }

    /**
     * Sets the current times of all working thread to their current synchronization times and then increments their synchronization times
     */
    public void synchronizeTimes() {
        for (int i = 0; i < currentTimes.length; i++) {
            currentTimes[i] = synchronizationTimes[i];
        }
        updateSynchronizationTime();
    }

    /**
     * Processes an event at the start of the priority queue of the working thread specified by its index or at the start of the border edges processing priority queue depending on the <code>borderEdgesProcessing</code> parameter
     * @param threadIndex the index of the working thread
     * @param borderEdgesProcessing indicates whether the event is processed during the sequential border edges processing or not
     */
    public void processEvent(int threadIndex, boolean borderEdgesProcessing) {
        DtmNode node = null;
        DtmEvent event = null;
        DtmTripleElement<List<Double>, List<Double>, List<DtmMixture<DtmIdentifiable>>> nodeModelUpdate = null;

        if (borderEdgesProcessing) {
            DtmPriorityQueueElement<DtmSubgraphParallelBorderEdgesQueuePriority, DtmSubgraphParallelBorderEdgesQueueElement> priorityQueueElement = borderEdgesPriorityQueue.poll();
            currentTimes[threadIndex] = priorityQueueElement.getPriority().getTime();
            event = priorityQueueElement.getValue().getEvent();
        }
        else {
            DtmPriorityQueueElement<Double, DtmEvent> priorityQueueElement = eventsPriorityQueues.get(threadIndex).poll();

            if (priorityQueueElement.getPriority() < currentTimes[threadIndex]) {
                throw new IllegalStateException("The time of the event must be higher or equal to the current time (" + priorityQueueElement.getPriority() + " < " + currentTimes[threadIndex] + ", " + Arrays.toString(currentTimes));
            }

            currentTimes[threadIndex] = priorityQueueElement.getPriority();
            event = priorityQueueElement.getValue();
        }

        node = event.perform(currentTimes[threadIndex]);
        if (node != null) { //The event is valid
            statistics.registerEvent(event);

            for (DtmEdge edge: node.getIncomingEdges()) { //Update demand and supply if there is a queue
                edge.updateDemand(currentTimes[threadIndex], cumulativeEpsilon);
            }

            nodeModelUpdate = node.getNodeModel().solve(currentTimes[threadIndex]);
            DtmLogger.info(currentTimes[threadIndex], "" + node + ", incoming edges outflows: " + nodeModelUpdate.getFirst() + ", outgoing edges inflows: " + nodeModelUpdate.getSecond(), node);

            updateIncomingEdges(threadIndex, node.getIncomingEdges(), nodeModelUpdate.getFirst(), epsilon, borderEdgesProcessing);
            updateOutgoingEdges(threadIndex, node.getOutgoingEdges(), nodeModelUpdate.getSecond(), epsilon, borderEdgesProcessing);
            updateMixtures(threadIndex, node.getOutgoingEdges(), nodeModelUpdate.getThird(), epsilon, borderEdgesProcessing);
        }
    }

    /**
     * Processes the updates of the border edges added to the border edges priority queue during the parallel run of the working threads and the subsequent events, which shall be performed prior the current synchronization times created by this processing.
     * The updates and events are performed chronologically and in time it should be performed - the updates based on the then-current time of the working thread, which added them to the queue and the events based on the time they shall  be performed
     * The processing is performed sequentially by the control thread.
     */
    public void processBorderEdges() {
        DtmPriorityQueueElement<DtmSubgraphParallelBorderEdgesQueuePriority, DtmSubgraphParallelBorderEdgesQueueElement> priorityQueueElement = null;
        DtmSubgraphParallelBorderEdgesQueueElement subgraphParallelBorderEdgesQueueElement = null;
        double currentTimeBackup = currentTimes[DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX];

        while (!borderEdgesPriorityQueue.isEmpty()) {
            priorityQueueElement = borderEdgesPriorityQueue.peek();
            currentTimes[DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX] = priorityQueueElement.getPriority().getTime();
            subgraphParallelBorderEdgesQueueElement = priorityQueueElement.getValue();

            if (subgraphParallelBorderEdgesQueueElement.getType() == DtmSubgraphParallelBorderEdgesQueueElementTypes.INCOMING_EDGE_FLOW) { //incoming edge start (backward events)
                borderEdgesPriorityQueue.poll();
                updateIncomingEdge(DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX, subgraphParallelBorderEdgesQueueElement.getEdge(), subgraphParallelBorderEdgesQueueElement.getFlow(), epsilon, true);
            }
            else if (subgraphParallelBorderEdgesQueueElement.getType() == DtmSubgraphParallelBorderEdgesQueueElementTypes.OUTGOING_EDGE_FLOW) { //outgoing edge start (backward events)
                borderEdgesPriorityQueue.poll();
                updateOutgoingEdge(DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX, subgraphParallelBorderEdgesQueueElement.getEdge(), subgraphParallelBorderEdgesQueueElement.getFlow(), epsilon, true);
            }
            else if (subgraphParallelBorderEdgesQueueElement.getType() == DtmSubgraphParallelBorderEdgesQueueElementTypes.OUTGOING_EDGE_MIXTURE) { //outgoing edge mixture events
                borderEdgesPriorityQueue.poll();
                updateMixture(DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX, subgraphParallelBorderEdgesQueueElement.getEdge(), subgraphParallelBorderEdgesQueueElement.getMixture(), epsilon, true);
            }
            else if (subgraphParallelBorderEdgesQueueElement.getType() == DtmSubgraphParallelBorderEdgesQueueElementTypes.EVENT) { //processing of events
                processEvent(DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX, true);
            }
        }

        currentTimes[DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX] = currentTimeBackup;
    }

    /**
     * Updates the outflows from incoming edges of a node processed by a working thread specified by its index and releases new events
     * @param threadIndex the index of the working thread
     * @param incomingEdges the incoming edges of a node, which shall be updated
     * @param outflows the new outflows of the incoming edges
     * @param epsilon the comparison tolerance
     * @param borderEdgesProcessing indicates whether the method is invoked during the sequential border edges processing or not
     */
    protected void updateIncomingEdges(int threadIndex, List<DtmEdge> incomingEdges, List<Double> outflows, double epsilon, boolean borderEdgesProcessing) {
        DtmEdge edge = null;
        double outflow = 0.0;

        for (int i = 0; i < incomingEdges.size(); i++) {
            edge = incomingEdges.get(i);
            outflow = outflows.get(i);

            if (borderEdgesProcessing || (!borderEdgesProcessing && !edge.isOnBorder())) {
                updateIncomingEdge(threadIndex, edge, outflow, epsilon, borderEdgesProcessing);
            }
            else {
                //borderEdgesPriorityQueue.offer(new DtmPriorityQueueElement<>(currentTimes[threadIndex], new DtmSubgraphParallelBorderEdgesQueueElement(DtmSubgraphParallelBorderEdgesQueueElementTypes.INCOMING_EDGE_FLOW, edge, outflow)));
                borderEdgesPriorityQueue.offer(new DtmPriorityQueueElement<>(new DtmSubgraphParallelBorderEdgesQueuePriority(currentTimes[threadIndex], edge, DtmSubgraphParallelBorderEdgesQueueElementTypes.INCOMING_EDGE_FLOW), new DtmSubgraphParallelBorderEdgesQueueElement(DtmSubgraphParallelBorderEdgesQueueElementTypes.INCOMING_EDGE_FLOW, edge, outflow)));
            }
        }
    }

    /**
     * Updates the inflows to outgoing edges of a node processed by a working thread specified by its index and releases new events
     * @param threadIndex the index of the working thread
     * @param outgoingEdges the outgoing edges of a node, which shall be updated
     * @param inflows the new inflows to the outgoing edges
     * @param epsilon the comparison tolerance
     * @param borderEdgesProcessing indicates whether the method is invoked during the sequential border edges processing or not
     */
    protected void updateOutgoingEdges(int threadIndex, List<DtmEdge> outgoingEdges, List<Double> inflows, double epsilon, boolean borderEdgesProcessing) {
        DtmEdge edge = null;
        double inflow = 0.0;

        for (int i = 0; i < outgoingEdges.size(); i++) {
            edge = outgoingEdges.get(i);
            inflow = inflows.get(i);

            if (borderEdgesProcessing || (!borderEdgesProcessing && !edge.isOnBorder())) {
                updateOutgoingEdge(threadIndex, edge, inflow, epsilon, borderEdgesProcessing);
            }
            else {
                borderEdgesPriorityQueue.offer(new DtmPriorityQueueElement<>(new DtmSubgraphParallelBorderEdgesQueuePriority(currentTimes[threadIndex], edge, DtmSubgraphParallelBorderEdgesQueueElementTypes.OUTGOING_EDGE_FLOW), new DtmSubgraphParallelBorderEdgesQueueElement(DtmSubgraphParallelBorderEdgesQueueElementTypes.OUTGOING_EDGE_FLOW, edge, inflow)));
            }
        }
    }

    /**
     * Updates the mixtures of the outgoing edges of a node processed by a working thread specified by its index and releases new event
     * @param threadIndex the index of the working thread
     * @param outgoingEdges the outgoing edges of a node, which shall be updated
     * @param mixtures the new mixtures of the outgoing edges
     * @param epsilon the comparison tolerance
     * @param borderEdgesProcessing indicates whether the method is invoked during the sequential border edges processing or not
     */
    protected void updateMixtures(int threadIndex, List<DtmEdge> outgoingEdges, List<DtmMixture<DtmIdentifiable>> mixtures, double epsilon, boolean borderEdgesProcessing) {
        DtmEdge edge = null;
        DtmMixture<DtmIdentifiable> mixture = null;

        for (int i = 0; i < outgoingEdges.size(); i++) {
            edge = outgoingEdges.get(i);
            mixture = mixtures.get(i);

            if (borderEdgesProcessing || (!borderEdgesProcessing && !edge.isOnBorder())) {
                updateMixture(threadIndex, edge, mixture, epsilon, borderEdgesProcessing);
            }
            else {
                borderEdgesPriorityQueue.offer(new DtmPriorityQueueElement<>(new DtmSubgraphParallelBorderEdgesQueuePriority(currentTimes[threadIndex], edge, DtmSubgraphParallelBorderEdgesQueueElementTypes.OUTGOING_EDGE_MIXTURE), new DtmSubgraphParallelBorderEdgesQueueElement(edge, mixture)));
            }
        }
    }

    /**
     * Releases the specified edge event (i.e., adds it to the queue) by a thread specified by its index
     * @param threadIndex the index of the working thread
     * @param edgeEvent the edge event, which shall be released
     * @param epsilon the comparison tolerance
     * @param borderEdgesProcessing indicates whether the method is invoked during the sequential border edges processing or not
     */
    protected void releaseEdgeEvent(int threadIndex, DtmEdgeEvent edgeEvent, double epsilon, boolean borderEdgesProcessing) {
        if (edgeEvent instanceof DtmForwardBackwardEdgeEvent) {
            DtmForwardBackwardEdgeEvent event = (DtmForwardBackwardEdgeEvent) edgeEvent;

            if (event.getMinimalTime() <= event.getPredictedTime() && event.getPredictedTime() <= event.getMaximalTime() && currentTimes[threadIndex] < event.getPredictedTime()) {
                if (borderEdgesProcessing && event.getPredictedTime() < synchronizationTimes[DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX]) {
                    borderEdgesPriorityQueue.offer(new DtmPriorityQueueElement<>(new DtmSubgraphParallelBorderEdgesQueuePriority(event.getPredictedTime(), edgeEvent.getEdge(), DtmSubgraphParallelBorderEdgesQueueElementTypes.EVENT), new DtmSubgraphParallelBorderEdgesQueueElement(event)));
                }
                else {
                    int index = 0;
                    if (!edgeEvent.getEdge().isOnBorder()) {
                        index = edgeEvent.getEdge().getSubgraphId();
                    }
                    else if (edgeEvent instanceof DtmForwardEdgeEvent) {
                        index = edgeEvent.getEdge().getNeighboringSubgraphId();
                    }
                    else {
                        index = edgeEvent.getEdge().getSubgraphId();
                    }
                    eventsPriorityQueues.get(index).offer(new DtmPriorityQueueElement<>(event.getPredictedTime(), event));
                }
                DtmLogger.info(currentTimes[threadIndex], "Valid: " + event, event.getEdge());
            }
            else if (event.getMinimalTime() <= event.getPredictedTime() + epsilon && event.getMaximalTime() >= event.getPredictedTime() && currentTimes[threadIndex] < event.getPredictedTime()) {
                event.setMinimalTime(event.getPredictedTime()); //TODO Potentially problematic

                if (borderEdgesProcessing && event.getPredictedTime() < synchronizationTimes[DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX]) {
                    borderEdgesPriorityQueue.offer(new DtmPriorityQueueElement<>(new DtmSubgraphParallelBorderEdgesQueuePriority(event.getPredictedTime(), edgeEvent.getEdge(), DtmSubgraphParallelBorderEdgesQueueElementTypes.EVENT), new DtmSubgraphParallelBorderEdgesQueueElement(event)));
                }
                else {
                    int index = 0;
                    if (!edgeEvent.getEdge().isOnBorder()) {
                        index = edgeEvent.getEdge().getSubgraphId();
                    }
                    else if (edgeEvent instanceof DtmForwardEdgeEvent) {
                        index = edgeEvent.getEdge().getNeighboringSubgraphId();
                    }
                    else {
                        index = edgeEvent.getEdge().getSubgraphId();
                    }
                    eventsPriorityQueues.get(index).offer(new DtmPriorityQueueElement<>(event.getPredictedTime(), event));
                }
                DtmLogger.info(currentTimes[threadIndex], "Valid: " + event, event.getEdge());
            }
            else {
                DtmLogger.info(currentTimes[threadIndex], "Invalid during release: " + event, event.getEdge());
            }
        }
        else if (edgeEvent instanceof DtmMixtureEdgeEvent) {
            DtmMixtureEdgeEvent event = (DtmMixtureEdgeEvent) edgeEvent;
            event.getEdge().setInflowMixture(event.getMixture());

            if (event.getEdge().getOutflowMixture() == null) {
                event.getEdge().setOutflowMixture(event.getMixture());
            }

            if (event.getEdge().getOutflow().getLastX() <= event.getPredictedTime() && currentTimes[threadIndex] < event.getPredictedTime()) { //TODO Potentially problematic
                if (borderEdgesProcessing && event.getPredictedTime() < synchronizationTimes[DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX]) {
                    borderEdgesPriorityQueue.offer(new DtmPriorityQueueElement<>(new DtmSubgraphParallelBorderEdgesQueuePriority(event.getPredictedTime(), edgeEvent.getEdge(), DtmSubgraphParallelBorderEdgesQueueElementTypes.EVENT), new DtmSubgraphParallelBorderEdgesQueueElement(event)));
                }
                else {
                    int index = (event.getEdge().isOnBorder()) ? event.getEdge().getNeighboringSubgraphId() : event.getEdge().getSubgraphId();
                    eventsPriorityQueues.get(index).offer(new DtmPriorityQueueElement<>(event.getPredictedTime(), event));
                }
                DtmLogger.info(currentTimes[threadIndex], "Valid: " + event, event.getEdge());
            }
        }
    }

    /**
     * Updates the specified forward or backward edge event by a thread specified by its index
     * @param threadIndex the index of the working thread
     * @param event the event, which shall be updated
     * @param epsilon the comparison tolerance
     * @param borderEdgesProcessing indicates whether the method is invoked during the sequential border edges processing or not
     */
    protected void updateForwardBackwardEdgeEvent(int threadIndex, DtmForwardBackwardEdgeEvent event, double epsilon, boolean borderEdgesProcessing) {
        if (!event.isProcessed()) {
            double predictedTime = event.calculatePredictedTime();

            if (predictedTime != -1.0) {
                event.setPredictedTime(predictedTime);
            }

            if (currentTimes[threadIndex] < event.getPredictedTime() && event.getMinimalTime() <= predictedTime && predictedTime <= event.getMaximalTime()) {
                if (borderEdgesProcessing && predictedTime < synchronizationTimes[DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX]) {
                    borderEdgesPriorityQueue.offer(new DtmPriorityQueueElement<>(new DtmSubgraphParallelBorderEdgesQueuePriority(predictedTime, event.getEdge(), DtmSubgraphParallelBorderEdgesQueueElementTypes.EVENT), new DtmSubgraphParallelBorderEdgesQueueElement(event)));
                }
                else {
                    int index = 0;
                    if (!event.getEdge().isOnBorder()) {
                        index = event.getEdge().getSubgraphId();
                    }
                    else if (event instanceof DtmForwardEdgeEvent) {
                        index = event.getEdge().getNeighboringSubgraphId();
                    }
                    else {
                        index = event.getEdge().getSubgraphId();
                    }
                    eventsPriorityQueues.get(index).offer(new DtmPriorityQueueElement<>(predictedTime, event));
                }
            }
            else if (currentTimes[threadIndex] < event.getPredictedTime() && event.getMinimalTime() - epsilon <= predictedTime && predictedTime <= event.getMaximalTime()) {
                event.setMinimalTime(predictedTime); //TODO Potentially problematic

                if (borderEdgesProcessing && predictedTime < synchronizationTimes[DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX]) {
                    borderEdgesPriorityQueue.offer(new DtmPriorityQueueElement<>(new DtmSubgraphParallelBorderEdgesQueuePriority(predictedTime, event.getEdge(), DtmSubgraphParallelBorderEdgesQueueElementTypes.EVENT), new DtmSubgraphParallelBorderEdgesQueueElement(event)));
                }
                else {
                    int index = 0;
                    if (!event.getEdge().isOnBorder()) {
                        index = event.getEdge().getSubgraphId();
                    }
                    else if (event instanceof DtmForwardEdgeEvent) {
                        index = event.getEdge().getNeighboringSubgraphId();
                    }
                    else {
                        index = event.getEdge().getSubgraphId();
                    }
                    eventsPriorityQueues.get(index).offer(new DtmPriorityQueueElement<>(predictedTime, event));
                }
            }
            else {
                //Incorrect event - no action
            }
        }
    }

    /**
     * Updates the specified mixture edge event by a thread specified by its index
     * @param threadIndex the index of the working thread
     * @param event the event, which shall be updated
     * @param epsilon the comparison tolerance
     * @param borderEdgesProcessing indicates whether the method is invoked during the sequential border edges processing or not
     */
    protected void updateMixtureEdgeEvent(int threadIndex, DtmMixtureEdgeEvent event, double epsilon, boolean borderEdgesProcessing) {
        double predictedTime = event.calculatePredictedTime();

        if (currentTimes[threadIndex] < predictedTime && predictedTime >= event.getEdge().getInflow().getLastX() && predictedTime != event.getPredictedTime()) {
            event.setPredictedTime(predictedTime);

            if (borderEdgesProcessing && predictedTime < synchronizationTimes[DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX]) {
                borderEdgesPriorityQueue.offer(new DtmPriorityQueueElement<>(new DtmSubgraphParallelBorderEdgesQueuePriority(predictedTime, event.getEdge(), DtmSubgraphParallelBorderEdgesQueueElementTypes.EVENT), new DtmSubgraphParallelBorderEdgesQueueElement(event)));
            }
            else {
                int index = (event.getEdge().isOnBorder()) ? event.getEdge().getNeighboringSubgraphId() : event.getEdge().getSubgraphId();
                eventsPriorityQueues.get(index).offer(new DtmPriorityQueueElement<>(predictedTime, event));
            }
        }
    }

    /**
     * Updates the outflows from the specified incoming edge by a working thread specified by its index and releases new events
     * @param threadIndex the index of the working thread
     * @param incomingEdge the incoming edges of a node, which shall be updated
     * @param outflow the new outflows of the incoming edge
     * @param epsilon the comparison tolerance
     * @param borderEdgesProcessing indicates whether the method is invoked during the sequential border edges processing or not
     */
    protected void updateIncomingEdge(int threadIndex, DtmEdge incomingEdge, double outflow, double epsilon, boolean borderEdgesProcessing) {
        double newOutflow = outflow;
        List<DtmBackwardEdgeEvent> backwardEvents = null;
        DtmPairElement<Double, Double> cumulativeInflowDownStreamProjection = null;
        double demand = 0.0;
        double predictedTime = 0.0;

        if (Math.abs(incomingEdge.getCurrentOutflow() - outflow) > flowThreshold * 0.5) { //TODO here can be approximation to discard very little changes
            if (incomingEdge.getOutflow().getLastX() <= currentTimes[threadIndex]) { //TODO pokud o opraveni paralelni chyby
                //Release backward events
                if (newOutflow > incomingEdge.getFundamentalDiagram().getFlowCapacity() && newOutflow - incomingEdge.getFundamentalDiagram().getFlowCapacity() < epsilon) {
                    newOutflow = incomingEdge.getFundamentalDiagram().getFlowCapacity();
                }

                backwardEvents = incomingEdge.updateOutflow(currentTimes[threadIndex], newOutflow, fanStep, epsilon, cumulativeEpsilon);
                for (DtmBackwardEdgeEvent backwardEvent: backwardEvents) {
                    releaseEdgeEvent(threadIndex, backwardEvent, releaseEventEpsilon, borderEdgesProcessing);
                }

                //Release node event if there is a queue
                if (incomingEdge.isQueuePresent()) {
                    cumulativeInflowDownStreamProjection = incomingEdge.getCumulativeInflowDownstreamProjection(currentTimes[threadIndex], null, epsilon);

                    if (outflow != cumulativeInflowDownStreamProjection.getSecond()) {
                        // demand - Number of vehicles unsuccessful in leaving the edge at time
                        demand = cumulativeInflowDownStreamProjection.getFirst() - incomingEdge.getCumulativeOutflow(currentTimes[threadIndex]);
                        predictedTime = currentTimes[threadIndex] + demand / (outflow - cumulativeInflowDownStreamProjection.getSecond());

                        if (predictedTime > currentTimes[threadIndex] && demand > flowThreshold) {
                            if (borderEdgesProcessing && predictedTime < synchronizationTimes[DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX]) {
                                //borderEdgesPriorityQueue.offer(new DtmPriorityQueueElement<>(predictedTime, new DtmSubgraphParallelBorderEdgesQueueElement(new DtmNodeEvent(incomingEdge.getEndNode(), predictedTime))));
                                borderEdgesPriorityQueue.offer(new DtmPriorityQueueElement<>(new DtmSubgraphParallelBorderEdgesQueuePriority(predictedTime, null, DtmSubgraphParallelBorderEdgesQueueElementTypes.EVENT), new DtmSubgraphParallelBorderEdgesQueueElement(new DtmNodeEvent(incomingEdge.getEndNode(), predictedTime))));
                            }
                            else {
                                eventsPriorityQueues.get(incomingEdge.getEndNode().getSubgraphId()).offer(new DtmPriorityQueueElement<>(predictedTime, new DtmNodeEvent(incomingEdge.getEndNode(), predictedTime)));
                            }
                        }
                    }
                }

                //Update forward events
                for (DtmForwardEdgeEvent forwardEvent: incomingEdge.getForwardEventsStorage()) {
                    updateForwardBackwardEdgeEvent(threadIndex, forwardEvent, releaseEventEpsilon, borderEdgesProcessing);
                }

                //Remove old events
                incomingEdge.getForwardEventsStorage().remove(new DtmFilter<>() {

                    @Override
                    public boolean isAccepted(DtmForwardEdgeEvent event) {
                        if (event.getMaximalTime() < currentTimes[threadIndex]) {
                            if (!event.isProcessed()) {
                                DtmLogger.warning(currentTimes[threadIndex], "Removing unprocessed " + event, event.getEdge());
                            }
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                });
            }
        }
    }

    /**
     * Updates the inflows to the specified outgoing edge by a working thread specified by its index and releases new events
     * @param threadIndex the index of the working thread
     * @param outgoingEdge the outgoing edges of a node, which shall be updated
     * @param inflow the new inflow to the outgoing edge
     * @param epsilon the comparison tolerance
     * @param borderEdgesProcessing indicates whether the method is invoked during the sequential border edges processing or not
     */
    protected void updateOutgoingEdge(int threadIndex, DtmEdge outgoingEdge, double inflow, double epsilon, boolean borderEdgesProcessing) {
        List<DtmForwardEdgeEvent> events = null;

        if (Math.abs(outgoingEdge.getCurrentInflow() - inflow) > flowThreshold || (inflow != outgoingEdge.getCurrentInflow() && inflow < 1E-12)) { //TODO nahradit nejakou vhodnou konstantou

            if (outgoingEdge.getInflow().getLastX() <= currentTimes[threadIndex]) { //TODO pokud o opraveni paralelni chyby
                //Release forward events
                events = outgoingEdge.updateInflow(currentTimes[threadIndex], inflow, fanStep, releaseEventEpsilon, cumulativeEpsilon);

                for (DtmForwardEdgeEvent event: events) {
                    releaseEdgeEvent(threadIndex, event, releaseEventEpsilon, borderEdgesProcessing);
                }

                //Update backward events
                for (DtmBackwardEdgeEvent backwardEvent: outgoingEdge.getBackwardEventsStorage()) {
                    updateForwardBackwardEdgeEvent(threadIndex, backwardEvent, releaseEventEpsilon, borderEdgesProcessing);
                }

                //Remove old events
                outgoingEdge.getBackwardEventsStorage().remove(new DtmFilter<>() {

                    @Override
                    public boolean isAccepted(DtmBackwardEdgeEvent event) {
                        if (event.getMaximalTime() < currentTimes[threadIndex]) {
                            if (!event.isProcessed()) {
                                DtmLogger.warning(currentTimes[threadIndex], "Removing unprocessed " + event, event.getEdge());
                            }
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                });
            }
        }
    }

    /**
     * Updates the mixtures of the specified outgoing edge by a working thread specified by its index and releases new event
     * @param threadIndex the index of the working thread
     * @param outgoingEdge the outgoing edges of a node, which shall be updated
     * @param mixture the new mixture of the outgoing edges
     * @param epsilon the comparison tolerance
     * @param borderEdgesProcessing indicates whether the method is invoked during the sequential border edges processing or not
     */
    protected void updateMixture(int threadIndex, DtmEdge outgoingEdge, DtmMixture<DtmIdentifiable> mixture, double epsilon, boolean borderEdgesProcessing) {
        double mixtureDifference = 0.0;
        DtmMixtureEdgeEvent event = null;

        if (mixture != null) {
            //Approximation to discard very little changes
            mixtureDifference = Double.POSITIVE_INFINITY;
            if (outgoingEdge.getInflowMixture() != null) {
                mixtureDifference = outgoingEdge.getInflowMixture().findMaximalDifference(mixture);
            }

            if (mixtureDifference > mixtureFlowThreshold) {
                if (outgoingEdge.getInflowMixtureFunction().getLastX() <= currentTimes[threadIndex]) { //TODO pokus o odstaneni paralelni chyby
                    event = outgoingEdge.updateMixture(currentTimes[threadIndex], mixture);

                    if (outgoingEdge.getOutflow().getLastX() <= event.getPredictedTime() && currentTimes[threadIndex] < event.getPredictedTime()) {
                        if (borderEdgesProcessing && event.getPredictedTime() < synchronizationTimes[DtmSubgraphParallelWorkingThread.CONTROL_THREAD_INDEX]) {
                            borderEdgesPriorityQueue.offer(new DtmPriorityQueueElement<>(new DtmSubgraphParallelBorderEdgesQueuePriority(event.getPredictedTime(), outgoingEdge, DtmSubgraphParallelBorderEdgesQueueElementTypes.EVENT), new DtmSubgraphParallelBorderEdgesQueueElement(event)));
                        }
                        else {
                            int index = (outgoingEdge.isOnBorder()) ? outgoingEdge.getNeighboringSubgraphId() : outgoingEdge.getSubgraphId();
                            eventsPriorityQueues.get(index).offer(new DtmPriorityQueueElement<>(event.getPredictedTime(), event));
                        }
                    }

                    //Update mixture events
                    for (DtmMixtureEdgeEvent mixtureEvent: outgoingEdge.getMixtureEventsStorage()) {
                        updateMixtureEdgeEvent(threadIndex, mixtureEvent, releaseEventEpsilon, borderEdgesProcessing);
                    }
                }
            }
        }
    }


    //Getters and setters

    /**
     *
     * @return the cumulativeEpsilon
     */
    public double getCumulativeEpsilon() {
        return cumulativeEpsilon;
    }

    /**
     *
     * @param cumulativeEpsilon the cumulativeEpsilon to set
     */
    public void setCumulativeEpsilon(double cumulativeEpsilon) {
        this.cumulativeEpsilon = cumulativeEpsilon;
    }

    /**
     *
     * @return the releaseEventEpsilon
     */
    public double getReleaseEventEpsilon() {
        return releaseEventEpsilon;
    }

    /**
     *
     * @param releaseEventEpsilon the releaseEventEpsilon to set
     */
    public void setReleaseEventEpsilon(double releaseEventEpsilon) {
        this.releaseEventEpsilon = releaseEventEpsilon;
    }

    /**
     *
     * @return the flowThreshold
     */
    public double getFlowThreshold() {
        return flowThreshold;
    }

    /**
     *
     * @param flowThreshold the flowThreshold to set
     */
    public void setFlowThreshold(double flowThreshold) {
        this.flowThreshold = flowThreshold;
    }

    /**
     *
     * @return the mixtureFlowThreshold
     */
    public double getMixtureFlowThreshold() {
        return mixtureFlowThreshold;
    }

    /**
     *
     * @param mixtureFlowThreshold the mixtureFlowThreshold to set
     */
    public void setMixtureFlowThreshold(double mixtureFlowThreshold) {
        this.mixtureFlowThreshold = mixtureFlowThreshold;
    }

    /**
     *
     * @return the fanStep
     */
    public double getFanStep() {
        return fanStep;
    }

    /**
     *
     * @param fanStep the fanStep to set
     */
    public void setFanStep(double fanStep) {
        this.fanStep = fanStep;
    }
}
