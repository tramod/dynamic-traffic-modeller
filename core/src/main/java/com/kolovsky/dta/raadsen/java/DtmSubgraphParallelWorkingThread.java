/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * Working thread for parallel dynamic traffic assignment. One thread (with index of <code>CONTROL_THREAD_INDEX</code>) also serves as the control thread of the computation
 * @author Tomas Potuzak
 */
public class DtmSubgraphParallelWorkingThread extends Thread {
    /** The index of the control thread */
    public static final int CONTROL_THREAD_INDEX = 0;
    /** The index of this working thread */
    protected int index;
    /** The reference to the dynamic network loading instance with methods performed by this thread */
    protected DtmSubgraphParallelEventGeneralLinkTransmissionModel subgraphParallelEventGeneralLinkTransmissionModel;
    /** The releasable cyclic barrier used for synchronization of the working threads */
    protected DtmReleasableCyclicBarrier barrier;

    /**
     * Creates a new working thread
     * @param index the index of the working thread
     * @param subgraphParallelEventGeneralLinkTransmissionModel the reference to the dynamic network loading instance with methods performed by this thread
     * @param barrier the releasable cyclic barrier used for synchronization of the working threads
     */
    public DtmSubgraphParallelWorkingThread(int index, DtmSubgraphParallelEventGeneralLinkTransmissionModel subgraphParallelEventGeneralLinkTransmissionModel, DtmReleasableCyclicBarrier barrier)  {
        this.index = index;
        this.subgraphParallelEventGeneralLinkTransmissionModel = subgraphParallelEventGeneralLinkTransmissionModel;
        this.barrier = barrier;
    }

    @Override
    public void run() {
        while (subgraphParallelEventGeneralLinkTransmissionModel.continueComputation()) {
            DtmPriorityQueueElement<Double, DtmEvent> currentEvent = subgraphParallelEventGeneralLinkTransmissionModel.peekCurrentEvent(index);
            if (currentEvent == null || currentEvent.getPriority() >= subgraphParallelEventGeneralLinkTransmissionModel.getSynchronizationTime(index)) {
                barrier.synchronize(index);

                if (index == CONTROL_THREAD_INDEX) {
                    subgraphParallelEventGeneralLinkTransmissionModel.processBorderEdges();
                    subgraphParallelEventGeneralLinkTransmissionModel.synchronizeTimes();
                }

                barrier.synchronize(index);
            }
            else {
                subgraphParallelEventGeneralLinkTransmissionModel.processEvent(index, false);
            }
        }
        barrier.release();
    }
}

