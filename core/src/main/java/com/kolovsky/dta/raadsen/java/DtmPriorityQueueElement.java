/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * Element of the priority queue
 * @param <P> the priority of the element
 * @param <V> the value of the element
 * @author Tomas Potuzak
 */
public class DtmPriorityQueueElement<P extends Comparable<P>, V> implements Comparable<DtmPriorityQueueElement<P, V>> {
    /** Priority of the element */
    protected P priority;
    /** Value of the element */
    protected V value;

    /**
     * Creates a new priority queue element with specified priority and value
     * @param priority priority of the new element
     * @param value value of the new element
     * @throws NullPointerException when the priority is <code>null</code>
     */
    public DtmPriorityQueueElement(P priority, V value) throws NullPointerException {
        if (priority == null) {
            throw new NullPointerException("Priority cannot be null.");
        }

        this.priority = priority;
        this.value = value;
    }

    //Getters and setters

    /**
     * Returns the priority of the element
     * @return the priority of the element
     */
    public P getPriority() {
        return priority;
    }

    /**
     * Sets the priority of the element
     * @param priority the new priority of the element
     */
    public void setPriority(P priority) {
        this.priority = priority;
    }

    /**
     * Returns the value of the element
     * @return the value of the element
     */
    public V getValue() {
        return value;
    }

    /**
     * Sets the value of the element
     * @param value the new value of he element
     */
    public void setValue(V value) {
        this.value = value;
    }

    //Inherited methods

    @Override
    public int compareTo(DtmPriorityQueueElement<P, V> other) {
        return priority.compareTo(other.priority);
    }

    @Override
    public String toString() {
        return priority + ": " + value;
    }
}

