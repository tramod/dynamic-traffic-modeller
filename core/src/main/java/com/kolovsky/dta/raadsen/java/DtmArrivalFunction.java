/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tomas Potuzak
 */
public class DtmArrivalFunction extends DtmNonDecreasingPiecewiseLinearFunction implements Comparable<DtmArrivalFunction> {
    /**  */
    protected double minimalTravelTime;
    /**  */
    protected double maximalTravelTime;
    /** Maximal slope of the segments of this arrival function, which is a non-decreasing piecewise linear function */
    protected double maximalSlope;

    /**
     * Creates a new arrival function with specified <i>x</i> and <i>y</i> values.
     * @param xValues the <i>x</i> values of the function corresponding to the departure times
     * @param yValues the <i>y</> = <i>f</i>(<i>x</i>) values of the function corresponding to the arrival times
     * @throws IllegalArgumentException if the numbers of <i>x</i> and <i>y</i> values are different or the <i>x</i> values are not increasing
     * @throws NullPointerException if <i>x</i> and/or <i>y</i> values are <code>null</code>
     */
    public DtmArrivalFunction(List<Double> xValues, List<Double> yValues) throws IllegalArgumentException, NullPointerException {
        super(xValues, yValues);

        DtmPairElement<Double, Double> minimalMaximalTravelTimes = calculateMimimalMaximalTravelTimes();
        minimalTravelTime = minimalMaximalTravelTimes.getFirst();
        maximalTravelTime = minimalMaximalTravelTimes.getSecond();
        maximalSlope = findMaximalSlope();
    }

    /**
     *
     */
    public DtmArrivalFunction() {
        super();
    }

    /**
     * Finds and returns the maximal slope of this function using the <code>getSlope()</code> method
     * @return the maximal slope of this function
     */
    protected double findMaximalSlope() {
        double maximalSlope = Double.NEGATIVE_INFINITY;
        double slope = 0.0;
        for (int i = 1; i < xValues.size(); i++) {
            slope = getSlope(i);

            if (slope > maximalSlope) {
                maximalSlope = slope;
            }
        }

        return maximalSlope;
    }

    /**
     * Calculates and returns the minimal and the maximal travel time of this function
     * @return the minimal and the maximal travel time of this function
     */
    protected DtmPairElement<Double, Double> calculateMimimalMaximalTravelTimes() {
        double minimalTime = Double.POSITIVE_INFINITY;
        double maximalTime = Double.NEGATIVE_INFINITY;
        double time = 0.0;

        if (!xValues.isEmpty()) {
            for (int i = 0; i < xValues.size(); i++) {
                time = yValues.get(i) - xValues.get(i);
                if (time < minimalTime) {
                    minimalTime = time;
                }
                if (time > maximalTime) {
                    maximalTime = time;
                }
            }
        }

        return new DtmPairElement<>(minimalTime, maximalTime);
    }

    /**
     * Returns <code>true</code> if this function have infinite travel time. Returns <code>false</code> otherwise.
     * @return <code>true</code> if this function have infinite travel time. Returns <code>false</code> otherwise.
     */
    public boolean isInfinite() {
        //return Double.isInfinite(minimalTravelTime) && minimalTravelTime > 0.0;
        return xValues.isEmpty();
    }

    /**
     * Returns
     * @param x
     * @param index
     * @param xAbsoluteError
     * @return
     */
    public double getYMaximalError(double x, int index, double xAbsoluteError) {
        if (xAbsoluteError == 0) { //TODO determine, whether this is ok
            return 0.0;
        }
        else {
            double xLowerBound = x - xAbsoluteError;
            double xUpperBound = x + xAbsoluteError;
            if (xLowerBound < getFirstX()) {
                xLowerBound = getFirstX();
            }
            if (xUpperBound > getLastX()) {
                xUpperBound = getLastX();
            }

            //TODO dokoncit
        }
        return 0.0;
    }

    /**
     * Returns the function value (<i>f</i>(<i>x</i>)) corresponding to the arrival time for the specified <i>x<i> value corresponding to the departure time
     * @param x the <i>x</i> value corresponding to the departure time, for which the function value (<i>f</i>(<i>x</i>)) corresponding to the arrival time shall be returned
     * @return the function value (<i>f</i>(<i>x</i>)) corresponding to the arrival time for the specified <i>x<i> value corresponding to the departure time
     */
    public double getArrival(double x) {
        return valueAt(x);
    }

    /**
     * Returns the function value (<i>f</i>(<i>x</i>)) corresponding to the arrival time for the specified <i>x<i> value corresponding to the departure time using the specified segment of the function
     * @param x the <i>x</i> value corresponding to the departure time, for which the function value (<i>f</i>(<i>x</i>)) corresponding to the arrival time shall be returned
     * @param index the index of the <i>x</i> upper bound of the segment of the function. For 0, the index is considered unspecified and is found first.
     * @return the function value (<i>f</i>(<i>x</i>)) corresponding to the arrival time for the specified <i>x<i> value corresponding to the departure time
     */
    public double getArrival(double x, int index) {
        if (index == 0) {
            return getArrival(x);
        }
        else {
            return DtmMathUtils.interpolateLine(index, xValues, yValues, x);
        }
    }

    /**
     * Returns the <i>x<i> value corresponding to the departure time for the specified function value (<i>f</i>(<i>x</i>)) corresponding to the arrival time
     * @param y the <i>y</i> value corresponding to the arrival time, for which the <i>x<i> value corresponding to the departure time shall be returned
     * @return the <i>x<i> value corresponding to the departure time for the specified function value (<i>f</i>(<i>x</i>)) corresponding to the arrival time
     */
    public double getDeparture(double y) {
        if (y == yValues.get(yValues.size() - 1)) {
            return xValues.get(xValues.size() - 1);
        }
        else {
            int index = DtmFunctionUtils.binarySearch(yValues, y);
            return DtmMathUtils.interpolateLine(index + 1, yValues, xValues, y);
        }
    }

    /**
     * Returns the <i>x<i> value corresponding to the departure time for the specified function value (<i>f</i>(<i>x</i>)) corresponding to the arrival time using the specified segment of the function
     * @param y the <i>y</i> value corresponding to the arrival time, for which the <i>x<i> value corresponding to the departure time shall be returned
     * @param index the index of the <i>y</i> upper bound of the segment of the function. For 0, the index is considered unspecified and is found first.
     * @return the <i>x<i> value corresponding to the departure time for the specified function value (<i>f</i>(<i>x</i>)) corresponding to the arrival time
     */
    public double getDeparture(double y, int index) {
        if (index == 0) {
            return getDeparture(y);
        }
        else {
            return DtmMathUtils.interpolateLine(index, yValues, xValues, y);
        }
    }

    @Override
    public DtmArrivalFunction clone() {
        List<Double> clonedXValues = new ArrayList<>(xValues);
        List<Double> clonedYValues = new ArrayList<>(yValues);
        return new DtmArrivalFunction(clonedXValues, clonedYValues);
    }

    @Override
    public String toString() {
        return xValues.toString() + "\n" + yValues.toString();
    }

    @Override
    public DtmArrivalFunction subFunction(double a, double b) {
        DtmPiecewiseLinearFunction subFunction = super.subFunction(a, b);
        return new DtmArrivalFunction(subFunction.xValues, subFunction.yValues);
    }

    /**
     *
     * @param other
     * @return
     * @throws IllegalArgumentException
     */
    public DtmArrivalFunction substitute(DtmArrivalFunction other) throws IllegalArgumentException { //TODO overit, ze se to chova stejne
        if (getFirstX() > other.getFirstX() || getLastX() < other.getLastX()) {
            throw new IllegalArgumentException("The substituting function must not be outside the range of this function.");
        }
        else {
            List<Double> resultXValues = new ArrayList<>();
            List<Double> resultYValues = new ArrayList<>();

            int i = 0;
            while (getX(i) < other.getX(0)) {
                resultXValues.add(getX(i));
                resultYValues.add(getY(i));
            }
            resultXValues.addAll(other.xValues);
            resultYValues.addAll(other.yValues);

            i = DtmFunctionUtils.binarySearch(xValues, other.getLastX());
            for (int j = i + 1; j < getLength(); j++) {
                resultXValues.add(getX(j));
                resultYValues.add(getY(j));
            }

            return new DtmArrivalFunction(resultXValues, resultYValues);
        }
    }

    /**
     * Returns the inverse arrival function to this function
     * @return the inverse arrival function to this function
     */
    public DtmArrivalFunction inverseFunction() {
        return new DtmArrivalFunction(yValues, xValues);
    }

    /**
     * Returns this function transformed to travel time function
     * @return this function transformed to travel time function
     */
    public DtmPiecewiseLinearFunction toTravelTimeFunction() {
        List<Double> resultYValues = new ArrayList<>();

        for (int i = 0; i < getLength(); i++) {
            resultYValues.add(getY(i) - getX(i));
        }

        return new DtmPiecewiseLinearFunction(xValues, resultYValues);
    }

    /**
     * Returns the minimal travel time
     * @return the minimal travel time
     */
    public double getMinimalTravelTime() {
        return minimalTravelTime;
    }

    /**
     * Returns the maximal travel time
     * @return the maximal travel time
     */
    public double getMaximalTravelTime() {
        return maximalTravelTime;
    }

    /**
     * Returns the maximal slope
     * @return the maximal slope
     */
    public double getMaximalSlope() {
        return maximalSlope;
    }

    @Override
    public int compareTo(DtmArrivalFunction o) {
        return Double.compare(minimalTravelTime, o.minimalTravelTime);
    }

    /**
     * Returns <code>true</code> if a part of this arrival function is under the specified arrival function with specified comparison tolerance. Return <code>false</code> otherwise.
     * @param other the arrival function, which shall be compared to this function
     * @param epsilon the comparison tolerance
     * @return <code>true</code> if part of this arrival function is under the specified arrival function with specified comparison tolerance. Return <code>false</code> otherwise.
     */
    public boolean isLowerWithEpsilon(DtmArrivalFunction other, double epsilon) {
        return isLowerWithEpsilon(this, other, epsilon);
    }

    /**
     * Returns <code>true</code> if a part of the first arrival function is under the second arrival function with specified comparison tolerance. Return <code>false</code> otherwise.
     * @param function1 the first arrival function to be compared with the second one
     * @param function2 the second arrival function to be compared with the first one
     * @param epsilon the comparison tolerance
     * @return <code>true</code> if the specified arrival functions are identical with specified comparison tolerance. Return <code>false</code> otherwise.
     */
    public static boolean isLowerWithEpsilon(DtmArrivalFunction function1, DtmArrivalFunction function2, double epsilon) {
        if (function1.isInfinite()) {
            return false;
        }
        else if (function2.isInfinite()) {
            return true;
        }
        else if (function1.minimalTravelTime > function2.maximalTravelTime) {
            return false;
        }
        else if (function1.minimalTravelTime < function2.minimalTravelTime) {
            return true;
        }

        int i = 0;
        int j = 0;

        while (i < function1.getLength() && j < function2.getLength()) {
            if (function1.getX(i) == function2.getX(j)) {
                if (function1.getY(i) < function2.getY(j) - epsilon) {
                    return true;
                }
                i++;
                j++;
            }
            else if (function1.getX(i) < function2.getX(j)) {
                if (function1.getY(i) < function2.getArrival(function1.getX(i), j) - epsilon) {
                    return true;
                }
                i++;
            }
            else {
                if (function1.getArrival(function2.getX(j), i) < function2.getY(j) - epsilon) {
                    return true;
                }
                j++;
            }
        }

        return false;
    }

    /**
     * Performs composition of the this output and the specified input arrival functions and returns the resulting function (<i>out</i>(<i>in</i>(<i>t</i>))) together with its differences (errors) caused by the approximation in the input functions
     * @param inputFunction the input arrival function
     * @param relativeError the maximal allowed relative error
     * @return the resulting function (<i>out</i>(<i>in</i>(<i>t</i>))) together with its differences (errors) caused by the approximation in the input functions
     */
    public DtmPairElement<DtmArrivalFunction, List<Double>> performComposition(DtmArrivalFunction inputFunction, double relativeError) {
        return performComposition(this, inputFunction, relativeError);
    }

    /**
     * Performs composition of the specified output and input arrival functions and returns the resulting function (<i>out</i>(<i>in</i>(<i>t</i>))) together with its differences (errors) caused by the approximation in the input functions
     * @param outputFunction the output arrival function
     * @param inputFunction the input arrival function
     * @param relativeError the maximal allowed relative error
     * @return the resulting function (<i>out</i>(<i>in</i>(<i>t</i>))) together with its differences (errors) caused by the approximation in the input functions
     */
    public static DtmPairElement<DtmArrivalFunction, List<Double>> performComposition(DtmArrivalFunction outputFunction, DtmArrivalFunction inputFunction, double relativeError) {
        DtmArrivalFunction composite = new DtmArrivalFunction();
        List<Double> differences = new ArrayList<>(); //Function differences for approximations
        double absoluteError = 0.0;
        double difference = 0.0;
        double x = 0.0;
        double y = 0.0;
        int i = 0; //Index of piece of the input function
        int j = DtmFunctionUtils.binarySearch(outputFunction.getXValues(), inputFunction.getY(0)) + 1; //Index of piece of the output function

        while (i < inputFunction.getLength() && j < outputFunction.getLength()) {
            if (inputFunction.getY(i) == outputFunction.getX(j)) {
                absoluteError = outputFunction.getYMaximalError(outputFunction.getX(j) - 10E-10, j, relativeError * (inputFunction.getY(i) - inputFunction.getX(i))); //TODO ta konstanta by chtela nahradit, ale asi ne parametrem epsilon - epsilon by moznma melo mit jmeno relativeError
                difference = (relativeError * (outputFunction.getY(j) - inputFunction.getX(i)) - absoluteError) / (1 + relativeError);
                x = inputFunction.getX(i);
                y = outputFunction.getY(j);

                if (composite.getLength() > 0){
                    if (x >= composite.getLastX() && y >= composite.getLastY()) {
                        composite.addPoint(x, y);
                        differences.add(difference);
                    }
                } else {
                    composite.addPoint(x, y);
                    differences.add(difference);
                }

                i++;
                j++;
            }
            else if (inputFunction.getY(i) < outputFunction.getX(j)) {
                absoluteError = outputFunction.getYMaximalError(inputFunction.getY(i) - 10E-10, j, relativeError * (inputFunction.getY(i) - inputFunction.getX(i)));
                difference = (relativeError * (outputFunction.getArrival(inputFunction.getY(i), j) - inputFunction.getX(i)) - absoluteError) / (1 + relativeError);
                x = inputFunction.getX(i);
                y = outputFunction.getArrival(inputFunction.getY(i), j);

                if (composite.getLength() > 0){
                    if (x >= composite.getLastX() && y >= composite.getLastY()) {
                        composite.addPoint(x, y);
                        differences.add(difference);
                    }
                } else {
                    composite.addPoint(x, y);
                    differences.add(difference);
                }

                i++;
            }
            else {
                x = inputFunction.getDeparture(outputFunction.getX(j), i);
                y = outputFunction.getY(j);

                absoluteError = outputFunction.getYMaximalError(outputFunction.getX(j) - 10E-10, j, relativeError * (outputFunction.getX(j) - x));
                difference = (relativeError * (outputFunction.getY(j) - x) - absoluteError) / (1 + relativeError);

                if (composite.getLength() > 0){
                    if (x >= composite.getLastX() && y >= composite.getLastY()) {
                        composite.addPoint(x, y);
                        differences.add(difference);
                    }
                } else {
                    composite.addPoint(x, y);
                    differences.add(difference);
                }

                j++;
            }
        }


        return new DtmPairElement<>(composite, differences);
    }

    /**
     * Creates and returns the minimum function of this and the specified function. The resulting function is in every point the minimum of both functions.
     * @param other the other function to be compared with this function and used for the resulting minimum function
     * @return the minimum function of this and the specified function
     */
    public DtmArrivalFunction minimumFunction(DtmArrivalFunction other) {
        if (other.isInfinite()){
            return new DtmArrivalFunction(xValues, yValues);
        } else if (this.isInfinite()){
            return new DtmArrivalFunction(other.xValues, other.yValues);
        }

        // check minimal and maximal travel times
        if(minimalTravelTime > other.maximalTravelTime){
            return new DtmArrivalFunction(other.xValues, other.yValues);
        }
        if(other.minimalTravelTime > maximalTravelTime){
            return new DtmArrivalFunction(xValues, yValues);
        }
        //DtmPiecewiseLinearFunction result = super.minimumFunction(other);
        DtmPiecewiseLinearFunction result = super.minimumFunctionOperators(other);
        //TODO vyuziva implementaci v Piecewise linear function - najit nejlepsi implementaci (mela by to byt ta z Operators), tu pouzit a uhladit implementaci vcetne optimalniho umisteni (Piecewise function nebo Arrival, static i ne-static)
        return new DtmArrivalFunction(result.getXValues(), result.getYValues());
    }

    /**
     * Creates and returns the minimum function of the specified functions. The resulting function is in every point the minimum of both functions.
     * @param function1 the first arrival function to be compared with the second one and used for the resulting minimum function
     * @param function2 the second arrival function to be compared with the first one and used for the resulting minimum function
     * @return the minimum function of this and the specified function
     */
    public static DtmArrivalFunction minimumFunction(DtmArrivalFunction function1, DtmArrivalFunction function2) {
        DtmPiecewiseLinearFunction result = function1.minimumFunction(function2);
        //TODO vyuziva implementaci v Piecewise linear function - najit nejlepsi implementaci (mela by to byt ta z Operators), tu pouzit a uhladit implementaci vcetne optimalniho umisteni (Piecewise function nebo Arrival, static i ne-static)
        return new DtmArrivalFunction(result.getXValues(), result.getYValues());
    }

    /**
     * Creates and returns the epsilon approximation of this function
     * @param differences differences used for simplification
     * @return and returns the epsilon approximation of this function
     */
    public DtmArrivalFunction keepEpsilon(List<Double> differences) {
        DtmPiecewiseLinearFunction result = simplify(differences);
        return new DtmArrivalFunction(result.getXValues(), result.getYValues());
    }
}
