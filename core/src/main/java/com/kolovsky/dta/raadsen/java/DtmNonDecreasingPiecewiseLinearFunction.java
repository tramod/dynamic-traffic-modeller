/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.List;

/**
 * Non-decreasing piecewise linear function for Dynamic Traffic Assignment
 * @author Tomas Potuzak
 */
public class DtmNonDecreasingPiecewiseLinearFunction extends DtmPiecewiseLinearFunction {

    /**
     * Creates a new non-decreasing piecewise linear function with specified <i>x</i> and <i>y</i> values
     * @param xValues the <i>x</i> values of the function
     * @param yValues the <i>y</> = <i>f</i>(<i>x</i>) values of the function
     * @throws IllegalArgumentException if the numbers of <i>x</i> and <i>y</i> values are different or the <i>x</i> values are not increasing or the <i>y</i> values are not non-decreasing
     * @throws NullPointerException if <i>x</i> and/or <i>y</i> values are <code>null</code>
     */
    public DtmNonDecreasingPiecewiseLinearFunction(List<Double> xValues, List<Double> yValues) throws IllegalArgumentException, NullPointerException {
        super(xValues, yValues);

        for (int i = 1; i < yValues.size(); i++) {
            if (yValues.get(i - 1) > yValues.get(i)) {
                throw new IllegalArgumentException("The function is not non-decreasing.");
            }
        }
    }

    /**
     * Creates a new non-decreasing piecewise linear function with empty <i>x</i> and <i>y</i> values
     */
    public DtmNonDecreasingPiecewiseLinearFunction() {
        super();
    }

    @Override
    public void addPoint(double x, double y) throws IllegalArgumentException {
        super.addPoint(x, y);

        if (y < yValues.get(yValues.size() - 1)) {
            throw new IllegalArgumentException("The function must be non-decreasing. The added value is lower than the last value.");
        }
    }

    @Override
    public void updatePoint(int index, double x, double y) throws IllegalArgumentException {
        super.updatePoint(index, x, y);

        if (index > 0) {
            if (yValues.get(index - 1) > yValues.get(index)) {
                throw new IllegalArgumentException("The non-decreasing condition is broken.");
            }
        }
        if (index < xValues.size() - 1) {
            if (yValues.get(index) > yValues.get(index + 1)) {
                throw new IllegalArgumentException("The non-decreasing condition is broken.");
            }
        }
    }

    /**
     * Returns <code>true</code>
     * @param index
     * @return
     */
    protected boolean isDerivationZero(int index) {
        if (index == 0) {
            return yValues.get(index) == yValues.get(index + 1);
        }
        else if (index == xValues.size() - 1) {
            return yValues.get(index) == yValues.get(index - 1);
        }
        else {
            return yValues.get(index - 1) == yValues.get(index) && yValues.get(index) == yValues.get(index + 1);
        }
    }

    /**
     * Creates and returns the horizontal difference of this and the specified function
     * @param other the other function, whose horizontal difference with this function shall be created
     * @return the horizontal difference of this and the specified function
     * @throws IllegalArgumentException if the functions have different starting <i>y</i> values
     */
    public DtmPiecewiseLinearFunction horizontalDifference(DtmNonDecreasingPiecewiseLinearFunction other) throws IllegalArgumentException {
        if (yValues.get(0) != other.yValues.get(0)) { //TODO potential issues - double comparison and index potentially out of range
            throw new IllegalArgumentException("First points of the functions must have the same y values.");
        }

        DtmPiecewiseLinearFunction difference = new DtmPiecewiseLinearFunction();

        int i = 0;
        int j = 0;

        while (i < other.xValues.size() && j < xValues.size()) {
            if (i < other.xValues.size() && other.isDerivationZero(i)) {
                i++;
            }
            else if (j < xValues.size() && isDerivationZero(j)) {
                j++;
            }
            else if (other.yValues.get(i) < yValues.get(j)) {
                double tv = xValues.get(j) - (yValues.get(j) - other.yValues.get(i)) / (yValues.get(j) - yValues.get(j - 1)) * (xValues.get(j) - xValues.get(j - 1));
                difference.addPoint(other.xValues.get(i), tv - other.xValues.get(i));
                i++;
            }
            else if (other.yValues.get(i) == yValues.get(j)) { //TODO potential double comparison problem
                difference.addPoint(other.xValues.get(i), xValues.get(j) - other.xValues.get(i));
                i++;
                j++;
            }
            else {
                double tu = other.xValues.get(i) - (other.yValues.get(i) - yValues.get(j)) / (other.yValues.get(i) - other.yValues.get(i - 1)) * (other.xValues.get(i) - other.xValues.get(i - 1));
                if (difference.xValues.get(difference.xValues.size() - 1) < tu) {
                    difference.addPoint(tu, xValues.get(j) - tu);
                }

                j++;
            }
        }

        return difference;
    }

    /**
     * Creates and returns the vertical cut of this function between specified bounds
     * @param lowerBound the lower <i>y</i> bound of the cut
     * @param upperBound the upper <i>y</i> bound of the cut
     * @return the vertical cut of this function between specified bounds
     */
    public DtmNonDecreasingPiecewiseLinearFunction verticulCut(double lowerBound, double upperBound) {
        int lowerIndex = DtmFunctionUtils.binarySearch(yValues, lowerBound);
        int upperIndex = DtmFunctionUtils.binarySearch(yValues, upperBound);
        if (xValues.get(upperIndex - 1) == upperBound) {
            upperIndex--;
        }

        List<Double> cutXValues = new ArrayList<>();
        List<Double> cutYValues = new ArrayList<>();
        cutXValues.add(lowerBound);  //TODO is it really ok to use lowerBound and upperBound in both xValues and yValues?
        cutYValues.add(lowerBound);
        for (int i = lowerIndex; i < upperIndex; i++) {
            cutXValues.add(xValues.get(i));
            cutYValues.add(yValues.get(i));
        }
        cutXValues.add(upperBound);
        cutYValues.add(upperBound);

        return new DtmNonDecreasingPiecewiseLinearFunction(cutXValues, cutYValues);
    }

}

