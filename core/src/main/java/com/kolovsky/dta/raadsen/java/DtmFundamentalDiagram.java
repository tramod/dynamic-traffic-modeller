/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * The fundamental diagram of an edge
 * @author Tomas Potuzak
 */
public class DtmFundamentalDiagram {
    /** Maximal vehicle speed [km/h] */
    protected double maximalSpeed;
    /** Flow capacity [veh/h] */
    protected double flowCapacity;
    /** Maximal (jam) vehicle density [veh/km] */
    protected double maximalDensity;
    /** Critical speed [km/h]  */
    protected double criticalSpeed;
    /** Minimal wave speed [km/h] - must be negative */
    protected double minimalWaveSpeed;
    /** Critical vehicle density [veh/km] */
    protected double criticalDensity;
    /** The alpha coefficient */
    protected double alpha;
    /** The beta coefficient */
    protected double beta;

    /**
     * Creates a new fundamental diagram with specified parameters
     * @param maximalSpeed the maximal speed
     * @param flowCapacity
     * @param maximalDensity
     * @param criticalSpeed
     * @param minimalWaveSpeed
     */
    public DtmFundamentalDiagram(double maximalSpeed, double flowCapacity, double maximalDensity, double criticalSpeed, double minimalWaveSpeed) throws IllegalArgumentException {
        this.maximalSpeed = maximalSpeed;
        this.flowCapacity = flowCapacity;
        this.maximalDensity = maximalDensity;
        this.criticalSpeed = criticalSpeed;
        this.minimalWaveSpeed = minimalWaveSpeed;

        criticalDensity = flowCapacity / criticalSpeed;
        alpha = (criticalSpeed / flowCapacity) * (maximalSpeed - criticalSpeed);
        beta = (flowCapacity + minimalWaveSpeed * (maximalDensity - flowCapacity / criticalSpeed)) / DtmMathUtils.power(maximalDensity - flowCapacity / criticalSpeed, 2);

        if (maximalSpeed / criticalSpeed < 1 || maximalSpeed / criticalSpeed >= 2) {
            throw new IllegalArgumentException("Invalid parameters of the fundamental diagram.");
        }

        double value = minimalWaveSpeed * (flowCapacity / criticalSpeed - maximalDensity) / flowCapacity;
        if (value < 1.0 || value >= 2.0) {
            throw new IllegalArgumentException("Invalid parameters of the fundamental diagram.");
        }
    }

    /**
     *
     * @param density
     * @return
     * @throws IllegalArgumentException
     */
    public double calculatePhiI(double density) throws IllegalArgumentException {
        if (density < 0.0 || density > criticalDensity) {
            throw new IllegalArgumentException("Invalid domain of density (" + density + ")");
        }

        return (maximalSpeed - alpha * density) * density;
    }

    /**
     *
     * @param flow
     * @return
     * @throws IllegalArgumentException
     */
    public double calculateInversePhiI(double flow) throws IllegalArgumentException {
        if (flow < 0 || flow > flowCapacity) {
            throw new IllegalArgumentException("Invalid domain of flow (" + flow + ")");
        }

        return (1 / (2 * alpha)) * (maximalSpeed - Math.sqrt(maximalSpeed * maximalSpeed - 4 * alpha * flow));
    }

    /**
     *
     * @param density
     * @return
     * @throws IllegalArgumentException
     */
    public double calculatePhiII(double density) throws IllegalArgumentException {
        if (density < criticalDensity || density > maximalDensity) {
            throw new IllegalArgumentException("Invalid domain of density (" + density + ")");
        }

        return (minimalWaveSpeed + beta * (density - 2 * maximalDensity)) * density + (beta * maximalDensity - minimalWaveSpeed) * maximalDensity;
    }

    /**
     *
     * @param flow
     * @return
     * @throws IllegalArgumentException
     */
    public double calculateInversePhiII(double flow) throws IllegalArgumentException {
        if (flow < 0 || flow > flowCapacity) {
            throw new IllegalArgumentException("Invalid domain of flow (" + flow + ")");
        }

        return maximalDensity - 1 / (2 * beta) * (minimalWaveSpeed + Math.sqrt(4 * beta * flow + minimalWaveSpeed * minimalWaveSpeed));
    }


    /**
     *
     * @param flow
     * @return
     */
    public double calculateGammaI(double flow) {
        return maximalSpeed - 2 * alpha * calculateInversePhiI(flow);
    }

    /**
     *
     * @param flow
     * @return
     */
    public double calculateGammaII(double flow) {
        return 2 * beta * (calculateInversePhiII(flow) - maximalDensity) + minimalWaveSpeed;
    }

    /**
     *
     * @param flow
     * @return
     */
    public double calculateSigmaI(double flow) {
        if (flow == 0.0) {
            return maximalSpeed;
        }
        else {
            return flow / calculateInversePhiI(flow);
        }
    }

    /**
     *
     * @param flow
     * @return
     */
    public double calculateSigmaII(double flow) {
        return flow / calculateInversePhiII(flow);
    }

    /**
     *
     * @param flow
     * @param length
     * @return
     */
    public double calculateXiI(double flow, double length) {
        if (flow == 0.0) {
            return 0.0;
        }
        else {
            return length * flow * ((1 / calculateGammaI(flow)) - (1 / calculateSigmaI(flow)));
        }
    }

    /**
     *
     * @param flow
     * @param length
     * @return
     */
    public double calculateXiII(double flow, double length) {
        if (flow == 0.0) {
            return maximalDensity;
        }
        else {
            return length * flow * ((1 / calculateSigmaII(flow)) - (1 / calculateGammaII(flow)));
        }
    }

    /**
     *
     * @param flow1
     * @param flow2
     * @return
     * @throws IllegalArgumentException
     */
    public double calculateEtaI(double flow1, double flow2) throws IllegalArgumentException {
        if (flow1 == flow2) {
            throw new IllegalArgumentException("Flows must be different.");
        }

        return (flow2 - flow1) / (calculateInversePhiI(flow2) - calculateInversePhiI(flow1));
    }

    /**
     *
     * @param flow1
     * @param flow2
     * @return
     * @throws IllegalArgumentException
     */
    public double calculateEtaII(double flow1, double flow2) throws IllegalArgumentException {
        if (flow1 == flow2) {
            throw new IllegalArgumentException("Flows must be different.");
        }

        return (flow2 - flow1) / (calculateInversePhiII(flow2) - calculateInversePhiII(flow1));
    }

    //Getters and setters

    /**
     * Returns the maximal vehicle speed
     * @return the maximal vehicle speed
     */
    public double getMaximalSpeed() {
        return maximalSpeed;
    }

    /**
     * Returns the capacity of the flow
     * @return the capacity of the flow
     */
    public double getFlowCapacity() {
        return flowCapacity;
    }
}
