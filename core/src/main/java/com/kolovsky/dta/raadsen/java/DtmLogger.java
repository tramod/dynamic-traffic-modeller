/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;

/**
 * Logger for the messages of watched objects
 * @author Tomas Potuzak
 */
public class DtmLogger {
    /** Default logging level*/
    public static final DtmLoggingLevels DEFAULT_LOGGING_LEVEL = DtmLoggingLevels.WARNING;
    /** Default number of digits after the decimal point in printed time */
    private static final int DEFAULT_TIME_PRECISSION = 8;
    /** The current instance of the logger */
    private static DtmLogger logger;
    /** The object watched by the logger */
    private HashSet<Object> watchedObjects;
    /** The logging level */
    private DtmLoggingLevels loggingLevel;
    /** The number of digits after the decimal point in printed time */
    private int timePrecission;

    /**
     * Creates new logger with default logging level
     */
    private DtmLogger() {
        watchedObjects = new HashSet<>();
        loggingLevel = DEFAULT_LOGGING_LEVEL;
        timePrecission = DEFAULT_TIME_PRECISSION;
    }

    /**
     * Initializes the logger. If the logger was already initialized, the existing instance of the logger is used.
     */
    private static void initialize() {
        if (logger == null) {
            logger = new DtmLogger();
        }
    }

    /**
     * Sets the logging level of the logger
     * @param loggingLevel the logging level
     */
    public static void setLoggingLevel(DtmLoggingLevels loggingLevel) {
        initialize();

        logger.loggingLevel = loggingLevel;
    }

    public static void setTimePrecission(int timePrecission) {
        initialize();

        logger.timePrecission = timePrecission;
    }

    /**
     * Add an object which shall be watched by the logger. Only messages of the watched object will be printed to the output.
     * @param watchedObject an object to be watched by the logger
     */
    public static void addWatchedObject(Object watchedObject) {
        initialize();

        logger.watchedObjects.add(watchedObject);
    }

    /**
     * Add objects which shall be watched by the logger. Only messages of the watched objects will be printed to the output.
     * @param watchedObjects objects to be watched by the logger
     */
    public static void addWatchedObjects(List<Object> watchedObjects) {
        initialize();

        logger.watchedObjects.addAll(watchedObjects);
    }

    /**
     * Prints the specified info message of the specified object with the specified time. The message will be printed to the output only if the logging level is set to INFO and the object is watched by the logger.
     * @param time the time of the message
     * @param message the message, which shall be printed to the output
     * @param watchedObject the object, which is associated with the message
     */
    public static void info(double time, String message, Object watchedObject) {
        initialize();

        if (logger.watchedObjects.contains(watchedObject) && logger.loggingLevel == DtmLoggingLevels.INFO) {
            System.out.format(Locale.US, "[%1." + logger.timePrecission + "f] INFO: %s%n", time, message);
        }
    }

    /**
     * Prints the specified warning message of the specified object with the specified time. The message will be printed to the output only if the logging level is set to INFO or WARNING and the object is watched by the logger.
     * @param time the time of the message
     * @param message the message, which shall be printed to the output
     * @param watchedObject the object, which is associated with the message
     */
    public static void warning(double time, String message, Object watchedObject) {
        initialize();

        if (logger.watchedObjects.contains(watchedObject) && logger.loggingLevel.getLevelValue() <= DtmLoggingLevels.WARNING.getLevelValue()) {
            System.out.format(Locale.US, "[%1." + logger.timePrecission + "f] WARNING: %s%n", time, message);
        }
    }

    /**
     * Prints the specified info message of the specified object with the specified time. The message will be printed to the output only if the object is watched by the logger.
     * @param time the time of the message
     * @param message the message, which shall be printed to the output
     * @param watchedObject the object, which is associated with the message
     */
    public static void error(double time, String message, Object watchedObject) {
        initialize();

        if (logger.watchedObjects.contains(watchedObject)) {
            System.out.format(Locale.US, "[%1." + logger.timePrecission + "f] ERROR: %s%n", time, message);
        }
    }
}

