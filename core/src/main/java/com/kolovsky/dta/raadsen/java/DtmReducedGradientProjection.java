/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * Reduced gradient projection. Switch from all alternatives to minimal alternative.
 * @author kolovsky
 */
public class DtmReducedGradientProjection implements DtmGradientProjection {
    /** gradient step */
    protected double alpha;

    /**
     * @param alpha gradient step
     */
    public DtmReducedGradientProjection(double alpha){
        this.alpha = alpha;
    }

    public double[] run(double[] proportions, double[] cost){
        if (proportions.length != cost.length){
            throw new IllegalArgumentException("All arrays must have the same length");
        }

        // new proportion
        double[] outP = new double[cost.length];

        // find fastest alternative
        int bestIndex = -1;
        double bestCost = Double.POSITIVE_INFINITY;
        for (int i = 0; i < cost.length; i++){
            if (cost[i] < bestCost){
                bestCost = cost[i];
                bestIndex = i;
            }
        }

        // shift flow
        double totalProp = 0.0;
        for (int i = 0; i < cost.length; i++){
            if (i != bestIndex){
                double shift = (bestCost - cost[i])/bestCost * alpha;
                outP[i] = Math.max(0.0, proportions[i] + shift);
                totalProp += outP[i];
            }
        }
        outP[bestIndex] = 1.0 - totalProp;
        return outP;
    }
}
