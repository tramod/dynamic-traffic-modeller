/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * A forward or backward edge event for Dynamic Traffic Assignment
 * @author Tomas Potuzak
 */
public abstract class DtmForwardBackwardEdgeEvent extends DtmEdgeEvent {
    /** Lower bound of the interval of the influence of this event */
    protected double minimalTime;
    /** Upper bound of the interval of the influence of this event */
    protected double maximalTime;
    /** Indicates whether or not this event was processed as a valid event */
    protected boolean processed;
    /** Indicates whether or not this event is a fan event */
    protected boolean fanEvent;
    /** Indicates whether or not this event is the last fan event */
    protected boolean lastFanEvent;
    /** Time of the current inflow or outflow */
    protected double flowTime;
    /** The current inflow or outflow */
    protected double flow;
    /** The current cumulative inflow or outflow */
    protected double cumulativeFlow;

    /**
     * Creates a new forward or backward edge event
     * @param edge the edge of this event
     * @param flowTime the time of the current inflow or outflow
     * @param flow the current inflow or outflow
     * @param cumulativeFlow the current inflow or outflow
     * @param fanEvent the value of whether or not this event is a fan event
     * @param lastFanEvent the value of whether or not this event is the last fan event
     */
    public DtmForwardBackwardEdgeEvent(DtmEdge edge, double flowTime, double flow, double cumulativeFlow, boolean fanEvent, boolean lastFanEvent) {
        this.edge = edge;
        this.flowTime = flowTime;
        this.flow = flow;
        this.cumulativeFlow = cumulativeFlow;
        this.fanEvent = fanEvent;
        this.lastFanEvent = lastFanEvent;
    }

    /**
     * Calculates and returns the lower bound of the interval of the influence of this event
     * @return the lower bound of the interval of the influence of this event
     */
    public abstract double calculateMinimalTime();

    /**
     * Calculates and returns the upper bound of the interval of the influence of this event
     * @return the upper bound of the interval of the influence of this event
     */
    public abstract double calculateMaximalTime();

    //Getters and setters

    /**
     * Returns the lower bound of the interval of the influence of this event
     * @return the lower bound of the interval of the influence of this event
     */
    public double getMinimalTime() {
        return minimalTime;
    }

    //TODO Potentially problematic! - added because of the EventGeneralLinkTransmissionModel
    public void setMinimalTime(double minimalTime) {
        this.minimalTime = minimalTime;
    }

    /**
     * Returns the upper bound of the interval of the influence of this event
     * @return the upper bound of the interval of the influence of this event
     */
    public double getMaximalTime() {
        return maximalTime;
    }

    /**
     * Returns <code>true</code> if this event was processed as a valid event. Returns <code>false</code> otherwise
     * @return <code>true</code> if this event was processed as a valid event. Returns <code>false</code> otherwise
     */
    public boolean isProcessed() {
        return processed;
    }

    /**
     * Sets whether this event was processed as a valid event
     * @param processed the new value of whether this event was processed as a valid event
     */
    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    /**
     * Returns <code>true</code> if this event is a fan event. Returns <code>false</code> otherwise.
     * @return <code>true</code> if this event is a fan event. Returns <code>false</code> otherwise.
     */
    public boolean isFanEvent() {
        return fanEvent;
    }

    /**
     * Sets whether this event is a fan event
     * @param fanEvent the new value of whether this event is a fan event
     */
    public void setFanEvent(boolean fanEvent) {
        this.fanEvent = fanEvent;
    }

    /**
     * Returns <code>true</code> if this event is the last fan event. Returns <code>false</code> otherwise
     * @return <code>true</code> if this event is the last fan event. Returns <code>false</code> otherwise
     */
    public boolean isLastFanEvent() {
        return lastFanEvent;
    }

    /**
     * Sets whether this event is the last fan event
     * @param lastFanEvent the new value of whether this event is the last fan event
     */
    public void setLastFanEvent(boolean lastFanEvent) {
        this.lastFanEvent = lastFanEvent;
    }

    /**
     * Returns the time of the current inflow or outflow
     * @return the time of the current inflow or outflow
     */
    public double getFlowTime() {
        return flowTime;
    }

    /**
     * Sets the time of the current inflow or outflow
     * @param flowTime the new value of the time of the current inflow or outflow
     */
    public void setFlowTime(double flowTime) {
        this.flowTime = flowTime;
    }

    /**
     * Returns the current inflow or outflow
     * @return the current inflow or outflow
     */
    public double getFlow() {
        return flow;
    }

    /**
     * Sets the current inflow or outflow
     * @param flow the new value of the current inflow or outflow
     */
    public void setFlow(double flow) {
        this.flow = flow;
    }

    /**
     * Returns the current cumulative inflow or outflow
     * @return the current cumulative inflow or outflow
     */
    public double getCumulativeFlow() {
        return cumulativeFlow;
    }

    /**
     * Sets the current cumulative inflow or outflow
     * @param cumulativeFlow the new value of the current cumulative inflow or outflow
     */
    public void setCumulativeFlow(double cumulativeFlow) {
        this.cumulativeFlow = cumulativeFlow;
    }
}