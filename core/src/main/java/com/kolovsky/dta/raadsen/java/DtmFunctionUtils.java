/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A utility class for the functions
 * @author Tomas Potuzak
 */
public class DtmFunctionUtils {

    /**
     * Searches for the position of the <code>value</code> in the list of sorted (ascending) values and returns the index of the nearest lower of equal value
     * @param values the sorted values, in which the value shall be searched
     * @param value the value, which shall be searched in the sorted values
     * @return the index of the nearest lower of equal value
     * @throws IllegalArgumentException if the <code>values</code> list is empty or the searched value is outside the interval defined by the first and last value of the <code>values</code>
     * @throws NullPointerException if the <code>values</code> list is <code>null</code>
     */
    public static int binarySearch(List<Double> values, double value) throws IllegalArgumentException, NullPointerException {
        if (values == null) {
            throw new NullPointerException("List with values must not be null.");
        }
        else if (values.isEmpty()) {
            throw new IllegalArgumentException("List with values must not be empty.");
        }
        else if (value > values.get(values.size() - 1) || value < values.get(0)) {
            throw new IllegalArgumentException("The searched value must not be outside the interval <" + values.get(0) + "; " + values.get(values.size() - 1) + ">.");
        }

        int index = Collections.binarySearch(values, value); //TODO overit, ze se to chova stejne jako Scala implementace
        if (index >= 0) {
            return index;
        }
        else {
            return -index - 2; //-1 because negative return value is (-(insertion point) - 1) and another -1, because we need previous value, if we are in the middle
        }
    }

    /**
     * Check if the last value of both functions is the same with the specified comparison tolerance. If not, the second function is updated according to the first function.
     * @param function1 the first function
     * @param function2 the second function
     * @param epsilon the comparison tolerance
     * @throws NullPointerException if the first and/or the second function is <code>null</code>
     */
    public static void fixFunctions(DtmNonDecreasingPiecewiseLinearFunction function1, DtmNonDecreasingPiecewiseLinearFunction function2, double epsilon) throws NullPointerException {
        //epsilon = 1000; //TODO toto bylo v puvodni implementaci
        if (function1 == null || function2 == null) {
            throw new NullPointerException("The functions must not be null.");
        }
        else {
            //if (!DtmMathUtils.isEqualWithEpsilon(function1.getYValues().get(lastIndex1), function2.getYValues().get(lastIndex2), epsilon)) {
            if (Math.abs(function1.getLastY() - function2.getLastY()) != 0.0) { //TODO toto bylo v puvodni implementaci
                double originalLast = function2.getLastY();
                int j = function2.getLength() - 1;
                function2.getYValues().set(j, function1.getLastY());

                while (j > 0 && (function2.getYValues().get(j - 1) > function2.getYValues().get(j) || originalLast == function2.getYValues().get(j - 1))) {
                    function2.getYValues().set(j - 1, function2.getY(j));
                    j--;
                }
            }
        }
    }

    /**
     * Calculates and returns the horizontal difference of the specified functions.
     * @param function1 the function, from which the function2 is "subtracted", i.e., the function values of function1 shall always be greater or equal to function values of function2
     * @param function2 the function, which is "subtracted" from function, i.e., the function values of function2 shall always be lower or equal to function values of function1
     * @param minimalDifference the minimal difference of function1 and function2 in each point
     * @return the horizontal difference of the specified functions. The horizontal difference is a function, whose function value in every point of function1 corresponds to the horizontal distance of the function1 to function2
     */
    public static DtmPiecewiseLinearFunction calculateHorizontalDifference(DtmNonDecreasingPiecewiseLinearFunction function1, DtmNonDecreasingPiecewiseLinearFunction function2, double minimalDifference) throws NullPointerException {
        if (function1 == null || function2 == null) {
            throw new NullPointerException("The functions must not be null.");
        }
        else {
            DtmPiecewiseLinearFunction result = new DtmPiecewiseLinearFunction();
            List<Double> function2PointsFunction1XValues = new ArrayList<>(function2.getLength());
            List<Double> function2PointsResultYValues = new ArrayList<>(function2.getLength());
            double function2PointFunction1XValue = 0.0;
            double function2PointResultYValue = 0.0;
            int i = 0;
            int j = 0;

            while (j < function2.getLength()) { //Pass function2 points and determine the corresponding x values of function1 and the difference (i.e., result) y values for these points; filter out all inner points of constants segments
                if (!isConstantSegmentInnerPoint(j, function2)) { //Skip inner points of constants segments
                    while (i < function1.getLength() && DtmMathUtils.isLowerThanWithEpsilon(function1.getY(i), function2.getY(j))) {
                        i++;
                    }
                    if (i < function1.getLength() && DtmMathUtils.isEqualWithEpsilon(function1.getY(i), function2.getY(j))) { //The function2 point corresponds to a function1 point (or points) - skip such point, it will be handled while passing function1
                        j++;
                    }
                    else { //A function1 point with a greater y than the function2 point was found or function2 is above function1 (which should not happen and must be treated)
                        if (i >= function1.getLength()) { //The function2 is above function1 (this should not happen, but will be be treated during passing of function1)
                            break; //All further points of function2 must be above function1, there is no need to continue with passing of function2
                        }
                        else { //A function1 point with a greater y than the function2 point was found
                            if (i == 0) { //The found function1 point with a greater y than the function2 point is the starting point of the function1
                                j++; //This function2 point will not be considered
                            }
                            else { //The found function1 point with a greater y than the function2 point is not the starting point of the function1 (i.e., normal or non-border situation)
                                function2PointFunction1XValue = DtmMathUtils.calculateIntersectionX(function1.getX(i - 1), function1.getY(i - 1), function1.getX(i), function1.getY(i), function2.getY(j));
                                function2PointResultYValue = function2.getX(j) - function2PointFunction1XValue;

                                if (isConstantSegmentEndPoint(j, function2)) { //To avoid 2 same x values for 2 different y values in the result due to a constant segment in function2 and linear segment in function1 - second x value shifted slightly to the right
                                    function2PointFunction1XValue += 2 * DtmMathUtils.DEFAULT_COMPARISON_EPSILON;
                                }

                                function2PointsFunction1XValues.add(function2PointFunction1XValue);
                                if (function2PointResultYValue < minimalDifference) { //The calculated result y is lower than the minimal difference (this shall not happen) - setting difference y to minimal difference
                                    function2PointResultYValue = minimalDifference;
                                }
                                function2PointsResultYValues.add(function2PointResultYValue);
                                j++;
                            }
                        }
                    }
                }
                else {
                    j++;
                }
            }

            i = 0;
            j = 0;
            int k = 0;
            double resultXValue = 0.0;
            double resultYValue = 0.0;
            double function1PointFunction2XValue  = 0.0;
            boolean function1ConstantSegment = false;
            boolean function2ConstantSegment = false;

            while (i < function1.getLength()) { //Pass function1 and function2PointsFunction1XValues and construct the difference
                if (j >= function2PointsFunction1XValues.size() || function1.getX(i) <= function2PointsFunction1XValues.get(j)) { //Next point to process is the function1 point
                    if (!isConstantSegmentInnerPoint(i, function1)) { //The point of function1 is not a constant segment inner point
                        while (k < function2.getLength() && DtmMathUtils.isLowerThanWithEpsilon(function2.getY(k), function1.getY(i))) {
                            k++;
                        }

                        if (k < function2.getLength() && DtmMathUtils.isEqualWithEpsilon(function1.getY(i), function2.getY(k))) { //The function1 point (or points) corresponds to a function2 point (or points)
                            resultXValue = function1.getX(i);
                            resultYValue = function2.getX(k) - resultXValue;
                            if (resultYValue < minimalDifference) { //The calculated result y is lower than the minimal difference (this shall not happen) - setting result y to minimal difference
                                resultYValue = minimalDifference;
                            }
                            result.addPoint(resultXValue, resultYValue);

                            function1ConstantSegment = isConstantSegmentStartPoint(i, function1); //If true, there are two points of the function1 (constant segment start and end point)
                            function2ConstantSegment = isConstantSegmentStartPoint(k, function2); //If true, there are two points of the function2 (constant segment start and end point)

                            if (!function1ConstantSegment && !function2ConstantSegment) {
                                i++;
                                k++;
                            }
                            else {
                                if (function1ConstantSegment) {
                                    i++;
                                    while (isConstantSegmentInnerPoint(i, function1)) {
                                        i++;
                                    }
                                }
                                if (function2ConstantSegment) {
                                    k++;
                                    while (isConstantSegmentInnerPoint(k, function2)) {
                                        k++;
                                    }
                                }

                                resultXValue = function1.getX(i);
                                resultYValue = function2.getX(k) - resultXValue;
                                if (resultYValue < minimalDifference) { //The calculated result y is lower than the minimal difference (this shall not happen) - setting result y to minimal difference
                                    resultYValue = minimalDifference;
                                }

                                if (!function1ConstantSegment && function2ConstantSegment) {
                                    resultXValue += 2 * DtmMathUtils.DEFAULT_COMPARISON_EPSILON;
                                }

                                result.addPoint(resultXValue, resultYValue);
                                i++;
                                k++;
                            }
                        }
                        else { //A function2 point with a greater y than the function1 point was found or the end of function2 was reached with no such point found
                            if (k >= function2.getLength()) { //The end of function2 was reached with no such point found
                                result.addPoint(function1.getX(i), minimalDifference);
                                i++;
                            }
                            else { //A function2 point with a greater y than the function1 point was found
                                if (k == 0) { //The found function2 point with a greater y than the function1 point is the starting point of the function2
                                    result.addPoint(function1.getX(i), minimalDifference);
                                    i++;
                                }
                                else { //The found function2 point with a greater y than the function1 point is not the starting point of the function2 (i.e., normal or non-border situation)
                                    function1PointFunction2XValue = DtmMathUtils.calculateIntersectionX(function2.getX(k - 1), function2.getY(k - 1), function2.getX(k), function2.getY(k), function1.getY(i));

                                    resultXValue = function1.getX(i);
                                    resultYValue = function1PointFunction2XValue - function1.getX(i);
                                    if (resultYValue < minimalDifference) { //The calculated result y is lower than the minimal difference (this shall not happen) - setting result y to minimal difference
                                        resultYValue = minimalDifference;
                                    }
                                    result.addPoint(resultXValue, resultYValue);
                                    i++;
                                }
                            }
                        }
                    }
                    else { //Skipping function1 constant segment inner point
                        i++;
                    }
                }
                else { //Next point to process is the function2 point
                    result.addPoint(function2PointsFunction1XValues.get(j), function2PointsResultYValues.get(j));
                    j++;
                }
            }

            return result;
        }
    }

    /**
     * Returns <code>true</code> if the point of the function at specified index is an inner point of a constant segment of the function. Returns <code>false</code> otherwise.
     * @param index the index of the investigated point
     * @param function the investigated function
     * @return <code>true</code> if the point of the function at specified index is an inner point of a constant segment of the function. Returns <code>false</code> otherwise.
     */
    public static boolean isConstantSegmentInnerPoint(int index, DtmPiecewiseLinearFunction function) {
        if (index > 0 && index < function.getLength() - 1 && DtmMathUtils.isEqualWithEpsilon(function.getY(index - 1), function.getY(index)) && DtmMathUtils.isEqualWithEpsilon(function.getY(index), function.getY(index + 1)) ) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Returns <code>true</code> if the point of the function at specified index is the end point of a constant segment of the function. Returns <code>false</code> otherwise.
     * @param index the index of the investigated point
     * @param function the investigated function
     * @return <code>true</code> if the point of the function at specified index is the end point of a constant segment of the function. Returns <code>false</code> otherwise.
     */
    public static boolean isConstantSegmentEndPoint(int index, DtmPiecewiseLinearFunction function) {
        if (index > 0 && index < function.getLength() && DtmMathUtils.isEqualWithEpsilon(function.getY(index - 1), function.getY(index)) && (index == function.getLength() - 1 || DtmMathUtils.isLowerThanWithEpsilon(function.getY(index), function.getY(index + 1)))) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Returns <code>true</code> if the point of the function at specified index is the start point of a constant segment of the function. Returns <code>false</code> otherwise.
     * @param index the index of the investigated point
     * @param function the investigated function
     * @return <code>true</code> if the point of the function at specified index is the start point of a constant segment of the function. Returns <code>false</code> otherwise.
     */
    public static boolean isConstantSegmentStartPoint(int index, DtmPiecewiseLinearFunction function) {
        if (index >= 0 && index < function.getLength() - 1 && DtmMathUtils.isEqualWithEpsilon(function.getY(index), function.getY(index + 1)) && (index == 0 || DtmMathUtils.isLowerThanWithEpsilon(function.getY(index - 1), function.getY(index)))) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Calculates and returns the horizontal difference of the specified functions
     * @param function1
     * @param function2
     * @return
     */
    public static DtmPiecewiseLinearFunction calculateHorizontalDifference(DtmNonDecreasingPiecewiseLinearFunction function1, DtmNonDecreasingPiecewiseLinearFunction function2) throws NullPointerException {
        if (function1 == null || function2 == null) {
            throw new NullPointerException("The functions must not be null.");
        }
        else {
            DtmPiecewiseLinearFunction difference = new DtmPiecewiseLinearFunction();
            int i = 0;
            int j = 0;

            while (i < function1.getLength() && j < function2.getLength()) {
                if (function1.getY(i) < function2.getY(j)) {
                    if (isConstantMiddle(i, function1)) { //Skip the inner point of a constant part
                        i++;
                    }
                    else if (isSingular(i, function1) && i == function1.getLength() - 1) { //Last and singular point
                        difference.addPoint(difference.getLastX() + difference.getLastY(), 0.0);
                        i++;
                    }
                    else {
                        difference.addPoint(function1.getX(i), function2.getX(j) - (function2.getY(j) - function1.getY(i)) / (function2.getY(j) - function2.getY(j - 1)) * (function2.getX(j) - function2.getX(j - 1)) - function1.getX(i));

                        if (i == function1.getLength() - 1) {
                            difference.addPoint(difference.getLastX() + difference.getLastY(), 0.0);
                        }
                        i++;
                    }
                }
                else if (function1.getY(i) == function2.getY(j)) {
                    if (isRegular(i, function1) && isSingular(j, function2)) {
                        if (i < function1.getLength() - 1) {
                            i++;
                        }
                        else {
                            difference.addPoint(function1.getX(i), findConstantLeftX(j, function2) - function1.getX(i));
                            difference.addPoint(difference.getLastX() + difference.getLastY(), 0.0);
                            i++;
                        }
                    }
                    else if (isSingular(i, function1) && isRegular(j, function2)) {
                        if (j < function2.getLength() - 1) {
                            j++;
                        }
                        else {
                            double x = findConstantLeftX(i, function1);
                            difference.addPoint(x, function2.getX(j) - x);
                            difference.addPoint(difference.getLastX() + difference.getLastY(), 0.0);
                            j++;
                        }
                    }
                    else if (isRegular(i, function1) && isRegular(j, function2)) {
                        difference.addPoint(function1.getX(i), function2.getX(j) - function1.getX(i));

                        if (i == function1.getLength() - 1 || j == function2.getLength() - 1) { //Last point
                            difference.addPoint(difference.getLastX() + difference.getLastY(), 0.0);
                        }
                        i++;
                        j++;
                    }
                    else {
                        if (isIncreasingLeft(i, function1)) {
                            difference.addPoint(function1.getX(i), findConstantLeftX(j, function2) - function1.getX(i));
                            i++;
                        }
                        else if (isIncreasingRight(i, function1)) {
                            DtmPairElement<Double, Integer> rightPoint = findConstantRightX(j, function2);
                            difference.addPoint(function1.getX(i), rightPoint.getFirst() - function1.getX(i));
                            i++;
                            j = rightPoint.getSecond() + 1;
                        }
                        else if (i == 0) { //First singular point
                            difference.addPoint(function1.getX(i), 0.0);
                            i++;
                        }
                        else if (i == function1.getLength() - 1) { //Last singular point
                            difference.addPoint(difference.getLastX() + difference.getLastY(), 0.0);
                            i++;
                        }
                        else {
                            i++;
                        }
                    }
                }
                else {
                    if (isConstantMiddle(j, function2)) { //Skip point within constant part
                        j++;
                    }
                    else if (isSingular(j, function2) && j == function2.getLength() - 1) { //Last and singular point
                        difference.addPoint(difference.getLastX() + difference.getLastY(), 0.0);
                        j++;
                    }
                    else {
                        double x = function1.getX(i) - (function1.getY(i) - function2.getY(j)) / (function1.getY(i) - function1.getY(i - 1)) * (function1.getX(i) - function1.getX(i - 1));
                        double y = function2.getX(j) - x;
                        if (difference.getLength() > 0 && difference.getLastX() == x) { //The output travel time function has infinity slope
                            if (difference.getLength() > 1 && difference.getX(difference.getLength() - 2) < difference.getX(difference.getLength() - 1) - 2.51E-4) { //TODO tohle je podezrela konstrukce
                                difference.updatePoint(difference.getLength() - 1, difference.getX(difference.getLength() - 1) - 2.5E-4, difference.getY(difference.getLength() - 1));
                                difference.addPoint(x, y);
                            }
                            else {
                                difference.updatePoint(difference.getLength() - 1, x, y);
                            }
                        }
                        else {
                            difference.addPoint(x, y);
                        }
                        j++;
                    }
                }
            }

            if (difference.getFirstX() != function1.getFirstX()) { //Check domain
                throw new ArithmeticException("Resulting function starts in incorrect point.");
            }

            double current = 0.0;
            double previous = 0.0;
            for (int k = 1; k < difference.getLength(); k++) {
                current = difference.getY(k) + difference.getX(k);
                previous = difference.getY(k - 1) + difference.getX(k - 1);

                if (current - previous <= -1E-15) { //TODO tady by bylo lepsi epsilon, nebo to udelat vubec jinak aby kontrola nebyla nutna, pokud chyba neni normani stav (pak kontrola nutna je)
                    //throw new ArithmeticException("The FIFO rule is broken");
                    System.out.println("The FIFO rule is broken");
                }
                else if (current - previous > -1E-15 && current - previous < 1E-15) {
                    difference.updatePoint(k, difference.getX(k), difference.getY(k) + 5E-16);
                }
            }

            //TODO tady ke konci je to s tou exception asi nevhodne, zvazit celou reimplementaci a radne testovani
            return difference;
        }
    }

    /**
     * Returns <code>true</code>, if the point at the specified index, the previous, and the next points have equal <i>y</i> values. Returns <code>false</code> otherwise.
     * @param index the index of the point of the function
     * @param function the function, whose points are examined
     * @return <code>true</code>, if the point at the specified index and the previous point have equal <i>y</i> values. Returns <code>false</code> otherwise.
     */
    public static boolean isConstantMiddle(int index, DtmPiecewiseLinearFunction function) {
        if (index > 0 && index < function.getLength() - 1 && function.getY(index) == function.getY(index - 1) && function.getY(index) == function.getY(index + 1)) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Returns <code>true</code>, if the previous point has lower <i>y</i> value than the point at the specified index and the next point has equal <i>y</i> value. Returns <code>false</code> otherwise.
     * @param index the index of the point of the function
     * @param function the function, whose points are examined
     * @return <code>true</code>, if the previous point has lower <i>y</i> value than the point at the specified index and the next point has equal <i>y</i> value. Returns <code>false</code> otherwise.
     */
    public static boolean isIncreasingLeft(int index, DtmPiecewiseLinearFunction function) {
        if (index > 0 && index < function.getLength() - 1 && function.getY(index - 1) < function.getY(index) && function.getY(index + 1) == function.getY(index)) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Returns <code>true</code>, if the point at the specified index and the previous have equal <i>y</i> values and the next point has higher <i>y</i> value. Returns <code>false</code> otherwise.
     * @param index the index of the point of the function
     * @param function the function, whose points are examined
     * @return <code>true</code>, if the point at the specified index and the previous have equal <i>y</i> values and the next point has higher <i>y</i> value. Returns <code>false</code> otherwise.
     */
    public static boolean isIncreasingRight(int index, DtmPiecewiseLinearFunction function) {
        if (index > 0 && index < function.getLength() - 1 && function.getY(index - 1) == function.getY(index) && function.getY(index + 1) > function.getY(index)) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Returns <code>true</code>, if the point at the specified index and the previous point or the next point have equal <i>y<i> values (i.e., the point starts or ends or is within a constant piece of the function). Returns <code>false</code> otherwise.
     * @param index the index of the point of the function
     * @param function the function, whose points are examined
     * @return <code>true</code>, if the point at the specified index and the previous point or the next point have equal <i>y<i> values (i.e., the point starts or ends or is within a constant piece of the function). Returns <code>false</code> otherwise.
     */
    public static boolean isSingular(int index, DtmPiecewiseLinearFunction function) {
        if (index > 0 && function.getY(index) == function.getY(index - 1)) {
            return true;
        }
        else if (index < function.getLength() - 1 && function.getY(index) == function.getY(index + 1)) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Returns <code>true</code>, if the point at the specified index and the previous and the next point do not have equal <i>y<i> values (i.e., the point does not start nor end and is not within a constant piece of the function - the negation of isSingular() method). Returns <code>false</code> otherwise.
     * @param index the index of the point of the function
     * @param function the function, whose points are examined
     * @return <code>true</code>, if the point at the specified index and the previous point or the next point have equal <i>y<i> values (i.e., the points starts or ends or is within a constant piece of the function). Returns <code>false</code> otherwise.
     */
    public static boolean isRegular(int index, DtmPiecewiseLinearFunction function) {
        return !isSingular(index, function);
    }

    /**
     * Finds and returns the <i>x</i> value of the left boundary point of a constant part (possible consisting of multiple pieces) of the function. If the point on the specified index is on the left boundary of the constant part or is not in a constant part at all the function returns <i>x</i> value of the specified point
     * @param index the index of the point of the function
     * @param function the function, whose points are examined
     * @return the <i>x</i> value of the left boundary point of a constant part (possible consisting of multiple pieces) of the function. If the point on the specified index is on the left boundary of the constant part or is not in a constant part at all the function returns <i>x</i> value of the specified point
     */
    public static double findConstantLeftX(int index, DtmPiecewiseLinearFunction function) {
        double y = function.getY(index);
        int i = index;

        while (i > 0 && function.getY(i - 1) == y) { //TODO determine whether epsilon is not needed
            i--;
        }

        return function.getX(i);
    }

    /**
     * Finds and returns the <i>x</i> value and the index of the right boundary point of a constant part (possible consisting of multiple pieces) of the function. If the point on the specified index is on the right boundary of the constant part or is not in a constant part at all the function returns <i>x</i> value and the index of the specified point
     * @param index the index of the point of the function
     * @param function the function, whose points are examined
     * @return the <i>x</i> value of the left boundary point of a constant part (possible consisting of multiple pieces) of the function. If the point on the specified index is on the left boundary of the constant part or is not in a constant part at all the function returns <i>x</i> value of the specified point
     */
    public static DtmPairElement<Double, Integer> findConstantRightX(int index, DtmPiecewiseLinearFunction function) {
        double y = function.getY(index);
        int i = index;

        while (i < function.getLength() - 1 && function.getY(i + 1) == y) { //TODO determine whether epsilon is not needed
            i++;
        }

        return new DtmPairElement<>(function.getX(i), i);
    }

    /**
     * Combines two time-variable flows represented by piecewise constant functions and list of corresponding mixtures and returns the resulting time-variable flow and corresponding list of mixtures
     * @param function1 first variable flow
     * @param function2 second variable flow
     * @param mixture1 mixture of the first flow
     * @param mixture2 mixture of the second flow
     * @return the resulting time-variable flow and corresponding list of mixtures
     */
    public static DtmPairElement<DtmPiecewiseConstantFunction, List<DtmMixture<DtmIdentifiable>>> sumFlowFunctions(DtmPiecewiseConstantFunction function1, DtmPiecewiseConstantFunction function2, List<DtmMixture<DtmIdentifiable>> mixture1, List<DtmMixture<DtmIdentifiable>> mixture2) {
        if (function1 == null) {
            return new DtmPairElement<>(function2, mixture2);
        }
        else {
            DtmPiecewiseConstantFunction combinedFunction = new DtmPiecewiseConstantFunction();
            List<DtmMixture<DtmIdentifiable>> combinedMixture = new ArrayList<>();

            int i = 0;
            int j = 0;
            double time1 = function1.getFirstX();
            double time2 = function2.getFirstX();

            while (i < function1.getLength() && j < function2.getLength()) {
                time1 = function1.getX(i);
                time2 = function2.getX(j);

                if (time1 < time2) {
                    combinedFunction.addPoint(function1.getX(i), function1.getY(i) + function2.getY(j - 1));
                    combinedMixture.add(mixture1.get(i).merge(mixture2.get(j - 1), function1.getY(i), function2.getY(j - 1)));
                    i++;
                }
                else if (time1 > time2) {
                    combinedFunction.addPoint(function2.getX(j), function1.getY(i - 1) + function2.getY(j));
                    combinedMixture.add(mixture1.get(i - 1).merge(mixture2.get(j), function1.getY(i - 1), function2.getY(j)));
                    j++;
                }
                else {
                    combinedFunction.addPoint(function1.getX(i), function1.getY(i) + function2.getY(j));
                    combinedMixture.add(mixture1.get(i).merge(mixture2.get(j), function1.getY(i), function2.getY(j)));
                    i++;
                    j++;
                }
            }

            return new DtmPairElement<>(combinedFunction, combinedMixture);
        }
    }
}
