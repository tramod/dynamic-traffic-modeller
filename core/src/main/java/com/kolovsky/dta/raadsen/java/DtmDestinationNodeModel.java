/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Abstract class that transform destination based formulation into universal formulation and back
 * @author kolovsky
 */
abstract public class DtmDestinationNodeModel implements DtmNodeModel{
    /** Associated node */
    DtmNode node;
    /** Distribution matrices for destinations */
    HashMap<DtmIdentifiable, DtmDistributionMatrix> distributions;

    /**
     *
     * @param node associated node for node model
     * @param distributions node distribution for each destination
     */
    public DtmDestinationNodeModel(DtmNode node, HashMap<DtmIdentifiable, DtmDistributionMatrix> distributions){
        this.node = node;
        this.distributions = distributions;
    }

    /**
     * Solve universal node model
     * @param demand demand for each intersection movement
     * @param supply supply for outgoing edges
     * @return movement flow
     */
    abstract public double[][] solveUniversal(double[][] demand, double[] supply);

    @Override
    public DtmTripleElement<List<Double>, List<Double>, List<DtmMixture<DtmIdentifiable>>> solve(double time) {
        // INCOMING
        DtmSource[] incomingSources;
        if (node.isOrigin()){
            incomingSources = new DtmSource[node.getIncomingEdges().size() + 1];
        }
        else {
            incomingSources = new DtmSource[node.getIncomingEdges().size()];
        }
        for(int i = 0; i < node.getIncomingEdges().size(); i++){
            incomingSources[i] = node.getIncomingEdges().get(i);
        }
        if (node.isOrigin()){
            incomingSources[incomingSources.length - 1] = node.getOrigin();
        }

        // DEMAND
        double[][] demandMatrix = new double[incomingSources.length][node.getOutgoingEdges().size()];
        double[][] alphaMatrix = new double[incomingSources.length][node.getOutgoingEdges().size()];
        for (int i = 0; i < incomingSources.length; i++){
            DtmMixture<DtmIdentifiable> mixture = incomingSources[i].getOutflowMixture();
            if (incomingSources[i].getDemand() > 0.0 && mixture != null){ // TODO if demand is not zero the mixture can not be null
                for (int j = 0; j < node.getOutgoingEdges().size(); j++){
                    for (DtmIdentifiable d: mixture.getKeys()){
                        DtmZone zone = (DtmZone) d;
                        if (zone.node != node && distributions.containsKey(d)){
                            alphaMatrix[i][j] += mixture.getPortion(d) * distributions.get(d).distribution(time, i, j);
                        }
                    }
                    demandMatrix[i][j] = incomingSources[i].getDemand() * alphaMatrix[i][j];
                }
            }
            else if (incomingSources[i].getDemand() > 0.0){
                throw new IllegalArgumentException("The demand is "+incomingSources[i].getDemand()+" but the mixture is null!");
            }
        }

        // DETERMINE IF SOME FLOW ENDS IN THIS NODE
        DtmZone thisZone = null;
        for (int i = 0; i < incomingSources.length; i++){
            DtmMixture<DtmIdentifiable> mixture = incomingSources[i].getOutflowMixture();
            if (mixture != null) {
                for (DtmIdentifiable d: mixture.getKeys()){
                    DtmZone zone = (DtmZone) d;
                    if (zone.node == node){
                        thisZone = zone;
                    }
                }
            }
        }

        // SUPPLY
        double[] supply = new double[node.getOutgoingEdges().size()];
        for (int j = 0; j < node.getOutgoingEdges().size(); j++){
            supply[j] = node.getOutgoingEdges().get(j).getSupply();
        }

        // PERFORM BASIC NODE MODEL
        double[][] q = solveUniversal(demandMatrix, supply);

        // AGGREGATE FLOW
        double finalDestinationFlow = 0.0;
        ArrayList<Double> incomingFlow = new ArrayList<>(incomingSources.length);
        for (int i = 0; i < incomingSources.length; i++){
            double sum = 0;
            for (int j = 0; j < node.getOutgoingEdges().size(); j++){
                sum += q[i][j];
            }

            // add flow that ends in this node (final destination flow)
            DtmMixture<DtmIdentifiable> mixture = incomingSources[i].getOutflowMixture();
            if (thisZone != null && incomingSources[i].getDemand() > 0.0 && mixture != null){ // TODO if demand is not zero the mixture can not be null
                double endFlow = mixture.getPortion(thisZone) * incomingSources[i].getDemand();
                finalDestinationFlow += endFlow;
                sum += endFlow;
            }

            // check if incoming flow is smaller than demand
            if (sum > incomingSources[i].getDemand()){
                sum = incomingSources[i].getDemand();
            }
            incomingFlow.add(sum);
        }

        ArrayList<Double> outgoingFlow = new ArrayList<>(node.getOutgoingEdges().size());
        for (int j = 0; j < node.getOutgoingEdges().size(); j++){
            double sum = 0;
            for (int i = 0; i < incomingSources.length; i++){
                sum += q[i][j];
            }

            // check if outgoing flow is smaller than supply
            if (sum > node.getOutgoingEdges().get(j).getSupply()){
                sum = node.getOutgoingEdges().get(j).getSupply();
            }
            outgoingFlow.add(sum);
        }

        // OUTGOING MIXTURE
        ArrayList<DtmMixture<DtmIdentifiable>> outgoingMixture = new ArrayList<>(node.getOutgoingEdges().size());
        for (int j = 0; j < node.getOutgoingEdges().size(); j++){
            ArrayList<DtmPairElement<DtmIdentifiable, Double>> mix = new ArrayList<>();
            for (DtmIdentifiable d: distributions.keySet()){
                double sum = 0;
                for (int i = 0; i < incomingSources.length; i++){
                    if (alphaMatrix[i][j] > 0){
                        sum += incomingSources[i].getOutflowMixture().getPortion(d) * distributions.get(d).distribution(time, i, j) / alphaMatrix[i][j] * q[i][j];
                    }
                }
                if (outgoingFlow.get(j) > 0){
                    mix.add(new DtmPairElement<>(d, sum/outgoingFlow.get(j)));
                }
            }
            if (mix.isEmpty()) {
                outgoingMixture.add(null); //TODO Added to mimic the behavior of the original implementation
            }
            else {
                outgoingMixture.add(new DtmMixture<>(mix));
            }
        }

        return new DtmTripleElement<>(incomingFlow, outgoingFlow, outgoingMixture);
    }

    @Override
    public DtmPairElement<DtmNodeEvent, Double> nextSheduledEvent(double time) {
        //TODO not implemented
        return null;
    }

    @Override
    public List<DtmNodeEvent> getAllEvents() {
        List<DtmNodeEvent> nodeEvents = new ArrayList<>();
        HashSet<Double> times =  new HashSet<>();
        DtmFunction function = null;

        if (distributions != null) {
            for (DtmDistributionMatrix distributionMatrix: distributions.values()) {
                for (int i = 0; i < distributionMatrix.matrix.length; i++) {
                    for (int j = 0; j < distributionMatrix.matrix[i].length; j++) {
                        function = distributionMatrix.matrix[i][j];

                        for (double time: function.getXValues()) {
                            if (!times.contains(time)) {
                                nodeEvents.add(new DtmNodeEvent(node, time));
                                times.add(time);
                            }
                        }
                    }
                }
            }
        }

        return nodeEvents;
    }

    public HashMap<DtmIdentifiable, DtmDistributionMatrix> getDistributions() {
        return distributions;
    }
}