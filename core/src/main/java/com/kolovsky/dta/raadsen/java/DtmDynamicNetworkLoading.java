/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.List;

/**
 * Dynamic network loading for dynamic traffic assignment
 * @author Tomas Potuzak
 */
public interface DtmDynamicNetworkLoading {

    /**
     * Performs dynamic network loading and returns arrival functions of individual edges (the order of functions correspond to the order of edges)
     * @param maximalTime maximal time of the simulation in hours
     * @param epsilon the comparison tolerance
     * @return arrival functions of individual edges (the order of functions correspond to the order of edges)
     */
    public List<DtmArrivalFunction> run(double maximalTime, double epsilon);
}
