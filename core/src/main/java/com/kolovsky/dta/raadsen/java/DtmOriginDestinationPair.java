/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * A pair of an origin and a destination representing the traffic flow between these two zones. The flow is represented by a piecewise constant function and, as such, can change in time.
 * @author Tomas Potuzak
 */
public class DtmOriginDestinationPair {
    /** The origin zone (i.e., the source of the flow) of this pair */
    protected DtmZone origin;
    /** The destination zone (i.e., the sink of the flow) of this pair */
    protected DtmZone destination;
    /** The flow between the origin and the destination zones */
    protected DtmPiecewiseConstantFunction flow;

    /**
     * Creates a new origin-destination pair with specified origin and destination zones
     * @param origin the origin zone of the pair
     * @param destination the destination zone of the pair
     * @param flow the flow between the origin and the destination zones
     * @throws NullPointerException if the origin, the destination and/or the flow are <code>null</code>
     */
    public DtmOriginDestinationPair(DtmZone origin, DtmZone destination, DtmPiecewiseConstantFunction flow) throws NullPointerException {
        if (origin == null || destination == null) {
            throw new NullPointerException("Origin and/or destination zones cannot be null.");
        }
        else {
            this.origin = origin;
            this.destination = destination;
            this.flow = flow;
        }
    }

    /**
     * Returns the origin zone of this pair
     * @return the origin zone of this pair
     */
    public DtmZone getOrigin() {
        return origin;
    }

    /**
     * Sets the origin zone of this pair
     * @param origin the new origin of this pair
     */
    public void setOrigin(DtmZone origin) {
        this.origin = origin;
    }

    /**
     * Returns the destination zone of this pair
     * @return the destination zone of this pair
     */
    public DtmZone getDestination() {
        return destination;
    }

    /**
     * Sets the origin zone of this pair
     * @param destination the new destination of this pair
     */
    public void setDestination(DtmZone destination) {
        this.destination = destination;
    }

    /**
     * Returns the flow between the origin and the destination zones
     * @return the flow between the origin and the destination zones
     */
    public DtmPiecewiseConstantFunction getFlow() {
        return flow;
    }

    /**
     * Sets the flow between the origin and the destination zones
     * @param flow the new flow between the origin and the destination zones
     */
    public void setFlow(DtmPiecewiseConstantFunction flow) {
        this.flow = flow;
    }

    @Override
    public int hashCode() {
        return origin.hashCode() + destination.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof DtmOriginDestinationPair) {
            DtmOriginDestinationPair other = (DtmOriginDestinationPair) o;
            return origin.equals(other.origin) && destination.equals(other.destination);
        }
        else {
            return false;
        }
    }
}
