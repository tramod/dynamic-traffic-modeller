/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.List;

/**
 * A general function for Dynamic Traffic Assignment
 * @param <T> the type of the <i>y</i> values
 * @author Tomas Potuzak
 */
public class DtmGeneralFunction<T> {
    /** The <i>x</i> values of the function */
    protected List<Double> xValues;
    /** The <i>y</> = <i>f</i>(<i>x</i>) values of the function */
    protected List<T> yValues;

    /**
     * Creates a new general function with specified <i>x</i> and <i>y</i> values
     * @param xValues the <i>x</i> values of the function
     * @param yValues the <i>y</> = <i>f</i>(<i>x</i>) values of the function
     * @throws IllegalArgumentException if the numbers of <i>x</i> and <i>y</i> values are different or the <i>x</i> values are not increasing
     * @throws NullPointerException if <i>x</i> and/or <i>y</i> values are <code>null</code>
     */
    public DtmGeneralFunction(List<Double> xValues, List<T> yValues) {
        this.xValues = new ArrayList<>();
        this.yValues = new ArrayList<>();

        if (xValues != null && yValues != null && xValues.size() == yValues.size()) {
            this.xValues.addAll(xValues);
            this.yValues.addAll(yValues);
        }
        else if (xValues.size() != yValues.size()) {
            throw new IllegalArgumentException("Number of x and y values must be identical.");
        }
        else {
            throw new NullPointerException("Both x and y values must not be null.");
        }

        for (int i = 1; i < xValues.size(); i++) {
            if (xValues.get(i - 1) >= xValues.get(i)) {
                throw new IllegalArgumentException("x[i - 1] is not smaller than x[i] : " + xValues.get(i - 1) + " >= " + xValues.get(i));
            }
        }

        this.xValues.addAll(xValues);
        this.yValues.addAll(yValues);
    }

    /**
     * Creates a new general function with empty <i>x</i> and <i>y</i> values
     */
    public DtmGeneralFunction() {
        xValues = new ArrayList<>();
        yValues = new ArrayList<>();
    }

    /**
     *
     * @param transformer
     * @param maximalTime
     * @return
     */
    public DtmPiecewiseConstantFunction toPiecewiseConstantFunction(DtmTransformer<T> transformer, double maximalTime) {
        if (yValues.isEmpty()) {
            return new DtmPiecewiseConstantFunction();
        }
        else {
            List<Double> resultYValues = new ArrayList<>();
            List<Double> resultXValues = new ArrayList<>();

            if (xValues.get(0) > 0.0) {
                resultXValues.add(0.0);
                resultYValues.add(0.0);
            }

            for (int i = 0; i < xValues.size(); i++) {
                resultXValues.add(xValues.get(i));
                resultYValues.add(transformer.getValue(yValues.get(i)));
            }

            if (maximalTime > 0.0) {
                resultXValues.add(maximalTime);
                resultYValues.add(resultYValues.get(resultYValues.size() - 1));
            }

            return new DtmPiecewiseConstantFunction(xValues, resultYValues);
        }
    }

    /**
     * Performs a specified arithmetic operation of this and other general functions using the specified transformer
     * @param <U> the type of the other general function
     * @param <V> the type of the resulting general function
     * @param other the function, with which the arithmetic operation shall be performed
     * @param transformer the transformer with method enabling perform the arithmetic operation
     * @param operation the operator of the arithmetic operation
     * @return the resulting function
     * @throws IllegalArgumentException if the specified operation is not supported or
     * @throws NullPointerException if the specified operation is <code>null</code>
     */
    protected <U, V> DtmGeneralFunction<V> performOperation(DtmGeneralFunction<U> other, DtmTransformer<T> transformer, String operation) throws IllegalArgumentException, NullPointerException {
        List<Double> resultXValues = new ArrayList<>();
        List<V> resultYValues = new ArrayList<>();
        double x = 0.0;
        V y = null;
        int i = 0;
        int j = 0;

        while (i < xValues.size() && j < other.xValues.size()) {
            if (xValues.get(i) == other.xValues.get(j)) {
                x = xValues.get(i);
                y = transformer.performOperation(yValues.get(i), other.yValues.get(j), operation);

                i++;
                j++;
            }
            else if (xValues.get(i) < other.xValues.get(j)) {
                x = xValues.get(i);
                y = transformer.performOperation(yValues.get(i), other.yValues.get(j - 1), operation);

                i++;
            }
            else {
                x = other.xValues.get(j);
                y = transformer.performOperation(yValues.get(i - 1), other.yValues.get(j), operation);

                j++;
            }

            resultXValues.add(x);
            resultYValues.add(y);
        }


        return new DtmGeneralFunction<V>(resultXValues, resultYValues);
    }

    /**
     * Adds a point to the end of this function
     * @param x the <i>x</i> value
     * @param y the <i>y</i> value at <code>x</code> (<i>f</i>(<i>x</i>))
     * @throws IllegalArgumentException if the <i>x</i> and/or <i>x</i> value is infinite or NaN or the <i>x</i> value is not larger that the last value of this function
     */
    public void addPoint(double x, T y) throws IllegalArgumentException {
        if (Double.isInfinite(x) || Double.isNaN(x)) {
            throw new IllegalArgumentException("The x values must be regular numbers (i.e., finite)");
        }
        else {
            if (xValues.isEmpty() || getLastX() < x) {
                xValues.add(x);
                yValues.add(y);
            }
            else {
                throw new IllegalArgumentException("The x value must be larger than last value of the function " + xValues.get(xValues.size() - 1));
            }
        }
    }

    /**
     * Updates the point of specified index of this function
     * @param index the index of the point
     * @param x the <i>x</i> value
     * @param y the <i>y</i> value at <code>x</code> (<i>f</i>(<i>x</i>))
     * @throws IllegalArgumentException if the <code>index</code> is out of range or the <code>x</code> does not correspond to the <code>index</code>
     */
    public void updatePoint(int index, double x, T y) throws IllegalArgumentException {
        if (index < 0 || index >= xValues.size()) {
            throw new IllegalArgumentException("The index of the point is out of range.");
        }
        else if (index > 0 && index < xValues.size() - 1) { //A point in inner parts of the function - not at the ends
            if (x <= xValues.get(index - 1) || x >= xValues.get(index + 1)) {
                throw new IllegalArgumentException("The x value must be between the neighboring values.");
            }
            else { //Everything ok, setting the x and y values
                xValues.set(index, x);
                yValues.set(index, y);
            }
        }
        else if (index == 0 && xValues.size() == 1) { //There is just one point (this should probably not happen)
            xValues.set(0, x);
            yValues.set(0, y);
        }
        else if (index == 0) {
            if (x >= xValues.get(index + 1)) { //The point at the start
                throw new IllegalArgumentException("The x value must be between the neighboring values.");
            }
            else { //Everything ok, setting the x and y values
                xValues.set(index, x);
                yValues.set(index, y);
            }
        }
        else if (index == xValues.size() - 1) { //The point at the end
            if (x <= xValues.get(index - 1)) {
                throw new IllegalArgumentException("The x value must be between the neighboring values.");
            }
            else { //Everything ok, setting the x and y values
                xValues.set(index, x);
                yValues.set(index, y);
            }
        }
    }

    public List<T> toDiscreteFunction(int numBins, double startX, double endX, double positionInInterval){
        List<T> values = new ArrayList<>(numBins);
        double step = (endX - startX)/numBins;

        int index = 1;
        for(int i = 0; i < numBins; i++){
            double x = startX + i * step + positionInInterval * step;
            if (x >= getLastX()){
                values.add(getLastY());
            }
            else {
                while (xValues.get(index) <= x){
                    index++;
                }
                values.add(yValues.get(index - 1));
            }
        }
        return values;
    }

    /**
     * Returns the <i>x</i> values of this function
     * @return the <i>x</i> values of this function
     */
    public List<Double> getXValues() {
        return xValues;
    }

    /**
     * Returns the <i>y</i> = <i>f</i>(<i>x</i>) values of this function
     * @return the <i>y</i> = <i>f</i>(<i>x</i>) values of this function
     */
    public List<T> getYVaues() {
        return yValues;
    }

    /**
     * Returns the <i>x</i> value of the point at the specified index
     * @param index the index of the point, whose <i>x</i> value shall be returned
     * @return the <i>x</i> value of the point at the specified index
     * @throws IndexOutOfBoundsException if the point at the specified index does not exist
     */
    public double getX(int index) throws IndexOutOfBoundsException {
        return xValues.get(index);
    }

    /**
     * Returns the <i>y</i> value of the point at the specified index
     * @param index the index of the point, whose <i>y</i> value shall be returned
     * @return the <i>y</i> value of the point at the specified index
     * @throws IndexOutOfBoundsException if the point at the specified index does not exist
     */
    public T getY(int index) throws IndexOutOfBoundsException {
        return yValues.get(index);
    }

    /**
     * Returns the number of points of this function
     * @return the number of points of this function
     */
    public int getLength() {
        return xValues.size();
    }

    /**
     * Returns the <i>x</i> value of the first point of the function (i.e., at index 0)
     * @return the <i>x</i> value of the first point of the function (i.e., at index 0)
     * @throws IndexOutOfBoundsException if the point at index 0 does not exist (i.e., the function is empty, its length is 0)
     */
    public double getFirstX() throws IndexOutOfBoundsException {
        return xValues.get(0);
    }

    /**
     * Returns the <i>y</i> value of the first point of the function (i.e., at index 0)
     * @return the <i>y</i> value of the first point of the function (i.e., at index 0)
     * @throws IndexOutOfBoundsException if the point at index 0 does not exist (i.e., the function is empty, its length is 0)
     */
    public T getFirstY() throws IndexOutOfBoundsException {
        return yValues.get(0);
    }

    /**
     * Returns the <i>x</i> value of the last point of the function (i.e., at index (length - 1))
     * @return the <i>x</i> value of the last point of the function (i.e., at index (length - 1))
     * @throws IndexOutOfBoundsException if the point at index (length - 1) does not exist (i.e., the function is empty, its length is 0)
     */
    public double getLastX() throws IndexOutOfBoundsException {
        return xValues.get(xValues.size() - 1);
    }

    /**
     * Returns the <i>y</i> value of the last point of the function (i.e., at index (length - 1))
     * @return the <i>y</i> value of the last point of the function (i.e., at index (length - 1))
     * @throws IndexOutOfBoundsException if the point at index (length - 1) does not exist (i.e., the function is empty, its length is 0)
     */
    public T getLastY() throws IndexOutOfBoundsException {
        return yValues.get(yValues.size() - 1);
    }
}