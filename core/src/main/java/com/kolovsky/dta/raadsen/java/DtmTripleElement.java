/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

/**
 * A triple of values of specified types
 * @param <F> type of first value
 * @param <S> type of second value
 * @param <T> type of third value
 * @author Tomas Potuzak
 */
public class DtmTripleElement<F, S, T> {
    /** First value */
    protected F first;
    /** Second value */
    protected S second;
    /** Third value */
    protected T third;

    /**
     * Creates a new triple of values
     * @param first first value
     * @param second second value
     * @param third third value
     */
    public DtmTripleElement(F first, S second, T third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    //Getters and setters

    /**
     * Returns the first value
     * @return the first value
     */
    public F getFirst() {
        return first;
    }

    /**
     * Sets the first value
     * @param first the new first value
     */
    public void setFirst(F first) {
        this.first = first;
    }

    /**
     * Returns the second value
     * @return the second value
     */
    public S getSecond() {
        return second;
    }

    /**
     * Sets the second value
     * @param second the new second value
     */
    public void setSecond(S second) {
        this.second = second;
    }

    /**
     * Returns the third value
     * @return the third value
     */
    public T getThird() {
        return third;
    }

    /**
     * Sets the third value
     * @param third the new third
     */
    public void setThird(T third) {
        this.third = third;
    }

    //Inherited methods

    @Override
    public String toString() {
        return "(" + first + ", " + second + ", " + third + ")";
    }
}