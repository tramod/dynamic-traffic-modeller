/**
 Dynamic Traffic Modeller
 Copyright (C) 2023  František Kolovský, Karel Jedlička, Tomáš Potužák, Jan Martolos, Daniel Beran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kolovsky.dta.raadsen.java;

import java.util.ArrayList;
import java.util.List;

/**
 * A node of the graph (i.e., a crossroad of the road traffic network) for dynamic traffic assignment
 * @author Tomas Potuzak
 */
public class DtmNode implements DtmIdentifiable {
    /** The ID of this node */
    protected int id;
    /** The index of this node for access via non-hash tables */
    protected int index;
    /** The node model of this node */
    protected DtmNodeModel nodeModel;
    /** The incoming edges of this node */
    protected List<DtmEdge> incomingEdges;
    /** The outgoing edges of this node */
    protected List<DtmEdge> outgoingEdges;
    /** The origin connected to this node */
    protected DtmOrigin origin;
    /** The ID of the subgraph, to which this node belongs */
    protected int subgraphId;

    // DUAL GRAPH
    private List<DtmDualEdge> dualEdges = new ArrayList<>();

    public List<DtmDualEdge> getDualEdges() {
        return dualEdges;
    }

    public void addDualEdge(DtmDualEdge dualEdge){
        dualEdges.add(dualEdge);
    }

    public void removeDualEdge(DtmDualEdge dualEdge){
        if (dualEdge != null){
            dualEdges.remove(dualEdge);
            dualEdge.getStartEdge().getOutgoingDualEdges().remove(dualEdge);
            dualEdge.getEndEdge().getIncomingDualEdges().remove(dualEdge);
        }
    }

    public DtmDualEdge getDualEdge(DtmEdge in, DtmEdge out){
        for (DtmDualEdge e: dualEdges){
            if (e.getStartEdge().getIndex() == in.getIndex() && e.getEndEdge().getIndex() == out.getIndex()){
                return e;
            }
        }
        return null;
    }

    /**
     * Creates a new node with specified id
     * @param id the ID of the node
     */
    public DtmNode(int id) {
        this.id = id;

        index = -1;
        incomingEdges = new ArrayList<>();
        outgoingEdges = new ArrayList<>();
    }

    /**
     * Returns <code>true</code>, if this node is an origin node. Returns <code>false</code> otherwise.
     * @return <code>true</code>, if this node is an origin node. Returns <code>false</code> otherwise.
     */
    public boolean isOrigin() {
        return origin != null;
    }

    /**
     * Adds an incoming edge to this node
     * @param incomingEdge the new incoming edge to be added to this node
     */
    public void addIncomingEdge(DtmEdge incomingEdge) {
        incomingEdges.add(incomingEdge);
    }

    /**
     * Adds an outgoing edge to this node
     * @param outgoingEdge the new incoming edge to be added to this node
     */
    public void addOutgoingEdge(DtmEdge outgoingEdge) {
        outgoingEdges.add(outgoingEdge);
    }

    //Getters and setters

    /**
     * Returns the node model of this node
     * @return the node Model of this node
     */
    public DtmNodeModel getNodeModel() {
        return nodeModel;
    }

    /**
     * Sets the node model of this node
     * @param nodeModel the new node model of this node
     */
    public void setNodeModel(DtmNodeModel nodeModel) {
        this.nodeModel = nodeModel;
    }

    /**
     * Returns the incoming edges of this node
     * @return the incoming edges of this node
     */
    public List<DtmEdge> getIncomingEdges() {
        return incomingEdges;
    }

    /**
     * Returns the outgoing edges of this node
     * @return the outgoing edges of this node
     */
    public List<DtmEdge> getOutgoingEdges() {
        return outgoingEdges;
    }

    /**
     * Returns the origin connected to this node
     * @return the origin connected to this node
     */
    public DtmOrigin getOrigin() {
        return origin;
    }

    /**
     * Sets the origin connected to this node
     * @param origin the new origin connected to this node
     */
    public void setOrigin(DtmOrigin origin) {
        this.origin = origin;
    }

    /**
     * Returns the ID of the subgraph, to which this node belongs
     * @return the ID of the subgraph, to which this node belongs
     */
    public int getSubgraphId() {
        return subgraphId;
    }

    /**
     * Sets the ID of the subgraph, to which this node belongs
     * @param subgraphId the new ID of the subgraph, to which this node shall belong
     */
    public void setSubgraphId(int subgraphId) {
        this.subgraphId = subgraphId;
    }

    //Inherited methods

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof DtmNode) {
            DtmNode other = (DtmNode) o;
            return id == other.id;
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Node ID: " + id;
    }
}
